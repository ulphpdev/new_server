<?php 
  $table = "tbl_audit_trail";
  $order_by = "id";
?>
<ol class="breadcrumb" style="background-color: #fff;">

    <li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>

    <li>Audit Trail</li>



</ol>

<div class="btn-navigation menu-btn col-md-12 pad-0" >
	<!-- <button data-type = "Trash" class="inactive btn-min btn btn-default bold" style="display: none;"><span class = "glyphicon glyphicon-trash"></span> TRASH</button> 
  <button data-type = "Active" class="inactive btn-min btn btn-default bold"><span class = "glyphicon glyphicon-ok"></span>Publish</button>
  <button data-type = "Inactive" class="inactive btn-min btn btn-default bold"><span class = "glyphicon glyphicon-remove-circle"></span>Unpublish</button>
	<button class="add btn-min btn btn-success bold"><span class = "glyphicon glyphicon-plus"></span> ADD DISPOSITION <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
	<button class="article_list btn-min btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CLOSE</button> -->
</div>
<div class="row">
	<div class="col-md-12" id="list_data">
		<div class="table-responsive">
			<table class = "table listdata" style="margin-bottom:0px;">
          <thead>
            <tr>
            <!-- <th style = "width:10px;"><input class = "selectall" type = "checkbox"></th> -->
            <th>Page</th>
            <th>Action</th>
            <th>Date</th>
            <th>User</th>
            <th>Type</th>
            <!-- <th>Permission</th> -->
          </tr>  
          </thead>
          <tbody>
            
          </tbody>
        
			</table>
		</div>
		<div class="pagination col-md-12"></div>
	</div>
	<div class="col-md-12 content-container" id="form_data">
	</div>
<div class="btn-navigation menu-btn col-md-12" >
	<button class="update btn-min btn btn-success bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button> 
	<button class="save btn-min btn btn-success bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
	<button class="article_list cancel btn-min btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>
</div>
<script>
$(document).ready(function() {
/* Start of list */
  var limit = '10';
  var offset = '1';
  get_list(limit,offset);
  get_pagination(offset,limit);
  $(document).on('click', '.btn-close2', function(){
      get_list(limit,offset);
        get_pagination(offset,limit);
      $('.inactive').hide();
      $('.selectall').attr('checked', false);
        $('#success-modal').modal('hide');    
     });
  $(document).on('change','.page-number', function() {
    var page_number = parseInt($(this).val());
      $('.listdata tbody').html('');
      get_list(limit,page_number);
  });
  $(document).on('click','.next-page', function() {
    var page_number = parseInt($('.page-number').val());
    var next = page_number +1;
    if(page_number!=last()){
      get_list(limit,next);
      $('.page-number').val(next);
    }
  });
  $(document).on('click','.last-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=last()){
      get_list(limit,last());
      $('.page-number').val($('.page-number option:last').val());
    }
  });
  $(document).on('click','.prev-page', function() {
    var page_number = parseInt($('.page-number').val());
    var prev = page_number -1;
    if(page_number!=first()){
      get_list(limit,prev);
      $('.page-number').val(prev);
    }
  });
  $(document).on('click','.first-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=first()){
      get_list(limit,first());
      $('.page-number').val($('.page-number option:first').val());
    }
  });
  function first(){
    return parseInt($('.page-number option:first').val());
  }
  function last(){
    return parseInt($('.page-number option:last').val());
  }
	function get_list(limit,offset){
    $('.delete').hide();
    $('.inactive').hide();
    $('.selectall').attr('checked', false);
  
    $.ajax({
        type: 'Post',
        url:'<?=base_url();?>Audit_trail/Audit_trail_list',
        data:{limit:limit, offset:offset},
      }).done( function(data){
      	
              var obj = JSON.parse(data);
              var htm = '';
            
          if(obj.length!=0){
            $.each(obj, function(index, row){ 
              if(row.status == 1){
                var status = "Active";
              } else {
                var status = "Inactive";
              }
              htm+="<tr>";
              // htm+="<td><input class = 'select' data-id = '"+row.id+"' type ='checkbox'></td>";
              htm+="<td><d style='color:#3071a9'>"+row.page+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+row.action+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+row.date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+row.email+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+row.type+"</d></td>";
              // htm+="<td><a class='edit action_list' data-status='' id='"+row.id+"' title='edit'><span class='glyphicon glyphicon-pencil'></span></a><a class='delete_data action_list' data-status='' id='"+row.id+"' title='delete'> <span class='glyphicon glyphicon-trash'></span></a></td>";
              htm+="</tr>";
            });
          }else{
            htm+="<tr><td colspan='5'>No record found</td></tr>";
          }
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
     }); 
  }
  function get_pagination(page_num,limit){
    var table = "<?=$table;?>";
    var order_by = "<?=$order_by;?>";
    $.ajax({
        type: 'Post',
        url:'<?=base_url();?>Audit_trail/Audit_trail_list_count',
        data:{limit:limit, table:table, order_by: order_by},
      }).done( function(data){
          var cnt = 0;
          var htm = '';
          htm += '<span class = "glyphicon  first-page"> &#60;&#60;First </span> | <span class = "glyphicon  prev-page"> &#60;Previous </span> | ';
          htm += 'Page <select class="page-number"  style="" disabled> ';
          for(var x =1; x<=data; x++){
            htm += "<option value='"+x+"'>"+x+"</option>";
            cnt++;
          }
          htm += '</select> of '+data+' | ';
          htm += '<span class = "glyphicon  next-page"> &#62;Next</span> | <span class = "glyphicon last-page"> Last&#62;&#62; </span>';
          $('.pagination').html('');
          if(data > 1){
          $('.pagination').html(htm);
          }
     }); 
  }
/* End of list */
/* Start of Add */
$(document).on('click', '.add', function(){
$(this).hide();
$('.save').show();
$('.update').hide();
$('.article_list').show();
$.ajax({
    type: 'Post',
    url:'<?=base_url();?>global_controller/action_global',
    data:{id:'', module:'disposition', type:'add'},
}).done( function(data){
	$('.content-container').html(data);
        $('#success-modal').modal('hide');
                  	// bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
        // get_list(limit,offset);
        // get_pagination('1',limit);
});
})
$(document).on('click', '.article_list', function(){
$('.article_list').hide();
$('.save').hide();
$('.update').hide();
$('.add').show();
$('#form_data').html('');
})
/* End of Add */
/* Start of update */
$(document).on('click', '.edit', function(){
var id = $(this).attr('id');
$('.update').show();
$('.add').show();
$('.save').hide();
$('.cancel').show();
$.ajax({
    type: 'Post',
    url:'<?=base_url();?>global_controller/action_global',
    data:{id:id, module:'disposition', type:'edit'},
}).done( function(data){
	$('.content-container').html(data);
        $('#success-modal').modal('hide');
                  	// bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
        // get_list(limit,offset);
        // get_pagination('1',limit);
});
});
$(document).on('click', '.article_list', function(){
$(this).hide();
$('.save').hide();
$('.update').hide();
$('.add').show();
get_list(limit,offset);
get_pagination('1',limit);
})
/* End of update */
/* start of delete */
$('.selectall').click(function(){
     if(this.checked) { 
            $('.select').each(function() { 
                this.checked = true;  
                $('.delete').show();
                $('.inactive').show();           
            });
        }else{
            $('.select').each(function() { 
              $('.delete').hide();
                $('.inactive').hide();
                this.checked = false;                 
            });         
        }
});
$(document).on('click', '.select', function(){
    var x = 0;
        $('.select').each(function() {                
                if (this.checked==true) { x++; } 
                if (x > 0 ) {
                   $('.delete').show();
                   $('.inactive').show(); 
                } else {
                $('.delete').hide();
                $('.inactive').hide();
                $('.selectall').attr('checked', true);
              	}
        });
});
$(document).on('click', '.inactive', function(){
    var x = 0;
    var data_type = $(this).attr('data-type');
    $('.select').each(function() {                
          if (this.checked==true) {  x++;   } 
          if (x > 0 ) {
            $('.modal-footer .btn-close').hide();
            $('.msg').html('<p>Are you sure you want to "'+data_type+'" selected record? <button data-type = "'+data_type+'" class = "btn-remove"> Yes </button><button class = "btn-close2"> No </button></p>');
            $('#success-modal').modal('show');
          }
      });
});
$(document).on('click', '.delete_data', function(){
    var x = 0;
    var data_type = $(this).attr('id');
            $('.modal-footer .btn-close').hide();
            $('.msg').html('<p>Are you sure you want to delete this record? <button data-type = "'+data_type+'" class = "btn-remove-data"> Yes </button><button class = "btn-close2"> No </button></p>');
            $('#success-modal').modal('show');
});
$(document).on('click', '.btn-remove-data', function(){
    var x = 0;
            var table = "<?=$table;?>";
            var order_by = "<?=$order_by;?>";
            var id = $(this).attr('data-type');
            var type = '-2';     
              $.ajax({
                  type: 'Post',
                  url:'<?=base_url();?>global_controller/inactive_global_update',
                  data:{id:id,type:type, table:table, order_by: order_by},
                }).done( function(data){
                  $('#success-modal').modal('hide');
                    // bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
                    $.ajax({
                      type: 'post',
                      url: "<?=base_url()?>/api/disposition",
                    }).done(function(data){
                      get_list(limit,offset);
                      get_pagination('1',limit);
                    });
                  });
             x++;
});
$(document).on('click', '.btn-remove', function(){
    var x = 0;
    var type = $(this).attr('data-type');
    if (type == 'Trash') {
      var type = '-2';
    }
    if (type == 'Inactive') {
      var type = '0';
    }
    if (type == 'Active') {
      var type = '1';
    }
  
      $('.select').each(function() {                
          if (this.checked==true) {
            
            var table = "<?=$table;?>";
            var order_by = "<?=$order_by;?>";
            var id = $(this).attr('data-id');
                        
              $.ajax({
                  type: 'Post',
                  url:'<?=base_url();?>global_controller/inactive_global_update',
                  data:{id:id,type:type, table:table, order_by: order_by},
                }).done( function(data){
             
                  $('#success-modal').modal('hide');
                    // bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
                    $.ajax({
                      type: 'post',
                      url: "<?=base_url()?>/api/disposition",
                    }).done(function(data){
                      get_list(limit,offset);
                      get_pagination('1',limit);
                    });
                  });
             x++;
          } 
        
      });
});
/* End of delete */
});
</script>