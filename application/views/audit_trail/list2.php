<?php 
  $table = "tbl_audit_trail";
  $order_by = "id";
?>
<ol class="breadcrumb" style="background-color: #fff;">
    <li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
    <li>Audit Trail</li>
</ol>
<div class="col-md-4">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Search" id="txt_search" placeholder="">
    <span class="er-msg file-err">This field is required</span>
    <div class="input-group-btn">
      <button class="btn btn-primary" id="btn_search" type="submit">
          <i class="glyphicon glyphicon-search"> </i> 
          Search
      </button>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12" id="list_data">
    <div class="table-responsive">
      <table class = "table listdata table-striped" style="margin-bottom:0px;">
          <thead>
            <tr>
              <th>Page</th>
              <th>Action</th>
              <th>Date</th>
              <th>User</th>
              <th>Type</th>
            </tr>  
          </thead>
          <tbody class="table_body"></tbody>
      </table>
    </div>
    <div style="text-align: center; margin-top: 20px;" class="form-inline pager_div"></div>
  </div>
  <div class="col-md-12 content-container" id="form_data">
  </div>
</div>
<script>

  var limit = '10';
  var offset = '0';
  var query = "";
  

$(document).ready(function() {
  <?php $this->load->view('layout/listing_function');?>
  
  get_list(query, limit,offset);
  get_pagination(query);

});

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  function get_list(query, limit, offset){
    $('.div-view-audit-report').hide();
    isLoading(true);
    var data = {'limit':limit, 'offset':offset, 'query':query};
    aJax.post(
      "<?=base_url('Audit_trail/get_list_audit_trail');?>",
      {
        'limit':limit, 
        'offset':offset, 
        'query':query
      },
      function(data){
        var obj = JSON.parse(data);
        var table_body = new TBody();
        if(obj.length!=0){
          $.each(obj, function(index, value){
            if(value.status == 1){
              var status = "Active";
            } else {
              var status = "Inactive";
            }
            table_body.td(value.page);
            table_body.td(value.action);
            table_body.td(moment(value.date).format('LLL'));
            table_body.td(value.email);
            table_body.td(capitalizeFirstLetter(value.type));
            table_body.set();
          });
        } else {
            table_body.td_norecord(5);
            table_body.set();
        }
        table_body.append(".table_body");
        isLoading(false);
      }
    );
  }
  function get_pagination(query){
    aJax.post(
      "<?=base_url('Audit_report_analysis/get_pagination_trail');?>",
      {
        'query':query
      },
      function(data){
        var no_of_page = data / limit;
        var pagination = new Pagination(); //please check helper.js for this function
        pagination.set_total_page(no_of_page);
        pagination.set('.pager_div');
      }
    );
  }

  $(document).on('keypress', '#txt_search', function(e){
    if (e.which == 13) { 
      $('#btn_search').click()
    }
  });

  $(document).on('click', '#btn_search', function(){
    var keyword = $('#txt_search').val();
    if(keyword.trim() == ""){
      query = "";
      get_list(query,limit,offset);
      get_pagination(query);
    } else {
      query = "page like '%" + keyword + "%' OR action like '%" + keyword + "%' OR email like '%" +keyword+ "%' OR action like '%" + keyword + "%'" ;
      get_list(query,limit,offset);
      get_pagination(query);
    }
  });


</script>
