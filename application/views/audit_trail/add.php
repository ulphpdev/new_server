<?php 
  $table = "tbl_disposition";
  $order_by = "disposition_id";
?>
<div class="col-md-12" id="form-content">

<div class="col-md-12 pad-5">
   <div class="col-md-2">Disposition name *: </div>
    <div class = "col-md-4"><input type = "text" id="disposition" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg">Disposition name should not be empty *</span></div>
</div>

</div>


<script type="text/javascript">
$(document).ready(function(){

var table = "<?=$table;?>";
var field = "<?=$order_by;?>";

/* Start of update */


$('.save').off('click').on('click', function() {

  
 var disposition = $('#disposition').val();

  if(validateFields() == 0){
    // if(isValidEmailAddress(email)){
    $(this).prop('disabled',true);
      $('.loader_gif').show();
      $.ajax({

              type: 'Post',
              url:'<?=base_url();?>global_controller/disposition_array',
              data:{ disposition:disposition,table:table, field:field, action: 'save'},
            }).done( function(data){
              $('.loader_gif').hide();
              $('.update').prop('disabled',false);
            $.ajax({
              type: 'post',
              url: "<?=base_url()?>/api/disposition",
            }).done(function(data){
              bootbox.alert('<b>Successfully Updated.</b>', function() {
                location.reload();
              }).off("shown.bs.modal");
            });

      }); 
    
    }


 });

/* End of update */

/* Start of Validation */

function validateFields() {

    var counter = 0;
    $('.inputs').each(function(){
          var input = $(this).val();
          if (input.length == 0) {
            $(this).css('border-color','red');
            $(this).next().show();
            counter++;
          }else{
            $(this).css('border-color','#ccc');
            $(this).next().hide();
          }
      });

    return counter;
}

})  

</script>