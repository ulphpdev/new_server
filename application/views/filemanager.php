<style type="text/css">

  #success-modal {
    z-index: 999999;
  }
  .loader {
    display: none;
  }

  .image-con {
    padding: 5px;
    border-radius: 3px;
    text-align: center;
    height: auto;
    background: rgba(113, 111, 111, 0.08);
    width: 22%;
    margin: 0.5%;
    word-wrap: break-word;
  }

.modal-head {
    padding: 5px;
    height: 36px;
    text-align: center;
    font-size: 17px;
    margin-bottom: 10px;
    cursor: move;
}

.dialog-image-manager {
  width: 63%;
}

.content-image-manager{
  width: 100%;
}

.folder-list {
      padding: 0 0 0 10px;
      margin: 0;
      list-style: none;
}

.col-folder li {
      list-style-type: none;
      margin: 0;
      padding: 1px 0 0 1px;
      position: relative;
  }

  ul.folder-list li a {
      color: #000;
      padding: 0 0;
      display: block;
      font-size: 12px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: clip;
  }

  .col-folder li a::before {
      content: url(<?= base_url() ?>asset/img/folderopened_yellow.png);
  }

  .col-folder li span {
      border: 0px solid #999;
      display: inline-block;
      padding: 1px 1px;
      text-decoration: none;
  }

  .col-folder li ul > li {
      display: none;
  }

  .mediapath-custom:hover{
    background: #cecece;
    cursor: pointer;
  }

  .file-manager-container {
    position: relative;
    float: left;
    width: 100%;
  }

  .col-folder {

    width: 20%;
    float: left;
    max-height: 375px;
    overflow: hidden;
    overflow-y: auto;
  }

  .file-manager {

    width: 80%;

    float: left;

  }

  .bootbox, .alert-modal {

    z-index: 999999999 !important;

  }

.modal-footer button.btn.btn-default {
    color: #fff;
    background-color: #d9534f;
    border-color: #d43f3a;
}

</style>

<div class="modal fade" id="filemanager-image-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">



  <div class="modal-dialog dialog-image-manager" role="document">



    <div class="modal-content content-image-manager" style = "overflow:hidden">



      <div class="modal-header modal-head">



        <button type="button" class="close modal-close" style = "color:#fff !important; opacity:10" data-dismiss="modal" aria-label="Close"><span style = "color:#000 !important; opacity:10"  aria-hidden="true">&times;</span></button>



        <h4 class="modal-title" id="myModalLabel">File Manager <d style = "display:none" class = "id-image"></d></h4>



      </div>

        <div class="file-manager-container">

            <div class="col-folder">

              <?php



              echo "<ul class='folder-list'>";

              echo '<li class="parent_li parent_me">'."<a id='abcdefghi1234' class='mediapath-custom'><span><i class=\"ui-icon ui-icon-triangle-1-e\"></i></span>Folders</a>";

              function listFolderFiles($dir){

                  $ffs = scandir($dir);

                  natcasesort($ffs);

                  

                   echo "<ul class='folder-list'>";

                   

                  foreach($ffs as $ff){

                      if($ff != '.' && $ff != '..' && $ff!='idacs'){

                        $datapath = $dir.'/'.$ff.'/';

                        $data_path = str_replace('images/', '', $datapath);



                        if(is_dir($dir.'/'.$ff)){

                            echo '<li class="parent_li parent_me">'."<a id='$data_path' class='mediapath-custom'><span><i class=\"ui-icon ui-icon-triangle-1-e\"></i></span>$ff</a>";

                        }

                        if(is_dir($dir.'/'.$ff)) 

                          {

                            listFolderFiles($dir.'/'.$ff);

                          }

                          

                        if(is_dir($dir.'/'.$ff)){

                          echo '</li>';

                        }



                      }

                  }

                  echo '</ul>';

                  

              }

              listFolderFiles('images');

              echo '</li>';

              echo '</ul>';

              ?>

    

            </div>

            <div class="file-manager file-manager">       



            </div>

        </div>

        



      <div class="modal-footer" style="float:left;width:100%;">



        <div class="col-md-10">



          <!-- <p align="left">Upload or Select file</p> -->



          <div class="row">

              <div class="col-md-5" style="padding:0px;">

                  <div class="col-md-6" style="padding:0px;">



                    <input type="text" class="" id="folder-new" style="width:100%;">

                    <input type="hidden" class="hidden-path-folder" value="">

                  </div>



                  <div class="col-md-6">



                    <button class = "btn btn-success btn-add-folder" style = "padding:2px 10px;"><d class=""> + Add Folder  </d></button>



                  </div>

              </div>

              <div class="col-md-7">

                <div class="col-md-6">



                  <input id = 'imgfiles_1' class="imgfiles_1" type = "file" name = 'imgfiles_1'>



                </div>



                <div class="col-md-6">



                  <button class = "uploadtofilemanager2 btn btn-success" style = "padding:2px 10px;"><d class="uploadlabel"> Upload </d><img class = "loader" src="<?= base_url() ?>asset/img/loader.gif"></button>



                </div>

              </div>

              



          </div>



        </div>





        <div class="col-md-2 c-copy pad-0" style = "display:none">

          <div class="col-md-3 pad-0">

          <input type = "hidden" class = "pathtocopy fullwidth">

          <button style = "padding:2px 10px;" class = "btn btn-primary btn-copy-file">Insert</button>
          <!-- insert image -->



          </div>



        </div>



      </div>



    </div>



  </div>



</div>

<script type="text/javascript">

  $(document).on('click', '.btn-copy-file', function() {

    var pathtocopy = $('#filemanager-image-modal .pathtocopy').attr('preview');

    file_type = $('#filemanager-image-modal .pathtocopy').attr('data-type');

    if(file_type=='image'){

      pathtocopy  = '../'+pathtocopy;

    } else {

    }

    var data_src = $(this).attr('attr-src');

    pathtocopyval = $('#filemanager-image-modal .pathtocopy').val();



    $('.'+data_src).attr('src',pathtocopy);

    $('.'+data_src+'val').attr('value',pathtocopyval);



    $('#filemanager-image-modal').modal('hide');

  });







  $('.col-folder li:has(ul)').addClass('parent_li').find(' > a').attr('title', 'Expand ++');

        $('.col-folder li:has(ul)').addClass('parent_me').find(' > a span > i').removeClass('ui-icon ui-icon-triangle-1-se').addClass('ui-icon ui-icon-triangle-1-e');

      $(document).on('click', '.mediapath-custom', function(e) {

      /*$('.mediapath-custom').on('click', function (e) {*/

        var folder = $(this).attr('id');


          var children = $(this).parent('li.parent_me').find(' > ul > li');

          if (children.is(":visible")) {

              children.hide('fast');

              $(this).attr('title', 'Expand ++ ').find(' > span > i').removeClass('ui-icon ui-icon-triangle-1-se').addClass('ui-icon ui-icon-triangle-1-e');

            }

            else{

              children.show('fast');

              $(this).attr('title', 'Collapse -- ').find(' > span > i').removeClass('ui-icon ui-icon-triangle-1-e').addClass('ui-icon ui-icon-triangle-1-se');

            }



      });



      $(document).on('click','.mediapath-custom', function() {



          folder = $(this).attr('id');



           if($(this).attr('id')=='abcdefghi1234'){



            folder ='';



          }

          $('.mediapath-custom').removeClass('active-folder');

          $(this).addClass('active-folder');

          $('.hidden-path-folder').val(folder);

          $.ajax({



            type: 'post',



            url: "<?=base_url()?>global_controller/getimages",



            data: { folder : folder }



          }).done(function(result) {



            $('.file-manager').html('');



            $('.file-manager').html(result);

          });



          



        });



      $(document).on('click', '.image-file-copy',  function(){

        $('.image-file-copy').removeClass("check_active");

        $(this).addClass("check_active");

        $('.image-con').css({'opacity':'1','border':'none'});

        $(this).parent().parent().css({'opacity':'.5'});

        var file_type = $(this).attr('data-type');



        var data_id = $(this).attr('data-id');



        $('#filemanager-image-modal .check').hide();



        $('#filemanager-image-modal .check_'+data_id).show();



        $('#filemanager-image-modal .c-copy').show();



        var image_src = $(this).attr('src');

        image_src = image_src.replace('../','');



        var file_name = $(this).attr('file-name');

        file_name = file_name.replace('../','');



        if(file_type=='image'){

          preview = file_name;

        }

        else{

          preview = image_src;

        }



        $('#filemanager-image-modal .pathtocopy').val(file_name).attr('preview',preview).attr('data-type',file_type);

        

        $('#filemanager-image-modal .c-insert').show();

      });



      $(document).on('click', '.btn-add-folder', function() {

        var path2 = $('.hidden-path-folder').val();

        var new_folder = $('#folder-new').val();

        if(path2==''){

          path = new_folder + '/';

        }

        else{

          path = path2 + new_folder + '/';

        }

        path3 = path2 + new_folder;



        bootbox.confirm('<h4 style="color:green">Would you like to save this changes?</h4>', function(res) {



          if (res == true) {



          $.ajax({



              type: 'post',



              // url: 'dashboard.php?function=create_folder',
              url: "<?=base_url()?>global_controller/create_folder",


              data: { new_folder : path3 }



            }).done(function(result) {

                if(result=='yes'){

                  var htm = '';

                  htm += '<li class="parent_li parent_me" style="display: list-item;">';

                  htm += '<a id="'+path+'" class="mediapath-custom" title="Expand ++"><span><i class="ui-icon-triangle-1-e ui-icon"></i></span>'+new_folder+'</a><ul class="folder-list"></ul>';

                  htm += '</li>';

                  $('.active-folder').siblings('ul').append(htm);

                  $('#folder-new').val('');




                }

            });

            }

          });



        

      });





$(document).on('click','.uploadtofilemanager2', function() {





      var file_data = $('#imgfiles_1').prop('files')[0];   



      var form_data = new FormData();                  

      var path =  $('.hidden-path-folder').val();

      form_data.append('file', file_data); 

      form_data.append('upload_path', path);



      



      if ($('#imgfiles_1').val() == '' ) {



         



      }else{



        $('.loader').show();



        $('.uploadlabel').html('Uploading ');



        $.ajax({



            // url: 'dashboard.php?function=uploadfile',
            url: "<?=base_url()?>global_controller/uploadfile",


            dataType: 'text', 



            cache: false,



            contentType: false,



            processData: false,



            data: form_data,                         



            type: 'post',



            success: function(php_script_response){



              setTimeout(function(){



                $('.loader').hide();



                  $('.uploadlabel').html('Upload');



                   $.ajax({



                    type: 'post',


                    url: "<?=base_url()?>global_controller/getimages",
                    // url: 'dashboard.php?function=getimages',



                    data: { folder : path }



                  }).done(function(result) {

                   

                    $('.file-manager').html('');



                    $('.file-manager').html(result);


                  });



                  $('#imgfiles_1').val('');

                  $('.filename').html('');



               }, 4000);

            }



       });



    }



  });



</script>