
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
 	 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>SAMS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php 
		echo "\t" . meta('description', "SAMS");
		echo "\t\t" . meta('Content-type', 'text/html; charset=utf-8', 'equiv'); 
	?>

	<?php
		echo "\t" . link_tag(base_url() . 'asset/css/bootstrap.min.css');
		echo "\t\t" . link_tag(base_url() . 'asset/css/bootstrap-table.min.css');
		// echo "\t\t" . link_tag(base_url() . 'asset/css/style.css');
		// echo "\t\t" . link_tag(base_url() . 'asset/css/styles.css');
		// echo "\t\t" . link_tag(base_url() . 'asset/css/responsive.css');
		echo "\t\t" . link_tag(base_url() . 'asset/css/loading.css');
		echo "\t\t" . link_tag(base_url() . 'asset/css/sb-admin.css');
		echo "\t\t" . link_tag(base_url() . 'asset/css/custom.css');

		if(isset($styles)){
			if(count($styles) > 0 ){
				foreach($styles as $value) {
					echo "\t\t" . link_tag($value);
				}
			}
		}
	?>

		<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-1.12.4.js"></script>
	    <script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-ui.js"></script>
	    <script type="text/javascript" src="<?php echo base_url();?>asset/js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="<?php echo base_url();?>asset/js/bootbox.min.js"></script>
	    <script type="text/javascript" src="<?php echo base_url();?>asset/js/bootstrap-table.min.js"></script>
	    <script type="text/javascript" src="<?php echo base_url();?>asset/js/moment.js"></script>
	    <script type="text/javascript" src="<?php echo base_url();?>asset/js/knockout-3.4.2.js"></script>
	    <script type="text/javascript" src="<?php echo base_url();?>asset/ckeditor/ckeditor.js"></script>
	    <script type="text/javascript" src="<?php echo base_url();?>asset/js/helper.js"></script>
	    <script type="text/javascript" src="<?php echo base_url();?>asset/js/custom.js"></script>
	    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

	<?php
		if(isset($script)){
			if(count($script) > 0 ){
				foreach($script as $value) {
	?>
	<script type="text/javascript" src="<?= $value;?>"></script>
	<?php
				}
			}
		}
	?>

	<script type="text/javascript">
      $(document).ready(function() {
        update_config();
        $('.btn-logout').click(function(e) {
          e.preventDefault();
          var el = document.createElement('a');
          el.href = window.location.href;
          aJax.post(
            "<?=base_url('global_controller/insert_audit_trail')?>",
            {
              action:'Log-out',
              uri:el.pathname
            },
            function(data){
              aJax.get(
                "<?=base_url('login/unsetSession');?>",
                function(result){
                  window.location = '<?=base_url();?>';
                }
              );
            }
          );
          
        })
        
      });

      function update_listing(reportid){
        aJax.get(
          "<?= base_url('api/update_listing'); ?>?report_id="+reportid,
          function(data){ 
            console.log("Listing Updated"); 
          }
        );
      }

      function isLoading(visible){
        if(visible == true){
          $('.loading').show();
        } else {
          $('.loading').hide();
        }
      }
      function updateAPI(api){
        update_config();
        aJax.get(
          "<?= base_url('api'); ?>/" + api,
          function(){
            console.log("API Update");
          }
        );
      }
      function insert_audit_trail(action){
        var el = document.createElement('a');
        el.href = window.location.href;
        aJax.post(
          "<?=base_url('global_controller/insert_audit_trail')?>",
          {
            action:action,
            uri:el.pathname
          },
          function(data){
            return 'success';
          }
        );
      }
      function update_config(){
        aJax.get(
          "<?=base_url("api/generate_config_json")?>",
          function(data){
             console.log("new generate_config_json saved!");
          }
        );
        aJax.get(
          "<?=base_url("api/audit_report_list")?>",
          function(data){
             console.log("new audit_report_list saved!");
          }
        );
        aJax.get(
          "<?=base_url("api/template_list")?>",
          function(data){
             console.log("new template_list saved!");
          }
        );
      }

      function isduplicate(table, query, cb){
        aJax.post(
          "<?= base_url('global_controller/check_if_exist');?>",
          {
            "table" : table,
            "query" : query
          },
          function(result){
            jQuery.globalEval(result);
            cb(result);
          }
        );
      }


      function check_inuse(query, cb){
        aJax.post(
          "<?=base_url('global_controller/getlist_in_use');?>",
          {
            'query': query
          },
          function(data){
            cb(data)
          }
        );
      } 

      function isIE() {
        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent) || navigator.userAgent.indexOf("Trident/") > -1 ){ 
          return true;
        } else {
          return false
        }
      }

    </script>

</head>
	<body class="fixed-nav sticky-footer bg-dark" id="page-top">
		<?php $this->load->view('layout/browser'); ?>
		<div class="loading" hidden>Loading&#8230;</div>

		<?php $this->load->view('layout/navigation'); ?>

		<div class="content-wrapper">
			<?php //$this->load->view($content); ?>
		</div>

	</body>
</html>

