<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SAMS Template</title>
  <link href="asset/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="asset/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="asset/css/style.css" rel="stylesheet">
  <link href="asset/css/styles.css" rel="stylesheet">
  <link href="asset/css/responsive.css" rel="stylesheet">
  <link href="asset/css/sb-admin.css" rel="stylesheet">
  <link href="asset/css/custom.css" rel="stylesheet">

  <script src="asset/vendor/jquery/jquery.min.js"></script>
  <script src="asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="asset/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-ui.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>asset/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>asset/js/bootbox.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>asset/js/bootstrap-table.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>asset/js/moment.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>asset/js/knockout-3.4.2.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>asset/ckeditor/ckeditor.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>asset/js/helper.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>asset/js/custom.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <script src="asset/js/sb-admin.min.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
        get_menus();
        update_config();

        $('.btn-logout').click(function(e) {
          e.preventDefault();
          var el = document.createElement('a');
          el.href = window.location.href;
          aJax.post(
            "<?=base_url('global_controller/insert_audit_trail')?>",
            {
              action:'Log-out',
              uri:el.pathname
            },
            function(data){
              aJax.get(
                "<?=base_url('login/unsetSession');?>",
                function(result){
                  window.location = '<?=base_url();?>';
                }
              );
            }
          );
          
        })
        
      });

      function update_listing(reportid){
        aJax.get(
          "<?= base_url('api/update_listing'); ?>?report_id="+reportid,
          function(data){ 
            console.log("Listing Updated"); 
          }
        );
      }

      function isLoading(visible){
        if(visible == true){
          $('.loading').show();
        } else {
          $('.loading').hide();
        }
      }
      function updateAPI(api){
        update_config();
        aJax.get(
          "<?= base_url('api'); ?>/" + api,
          function(){
            console.log("API Update");
          }
        );
      }
      function insert_audit_trail(action){
        var el = document.createElement('a');
        el.href = window.location.href;
        aJax.post(
          "<?=base_url('global_controller/insert_audit_trail')?>",
          {
            action:action,
            uri:el.pathname
          },
          function(data){
            return 'success';
          }
        );
      }
      function update_config(){
        aJax.get(
          "<?=base_url("api/generate_config_json")?>",
          function(data){
             console.log("new generate_config_json saved!");
          }
        );
        aJax.get(
          "<?=base_url("api/audit_report_list")?>",
          function(data){
             console.log("new audit_report_list saved!");
          }
        );
        aJax.get(
          "<?=base_url("api/template_list")?>",
          function(data){
             console.log("new template_list saved!");
          }
        );
      }

      function isduplicate(table, query, cb){
        aJax.post(
          "<?= base_url('global_controller/check_if_exist');?>",
          {
            "table" : table,
            "query" : query
          },
          function(result){
            jQuery.globalEval(result);
            cb(result);
          }
        );
      }


      function check_inuse(query, cb){
        aJax.post(
          "<?=base_url('global_controller/getlist_in_use');?>",
          {
            'query': query
          },
          function(data){
            cb(data)
          }
        );
      } 

      function isIE() {
        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent) || navigator.userAgent.indexOf("Trident/") > -1 ){ 
          return true;
        } else {
          return false
        }
      }

    </script>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php $this->load->view('layout/browser'); ?>
  <div class="loading" hidden>Loading&#8230;</div>

  <?php $this->load->view("layout/navigation"); ?>

  <div class="content-wrapper" style="height: 100vh;">
    <div class="container-fluid">
      <?php $this->load->view($content); ?>
    </div>

    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small></small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

  </div>
</body>

</html>
