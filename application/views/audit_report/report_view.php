<div class="col-md-12 bg-white">
	<div class="col-md-12">
		<div class="header"><h4>GMP AUDIT REPORT</h4><label class="gmp_report_no">Report Number</label></div>
	</div>
 
	<!-- Part I. Audit Information -->
	<div class="col-md-12"> 
		<div class="col-md-12 audit-header">Part I. Audit Information</div>
		<div class="col-md-12 sub-header">A. General Information</div>
		<div class="col-md-12 pad-0 part1_content">
			<div class="col-md-12 pad-0">
				<div class="col-md-6 pad-0">
					<div class="col-md-12 pad-0">
						<span>Name of Manufacturer: </span> <label class="company_name"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Site Address 1: </span> <label class="address1"></label>,<label class="address2"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Site Address 2: </span> <label class="address3"></label>, <label class="country"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Date of Audit: </span> <label class="audit_date"></label>
					</div>
				</div>
				<div class="col-md-6 pad-0">
					<div class="col-md-12 pad-0">
						<span>Name of Lead Auditor: </span> <label class="lead_auditor"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Position: </span> <label class="position"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Department/Division &amp; Company:  </span> <label class="auditor-dept"></label>
					</div>
				</div>
			</div>
			<div class="col-md-12 pad-0">
				<div class="table-responsive mb15">
					<table border="1" class="auditors_table tbl_format">
						<thead>
							<tr>
								<td>Name</td>
								<td>Position</td>
								<td>Department/Designation</td>
							</tr>
						</thead>
						<tbody class="auditors_tbody">
						
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-md-12 sub-header">B. Company Background/Audit History</div>
		<div class="col-md-12 pad-0 part2_content">
			<div class="col-md-12 pad-0">
				<span>Supplier Background/History: </span>
				<textarea class="form-control Supplier-history mb15" disabled></textarea>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Date of Previous Inspection:</span><span class="previous-inspection"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Inspector(s) Involved:</span>
			<ol class="inspectors">
			</ol>
			<span>Major Changes Since Previous Inspection: </span>
			<ol class="changes">
			</ol>
		</div>
		<div class="col-md-12 sub-header">C. Audit Information</div>
		<div class="col-md-12 pad-0">
			<span>Activities Carried out by the company: </span>
			<ol class="activities">
			</ol>
			<span>Other Activities Carried out by the company: </span><span class="other-activities"></span><br>
			<span>Scope of Audit: </span>
			<ol class="scope_audit">
			</ol>
			<!-- <span>Products of Interest (for Insert Scope of Audit Name 1) </span>
			<ol class="product_interest">
				<li>Product Name</li>
				<li>Product Name</li>
			</ol> -->
		</div>
		<div class="col-md-12 pad-0">
			<div class="table-responsive mb15">
				<table border="1" class="license_table tbl_format">
					<thead>
						<tr>
							<td>Licenses/Certification</td>
							<td>Issuing Regulatory</td>
							<td>License/Certification Number</td>
							<td>Validity</td>
						</tr>
					</thead>
					<tbody class="license_tbody">
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Pre-Audit Documents:</span>
			<ol class="pre-documents">
			</ol>
		</div>
		<div class="col-md-12 pad-0">
			<span>Audited areas:</span> <span class="audited_areas"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Not Audited areas:</span> <span class="not_audited_areas"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Date of Wrap-up or Close out Meeting:</span> <span class="wrapup-date"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Name of Present During Close-out Meeting</span>
			<div class="table-responsive mb15">
				<table border="1" class="present-meeting tbl_format">
					<thead>
						<tr>
							<td>Name</td>
							<td>Position/Department</td>
						</tr>
					</thead>
					<tbody class="present_met_tbody">
						<tr>
							<!-- <td>Juan Dela Cruz</td>
							<td>CIT</td> -->
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Name of Personnel Met During Inspection </span>
			<div class="table-responsive mb15">
				<table border="1" class="personel-met tbl_format">
					<thead>
						<tr>
							<td>Name</td>
							<td>Position/Department</td>
						</tr>
					</thead>
					<tbody class="personel_met_tbody">
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Distribution List (except Supplier and GMP Inspection)</span>
			<ol class="distribution-list">
			</ol>
		</div>
		<div class="col-md-12 sub-header">D. Templates Answers</div>
		<div class="col-md-12 pad-0">
			<div class="col-md-12 pad-0">
				<span>Product Type: </span><span class="report_producttype"></span>
			</div>
			<div class="col-md-12 pad-0">
				<span>Standard/Reference: </span><span class="report_standardreference"></span>
			</div>
		</div>
		<!-- element tables -->
		<div class="col-md-12 pad-0">
			<div class="col-md-12 pad-0 element_con mb15"></div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="col-md-12 audit-header">Part II. Audit Observation</div>
		<div class="col-md-12 pad-0">
			<span>Elements Requiring Recommendation: </span>
			<ol class="element-reco"></ol>
		</div>

		<div class="col-md-12 pad-0">
			<span>Audit Team's Findings and Observations Relevant to the Audit</span>
			<ol class="aud_findings"></ol>
		</div>

		<div class="col-md-12 pad-0">
			<span>Definition/Categorization of Audit Observations</span>
			<ol class="aud_definition"></ol>
		</div>

		<div class="col-md-12 pad-0">
			<span>Listing of Audit Observations and Concerns (in decreasing order of criticality)</span>
		</div>

		<div class="col-md-12 pad-0">
			<span>Critical Observation - <i class="critical_observation"></i></span><br>
			<span>Major Observation - <i class="major_observation"></i></span><br>
			<span>Minor Observation - <i class="minor_observation"></i></span>
		</div>
		<div class="clearfix"></div>
		<br>
		<div class="col-md-12 pad-0">
			<span>Disposition</span>
			<div class="table-responsive mb15">
				<table border="1" class="scope-disposition tbl_format">
					<thead>
						<tr>
							<td>Disposition</td>
							<td>Product</td>
						</tr>
					</thead>
					<tbody class="disposition_tbody">
						
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-md-12 pad-0">
			<span>Other Issue for Audit Report: </span> <span class="other-issue-audit"></span>
		</div>	
		<div class="col-md-12 pad-0">
			<span>Other Issues for Executive Summary: </span> <span class="other-issue-executive"></span>
		</div>	
<!-- 
		<div class="col-md-12 pad-0">

			<span>Audit Closure Date:</span><label class="audit-close-date">02 September 2017 </label>

		</div>	 -->

	</div>

<hr noshade size=1 width="100%">
	<div class="col-md-12 remarks-view">
		<div class="col-md-12 pad-0">
			<span>REMARKS</span>
			<div class="table-responsive mb15">
				<table border="1" class="report-remarks tbl_format">
					<thead>
						<tr>
							<td>Date</td>
							<td>Name</td>
							<td>Position</td>
							<td>Status</td>
							<td>Remarks</td>
						</tr>
					</thead>
					<tbody class="remarks_tbody">
					</tbody>
				</table>
			</div>
		</div>	
	</div>

	<div class="col-md-12 remarks_inputs remarks-reviewer">
		<div class="col-md-12 pad-0 input-remarks">
			<span><b>REMARKS<b></span>
			<div class="">

				<div class="col-md-12">
						<textarea class="form-control" id="txt_remarks" rows="4" cols="50"></textarea><br>
				
						
		  				<div class="col-md-6">
		  					<button name="subject" type="submit" class="btn btn-warning reject_submit" value="reject">SUBMIT TO LEAD-AUDITOR</button>
		  				</div>
		  				<div class="col-md-6">		
		  					<button name="subject" type="submit" class="btn btn-success remarks_submit" value="approved">SUBMIT TO DIVISION HEAD</button>
		  				</div>
				</div>
				
			</div>
		</div>
	</div>

	<div class="col-md-12 remarks_inputs remarks-approver">
		<div class="col-md-12 pad-0 input-remarks">
			<span><b>REMARKS<b></span>
			
			<div class="table-responsive mb15">

				<div class="col-md-12">
						
						<textarea class="form-control" id="txt_remarks" rows="4" cols="50"></textarea><br>
						
						<div class="col-md-6">
  							<button name="subject" type="submit" class="btn btn-warning reject_submit" value="reject">SUBMIT TO LEAD-AUDITOR</button>
  						</div>
  						<div class="col-md-6">
							<button name="subject" type="submit" class="btn btn-success remarks_submit " value="approved">APPROVE</button>
						</div>
				</div>
				
			</div>
		</div>	
	</div>
</div>

<script type="text/javascript">

$(document).ready(function(){

var id = "<?= $id; ?>";
var div_remarks = "<?= $div;?>"




var answer_no = 0;

get_report(id);

function get_report(id){
	isLoading(true);
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>Preview_report",
		data:{id:id}
	}).done(function(data){
		var obj = JSON.parse(data);
		a = '';
		$.each(obj, function(x,y){
			$('.gmp_report_no').text(y.report_no);
			$('.company_name').text(y.name);
			$('.address1').text(y.address1);
			$('.country').text(y.country);
	    	$('.address2').text(y.address2);
	    	$('.address3').text(y.address3);
			$('.lead_auditor').text(y.fname+' '+y.mname+' '+y.lname);
			if(y.designation != 0){
				$('.position').text(y.designation);
			}
			$('.auditor-dept').text(y.department+', '+y.company);
			get_audit_dates(y.report_id);
			// $('.audit_date').text(y.audit_date);
			// $('.audit_date').text(moment(y.audit_date).format('LLL'));
			auditors(y.report_id,'get_co_auditors','Co-auditor');
			auditors(y.report_id,'get_approvers','Approver');
			auditors(y.report_id,'get_reviewer','Reviewer');
			auditors(y.report_id,'get_translator','Translator');
			$('.Supplier-history').text(y.background);
			qv_insepection_date(y.report_id);
			inspectors(y.report_id);
			inspection_changes(y.report_id);
			activities(y.report_id);
			$('.other-activities').text(y.other_activities);
			scope_audit(y.report_id);
			get_scope_product(y.report_id);
			license(y.report_id);
			pre_document(y.report_id);
			$('.audited_areas').text(y.audited_areas);
			if(y.areas_to_consider != "" || y.areas_to_consider != null){
				$('.not_audited_areas').text(y.areas_to_consider);
			} else {
				$('.not_audited_areas').text("None");
			}
			
			$('.wrapup-date').text(y.wrap_up_date);	
			personel_met(y.report_id);
			distribution(y.report_id);
			disposition(y.report_id);
			other_issues_audit(y.report_id)
			other_issues_exec(y.report_id)		
			// $('.audit-close-date').text(y.closure_date);
			get_template(y.report_id);
			get_element_reco(y.report_id);
			present_during_meeting(y.report_id)
			remarks(y.report_id);
			qv_observation_yes(y.report_id);
			qv_audit_observation(y.report_id);
			// qv_audit_observation_count(y.template_id, y.report_id);
			last_inspection(y.company_id);

			if(div_remarks == "remarks-view"){
				$('.remarks-view').show();
				$('.remarks-reviewer').remove();
				$('.remarks-approver').remove();
			}

			if(div_remarks == "remarks-reviewer"){
				$('.remarks-view').remove();
				$('.remarks-reviewer').show();
				$('.remarks-approver').remove();


			}

			if(div_remarks == "remarks-approver"){
				$('.remarks-view').remove();
				$('.remarks-reviewer').remove();
				$('.remarks-approver').show();
			}


			check_stamp(y.report_id);

		});

		$('.auditors_tbody').append(a);

	});

}

function check_stamp(report_id)
{
	aJax.post(
		"<?= base_url("preview_report/check_stamp");?>",
		{
			'report_id' : report_id
		},
		function(data){
			console.log(data);
			var obj = JSON.parse(data);
			console.log(obj.length);
			if(obj.length > 0){
				if(div_remarks == "remarks-approver"){
					if(obj[0].approved_date == null){
						$('.remarks_inputs').show();
					} else {
						$('.remarks_inputs').hide();
					}
				}

				if(div_remarks == "remarks-reviewer"){
					if(obj[0].review_date == null){
						$('.remarks_inputs').show();
					} else {
						$('.remarks_inputs').hide();
					}
				}
			} else {
				$('.remarks_inputs').show();
			}			
		}
	);
}

function isValidDate (date) {
    return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
}


function get_audit_dates(report_id){
	aJax.post(
		"<?=base_url('preview_report/get_audit_dates');?>",
		{
			report_id : report_id
		},
		function(data){
			var obj = JSON.parse(data);
			$('.audit_date').html(gmp.audit_dates(obj));
		}
	);
}
function other_issues_exec(report_id){
	aJax.post(
		"<?=base_url('global_controller/get_by_id');?>",
		{
			'table': "qv_report_other_issues_executive", 
			'field' : 'report_id', 
			'id' : report_id
		},
		function(result){
			var obj = JSON.parse(result);
			var issues = "<ul>";
			$.each(obj, function(x,y){
				issues += "<li>"  + y.other_issues_executive + "</li>"
			});
			issues += "</ul>"; 
			$('.other-issue-executive').html(issues);
		}
	);
}
function other_issues_audit(report_id){
	aJax.post(
		"<?=base_url('global_controller/get_by_id');?>",
		{
			'table': "qv_report_other_issues_audit", 
			'field' : 'report_id', 
			'id' : report_id
		},
		function(result){
			var obj = JSON.parse(result);
			var issues = "<ul>";
			$.each(obj, function(x,y){
				issues += "<li>" + y.other_issues_audit + "</li>"
			});
			issues += "</ul>"; 
			$('.other-issue-audit').html(issues);
		}
	);
}

function last_inspection(company_id){
	aJax.post(
		"<?=base_url('global_controller/get_by_id');?>",
		{ 'table': "tbl_audit_history", 'field' : 'supplier_id', 'id' : company_id },
		function(result){
			var obj = JSON.parse(result);
			if(obj.length > 0){
				var last_history = obj[obj.length-1];
				var history_id = last_history.id;
				aJax.post(
					"<?=base_url('global_controller/get_by_id');?>",
					{
						'table': "tbl_audit_history_dates", 
						'field' : 'history_id', 
						'id' : history_id
					},
					function(audit_dates){
						var obj_dates = JSON.parse(audit_dates);
						var audit_date = "";
					    var audit_month = "";
					    var audit_year = "";
	    				audit_month = ""; audit_date = ""; audit_year = "";
	    				var len = obj_dates.length;

					    $.each(obj_dates, function(x,y){
					    	audit_month = moment(y.Date).format('MMMM');
	        				audit_year = moment(y.Date).format('Y');
	        				if (x === len - 1) {
						      audit_date += " & " + moment(y.Date).format('D') + "";
						    } else {
						      audit_date +=  ", " + moment(y.Date).format('D');
						    }

					    })

					    if(audit_date.trim().charAt(0) == "&") {
						    audit_date = audit_date.trim().substr(1);
						}

						$('.previous-inspection').html(audit_month + " " + audit_date.substr(1) + ", " + audit_year);
					}
				);

				//INSPECTOR
				aJax.post(
					"<?=base_url('global_controller/get_by_id');?>",
					{
						'table': "tbl_audit_history_inspectors", 
						'field' : 'history_id', 
						'id' : history_id
					},
					function(inspectors){
						var obj_inspectors = JSON.parse(inspectors);
						var html ="";
						$.each(obj_inspectors, function(a,b){
							html += '<li>' + b.inspector + '</li>';
						});
						$('.inspectors').html(html);

					}
				);
				isLoading(false);
			} else {
				$('.previous-inspection').html("None");
				$('.inspectors').html("None");
				isLoading(false);
			}
			

			
		}

	);
}

function qv_insepection_date(reportid){
	aJax.post(
		"<?=base_url('global_controller/get_by_id');?>",
		{ 'table': "tbl_report_inspection_date", 'field' : 'report_id', 'id' : reportid },
		function(result){
			var obj = JSON.parse(result);
			 	var inspection_date = "";
			    var inspection_month = "";
			    var inspection_year = "";

			    var len = obj.length;

			    $.each(obj, function(index, row){
				    inspection_month = moment(row.inspection_date).format('MMMM');
				    inspection_year = moment(row.inspection_date).format('Y');

				    if (index === len - 1) {
				      inspection_date += " & " + moment(row.inspection_date).format('D') + "";
				    } else {
				        inspection_date +=  ", " + moment(row.inspection_date).format('D');
				    }
				});

				if(inspection_date.trim().charAt(0) == "&") {
				    inspection_date = inspection_date.trim().substr(1);
				}
	
			    var return_date = inspection_month + " " + inspection_date.substr(1) + ", " + inspection_year;
			    $('.previous-inspection').text(" " + return_date);
			}

	);
}
function qv_audit_observation_count(templateid,reportid){

	var ctrx = 1;
    var critical = "";
    var major = "";
    var minor = "";

	$.ajax({
    type: 'post',
    url: '<?=base_url();?>global_controller/get_by_id',        
    data:  { 'table': "qv_elements_observation", 'field' : 'template_id', 'id' : templateid } ,
  	}).done(function(data){  
  		var obj = JSON.parse(data);
  		$.each(obj, function(x,y){
  			var pass_data = {'report_id':reportid,'element_id':y.element_id};
  			$.ajax({
			    type: 'post',
			    url: '<?=base_url();?>global_controller/get_no_answer',        
			    data:  pass_data,
			  	}).done(function(result){  
			  		var no_answer = JSON.parse(result);
			  		console.log(no_answer);
			  		if(no_answer.length > 0){
				  		$.each(no_answer, function(index,value){

				  			switch(value.category_name) {
							    case 'Critical':
							        critical += ctrx + ',';
							        break;
							    case 'Major':
							        major += ctrx + ',';
							        break;
							    case 'Minor':
							        minor += ctrx + ',';
							        break;
							}
							ctrx +=1;
							if(critical == ""){
					  			$('.critical_observation').html("None");
					  		} else {
					  			$('.critical_observation').html("Please refer to item no(s) " + critical);
					  		}

					  		if(major == ""){
					  			$('.major_observation').html("None");
					  		} else {
					  			$('.major_observation').html("Please refer to item no(s) " + major);
					  		}

					  		if(minor == ""){
					  			$('.minor_observation').html("None");
					  		} else {
					  			$('.minor_observation').html("Please refer to item no(s) " + minor);
					  		}
				  		});
				  	} else {
				  		$('.critical_observation').html("None");
				  		$('.major_observation').html("None");
				  		$('.minor_observation').html("None");
				  	}
			  	});

  		});

  		
	});

}

function qv_audit_observation(reportid){
	$.ajax({
    type: 'post',
    url: '<?=base_url();?>preview_report/get_audit_observation',        
    data:  { 'report_id': reportid} ,
  	}).done(function(data){  
  		var obj = JSON.parse(data);
  		console.log(obj);
  		var htm = '';
		$.each(obj, function(x,y){
			htm += '<u>'+y.category_name+' Observation</u><br>';
			htm += y.description + '<br><br>';
		});

		$('.aud_definition').html(htm);
	});

}
 
function qv_observation_yes(reportid){
	$.ajax({
    type: 'post',
    url: '<?=base_url();?>preview_report/report_observation',        
    data:  { 'report_id' : reportid } ,
  	}).done(function(data){  
  		var obj = JSON.parse(data);
  		console.log("obj");
  		console.log(obj);
  		var htm = '';
		$.each(obj, function(x,y){
			htm += '<li>' + y.Element + " - " + y.Observation;
			htm += '</li>';
		});
		

		$('.aud_findings').html(htm);
  	});
}



function auditors(report_id,func,position){

	// $('.auditors_tbody').html('');

	$.ajax({

		type: 'post',

		url: "<?=base_url()?>preview_report/"+func,

		data:{report_id:report_id}

	}).done(function(data){

		var obj = JSON.parse(data);

		var htm = '';

		

		$.each(obj, function(x,y){

			if(func == "get_translator"){
				htm += '<tr>';
				htm += '<td>'+y.translator+'</td>';
				htm += '<td>Translator</td>';
				htm += '<td></td>';
				htm += '</tr>';
			} else {
				htm += '<tr>';
				htm += '<td>'+y.fname+' '+y.mname+' '+y.lname+'</td>';
				htm += '<td>'+position+'</td>';
				htm += '<td>'+y.department+', '+y.company+'</td>';
				htm += '</tr>';
			}
			
		});

		
		if(func == "Co-auditor" || func == "get_translator"){
			$('.auditors_tbody').after(htm);
		} else {
			$('.auditors_tbody').append(htm);
		}

	});

}

function inspectors(report_id){

	$.ajax({

		type: 'post',

		url: "<?=base_url()?>preview_report/get_inspectors",

		data:{report_id:report_id}

	}).done(function(data){

		var obj = JSON.parse(data);

		var htm = '';

		

		$.each(obj, function(x,y){

			htm += '<li>'+y.name+'</li>';

		});		

		$('.inspectors').append(htm);

	});

}

function inspection_changes(report_id){

	$.ajax({

		type: 'post',

		url: "<?=base_url()?>preview_report/get_inspection_changes",

		data:{report_id:report_id}

	}).done(function(data){

		var obj = JSON.parse(data);

		var htm = '';

		

		$.each(obj, function(x,y){

			htm += '<li>'+y.changes+'</li>';

		});		

		$('.changes').append(htm);

	});

}

function activities(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_activities",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		$.each(obj, function(x,y){
			htm += '<li>'+y.activity_name;

			if(y.sub_activity.length > 0){
				htm += '<ul style="list-style-type: square;">';
				$.each(y.sub_activity, function(a, b){
					htm += '<li>' + b.sub_item_name+ '</li>';
				});
				htm += '</ul>'
			}

			htm += '</li>';
		});		
		$('.activities').append(htm);
	});
}

function get_scope_product(reportid){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_report_scope_product",
		data:{report_id:reportid}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';

		
		$.each(obj, function(x,y){	
			htm += '<span>Products of Interest ('+ y.scope +')</span>';
			htm += '<ol class="product_interest">';		
			$.each(y.products, function(a,b){
				htm += '<li>'+b.product_name+'</li>';	
			})		
			htm += '</ol>';	
		});
		

		$('.scope_audit').after(htm);
	});
}

function scope_audit(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_scope_audit",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		$.each(obj, function(x,y){
			htm += '<li>'+y.scope_name;
			htm +='</li>';
		});		
		$('.scope_audit').append(htm);
	});
}

function scope_product(scope_id,report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_scope_product",
		data:{'scope_id':scope_id,'report_id':report_id}
	}).done(function(data){
		var obj =JSON.parse(data);
		var htm = '';
		htm += '<span>Products of Interest</span>';
		htm += '<ol class="product_interest">';
		$.each(obj, function(x,y){						
			htm += '<li>'+y.product_name+'</li>';		
		});
		htm += '</ol>';
		$('.scope_audit').after(htm);
	});
}

function license(report_id){

	$.ajax({

		type: 'post',

		url: "<?=base_url()?>preview_report/get_license",

		data:{report_id:report_id}

	}).done(function(data){

		var obj = JSON.parse(data);

		var htm = '';

		

		$.each(obj, function(x,y){

			htm += '<tr>';

			htm += '<td>'+y.reference_name+'</td>'; 

			htm += '<td>'+y.issuer+'</td>';

			htm += '<td>'+y.reference_no+'</td>';

			htm += '<td>'+moment(y.validity).format("D MMMM YYYY")+'</td>';

			htm += '</tr>';

		});

		

		$('.license_tbody').append(htm);

	});

}

function pre_document(report_id){

	$.ajax({

		type: 'post',

		url: "<?=base_url()?>preview_report/get_pre_document",

		data:{report_id:report_id}

	}).done(function(data){

		var obj = JSON.parse(data);

		var htm = '';

		

		$.each(obj, function(x,y){

			htm += '<li>'+y.document_name+'</li>';

		});		

		$('.pre-documents').append(htm);

	});

}

function personel_met(report_id){

	$.ajax({

		type: 'post',

		url: "<?=base_url()?>preview_report/get_personel_met",

		data:{report_id:report_id}

	}).done(function(data){

		var obj = JSON.parse(data);

		var htm = '';

		

		$.each(obj, function(x,y){

			htm += '<tr>';

			htm += '<td>'+y.name+'</td>';

			htm += '<td>'+y.designation+'</td>';

			htm += '</tr>';

		});

		

		$('.personel_met_tbody').append(htm);

	});

}

function present_during_meeting(report_id){

	$.ajax({

		type: 'post',

		url: "<?=base_url()?>preview_report/get_present_during_meeting",

		data:{report_id:report_id}

	}).done(function(data){

		var obj = JSON.parse(data);

		var htm = '';

		

		if(obj.length!=0){

			$.each(obj, function(x,y){

				htm += '<tr>';

				htm += '<td>'+y.name+'</td>';

				htm += '<td>'+y.position+'</td>';

				htm += '</tr>';

			});

		}else{

			htm += '<tr><td colspan="2">N/A</td></tr>';

		}

		

		$('.present_met_tbody').append(htm);

	});

}

function distribution(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_distribution",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		if(obj.length == 0){
			htm += 'Not Applicable';
		}else{
			$.each(obj, function(x,y){
				htm += '<li>'+y.distribution_name+'</li>';
			});	
		}	
		$('.distribution-list').append(htm);
	});
}

function disposition(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_disposition",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		$.each(obj, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.Disposition+'</td>';
			htm += '<td>'+y.Products+'</td>';
			htm += '</tr>';
		});
		$('.disposition_tbody').append(htm);
	});
}



function get_template(report_id){

	$.ajax({

		type: 'post',

		url: "<?=base_url()?>preview_report/get_template",

		data: {report_id:report_id}

	}).done(function(data){

		var obj = JSON.parse(data);

		$.each(obj, function(x,y){

			$('.report_producttype').text(y.classification_name);

			$('.report_standardreference').text(y.standard_name);

			get_element(y.template_id,report_id);

		});

	});

}



function get_element(template_id,report_id){
	var critical = "";
    var major = "";
    var minor = "";

	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_element_questions_answers",
		data:{report_id: report_id, template_id:template_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var index = 65; 
		var alpha = ["A","B","C","D","E","F","G","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
		
		$.each(obj, function(x,y){
			var htm = '';
			htm += '<div class="col-md-12 pad-0 element_item ei_'+y.element_id+'">';
			htm += '<span class="element_name">ELEMENT '+alpha[y.order -1]+' - '+y.element_name.toUpperCase();+'</span>';
			htm += '<table border="1" class="element_table_'+y.element_id+' tbl_format">';
			htm += '<thead>';
			htm += '<tr>';
			htm += '<td>Questions</td>';
			htm += '<td>Answer(Yes/No/NA)</td>';
			htm += '<td>Details</td>';
			htm += '<td>Item No.</td>';
			htm += '<td>Category</td>';
			htm += '</tr>';
			htm += '</thead>';
			htm += '<tbody>';
				$.each(y.questions, function(a,b){
					var category = '';
					htm += '<tr>';
					htm += '<td style="width:40%;">'+b.question+'</td>';
					htm += '<td style="width:10%;">'+b.answer_name+'</td>';
					htm += '<td style="width:30%;">'+b.answer_details+'</td>';
					htm += '<td style="width:10%;">'+b.answer_no+'</td>';

					if(b.category != null){
						category = b.category;
					}

					htm += '<td style="width:10%;">'+category+'</td>';
					htm += '</tr>';

					switch(b.category) {
					    case 'Critical':
					        critical += b.answer_no + ',';
					        break;
					    case 'Major':
					        major += b.answer_no + ',';
					        break;
					    case 'Minor':
					        minor += b.answer_no + ',';
					        break;
					}

					if(critical == ""){
			  			$('.critical_observation').html("None");
			  		} else {
			  			$('.critical_observation').html("Please refer to item no(s) " + critical);
			  		}

			  		if(major == ""){
			  			$('.major_observation').html("None");
			  		} else {
			  			$('.major_observation').html("Please refer to item no(s) " + major);
			  		}

			  		if(minor == ""){
			  			$('.minor_observation').html("None");
			  		} else {
			  			$('.minor_observation').html("Please refer to item no(s) " + minor);
					  		}

				});
			htm += '</tbody>';
			htm += '</table>';
			htm += '</div>';

			$('.element_con').append(htm);
		});
		
	});
}

function get_question_answer(element_id,report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_report_answer",
		data:{report_id:report_id,element_id:element_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		var category = '';
		$.each(obj, function(x,y){
			
			htm += '<tr>';
			htm += '<td style="width:40%;">'+y.question+'</td>';
			htm += '<td style="width:10%;">'+y.answer_name+'</td>';
			htm += '<td style="width:30%;">'+y.answer_details+'</td>';
			if(y.answer_name == 'No'){
				answer_no++; 
				htm += '<td style="width:10%;">'+answer_no+'</td>';
			}else{
				htm += '<td style="width:10%;"></td>';
			}
			
			if(y.category_name == null){
				category = '';
			}else{
				category = y.category_name;
			}
			htm += '<td style="width:10%;">'+category+'</td>';
			htm += '</tr>';
		});
		$('.element_tbody_'+element_id).html(htm);
	});
}



function get_element_reco(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_element_reco",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		var alpha = ["A","B","C","D","E","F","G","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
		if(obj.length == 0){
			htm += 'Not Applicable';
		}else{
			$.each(obj, function(x,y){
				var order = y.order - 1;
				htm += '<li>Element '+alpha[order]+' - '+y.recommendation+'</li>';
			});	
		}	
		$('.element-reco').html(htm);
	});
}

});

function remarks(report_id){
	
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_remarks",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		var fname = ""
		
		$.each(obj, function(row,data2){
			htm += "<tr>";
			htm += "<td>" + moment(data2.date).format("LL") + "</td>";
			htm += "<td>" + data2.name + "</td>";
			htm += "<td>" + data2.position + "</td>";
			htm += "<td>" + data2.status + "</td>";
			htm += "<td>" + data2.remarks + "</td>";
			htm += "</tr>";	
		});		

		$(".remarks_tbody").append(htm);
	});

	
}


function getdetails(type){
	return type;
}


</script>