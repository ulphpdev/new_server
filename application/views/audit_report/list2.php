
<style>

	.control-label{
		font-size: 12px;
	}

</style>
<ol class="breadcrumb" style="background-color: #fff;">
  	<li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
  	<li class="active">Audit Report</li>
</ol>


<div class="panel panel-default">
	<?php $this->load->view("audit_report/filter_form"); ?>

	<form hidden id="csv_form" action="<?= base_url('c_auditreport/get_csv');?>" method="post" target="_blank">
		<input class="csv_query" name="query" />
	</form>

</div>


<div class="row" style="margin-top: -10px;padding-left: 17px;padding-right: 15px;">
	<div class="col-md-12 table-responsive">
		<table class = "table listdata table-striped" style="margin-bottom:0px; ">
          	<thead>
            	<tr>
           		 	<th>Audit Report No.</th>
            		<th>Audited Site</th>
            		<th>Auditor</th>
            		<th>Audit Date</th>
            		<th>Modified Date</th>
            		<th>Status</th>
            		<th style="width:10px;">Version</th>
            		<th style="width:150px; text-align: right;">Actions</th>
          		</tr>
          	</thead>
          	<tbody class="table_body"></tbody>
		</table>
	</div>

	<div style="text-align: center; margin-top: 20px;" class="form-inline pager_div"></div>

</div>



<div id="view-div-panel" class="rows div-view-audit-report " hidden style="margin-top: 30px;" >
	<div class="panel panel-default">
  		<div class="panel-body"  style="padding-bottom: 17px;">
			<div class="view_report_content"></div>
		</div>
	</div>
</div>


<div id="generate_PDF_modal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Generate Report</h4>
      		</div>
      		<div class="modal-body">
      			<input class="pdf-picker-id" hidden />
     			<input class="pdf-picker-no" hidden />
        		<select class="form-control pdf-picker">
        			<option value="audit_report">AUDIT REPORT</option>
        			<option value="executive_summary">EXECUTIVE SUMMARY</option>
        			<option value="annexure_pdf">ANNEXURE - GMP AUDIT DETAILS [.PDF]</option>
        			<option value="annexure_doc">ANNEXURE - GMP AUDIT DETAILS [.DOC]</option>
        			<option value="raw_data">RAW DATA REPORT</option>
        			<option value="approval_history">APPROVAL HISTORY</option>
        		</select>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary btn-generate-pdf">Generate</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<div id="export_PDF_modal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Generate Report</h4>
      		</div>
      		<div class="modal-body">
      			<input class="pdf-picker-id" hidden />
     			<input class="pdf-picker-no" hidden />
        		<select class="form-control pdf-picker-export">
        			<option value="Audit_Report.pdf">AUDIT REPORT</option>
        			<option value="Executive_Report.pdf">EXECUTIVE SUMMARY</option>
        			<option value="Annexure.pdf">ANNEXURE - GMP AUDIT DETAILS [.PDF]</option>
        			<option value="Annexure.docx">ANNEXURE - GMP AUDIT DETAILS [.DOC]</option>
        			<option value="Raw_Data_Report.pdf">RAW DATA REPORT</option>
        		</select>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary btn-generate-export">Generate</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<div id="approve_send_email" class="modal fade" role="dialog">
  	<div class="modal-dialog modal-lg">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Send Audit Report</h4>
      		</div>
      		<div class="modal-body">
        		<form action="" class="form-horizontal">
		            <div class="form-group">
		            	<div class="col-md-12 pad-0 mb5">
					        <div class="col-md-2">To*: </div>
					        <div class="col-md-10">
					        	<textarea class="form-control inputs txthidden inputs-email" id="sender_other_approved" hidden></textarea>
					        	<span class=""  style="font-size:10px;">Note: To add (+) more email, please separate with a comma (,)</span>
					        	<br>
					        	<span class="er-msg">Email should not be empty</span>
					        </div>
					    </div>
					    <div class="col-md-12 pad-0 mb5">
					        <div class="col-md-2" >Subject*: </div>
					        <div class="col-md-10">
					        	<input type="text"  name="" class="form-control inputs inputs-email subject_other_approved " id="subject_other_approved " >
					        	<span class="er-msg">Subject should not be empty</span>
					        </div>
					    </div>
					    <div class="col-md-12 pad-0 mb5">
					        <div class="col-md-2">Message: </div>
					        <div class="col-md-10">
					        	<textarea class="form-control" id="message_other_approved" rows="5"></textarea>
					        </div>
				      	</div>
				      	<div class="col-md-12 pad-0 mb5">

					        <div class="col-md-2"></div>
					        <div class="col-md-5">
					        	<div class="checkbox">
								  <label><input type="checkbox" value="audit_report" class="file_send ar">Audit Report</label>
								</div>
								<div class="checkbox">
								  <label><input type="checkbox" value="executive_summary" class="file_send esr">Executive Summary Report</label>
								</div>
								<div class="checkbox">
								  <label><input type="checkbox" value="raw_data" class="file_send rr">Rating Report</label>
								</div>
                                <div class="checkbox">
                                  <label><input type="checkbox" value="approval_history" class="file_send ah">Approval History</label>
                                </div>
								<span class="er-msg file-err">Attachment is required</span>
					        </div>
					        <div class="col-md-5">
					        	<div class="checkbox">
								  <label><input type="checkbox" value="annexure_pdf" class="file_send annex_pdf">Annexure Report (PDF)</label>
								</div>
								<div class="checkbox">
								  <label><input type="checkbox" value="annexure_doc" class="file_send annex_word">Annexure Report (WORD)</label>
								</div>
								<div class="checkbox">
								  <label><input type="checkbox" value="all" class="file_all">All</label>
								</div>
					        </div>
					        <div class="clearfix"></div>

					    </div>
		            </div>
        		</form>
      		</div>

      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary" id="send_audit_email_approved"  data-id="">Send</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<div id="audit_email-modal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Send Audit Report</h4>
      		</div>
      		<div class="modal-body">
      			<div class="alert alert-warning email_alert">
				  	Answers for the template used are incomplete. Kindly log on to DART to answer them. (<a href="#" class="unanswered_question">?</a>)
				</div>
        		<form action="" class="form-horizontal">
            		<input type="checkbox" id="co_auditor" class="dda ca cb" value="co_auditor"> <span class="co_auditor_label">Co-Auditor(s)<br/></span>
            		<input type="checkbox" id="department_head" class="dda dh cb" value="department_head"> <span class="reviewer_label">Department Head</span><br>
            		<input type="checkbox" id="division_head" class="dda cb" value="division_head"> <span class="approver_label">Division Head</span><br>
            		<!-- <input type="checkbox" id="other" class="other cb hidden" id="other" value="other"  disabled=""> Other<br>      -->
		            <div class="form-group">



		              	<div class="col-md-12 pad-0 mb5">
					        <div class="col-md-2 txthidden" hidden>To*: </div>
					        <div class="col-md-10 txthidden" hidden>
					        	<textarea class="form-control inputs txthidden" id="sender_other" hidden></textarea>
					        	<span class="txthidden" hidden style="font-size:10px;">Note: To add (+) more email, please separate with a comma (,)</span>
					        	<br><span class="er-msg">Email should not be empty</span>
					        </div>
					    </div>

					    <div class="col-md-12 pad-0 mb5">
					        <div class="col-md-2 txthidden" hidden>Subject*: </div>
					        <div class="col-md-10 txthidden" hidden>
					        	<input type="text"  name="" class="form-control inputs txthidden" id="subject_other" hidden>
					        	<span class="er-msg">Subject should not be empty</span>
					        </div>
					    </div>

					    <div class="col-md-12 pad-0 mb5">
					        <div class="col-md-2 txthidden" hidden>Message: </div>
					        <div class="col-md-10 txthidden" hidden>
					        	<textarea class="form-control txthidden" id="message_other" height="100" hidden></textarea>
					        </div>
				      	</div>
        		</form>
      		</div>

      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary" id="send_audit_email"  data-id="">Send</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<script	src="<?= base_url();?>asset/js/helper.js"></script>
<script>

	var limit = 10;
	var offset = 0;
	var query = "";

	<?php $this->load->view('layout/listing_function');?>

	$(document).ready(function() {
		get_list("",limit,offset);
  		get_pagination("");
		CKEDITOR.replace('message_other');
		CKEDITOR.replace('message_other_approved');
	})



	$(function(){
	    $(".filter-date-modified").datepicker({ dateFormat: 'yy-mm-dd' });
	    $(".date-reviewed-from").datepicker({ dateFormat: 'yy-mm-dd' });
	    $(".date-reviewed-to").datepicker({ dateFormat: 'yy-mm-dd' });
	    $(".date-approved-from").datepicker({ dateFormat: 'yy-mm-dd' });
	    $(".date-approved-to").datepicker({ dateFormat: 'yy-mm-dd' });
	    $("#datepicker1").datepicker({ dateFormat: 'yy-mm-dd' });
	    $("#datepicker2").datepicker({ dateFormat: 'yy-mm-dd' });
	    $("#datepicker3").datepicker({ dateFormat: 'yy-mm-dd' });
	    $("#datepicker4").datepicker({ dateFormat: 'yy-mm-dd' });
	    $('.submission-date-start').datepicker({ dateFormat: 'yy-mm-dd' });
	    $('.submission-date-end').datepicker({ dateFormat: 'yy-mm-dd' });
	});


	////////////////////////////////////////////FILTER//////////////////////////////////////////
	$(document).on('click','.filter_reset', function($e) {
		$e.preventDefault();
		query = "";
		query_join = "";
		query = "";

        $(".form-control").each(function( index ) {
            $(this).val("")
        });


		$('.filter-audit-status').val($(".filter-audit-status option:first").val());
		$('.auditor-filter').val($(".auditor-filter option:first").val());
		$('.type-of-product-filter').val($(".type-of-product-filter option:first").val());
		$('.co-auditor-filter').val($(".co-auditor-filter option:first").val());
		$('.country-filter').val($(".country-filter option:first").val());
		$('.product-filter').val($(".product-filter option:first").val());
		$('.disposition-filter').val($(".disposition-filter option:first").val());

		get_list("",limit,offset);
  		get_pagination("");
	});


	$(document).on('click','.filter', function(e) {
		e.preventDefault();

		query = "";
		if($('.filter-report-no').val() != ""){
			query += "AND report_no = '"+ $('.filter-report-no').val() + "' ";
		}

		if($('.filter-audited-site').val() != ""){
			query += "AND site_name like '%"+ $('.filter-audited-site').val() + "%' ";
		}

		if($('.filter-audit-status').val() != ""){
			if($('.filter-audit-status').val() == 1){
				query += "AND (tbl_report_listing.status = 1 OR tbl_report_listing.status = 2) ";
			} else {
				query += "AND tbl_report_listing.status = '"+ $('.filter-audit-status').val() + "' ";
			}

		}

		if($('.auditor-filter').val() != ""){
			query += "AND lead_auditor = '"+ $('.auditor-filter').val() + "' ";
		}

		if($('.date-modified-start').val() != "" && $('.date-modified-end').val() != ""){
			query += "AND DATE(date_modified) BETWEEN '" + $('.date-modified-start').val() + "' AND '" + $('.date-modified-end').val() + "' ";
		}

		if($('.audit-date-start').val() != "" && $('.audit-date-end').val() != ""){
			query += "AND first_audit_date >= '" + $('.audit-date-start').val() + "' AND last_audit_date <='" + $('.audit-date-end').val() + "' ";
		}

		if($('.type-of-product-filter').val() != ""){
			query += "AND product_type like '%" + $('.type-of-product-filter').val() + "%' ";
		}

		if($('.co-auditor-filter').val() != ""){
			query += "AND co_auditor like '%" + $('.co-auditor-filter').val() + "%' ";
		}

		if($('.country-filter').val() != ""){
			query += "AND country ='" + $('.country-filter').val() + "' ";
		}

		if($('.product-filter').val() != ""){
			query += "AND product like '%" + $('.product-filter').val() + "%' ";
		}

		if($('.disposition-filter').val() != ""){
			query += "AND disposition like '%" + $('.disposition-filter').val() + "%' ";
		}

		if($('.date-reviewed-from').val() != "" && $('.date-reviewed-to').val() != ""){
			query += "AND DATE(date_review) >= '" + $('.date-reviewed-from').val() + "' AND DATE(date_review) <= '" + $('.date-reviewed-to').val() + "' ";
		}

		if($('.date-approved-from').val() != "" && $('.date-approved-to').val() != ""){
			query += "AND DATE(date_approved) >= '" + $('.date-approved-from').val() + "' AND DATE(date_approved) <= '" + $('.date-approved-to').val() + "' ";
		}

		if($('.lead-time-preparation-start').val() != "" && $('.lead-time-preparation-end').val() != ""){
			query += "AND  lead_time_submission >= " + $('.lead-time-preparation-start').val() + " AND lead_time_submission <= " + $('.lead-time-preparation-end').val() + " ";
		}

		if($('.lead-time-review-start').val() != "" && $('.lead-time-review-end').val() != ""){
			query += "AND  lead_time_review >= " + $('.lead-time-review-start').val() + " AND  lead_time_review <= " + $('.lead-time-review-end').val()+ " ";
		}

		if($('.lead-time-approval-start').val() != "" && $('.lead-time-approval-end').val() != ""){
			query += "AND lead_time_approved >= " + $('.lead-time-approval-start').val() + " AND lead_time_approved <= " + $('.lead-time-approval-end').val() + " ";
		}

		if($('.lead-time-total-start').val() != "" && $('.lead-time-total-end').val() != ""){
			query += "AND lead_time_total >= " + $('.lead-time-total-start').val() + " AND lead_time_total <= " + $('.lead-time-total-end').val()+ " ";
		}

		if($('.gmp-score-from').val() != "" && $('.gmp-score-to').val() != ""){
			query += "AND gmp_score >= " + $('.gmp-score-from').val() + " AND  gmp_score <= " + $('.gmp-score-to').val()+ " ";
		}

		if($('.coverage-from').val() != "" && $('.coverage-to').val() != ""){
			query += "AND gmp_coverage >= " + $('.coverage-from').val() + " AND gmp_coverage <= " + $('.coverage-to').val()+ " ";
		}

		if($('.submission-date-start').val() != "" && $('.submission-date-end').val() != ""){
			query += "AND date_submitted >= '" + $('.submission-date-start').val() + "' AND date_submitted <='" + $('.submission-date-end').val() + "' ";
		}

		if($(this).attr('data-value') == 'search'){

			query = query.substring(3)
			get_list(query,limit,offset);
			get_pagination(query);
			$('.filter-csv').show();
		}

		if($(this).attr('data-value') == 'csv'){
			get_csv();
		}

	});

	function get_list(query,limit,offset){
	    isLoading(true);

	    aJax.post(
	        "<?=base_url('global_controller/getlist_global_group');?>",
	        {
	            'limit':limit,
	            'offset':offset,
	            'query':query,
	        },
	        function(result){
	            var obj = JSON.parse(result);
	            var table_body = new TBody(); //helper.js
	            if(obj.length!=0){
	            	$('.view_report_content').html("");
	            	$('.div-view-audit-report').hide();
	            	$.each(obj, function(key,value){
						table_body.td(value.report_no);
						table_body.td(value.site_name);
						table_body.td(value.auditor_name);
						table_body.td(value.audit_date_formatted);
						table_body.td(moment(value.date_modified).format('LL'));
						table_body.td(gmp.status(value.status));
						table_body.td(value.version);

						var list_data = new UList();
						list_data.set_liclass("li-action");
						list_data.set_ulclass("ul-action");

						//Display delete btn when status is Draft and Co-Auditor's Review
						if(value.status == 0 || value.status == 1 || value.status == 2) {
							if(displayeEmail(value.lead_auditor, value.co_auditor)) {
								list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' report-status="+value.status+" data-toggle='tooltip' data-placement='bottom' title='Email' class='report-send action'><span class='glyphicon glyphicon-envelope'></span></a>");
								list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Trash' class='report-trash action'><span class='glyphicon glyphicon-trash'></span></a>");
							} else {
								<?php if($this->session->userdata('sess_role') == 1) { ?>
									list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Trash' class='report-trash action'><span class='glyphicon glyphicon-trash'></span></a>");
								<?php } ?>
							}
                            list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Generate Report' class='report-generate action'><span class='glyphicon glyphicon-save-file'></span></a>");
							list_data.li("<a href='#' id='"+value.report_id+"'  id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='View' class='report-view action'><span class='glyphicon glyphicon-eye-open'></span></a>");
						}

						if(value.status == 5) {
							<?php if($this->session->userdata('sess_role') == 1) { ?>
								list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Archive' class='report-archive action'><span class='glyphicon glyphicon-briefcase'></span></a>");
							<?php } ?>

							if(displayeEmail(value.lead_auditor, value.co_auditor)) {
								list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Email' class='export-send action'><span class='glyphicon glyphicon-envelope'></span></a>");
/*
								list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Trash' class='report-trash action'><span class='glyphicon glyphicon-trash'></span></a>");
							} else {
								<?php if($this->session->userdata('sess_role') == 1) { ?>
									list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Trash' class='report-trash action'><span class='glyphicon glyphicon-trash'></span></a>");
								<?php } ?>
*/
							}
                            list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Generate Report' class='report-generate action'><span class='glyphicon glyphicon-save-file'></span></a>");
							list_data.li("<a href='#' id='"+value.report_id+"'  id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='View' class='report-view action'><span class='glyphicon glyphicon-eye-open'></span></a>");

						}

						if(value.status != 5 && value.status != 0 && value.status != 1 && value.status != 2) {
							if(displayeEmail(value.lead_auditor, value.co_auditor)) {
								list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' report-status="+value.status+" data-toggle='tooltip' data-placement='bottom' title='Email' class='report-send action'><span class='glyphicon glyphicon-envelope'></span></a>");
/*
								list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Trash' class='report-trash action'><span class='glyphicon glyphicon-trash'></span></a>");

							} else {
								<?php if($this->session->userdata('sess_role') == 1) { ?>
									list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Trash' class='report-trash action'><span class='glyphicon glyphicon-trash'></span></a>");
								<?php } ?>
*/
							}
                            list_data.li("<a href='#' id='"+value.report_id+"' id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Generate Report' class='report-generate action'><span class='glyphicon glyphicon-save-file'></span></a>");
							list_data.li("<a href='#' id='"+value.report_id+"'  id-number='"+value.report_no+"' data-toggle='tooltip' data-placement='bottom' title='View' class='report-view action'><span class='glyphicon glyphicon-eye-open'></span></a>");
						}
						table_body.td(list_data.set());
						table_body.set();
					});

	            } else {
	                table_body.td_norecord(8);
	                table_body.set();
	            }

	            table_body.append(".listdata tbody");
	            isLoading(false);
	        }
	    );
	  }


	  function get_pagination(query){
	    aJax.post(
	        "<?=base_url('global_controller/getlist_global_group');?>",
	        {
	            'limit':99999,
	            'offset':offset,
	            'query':query,
	        },
	        function(result){
	        	var obj = JSON.parse(result);
	        	var result_count = obj.length;
	            var no_of_page = Math.ceil(result_count / limit);
	            var pagination = new Pagination(); //helper.js
	            pagination.set_total_page(no_of_page);
	            pagination.set('.pager_div');
	        }
	    );
	  }

	function displayeEmail(lead, co)
	{
		var ret = false;
		var logged_id = <?= $this->session->userdata('userid');?>;
		if(lead == logged_id){
			ret = true;
		}

		var array = co.split(",").map(Number);
		var found = jQuery.inArray( logged_id, array );
		if(found >= 0){
			ret = true;
		}

		return ret;

	}


	function getFirstWord(str) {
        var spacePosition = str.indexOf(' ');
        if (spacePosition === -1)
            return str;
        else
            return str.substr(0, spacePosition);
    };


	$(document).on('click','.report-view', function($e) {
		isLoading(true);
		$e.preventDefault();
		var id = $(this).attr('id');
		$.ajax({
		  type: 'Post',
		  url:'<?=base_url('export/view');?>',
		  data:{id:id, div : 'remarks-view'},
		}).done( function(data){
		  	$('.view_report_content').html(data);
		  	$('.div-view-audit-report').show();
		  	$('html, body').animate({
	            scrollTop: $("#view-div-panel").offset().top
	        }, 1000);
	        isLoading(false);
		})
	}) ;



	$(document).on('click','.report-generate', function($e) {
		$e.preventDefault();
		var id = $(this).attr('id');
		var report_no = $(this).attr('id-number');
		$('.pdf-picker-id').val(id);
		$('.pdf-picker-no').val(report_no);
		$("#generate_PDF_modal").modal({ show: true, backdrop: "static"});
	}) ;


	$(document).on('click','.generate-export', function($e) {
		$e.preventDefault();
		var id = $(this).attr('id');
		var report_no = $(this).attr('id-number');
		$('.pdf-picker-id').val(id);
		$('.pdf-picker-no').val(report_no);
		$('.btn-generate-export').attr('id-number',report_no);
		$("#export_PDF_modal").modal({ show: true, backdrop: "static"});
	}) ;

	$(document).on('click','.btn-generate-export', function($e) {
		$e.preventDefault();
		var report_no = $(this).attr('id-number');
		var id = $('.pdf-picker-id').val();
		var report_no = $('.pdf-picker-no').val();
		var link = $('.pdf-picker-export').val();
		var text = $('.pdf-picker-export option:selected').text()
		insert_audit_trail("Generate " + report_no + " - " + text);
		window.open("<?=base_url()?>json/export/approved/" + id + "/" + link);
	});

	$(document).on('click','.btn-generate-pdf', function($e) {
		$e.preventDefault();
		var id = $('.pdf-picker-id').val();
		var report_no = $('.pdf-picker-no').val();
		var link = $('.pdf-picker').val();
		var text = $('.pdf-picker option:selected').text()
		insert_audit_trail("Generate " + report_no + " - " + text);
		window.open("<?=base_url('generate')?>/" + link + '?report_id=' + id + "&action=I");
	});

	$(document).on('click','.report-trash', function($e) {
		$e.preventDefault();
		var id = $(this).attr('id');
		var report_no = $(this).attr('id-number');
		var dialog = bootbox.confirm({
			    message: "Are you sure you want to delete this record?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			    	if(result){
                        //remove audit hsitory
                        aJax.post(
                            "<?=base_url('audit_history/remove_audit_history');?>",
                            {
                                'id':id,
                            },
                            function(result){
                            }
                        );


                        //update listing
						$.ajax({
		                  type: 'Post',
		                  url:'<?=base_url();?>c_auditreport/audit_report_status_update',
		                  data:{id:id, status:"-2"},
		                }).done( function(data){
		                    dialog.modal('hide');
	                    	update_audit_report_json();
	                    	insert_audit_trail("Delete Record " + report_no);
	                    	get_list(query, limit, offset, "Report_No");
							get_pagination(query, "Report_No");
		                });
					}
			    }
			});
	}) ;



	$(document).on('click','.report-archive', function($e) {
		$e.preventDefault();

		var id = $(this).attr('id');
		var report_no = $(this).attr('id-number');
		var dialog = bootbox.confirm({
			message:"Are you sure you want to archive this record?",
		 	buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function(result){
			if(result){
				isLoading(true);

				aJax.post(
					'<?=base_url();?>c_auditreport/audit_report_status_update',
					{
						id:id,
						status:"-1"
					},
					function(data){

						dialog.modal('hide');

						aJax.get(
				        	"<?=base_url("api/generate_audit_report_json");?>/" + id,
				        	function(data)
				        	{

				        	}
				        );

				        update_audit_report_json();
	                    insert_audit_trail("Archive Record " + report_no);
						isLoading(false);
						update_listing(id)
						bootbox.alert('<b>Successfully moved to archive folder.</b>', function() {
							get_list("",limit,offset);
							get_pagination("",limit,offset);
							location.reload();
						})


					}
				);

				}
			}
		});
	}) ;



	$(document).on('click','#send_audit_email_approved', function() {
  	// $('#send_audit_email_approved').one( "click", function() {
		var reportid = $(this).attr('data-id');
		var email = $('#sender_other_approved').val();
		var subject_other = $('.subject_other_approved').val();
		var message = CKEDITOR.instances.message_other_approved.getData();
		var files_to_send = "";
		$('.file-err').hide();
		var ctr = 0;
	    $('.inputs-email').each(function(){
	      if($(this).val() == ''){
	        $(this).css('border','1px solid red');
	        $(this).siblings('.er-msg').show();
	        ctr++;
	      }else{
	        $(this).siblings('.er-msg').hide();
	        $(this).css('border','1px solid #ccc');
	      }
	    });

	    var files_cnt = 0;

		$('.file_send').each(function() {
	      if (this.checked==true) {
	        var val = $(this).attr("value");
	        files_to_send += val + ",";
	        files_cnt++;
	      }
	    });

	    if(files_cnt == 0){
	    	$('.file-err').show();
	    }
	    files_to_send = files_to_send.slice(0, -1);
	    if(ctr == 0 && files_cnt != 0){


			var email_error = 0;
			var emails = email.split(",");
			var email_with_error = "";
			$.each(emails, function(x,y){
				if(isEmail(y.trim())){
				} else {
				email_error++;
				email_with_error += "<li>" + y + "</li>";
				}
			});
			
			if(email_error == 0){
				
				isLoading(true);
				$('#approve_send_email').modal('hide');
				aJax.post(
					"<?=base_url("email/sendmail_other_approved");?>",
					{
						sender_other:email,
						subject_other:subject_other,
						message_other:message, 
						report_id:reportid,
						files:files_to_send
					},
					function(data){
						isLoading(false);
						insert_audit_trail("Email "+ reportid +" to " + email);
						$('#sender_other_approved').val("");
						$('.subject_other_approved').val("");
						$('#message_other_approved').html("");
						CKEDITOR.instances.message_other_approved.setData("");

						$('.file_send').each(function() { 
						this.checked = false;                 
						});

						$('.file_all').each(function() { 
						this.checked = false;                 
						});

						bootbox.alert("Generated Reports are successfully sent!", function(){
							location.reload();
						});
					}
				);
			} else {
				bootbox.alert("Error sending report! Invalid email(s) : <ul>" + email_with_error + "</ul>");
			}
	    } 
		
	});

	function isEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}


	$(document).on('click','.export-send', function($e) {

		$e.preventDefault();
		$('#approve_send_email').modal('show');
		var report_id = $(this).attr('id');
		var report_no= $(this).attr('id-number');
		$('#send_audit_email_approved').attr('data-id', report_id);
		$('#send_audit_email').attr('id-number', report_no);
	});

	$(document).on('click','.report-send', function($e) {

		$e.preventDefault();
		isLoading(true);

		$('.email_alert').hide();
		$('#co_auditor').attr('checked', false);
		$('#department_head').attr('checked', false);
		$('#division_head').attr('checked', false);
		$('#other').attr('checked', false);

	  	var report_id = $(this).attr('id');
	  	var report_no= $(this).attr('id-number');
		$('#send_audit_email').attr('data-id', report_id);
		$('#send_audit_email').attr('id-number', report_no);
		var template_id = $(this).attr('data-id');

		//check if answered all question
		var total_unanswerd = 0;

		//start checking status
		aJax.post(
			"<?php echo base_url('Audit_report/check_status'); ?>",
			{
				'report_id':report_id
			},
			function(result){
				var obj = JSON.parse(result);
				$.each(obj, function(x,y){
					$('.reviewer_label').html(y.reviewer);
					$('.approver_label').html(y.approver);
					var report_status = y.status;

					if(report_status == 5){
		          		$('.other').prop("disabled",false);
		          		$('.dda').prop("disabled", true);
		        	} else {

		        		aJax.post(
		        			"<?php echo base_url("c_auditreport/check_auditor_details"); ?>",
		        			{
		        				'report_id':report_id
		        			},
		        			function(result){
		        				if(result == 1){
			    					$('.other').prop("disabled",true);
			          				$('.dda').prop("disabled", false);
			          				$('.dh').prop("disabled", true);
			    				} else {
			    					$('.other').prop("disabled",true);
			          				$('.dda').prop("disabled", false);
			    				}

			    				aJax.post(
									"<?= base_url('c_auditreport/count_total_question');?>",
									{
										report_id: report_id
									},
									function(result){
										var obj	= isJson(result);
										if(obj.count > 0){
											$('.other').prop("disabled",true);
						      				$('.dda').prop("disabled", true);
						      				$('.dh').prop("disabled", true);
						      				$('#co_auditor').prop("disabled", false);
						      				$('.email_alert').show();
						      				$('.unanswered_question').attr("template-id",obj.template_id);
						      				$('.unanswered_question').attr("report-id",report_id);
						      				$('#send_audit_email').prop("disabled",false);
										} else {

                                            //draft view
                                            if(report_status == 0){
                                                $('#co_auditor').prop("disabled",false);
                                                $('#department_head').prop("disabled",false);
                                                $('#division_head').prop("disabled",true);
                                            }

											// co auditor review
											if(report_status  == 1){
												$('#co_auditor').prop("disabled",false);
												$('#department_head').prop("disabled",false);
												$('#division_head').prop("disabled",true);
											}

                                            if(report_status == 2){
                                                $('#co_auditor').prop("disabled",false);
                                                $('#department_head').prop("disabled",false);
                                                $('#division_head').prop("disabled",true);
                                            }

											//submitted to dept head
											if(report_status == 3){
												$('#co_auditor').prop("disabled",true);
												$('#department_head').prop("disabled",false);
												$('#division_head').prop("disabled",true);
											}

											//submitted to divhead
											if(report_status == 4){
												$('#co_auditor').prop("disabled",true);
												$('#department_head').prop("disabled",true);
												$('#division_head').prop("disabled",false);
											}

											//returnd div and dept head
											if(report_status > 5){
												$('#co_auditor').prop("disabled",false);
												$('#department_head').prop("disabled",false);
												$('#division_head').prop("disabled",true);
											}


											$('.email_alert').hide();
											$('#send_audit_email').prop("disabled",false);
										}
										$("#audit_email-modal").modal("show");
										isLoading(false);
									}
								);

		        			}

		        		);
		        	}

                    if(y.co_auditor_count == "0"){
                        $('#co_auditor').hide();
                        $('.co_auditor_label').hide();
                    }

				});
			}
		);

	});



$(document).on('change', '#other', function(e){
	// alert('test');
   e.preventDefault();
     if($('#other').is(':checked') == true){
            $('.txthidden').removeAttr("hidden");
          }
      if($('#other').is(':checked') == false){
      	$('.txthidden').prop("hidden", true);

      }
 });

// email audit report
$(document).on('click', '.unanswered_question', function(e){

	var id = $(this).attr("report-id");
	var template = $(this).attr("template-id");

	aJax.get(
		"<?= base_url('c_auditreport/unanswered_question');?>?report_id=" + id + "&template_id=" + template ,
		function(result){
			var obj = isJson(result);
			var html = "";



			$.each(obj, function(key, value){
				html += "<strong>" +value.element_name+ "</strong>";
				html += "<table class='table' style='width: 100%;'>";
				$.each(value.questions, function(x,y){
					html += "<tr>";
					html += "	<td style='width: 95%;'>" + y.question+ "</td>";
					html += "	<td style='width: 5%;'>";
					if(y.answered == 1){
						html += '<img src="<?= base_url();?>asset/img/checkbox_block.jpg" width="15">';
					} else {
						html +='<img src="<?= base_url();?>asset/img/checkbox_open.jpg" width="15">';
					}
					html += "	</td>";
					html += "</tr>";
				});
				html += "</table>";
			});



			bootbox.alert({
			    message: html,
			    size: 'large',
			    title: "Template-" + template
			});
		}
	);


});


$(document).on('click', '#send_audit_email', function(e){

	isLoading(true);
	$("#audit_email-modal").modal("hide");

	var reportid = $(this).attr('data-id');
	var report_no = $(this).attr('id-number');

	//co-auditors
	if($("#co_auditor").is(":checked")){

		aJax.post(
			"<?=base_url("email/email_to_coauditor");?>",
			{
				report_id:reportid
			},
			function(data){
				isLoading(false);
            	update_listing(reportid);
	            update_audit_report_json();
	            insert_audit_trail("Email "+ report_no +" to Co-Auditor(s)");
            	bootbox.alert("Report successfully sent to Co-Auditor(s).", function(){
            		location.reload();
            	});
			}
		);
	}

	//reviewer
	if($("#department_head").is(":checked")){
		aJax.post(
			"<?=base_url("email/email_to_depthead");?>",
			{
				report_id:reportid
			},
			function(data){
				update_listing(reportid);
            	update_audit_report_json();
            	insert_audit_trail("Email "+ report_no +" to Department Head");
            	bootbox.alert("Report successfully sent to Department Head.", function(){
            		location.reload();
            	});
			}
		);
	}

	//approver
	if($("#division_head").is(":checked")){
		aJax.post(
			"<?=base_url("email/email_to_divhead");?>",
			{
				report_id:reportid
			},
			function(data){
				update_audit_report_json();
            	update_listing(reportid);
            	insert_audit_trail("Email "+ report_no +" to Division Head");
            	bootbox.alert("Report successfully sent to Division Head.", function(){
            		location.reload();
            	});
			}
		);
	}
});





function display_email(report_id){
	$.ajax({
    type: 'Post',
    url:'<?=base_url("c_auditreport/check_if_auditor");?>',
    data:{report_id:report_id},
    }).done(function(data){
    	return data;
    });
}


function get_csv(){
	query = query.substring(3)
	isLoading(true);
	insert_audit_trail("Generate Filter CSV");
	$('.csv_query').val(query);
	$("#csv_form").submit();
	isLoading(false);
	query = "";
}

$('.cb').change(function() {
    $('.cb').prop("checked", false);
    $(this).prop("checked", true);
});

$('.file_all').click(function(){
  var del = 0;
  if(this.checked) {
    $('.file_send').each(function() {
      this.checked = true;
    });
  }else{
    $('.file_send').each(function() {
      this.checked = false;
    });
  }
});


</script>

