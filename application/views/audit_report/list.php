<?php 
  $table = "tbl_report_summary";
  $order_by = "report_id";
?>
<head>
  <!-- ... -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>   
<script type="text/javascript" src="<?php echo base_url();?>asset/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
</head>
<div class="col-md-12 sub_content">
  <span class="glyphicon glyphicon-home"></span> <span class="sub_content_title">Data Maintenance > Audit Report </span>
</div>
<div class="col-md-12"  id="for_search">
<div class="col-md-3">
  <label>Filtered by: </label>
 <select id="audit_report_search">
 <option >-FILTERED BY-</option>
  <option value="date_of_audit">Date of Audit</option>
  <option value="date_of_report_submission">Date of Report Submission</option>
  <option value="date_of_report_issuance">Date of Report Issuance</option>
  <option value="type_of_product">Type of Product</option>
  <option value="disposition">Disposition</option>
  <option value="country">Country</option>
  <option value="state_province">State / Province</option>
  <option value="lead_auditor">Lead Auditor</option>
  <option value="auditor">Auditor</option>
  <option value="product_of_interest">Product-of-Interest</option>
</select>
</div>
 <input type="hidden"  value="" id="secretinput">
<div class="container date_of_audit">
    <div class="row">
        <div class='col-sm-6'>
       
            <div class="form-group ">
            <br>
              <div class='input-group date hidden date_of_audit date_of_report_submission' id='datetimepicker1'>
                  <input type='text' class="form-control" />
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                   <button class="test date_of_report_submission">TEST</button>
                  <div class='input-group date hidden date_of_audit date_of_submission' id='datetimepicker2'>
                  <input type='text' class="form-control" />
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                  <button class="test">TEST</button>
                  </div>
 
              </div>
              </div>
          </div>
        </div>
        
    </div>
</div>
<div class="col-md-9 form-group halo ">
  <!-- div class="container ">
    <div class="row">
     <select id="audit_report_search">
     <option >-FILTERED BY-</option>
      <option value="date_of_audit">Date of Audit</option>
      </select>
    </div>
  </div> -->
</div>
<div class="btn-navigation menu-btn col-md-12 pad-0" >
<!-- 	<button data-type = "Trash" class="inactive btn-min btn btn-default bold" style="display: none;"><span class = "glyphicon glyphicon-trash"></span> TRASH</button> 
  <button data-type = "Active" class="inactive btn-min btn btn-default bold"><span class = "glyphicon glyphicon-ok"></span>Publish</button>
  <button data-type = "Inactive" class="inactive btn-min btn btn-default bold"><span class = "glyphicon glyphicon-remove-circle"></span>Unpublish</button> -->
<!-- 	<button class="add btn-min btn btn-success bold"><span class = "glyphicon glyphicon-plus"></span> ADD USER <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button> -->
	<button class="article_list btn-min btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CLOSE</button>
</div>
<div class="row">
	<div class="col-md-12" id="list_data">
		<div class="table-responsive">
			<table class = "table listdata" style="margin-bottom:0px;">
          <thead>
            <tr>
            <!-- <th style = "width:10px;"><input class = "selectall" type = "checkbox"></th> -->
            <th>Audit Report No.</th>
            <th>Audited Site</th>
            <th>Auditor</th>
            <th>Audit Date</th>
            <th>Status</th>
            <th>Action</th>
          </tr>  
          </thead>
          <tbody>
            
          </tbody>
        
			</table>
		</div>
		<div class="pagination col-md-12"></div>
	</div>
	<div class="col-md-12 content-container" id="form_data">
	</div>
<div class="btn-navigation menu-btn col-md-12" >
	<button class="update btn-min btn btn-success bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button> 
	<button class="save btn-min btn btn-success bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
	<button class="article_list cancel btn-min btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>
</div>
<script>
$(document).ready(function() {
/* Start of list */
  var limit = '10';
  var offset = '1';
  get_list(limit,offset);
  get_pagination(offset,limit);
  $(document).on('click', '.btn-close2', function(){
      get_list(limit,offset);
        get_pagination(offset,limit);
      $('.inactive').hide();
      $('.selectall').attr('checked', false);
        $('#success-modal').modal('hide');    
     });
  $(document).on('change','.page-number', function() {
    var filter = $('.pagination').attr('filter-page');
    var page_number = parseInt($(this).val());
    var auditor_id = $('.val option:selected').attr('id');
      $('.listdata tbody').html('');
      if(filter == 'auditor'){
        get_auditors(limit,page_number,auditor_id);
      }else 
      if(filter == 'date_of_audit'){
        var table = 'tbl_report_summary';
        var date1 =  $("#datetimepicker1").find("input").val();
        var date2 =  $("#datetimepicker2").find("input").val();
        get_date_of_audit(limit,page_number,date1)
      }
      // dito ko tumigil 
      else if(filter == 'date_of_report_submission'){
        var table = 'tbl_report_summary';
        var create_date =  $("#datetimepicker1").find("input").val();
        // var date2 =  $("#datetimepicker2").find("input").val();
        date_of_report_submission(limit,page_number,create_date)
      }
      else{
        get_list(limit,page_number);
      }
      
  });
  $(document).on('click','.next-page', function() {
    var filter = $('.pagination').attr('filter-page');
    var page_number = parseInt($('.page-number').val());
    var auditor_id = $('.val option:selected').attr('id');
    var next = page_number +1;
    if(page_number!=last()){
      if(filter == 'date_of_audit'){
        var table = 'tbl_report_summary';
        var date1 =  $("#datetimepicker1").find("input").val();
        var date2 =  $("#datetimepicker2").find("input").val();
        get_date_of_audit(limit,next,date1,date2,table)
      }
      else if(filter == 'date_of_report_submission'){
        var table = 'tbl_report_summary';
        var create_date =  $("#datetimepicker1").find("input").val();
        // var date2 =  $("#datetimepicker2").find("input").val();
         date_of_report_submission(limit,next,create_date,table)
      }
      else{
        get_list(limit,next);  
      }
       $('.page-number').val(next);
    }
  });
  $(document).on('click','.last-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=last()){
      get_list(limit,last());
      $('.page-number').val($('.page-number option:last').val());
    }
  });
  $(document).on('click','.prev-page', function() {
    var page_number = parseInt($('.page-number').val());
    var prev = page_number -1;
    if(page_number!=first()){
      get_list(limit,prev);
      $('.page-number').val(prev);
    }
  });
  $(document).on('click','.first-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=first()){
      get_list(limit,first());
      $('.page-number').val($('.page-number option:first').val());
    }
  });
  function first(){
    return parseInt($('.page-number option:first').val());
  }
  function last(){
    return parseInt($('.page-number option:last').val());
  }
	function get_list(limit,offset){
    $('.delete').hide();
    $('.inactive').hide();
    $('.selectall').attr('checked', false);
    var table = "<?=$table;?>";
    var order_by = "<?=$order_by;?>";
    $.ajax({
        type: 'Post',
        url:'<?=base_url();?>audit_report/get_report_list',
        data:{limit:limit, offset:offset, table:table, order_by:order_by},
      }).done( function(data){
      	
              var obj = JSON.parse(data);
              console.log(obj);
              var htm = '';
            
          $.each(obj, function(index, row){ 
              if(row.status == 0){
                var status = "Draft";
              }
               else {
                var status = "";
              }
              htm+="<tr>";
              // htm+="<td><input class = 'select' data-id = '"+row.report_id+"' type ='checkbox'></td>";
              htm+="<td><d style='color:#3071a9'>"+row.report_no+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='company'>"+row.name+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='auditor'>"+row.auditor_name+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+row.audit_date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
              htm+="<td><a class='view_report action_list' data-status='' id='"+row.report_id+"' data-id='"+row.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+row.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+row.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+row.report_id+"' data-id='"+row.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
              htm += "<a class='archive action_list' data-status='' id='"+row.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
              htm += "</td>";
              htm+="</tr>";
            });
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
     }); 
  }
  function get_pagination(page_num,limit){
    var table = "<?=$table;?>";
    var order_by = "<?=$order_by;?>";
    $.ajax({
        type: 'Post',
        url:'<?=base_url();?>global_controller/getcount_global',
        data:{limit:limit, table:table, order_by: order_by},
      }).done( function(data){
          var cnt = 0;
          var htm = '';
          htm += '<span class = "glyphicon  first-page"> &#60;&#60;First </span> | <span class = "glyphicon  prev-page"> &#60;Previous </span> | ';
          htm += 'Page <select class="page-number"  style="" disabled> ';
          for(var x =1; x<=data; x++){
            htm += "<option value='"+x+"'>"+x+"</option>";
            cnt++;
          }
          htm += '</select> of '+data+' | ';
          htm += '<span class = "glyphicon  next-page"> &#62;Next</span> | <span class = "glyphicon last-page"> Last&#62;&#62; </span>';
          $('.pagination').html('');
          // if(cnt > 1){
          $('.pagination').html(htm);
          // }
     }); 
  }
/* End of list */
/* Start of Add */
$(document).on('click', '.add', function(){
$(this).hide();
$('.save').show();
$('.update').hide();
$('.article_list').show();
$.ajax({
    type: 'Post',
    url:'<?=base_url();?>global_controller/action_global',
    data:{id:'', module:'audit_report', type:'add'},
}).done( function(data){
	$('.content-container').html(data);
        $('#success-modal').modal('hide');
                  	// bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
        // get_list(limit,offset);
        // get_pagination('1',limit);
});
})
$(document).on('click', '.article_list', function(){
$('.article_list').hide();
$('.save').hide();
$('.update').hide();
$('.add').show();
$('#form_data').html('');
})
/* End of Add */
/* Start of update */
 
$(document).on('click', '.view_report', function(){
var id = $(this).attr('id');
var template_id = $(this).attr('data-id');
// $('.update').show();
// $('.add').show();
// $('.save').hide();
// $('.cancel').show();
$.ajax({
    type: 'Post',
    url:'<?=base_url();?>global_controller/action_global',
    data:{id:id, template_id:template_id, module:'audit_report', type:'edit_tek'},
}).done( function(data){
  $('.content-container').html(data);
        $('#success-modal').modal('hide');
})
});
$(document).on('click', '.article_list', function(){
$(this).hide();
$('.save').hide();
$('.update').hide();
$('.add').show();
get_list(limit,offset);
get_pagination('1',limit);
})
/* End of update */
/* start of delete */
$('.selectall').click(function(){
     if(this.checked) { 
            $('.select').each(function() { 
                this.checked = true;  
                $('.delete').show();
                $('.inactive').show();           
            });
        }else{
            $('.select').each(function() { 
              $('.delete').hide();
                $('.inactive').hide();
                this.checked = false;                 
            });         
        }
});
$(document).on('click', '.select', function(){
    var x = 0;
        $('.select').each(function() {                
                if (this.checked==true) { x++; } 
                if (x > 0 ) {
                   $('.delete').show();
                   $('.inactive').show(); 
                } else {
                $('.delete').hide();
                $('.inactive').hide();
                $('.selectall').attr('checked', true);
              	}
        });
});
$(document).on('click', '.inactive', function(){
    var x = 0;
    var data_type = $(this).attr('data-type');
    $('.select').each(function() {                
          if (this.checked==true) {  x++;   } 
          if (x > 0 ) {
            $('.modal-footer .btn-close').hide();
            $('.msg').html('<p>Are you sure you want to "'+data_type+'" selected record? <button data-type = "'+data_type+'" class = "btn-remove"> Yes </button><button class = "btn-close2"> No </button></p>');
            $('#success-modal').modal('show');
          }
      });
});
$(document).on('click', '.delete_data', function(){
    var x = 0;
    var data_type = $(this).attr('id');
            $('.modal-footer .btn-close').hide();
            $('.msg').html('<p>Are you sure you want to delete this record? <button data-type = "'+data_type+'" class = "btn-remove-data"> Yes </button><button class = "btn-close2"> No </button></p>');
            $('#success-modal').modal('show');
});
$(document).on('click', '.btn-remove-data', function(){
    var x = 0;
            var table = "<?=$table;?>";
            var order_by = "<?=$order_by;?>";
            var id = $(this).attr('data-type');
            var type = '-2';     
              $.ajax({
                  type: 'Post',
                  url:'<?=base_url();?>global_controller/inactive_global_update',
                  data:{id:id,type:type, table:table, order_by: order_by},
                }).done( function(data){
                  $('#success-modal').modal('hide');
                    // bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
                    get_list(limit,offset);
                    get_pagination('1',limit);
                  });
             x++;
});
$(document).on('click', '.btn-remove', function(){
    var x = 0;
    var type = $(this).attr('data-type');
    if (type == 'Trash') {
      var type = '-2';
    }
    if (type == 'Inactive') {
      var type = '0';
    }
    if (type == 'Active') {
      var type = '1';
    }
  
      $('.select').each(function() {                
          if (this.checked==true) {
            
            var table = "<?=$table;?>";
            var order_by = "<?=$order_by;?>";
            var id = $(this).attr('data-id');
                        
              $.ajax({
                  type: 'Post',
                  url:'<?=base_url();?>global_controller/inactive_global_update',
                  data:{id:id,type:type, table:table, order_by: order_by},
                }).done( function(data){
             
                  $('#success-modal').modal('hide');
                    // bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
                    get_list(limit,offset);
                    get_pagination('1',limit);
                  });
             x++;
          } 
        
      });
});
/* End of delete */
// generate report
$(document).on('click', '.generate', function(){
    // var x = 0;
    var id = $(this).attr('id');
    $('#generate_report').attr('data-id', id);
            // $('.modal-footer .btn-close').hide();
            // $('.msg').html('<p>TEST</p>');
            $('#generate_modal').modal('show');
});
 
});
// modal show
$(document).on('click', '.audit_email', function(){
 $("#audit_email-modal").modal("show");
  var report_id = $(this).attr('id');
  $('#send_audit_email').attr('data-id', report_id);
  var template_id = $(this).attr('data-id');
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/check_status',
      type: 'POST',
      data: {
              'report_id':report_id
             },
    success: function(data) {
      var obj = JSON.parse(data);
      $.each(obj, function(x,y){
        if(y.status == 4){
          $('.other').prop("disabled",false);
        }
        else{
         
        }
      });
      
      
    }
  })
});
$(document).on('change', '#other', function(e){
   e.preventDefault();
     if($('#other').is(':checked') == true){
            $('.txthidden').removeAttr("hidden");
          }
 });
// email audit report
$(document).on('click', '#send_audit_email', function(e){
   e.preventDefault();
  var id = $(this).attr('data-id');
var audit_email = $('input[name=audit_email]:checked').map(function()
            {
                return $(this).val();
            }).get();
if(audit_email == 'co_auditor'){
   
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_lead_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(lead_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_co_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(co_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>/smtp/Send/mail_co_auditor',
      type: 'POST',
      data: {
              'lead_auditor':lead_auditor,
              'co_auditor':co_auditor
             },
    success: function(data) {
      var status = 1;
      $.ajax({
        url:'<?php echo base_url(); ?>Audit_report_link/add_status',
        type: 'POST',
        data: {
                'id':id,
                'status':status
               },
   success: function(data) {
    }
        })
    }
  })
    }
        })
  }
})
}
    if(audit_email == 'department_head'){
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_lead_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(lead_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_department_head',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(department_head) {
      $.ajax({
      url:'<?php echo base_url(); ?>/smtp/Send/mail_department_head',
      type: 'POST',
      data: {
              'lead_auditor':lead_auditor,
              'department_head':department_head
             },
    success: function(data) {
    }
        })
    }
        })
    }
        })
    }
     if(audit_email == 'division_head'){
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_lead_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(lead_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_division_head',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(division_head) {
      $.ajax({
      url:'<?php echo base_url(); ?>/smtp/Send/mail_division_head',
      type: 'POST',
      data: {
              'lead_auditor':lead_auditor,
              'division_head':division_head
             },
    success: function(data) {
       var status = 3;
      $.ajax({
        url:'<?php echo base_url(); ?>Audit_report_link/add_status',
        type: 'POST',
        data: {
                'id':id,
                'status':status
               },
   success: function(data) {
    }
        })
    }
        })
    }
        })
    }
    })
    }
    if(audit_email == 'co_auditor,department_head'){
      $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_lead_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(lead_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_department_head',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(department_head) {
       $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_co_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(co_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>/smtp/Send/coauditor_departmenthead',
      type: 'POST',
      data: {
              'co_auditor':co_auditor,
              'lead_auditor':lead_auditor,
              'department_head':department_head
             },
    success: function(data) {
    }
        })
    }
        })
    }
        })
    }
        })
    }
     if(audit_email == 'co_auditor,division_head'){
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_lead_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(lead_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_division_head',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(division_head) {
       $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_co_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(co_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>/smtp/Send/coauditor_divisionhead',
      type: 'POST',
      data: {
              'co_auditor':co_auditor,
              'lead_auditor':lead_auditor,
              'division_head':division_head
             },
    success: function(data) {
    }
        })
    }
        })
    }
        })
    }
        })
    }
      if(audit_email == 'department_head,division_head'){
 
   $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_lead_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(lead_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_division_head',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(division_head) {
       $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_department_head',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(department_head) {
      $.ajax({
      url:'<?php echo base_url(); ?>/smtp/Send/departmenthead_divisionhead',
      type: 'POST',
      data: {
              'lead_auditor':lead_auditor,
              'department_head':department_head,
              'division_head':division_head
             },
    success: function(data) {
    }
        })
    }
        })
    }
        })
    }
        })
    }
      if(audit_email == 'co_auditor,department_head,division_head'){
  
   $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_lead_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(lead_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_co_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(co_auditor) {
      $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_division_head',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(division_head) {
       $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_department_head',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(department_head) {
      $.ajax({
      url:'<?php echo base_url(); ?>/smtp/Send/sendmail_all',
      type: 'POST',
      data: {
              'lead_auditor':lead_auditor,
              'co_auditor':co_auditor,
              'department_head':department_head,
              'division_head':division_head
             },
    success: function(data) {
    }
        })
    }
        })
    }
        })
    }
        })
    }
        })
    }
    if(audit_email == 'other'){
   // alert('other');
  var sender_other = $('#sender_other').val();
  var subject_other = $('#subject_other').val();
  var message_other = $('#message_other').val();
 
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_email_lead_auditor',
      type: 'POST',
      data: {
              'id':id
             },
    success: function(lead_auditor) {
       $.ajax({
      url:'<?php echo base_url(); ?>/smtp/Send/sendmail_other',
      type: 'POST',
      data: {
              'lead_auditor':lead_auditor,
              'sender_other':sender_other,
              'subject_other':subject_other,
              'message_other':message_other
             },
    success: function(data) {
    }
        })
      
      }
  })
}
 // window.location.reload();
});
$(document).on('change', '#audit_report_search', function(e){
   e.preventDefault();
    search_audit = $('#audit_report_search').val();
    $('#secretinput').val(search_audit);
    if(search_audit == 'date_of_audit' ){
     $('.date_of_audit').removeClass("hidden");
     $('.pagination').attr('filter-page','date_of_audit');
      
    }
    if(search_audit == 'date_of_report_submission' ){
      $('.date_of_report_submission').removeClass("hidden");
      
    }
    if(search_audit == 'date_of_report_issuance' ){
      $('.date_of_report_issuance').removeClass("hidden");
    }
    if(search_audit == 'type_of_product' ){
      // $('.type_of_product').removeClass("hidden");
      $('.type_of_product').addClass("search_input");
      var table = 'tbl_classification';
      $.ajax({
          url:'<?php echo base_url(); ?>Audit_report/audit_search',
          type: 'POST',
          data: {
                  'table':table
                 },
        success: function(data) {
            var obj = JSON.parse(data);
            var htm = '';
            htm+= '<div class="col-md-5 form-group">';
            htm+= '<div class="container">';
            htm+= '<br>';
            htm+= '<select class="val">';
          $.each(obj, function(index, row){ 
            htm+= '<option data-id="tbl_classification" id="classification_name" >'+row.classification_name+' </option>';
          });
            htm+= '</select>';
            htm+='<button class="search_submit">test</button>';
            htm+= '</div>';
            htm+= '</div>';
          $('.halo').append(htm);
        }
            })
    }
    if(search_audit == 'disposition' ){
      // $('.disposition').removeClass("hidden");
      $('.disposition').addClass("search_input");
        var table = 'tbl_disposition';
      $.ajax({
          url:'<?php echo base_url(); ?>Audit_report/audit_search',
          type: 'POST',
          data: {
                  'table':table
                 },
        success: function(data) {
            var obj = JSON.parse(data);
            var htm = '';
            htm+= '<div class="col-md-5 form-group">';
            htm+= '<div class="container">';
             htm+= '<br>';
            htm+= '<select class="val">';
          $.each(obj, function(index, row){ 
            htm+= '<option data-id="tbl_disposition" id="disposition_name" >'+row.disposition_name+' </option>';
          });
            htm+= '</select>';
            htm+='<button class="search_submit">test</button>';
            htm+= '</div>';
            htm+= '</div>';
          $('.halo').append(htm);
        }
            })
    }
    if(search_audit == 'country' ){
     // $('.country').removeClass("hidden");
     $('.country').addClass("search_input");
        var table = 'tbl_country';
      $.ajax({
          url:'<?php echo base_url(); ?>Audit_report/audit_search',
          type: 'POST',
          data: {
                  'table':table
                 },
        success: function(data) {
            var obj = JSON.parse(data);
            var htm = '';
            htm+= '<div class="col-md-5 form-group">';
            htm+= '<div class="container">';
             htm+= '<br>';
            htm+= '<select class="val">';
          $.each(obj, function(index, row){ 
            htm+= '<option data-id="tbl_country" id="country_id" >'+row.country_id+' </option>';
          });
            htm+= '</select>';
            htm+='<button class="search_submit">test</button>';
            htm+= '</div>';
            htm+= '</div>';
          $('.halo').append(htm);
        }
            })
    }
    if(search_audit == 'state_province' ){
      // $('.state_province').removeClass("hidden");
      $('.state_province').addClass("search_input");
          var table = 'tbl_province';
      $.ajax({
          url:'<?php echo base_url(); ?>Audit_report/audit_search',
          type: 'POST',
          data: {
                  'table':table
                 },
        success: function(data) {
            var obj = JSON.parse(data);
            var htm = '';
            htm+= '<div class="col-md-5 form-group">';
            htm+= '<div class="container">';
             htm+= '<br>';
            htm+= '<select class="val">';
          $.each(obj, function(index, row){ 
            htm+= '<option data-id="tbl_province" id="province_name" >'+row.province_name+' </option>';
          });
            htm+= '</select>';
            htm+='<button class="search_submit">test</button>';
            htm+= '</div>';
            htm+= '</div>';
          $('.halo').append(htm);
        }
            })
    }
    if(search_audit == 'lead_auditor' ){
        // $('.lead_auditor').removeClass("hidden");
        $('.pagination').attr('filter-page','lead_auditor');
        $('.lead_auditor').addClass("search_input");
          var table = 'tbl_auditor_info';
      $.ajax({
          url:'<?php echo base_url(); ?>Audit_report/audit_search',
          type: 'POST',
          data: {
                  'table':table
                 },
        success: function(data) {
            var obj = JSON.parse(data);
            var htm = '';
            htm+= '<div class="col-md-5 form-group">';
            htm+= '<div class="container">';
             htm+= '<br>';
            htm+= '<select class="val">';
          $.each(obj, function(index, row){ 
             htm+= '<option data-id="tbl_auditor_info" id="'+row.auditor_id+'" >'+row.fname+' '+row.lname+'</option>';
              // get_search_auditor(row.auditor_id);
          });
            htm+= '</select>';
            htm+='<button class="search_submit2">test</button>';
            htm+= '</div>';
            htm+= '</div>';
          $('.halo').append(htm);
        }
            })
    }
    if(search_audit == 'auditor' ){
      // $('.auditor').removeClass("hidden");
      $('.auditor').addClass("search_input");
          var table = 'tbl_auditor_info';
      $.ajax({
          url:'<?php echo base_url(); ?>Audit_report/audit_search',
          type: 'POST',
          data: {
                  'table':table
                 },
        success: function(data) {
            var obj = JSON.parse(data);
            var htm = '';
            htm+= '<div class="col-md-5 form-group">';
            htm+= '<div class="container">';
             htm+= '<br>';
            htm+= '<select class="val">';
          $.each(obj, function(index, row){ 
            htm+= '<option data-id="tbl_co_auditors" id="'+row.auditor_id+'" >'+row.fname+' '+row.lname+'</option>';
          });
            htm+= '</select>';
            htm+='<button class="search_submit3">test</button>';
            htm+= '</div>';
            htm+= '</div>';
          $('.halo').append(htm);
        }
            })
    }
    if(search_audit == 'product_of_interest' ){
      // $('.product_of_interest').removeClass("hidden");
      $('.product_of_interest').addClass("search_input");
          var table = 'tbl_product';
      $.ajax({
          url:'<?php echo base_url(); ?>Audit_report/audit_search',
          type: 'POST',
          data: {
                  'table':table
                 },
        success: function(data) {
            var obj = JSON.parse(data);
            var htm = '';
            htm+= '<div class="col-md-5 form-group">';
            htm+= '<div class="container">';
             htm+= '<br>';
            htm+= '<select class="val" >';
          $.each(obj, function(index, row){ 
            htm+= '<option data-id="tbl_product" id="product_name">'+row.product_name+' </option>';
          });
            htm+= '</select>';
            htm+='<button class="search_submit">test</button>';
            htm+= '</div>';
            htm+= '</div>';
          $('.halo').append(htm);
        }
            })
    }
 });
function get_search_pagination(page_num,id,param){
    var func = '';
    if(param == 'classification'){
      func = 'getcount_search_classification';

    }else 
    if(param == 'disposition'){
      func = 'getcount_search_disposition';
    }
    if(param == 'country'){
      func = 'getcount_search_country';
    }
    if(param == 'province'){
      func = 'getcount_search_province';
    }
    if(param == 'product'){
      func = 'getcount_search_product';
    }
    if(param == 'lead_auditor'){
      func = 'getcount_search_lead_auditor';
    }
    if(param == 'co_auditor'){
      func = 'getcount_search_co_auditor';
    }
    if(param == 'search_date_of_audit'){
      func = 'getcount_search_date_of_audit';
    }
    if(param == 'search_date_of_report_submission'){
      func = 'getcount_search_date_of_report_submission';
    }
    
    var table = '"<?=$table;?>"';
    var order_by = "<?=$order_by;?>";
  var date1 =  $("#datetimepicker1").find("input").val();
  var date2 =  $("#datetimepicker2").find("input").val();
    var limit ='10';
    $.ajax({
        type: 'Post',
        url:'<?=base_url();?>Audit_report/'+func,
        data:{limit:limit, table:table, order_by: order_by, id:id, date1:date1, date2:date2},
      }).done( function(data){
          var cnt = 0;
          var htm = '';
          htm += '<span class = "glyphicon  first-page"> &#60;&#60;First </span> | <span class = "glyphicon  prev-page"> &#60;Previous </span> | ';
          htm += 'Page <select class="page-number"  style="" disabled> ';
          for(var x =1; x<=data; x++){
            htm += "<option value='"+x+"'>"+x+"</option>";
            cnt++;
          }
          htm += '</select> of '+data+' | ';
          htm += '<span class = "glyphicon  next-page"> &#62;Next</span> | <span class = "glyphicon last-page"> Last&#62;&#62; </span>';
          $('.pagination').html('');
          // if(cnt > 1){
          $('.pagination').html(htm);
          // }
     }); 
  }
 
// button search
$(document).on('click', '.search_submit', function(e){
    e.preventDefault();
  
 var search_input = $('.val').val();
 var table = $('.val option:selected').attr('data-id');
 var field_name = $('.val option:selected').attr('id');
    $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/audit_search_keyword',
      type: 'POST',
      data: {
              'table':table,
              'field_name':field_name,
              'search_input':search_input
             },
    success: function(data) {
      var obj = JSON.parse(data);
        $.each(obj, function(x,y){
          if (field_name == 'classification_name') {
            // var classification_id = y.classification_id;
            get_search_classification(y.classification_id);
                get_search_pagination('1', y.classification_id,'classification');
          }
          if(field_name == 'disposition_name'){
            get_search_disposition(y.disposition_id);
                get_search_pagination('1', y.disposition_id,'disposition');
          }
          if(field_name == 'country_id'){
            get_search_country(y.id);
                get_search_pagination('1', y.id,'country');
          }
          if(field_name == 'province_name'){
            get_search_province(y.province_id);
                get_search_pagination('1', y.province_id,'province');
          } 
          if(field_name == 'product_name'){
            get_search_product_interest(y.product_id);
                get_search_pagination('1', y.product_id,'product');
          }
          
        });
    }
        })
 
 });
//lead auditor pinag hiwalay ko para di ako malito naka rekta to
$(document).on('click', '.search_submit2', function(e){
    e.preventDefault();
  
  var limit = '10';
  var offset = '1';
 // var search_input = $('.val').val();
 var table = $('.val option:selected').attr('data-id');
 var auditor_id = $('.val option:selected').attr('id');

 get_auditors(limit,offset,auditor_id);

 });

function get_auditors(limit,offset,auditor_id){
      $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/audit_search_auditor',
      type: 'POST',
      data: {
              'auditor_id':auditor_id,
              'limit':limit,
              'offset':offset
             },
    success: function(data) {
      
      var obj = JSON.parse(data);
      get_search_pagination('1', auditor_id, 'lead_auditor');
                var htm = '';
            if(obj.length != 0){
              $.each(obj, function(x,y){
                if(y.status == 0){
                var status = "Draft";
              }
               else {
                var status = "";
              }
              htm+='wew';
              htm+="<tr>";
              htm+="<td><d style='color:#3071a9'>"+y.report_no+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='company'>"+y.name+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='auditor'>"+y.auditor_name+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+y.audit_date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
              htm+="<td><a class='view_report action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+y.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+y.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
              htm += "<a class='archive action_list' data-status='' id='"+y.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
              htm += "</td>";
              htm+="</tr>";
            });
            }else{
              htm+='No Record Found!';
            }
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
         
    }
        })
}


//co auditor
$(document).on('click', '.search_submit3', function(e){
    e.preventDefault();
  
 // var search_input = $('.val').val();
 var table = $('.val option:selected').attr('data-id');
 var auditor_id = $('.val option:selected').attr('id');
  var limit = '10';
  var offset = '1';
    $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_search_co_auditor',
      type: 'POST',
      data: {
              'auditor_id':auditor_id,
              'limit':limit,
              'offset':offset
             },
    success: function(data) {
      var obj = JSON.parse(data);
      get_search_pagination('1', auditor_id, 'co_auditor');
                var htm = '';
            if(obj.length != 0){
              $.each(obj, function(x,y){
                if(y.status == 0){
                var status = "Draft";
              }
               else {
                var status = "";
              }
              htm+='wew';
              htm+="<tr>";
              htm+="<td><d style='color:#3071a9'>"+y.report_no+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='company'>"+y.name+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='auditor'>"+y.auditor_name+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+y.audit_date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
              htm+="<td><a class='view_report action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+y.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+y.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
              htm += "<a class='archive action_list' data-status='' id='"+y.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
              htm += "</td>";
              htm+="</tr>";
            });
            }else{
              htm+='No Record Found!';
            }
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
        
    }
        })
 
 });
//start of calling functions for search
function get_search_classification(classification_id){
  var limit = '10';
  var offset = '1';
  classification_id = classification_id;
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_search_classification',
      type: 'POST',
      data: {
              'classification_id':classification_id,
              'limit':limit,
              'offset':offset
             },
    success: function(data) {
      var obj = JSON.parse(data);
                var htm = '';
            if(obj.length != 0){
              $.each(obj, function(x,y){
                if(y.status == 0){
                var status = "Draft";
              }
               else {
                var status = "";
              }
              htm+='wew';
              htm+="<tr>";
              htm+="<td><d style='color:#3071a9'>"+y.report_no+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='company'>"+y.name+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='auditor'>"+y.auditor_name+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+y.audit_date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
              htm+="<td><a class='view_report action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+y.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+y.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
              htm += "<a class='archive action_list' data-status='' id='"+y.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
              htm += "</td>";
              htm+="</tr>";
             
            });
            }else{
              htm+='No Record Found!';
            }
           
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
    }
        })
}
function get_search_disposition(disposition_id){
  disposition_id = disposition_id;
    var limit = '10';
  var offset = '1';
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_search_disposition',
      type: 'POST',
      data: {
              'disposition_id':disposition_id,
               'limit':limit,
              'offset':offset
             },
    success: function(data) {
      // alert(data);
      var obj = JSON.parse(data);
                var htm = '';
                // alert(obj.length);
            if(obj.length != 0){
              $.each(obj, function(x,y){
                if(y.status == 0){
                var status = "Draft";
              }
               else {
                var status = "";
              }
              // alert(obj+' 1212');
              // htm+='wew';
              htm+="<tr>";
              htm+="<td><d style='color:#3071a9'>"+y.report_no+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='company'>"+y.name+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='auditor'>"+y.auditor_name+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+y.audit_date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
              htm+="<td><a class='view_report action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+y.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+y.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
              htm += "<a class='archive action_list' data-status='' id='"+y.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
              htm += "</td>";
              htm+="</tr>";
            });
            }else{
              htm+='No Record Found!';
            }
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
    }
        })
}
function get_search_country(id){
  var limit = '10';
  var offset = '1';
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_search_country',
      type: 'POST',
      data: {
              'id':id,
              'limit':limit,
              'offset':offset
             },
    success: function(data) {
      var obj = JSON.parse(data);
                var htm = '';
            if(obj.length != 0){
              $.each(obj, function(x,y){
                if(y.status == 0){
                var status = "Draft";
              }
               else {
                var status = "";
              }
              htm+="<tr>";
              htm+="<td><d style='color:#3071a9'>"+y.report_no+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='company'>"+y.name+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='auditor'>"+y.auditor_name+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+y.audit_date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
              htm+="<td><a class='view_report action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+y.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+y.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
              htm += "<a class='archive action_list' data-status='' id='"+y.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
              htm += "</td>";
              htm+="</tr>";
            });
            }else{
              htm+='No Record Found!';
            }
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
    }
        })
}
function get_search_province(province_id){
   var limit = '10';
  var offset = '1';
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_search_province',
      type: 'POST',
      data: {
              'province_id':province_id,
              'limit':limit,
              'offset':offset
       
             },
    success: function(data) {
      var obj = JSON.parse(data);
                var htm = '';
            if(obj.length != 0){
              $.each(obj, function(x,y){
                if(y.status == 0){
                var status = "Draft";
              }
               else {
                var status = "";
              }
              htm+='wew';
              htm+="<tr>";
              htm+="<td><d style='color:#3071a9'>"+y.report_no+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='company'>"+y.name+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='auditor'>"+y.auditor_name+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+y.audit_date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
              htm+="<td><a class='view_report action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+y.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+y.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
              htm += "<a class='archive action_list' data-status='' id='"+y.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
              htm += "</td>";
              htm+="</tr>";
            });
            }else{
              htm+='No Record Found!';
            }
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
    }
        })
}
function get_search_co_auditor(auditor_id){
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_search_co_auditor',
      type: 'POST',
      data: {
              'auditor_id':auditor_id
             },
    success: function(data) {
      var obj = JSON.parse(data);
                var htm = '';
            if(obj.length != 0){
              $.each(obj, function(x,y){
                if(y.status == 0){
                var status = "Draft";
              }
               else {
                var status = "";
              }
              htm+='wew';
              htm+="<tr>";
              htm+="<td><d style='color:#3071a9'>"+y.report_no+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='company'>"+y.name+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='auditor'>"+y.auditor_name+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+y.audit_date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
              htm+="<td><a class='view_report action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+y.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+y.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
              htm += "<a class='archive action_list' data-status='' id='"+y.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
              htm += "</td>";
              htm+="</tr>";
            });
            }else{
              htm+='No Record Found!';
            }
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
    }
        })
}
function get_search_product_interest(product_id){
  var limit = '10';
  var offset = '1';
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_search_product_interest',
      type: 'POST',
      data: {
              'product_id':product_id,
              'limit':limit,
              'offset':offset
       
             },
    success: function(data) {
      var obj = JSON.parse(data);
                var htm = '';
            if(obj.length != 0){
              $.each(obj, function(x,y){
                if(y.status == 0){
                var status = "Draft";
              }
               else {
                var status = "";
              }
              htm+='wew';
              htm+="<tr>";
              htm+="<td><d style='color:#3071a9'>"+y.report_no+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='company'>"+y.name+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='auditor'>"+y.auditor_name+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+y.audit_date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
              htm+="<td><a class='view_report action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+y.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+y.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
              htm += "<a class='archive action_list' data-status='' id='"+y.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
              htm += "</td>";
              htm+="</tr>";
            });
            }else{
              htm+='No Record Found!';
            }
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
    }
        })
}
// date picker search
 $(function () {
    $('#datetimepicker1').datetimepicker({
      format: 'YYYY-MM-DD'
    });
    $('#datetimepicker2').datetimepicker({
      format: 'YYYY-MM-DD'
    });
  });

function get_date_of_audit(limit,offset,date1,date2,table){
  $.ajax({
      url:'<?php echo base_url(); ?>Audit_report/get_search_date_of_audit',
      type: 'POST',
      data: {
              'table':table,
              'date1':date1,
              'date2':date2,
              'limit':limit,
              'offset':offset
             },
    success: function(data) {
    
      var obj = JSON.parse(data);
                var htm = '';
            if(obj.length != 0){
              $.each(obj, function(x,y){
                if(y.status == 0){
                var status = "Draft";
              }
               else {
                var status = "";
              }
              htm+="<tr>";
              htm+="<td><d style='color:#3071a9'>"+y.report_no+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='company'>"+y.name+"</d></td>";
              htm+="<td><d style='color:#3071a9' class='auditor'>"+y.auditor_name+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+y.audit_date+"</d></td>";
              htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
              htm+="<td><a class='view_report action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+y.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+y.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
              htm += "<a class='archive action_list' data-status='' id='"+y.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
              htm += "</td>";
              htm+="</tr>";
            });
            }else{
              htm+='No Record Found!';
            }
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
    
    }
        })
}


$('.test').click(function(){
  if($('#secretinput').val() == 'date_of_audit'){
  var table = 'tbl_report_summary';
  var date1 =  $("#datetimepicker1").find("input").val();
  var date2 =  $("#datetimepicker2").find("input").val();
  var limit = '10';
  var offset = '1';
  get_date_of_audit(limit,offset,date1,date2,table);

  get_search_pagination(offset, 'tbl_report_summary', 'search_date_of_audit');
  }
  if($('#secretinput').val() == 'date_of_report_submission'){
  var table = 'tbl_report_summary';
  var create_date =  $("#datetimepicker1").find("input").val();
  // var date2 =  $("#datetimepicker2").find("input").val();
  var limit = '10';
  var offset = '1';
  date_of_report_submission(limit,offset,create_date,table);
  get_search_pagination(offset ,'tbl_report_summary', 'search_date_of_report_submission');
  }
  function date_of_report_submission(limit,offset,create_date,table){
        $.ajax({
            url:'<?php echo base_url(); ?>Audit_report/date_of_report_submission',
            type: 'POST',
            data: {
                    'table':table,
                    'create_date':create_date,
                    'limit':limit,
                    'offset':offset
                  
                   },
          success: function(data) {
         
            var obj = JSON.parse(data);
                      var htm = '';
                  if(obj.length != 0){
                    $.each(obj, function(x,y){
                      if(y.status == 0){
                      var status = "Draft";
                    }
                     else {
                      var status = "";
                    }
                    htm+="<tr>";
                    htm+="<td><d style='color:#3071a9'>"+y.report_no+"</d></td>";
                    htm+="<td><d style='color:#3071a9' class='company'>"+y.name+"</d></td>";
                    htm+="<td><d style='color:#3071a9' class='auditor'>"+y.auditor_name+"</d></td>";
                    htm+="<td><d style='color:#3071a9'>"+y.audit_date+"</d></td>";
                    htm+="<td><d style='color:#3071a9'>"+status+"</d></td>";
                    htm+="<td><a class='view_report action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='View Report'><span class='glyphicon glyphicon-eye-open'></span></a><a class='delete_data action_list' data-status='' id='"+y.report_id+"' title='Delete Report'> <span class='glyphicon glyphicon-trash'></span></a><a class='audit_email action_list' data-status='' id='"+y.report_id+"' title='Send Report'> <span class='glyphicon glyphicon-envelope'></span></a><a class='generate action_list' data-status='' id='"+y.report_id+"' data-id='"+y.template_id+"' title='Generate Report'> <span class='glyphicon glyphicon-circle-arrow-down'></span></a>";
                    htm += "<a class='archive action_list' data-status='' id='"+y.report_id+"'  title='Archive Report'> <span class='glyphicon glyphicon-briefcase'></span></a>";
                    htm += "</td>";
                    htm+="</tr>";
                  });
                  }else{
                    htm+='No Record Found!';
                  }
                $('.listdata tbody').html('');
                $('.listdata tbody').html(htm);
       
          }
              })
}
  
});
</script>
<!-- modal email send -->
<div class="modal fade" id="audit_email-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style = "overflow:hidden">
      <div class="modal-header modal-head">
        <button type="button" class="close modal-close btn-close" style = "color:#fff !important; opacity:10" data-dismiss="modal" aria-label="Close"><span style = "color:#fff !important; opacity:10"  aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Audit Report Send Email</h4>
      </div>
      <div class="modal-body"> 
        <form action="" class="form-horizontal">
            <input type="checkbox" name="audit_email" class="" value="co_auditor"> Co-Auditor<br>
            <input type="checkbox" name="audit_email" class="" value="department_head"> Department Head<br>
            <input type="checkbox" name="audit_email" class="" value="division_head"> Division Head<br>
            <input type="checkbox" name="audit_email" class="other" id="other" value="other" disabled> Other<br>
            
       
            <div class="form-group">
             <div class="col-sm-12">
                  <label class="txthidden" hidden>To: </label><br>
                  <input type="text" id="sender_other" name="fname" class="txthidden" hidden><br>
                  </div>
                  <div class="col-sm-12">
                  <label class="txthidden" hidden>Subject: </label><br>
                    <input type="text" id="subject_other" name="fname" class="txthidden" hidden><br>
                  </div>
              </div>
              <div class="form-group">
              <div class="col-sm-12">
              <label class="txthidden" hidden>Message: </label><br>
              <textarea rows="4" id="message_other" class="txthidden" cols="50" hidden></textarea>
              </div>
              </div>
           
            
            
          
           
        </form>
       
      </div>
      <div class="modal-footer" style = "border:0px">
           <button class = "btn btn-info" data-dismiss="modal" style = "padding:2px 10px;" id="send_audit_email"  data-id="">Send</button>
          <button class = "btn btn-primary btc2 btn-close" data-dismiss="modal" style = "padding:2px 10px;" >Close</button>
  
      </div>
    </div>
  </div>
</div>