<div class="panel-heading" style="background-color:  #fff">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#filter">Filter</a>
      </h4>
    </div>
  	<div class="panel-body"  style="padding-bottom: 17px;">
  		<div class="col-md-6 form-horizontal">
  			<span id="report_no">
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Audit Report No :</label>
				    <div class="col-sm-8">
				      	<input type="text" class="form-control filter-report-no input-sm" placeholder="">
				    </div>
				</div>
	      	</span>
	      	<span id="report_no">
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Audited Site :</label>
				    <div class="col-sm-8">
				      	<input type="text" class="form-control filter-audited-site input-sm" placeholder="">
				    </div>
				</div>
	      	</span>
	      	<span id="report_no">
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Report Status : </label>
				    <div class="col-sm-8">
				      	<select type="text" class="form-control filter-audit-status input-sm">
		            		<option selected value=""></option>
		            		<option value="0">Draft</option>
		            		<option value="1">Co-Auditor's Review</option>
		            		<option value="3">Submitted to Department Head</option>
		            		<option value="4">Submitted to Division Head</option>
		            		<option value="6">Returned by Department Head</option>
		            		<option value="7">Returned by Division Head</option>
		            		<option value="5">Approved by Division Head</option>
		            	</select>
				    </div>
				</div>
	      	</span>
	      	<span id="report_no">
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Auditor : </label>
				    <div class="col-sm-8">
				      	<select class="form-control auditor-filter input-sm">
							<option class="pull-right" value="" selected></option>
							<?php foreach ($auditors as $value) { ?>
								<option value="<?= $value->auditor_id;?>"><?= $value->fname . " " . $value->lname;?></option>
							<?php } ?>
						</select>
				    </div>
				</div>
	      	</span>
	      	<span id="report_no">
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Co-Auditor : </label>
				    <div class="col-sm-8">
				      	<select class="form-control co-auditor-filter input-sm">
							<option class="pull-right" style="max-width: 300px;" value="" selected></option>
							<?php foreach ($auditors as $value) { ?>
								<option value="<?= $value->auditor_id;?>"><?= $value->fname . " " . $value->lname;?></option>
							<?php } ?>
						</select> 
				    </div>
				</div>
	      	</span>

	      	<div class="form-group">
				    <label class="col-sm-4 control-label">Country :</label>
				    <div class="col-sm-8">
				      	<select class="form-control country-filter input-sm">
							<option value="" selected ></option>
							<?php foreach ($country_list as $value) { ?>
								<option value="<?= $value->country_code;?>"><?= $value->country_name;?></option>
							<?php } ?>
						</select> 
				    </div>
				</div>
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Product Type :</label>
				    <div class="col-sm-8">
				      	<select class="form-control type-of-product-filter input-sm">
							<option class="pull-right" value="" selected></option>
							<?php foreach ($type_of_products as $value) { ?>
								<option style="max-width: 300px;" value="<?= $value->classification_id;?>"><?= $value->classification_name;?></option>
							<?php } ?>
						</select> 
				    </div>
				</div>
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Product of Interest :</label>
				    <div class="col-sm-8">
				      	<select class="form-control product-filter input-sm">
							<option class="pull-right" style="max-width: 300px;" value="" selected></option>
							<?php foreach ($products_list as $value) { ?>
								<option value="<?= $value->product_id;?>"><?=  $value->product_name;?></option>
							<?php } ?>
						</select>
				    </div>
				</div>

	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Disposition :</label>
				    <div class="col-sm-8">
				      	<select class="form-control disposition-filter input-sm">
							<option class="pull-right" style="max-width: 300px;" value="" selected></option>
							<?php foreach ($disposition_list as $value) { ?>
								<?php if($value->disposition_label != ""){
									$disposition_text = $value->disposition_name . " - " . $value->disposition_label;
								} else {
									$disposition_text = $value->disposition_name;
								} ?>
								<option value="<?= $value->disposition_id;?>"><?=  $disposition_text;?></option>
							<?php } ?>
						</select>
				    </div>
				</div>
				

  				<div class="form-group">
				    <label class="col-sm-4 control-label">Date Modified :</label>
				    <div class="col-sm-4">
				    	<input type="text" id="datepicker3" class="form-control date-modified-start input-sm" placeholder="From" />
				    </div>
				    <div class="col-sm-4">
				    	<input type="text" id="datepicker4" class="form-control date-modified-end input-sm" placeholder="To" />
				    </div>
				</div>
		      	
		      	


  		</div>
  		<div class="col-md-6 form-horizontal">
  			<span id="report_no">


  				<div class="form-group">
				    <label class="col-sm-4 control-label">Audit Date :</label>
				    <div class="col-sm-4">
				    	<input type="text" id="datepicker1" class="form-control audit-date-start input-sm" placeholder="From" />
				    </div>
				    <div class="col-sm-4">
				    	<input type="text" id="datepicker2" class="form-control audit-date-end input-sm" placeholder="To" />
				    </div>
				</div>

				<div class="form-group">
				    <label class="col-sm-4 control-label">Date Submitted :</label>
				    <div class="col-sm-4">
				    	<input type="text" id="datesubmission1" class="form-control submission-date-start input-sm" placeholder="From" />
				    </div>
				    <div class="col-sm-4">
				    	<input type="text" id="datesubmission2" class="form-control submission-date-end input-sm" placeholder="To" />
				    </div>
				</div>


				<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Lead Time Preparation :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-preparation-start input-sm" placeholder="Minimum">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-preparation-end input-sm" placeholder="Maximum">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Date Reviewed :</label>
					    <div class="col-sm-4">
					      	<input type="text" class="form-control date-reviewed-from input-sm" placeholder="From">
					    </div>
					    <div class="col-sm-4">
					      	<input type="text" class="form-control date-reviewed-to input-sm" placeholder="To">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Lead Time Review :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-review-start input-sm" placeholder="Minimum">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-review-end input-sm" placeholder="Maximum">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Date Approved :</label>
					    <div class="col-sm-4">
					      	<input type="text" class="form-control date-approved-from input-sm" placeholder="From">
					    </div>
					    <div class="col-sm-4">
					      	<input type="text" class="form-control date-approved-to input-sm" placeholder="To">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Lead Time Approval :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-approval-start input-sm" placeholder="Minimum">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-approval-end input-sm" placeholder="Maximum">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Lead Time Total :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-total-start input-sm" placeholder="Minimum">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-total-end input-sm" placeholder="Maximum">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">GMP Score % :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control gmp-score-from input-sm" placeholder="From">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control gmp-score-to input-sm" placeholder="To">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Coverage % :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control coverage-from input-sm" placeholder="From">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control coverage-to input-sm" placeholder="To">
					    </div>
					</div>
		      	</span>
	      	</span>
  		</div>
  	</div>
  	<div class="panel-footer" style="background-color: #fff;text-align: right;">
  		<button data-value="search" class="filter btn btn-success btn-sm">Search</button>

  		<iframe id="download_csv" hidden></iframe>
  		<button data-value="csv" class="filter filter-csv btn btn-info btn-sm">Export CSV</button>
  		<button class="filter_reset btn btn-default btn-sm">Reset</button>
  		<div class="clearfix"></div>
  	</div>