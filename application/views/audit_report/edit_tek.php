<div class="col-md-12 bg-white">
	<div class="col-md-12">
		<div class="header"><h4>GMP AUDIT REPORT</h4><label class="gmp_report_no">Report Number</label></div>
	</div>
	<!-- Part I. Audit Information -->
	<div class="col-md-12">
		<div class="col-md-12 audit-header">Part I. Audit Information</div>
		<div class="col-md-12 sub-header">A. General Information</div>
		<div class="col-md-12 pad-0 part1_content">
			<div class="col-md-12 pad-0">
				<div class="col-md-6 pad-0">
					<div class="col-md-12 pad-0">
						<span>Name of Manufacturer: </span> <label class="company_name"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Site Address 1: </span> <label class="address1"></label>,<label class="address2"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Site Address 2: </span> <label class="address3"></label>, <label class="country"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Date of Audit: </span> <label class="audit_date"></label>
					</div>
				</div>
				<div class="col-md-6 pad-0">
					<div class="col-md-12 pad-0">
						<span>Name of Lead Auditor: </span> <label class="lead_auditor"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Position: </span> <label class="position"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Department/Division &amp; Company:  </span> <label class="auditor-dept"></label>
					</div>
				</div>
			</div>
			<div class="col-md-12 pad-0">
				<div class="table-responsive mb15">
					<table border="1" class="auditors_table tbl_format">
						<thead>
							<tr>
								<td>Name</td>
								<td>Position</td>
								<td>Department/Designation</td>
							</tr>
						</thead>
						<tbody class="auditors_tbody">
						
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-12 sub-header">B. Company Background/Audit History</div>
		<div class="col-md-12 pad-0 part2_content">
			<div class="col-md-12 pad-0">
				<span>Supplier Background/History: </span>
				<textarea class="form-control Supplier-history mb15" disabled></textarea>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Date of Previous Inspection:</span><span class="previous-inspection"> 18 & 19 August 2017</span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Inspector(s) Involved:</span>
			<ol class="inspectors">
			</ol>
			<span>Major Changes Since Previous Inspection: </span>
			<ol class="changes">
			</ol>
		</div>
		<div class="col-md-12 sub-header">C. Audit Information</div>
		<div class="col-md-12 pad-0">
			<span>Activities Carried out by the company: </span>
			<ol class="activities">
			</ol>
			<span>Other Activities Carried out by the company: </span><span class="other-activities"></span><br>
			<span>Scope of Audit: </span>
			<ol class="scope_audit">
			</ol>
			<!-- <span>Products of Interest (for Insert Scope of Audit Name 1) </span>
			<ol class="product_interest">
				<li>Product Name</li>
				<li>Product Name</li>
			</ol> -->
		</div>
		<div class="col-md-12 pad-0">
			<div class="table-responsive mb15">
				<table border="1" class="license_table tbl_format">
					<thead>
						<tr>
							<td>Licenses/Certification</td>
							<td>Issuing Regulatory</td>
							<td>License/Certification Number</td>
							<td>Validity</td>
						</tr>
					</thead>
					<tbody class="license_tbody">
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Pre-Audit Documents:</span>
			<ol class="pre-documents">
			</ol>
		</div>
		<div class="col-md-12 pad-0">
			<span>Audited areas:</span> <span class="audited_areas"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Not Audited areas:</span> <span class="not_audited_areas"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Date of Wrap-up or Close out Meeting:</span> <span class="wrapup-date"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Name of Present During Close-out Meeting</span>
			<div class="table-responsive mb15">
				<table border="1" class="present-meeting tbl_format">
					<thead>
						<tr>
							<td>Name</td>
							<td>Position/Department</td>
						</tr>
					</thead>
					<tbody class="present_met_tbody">
						<tr>
							<!-- <td>Juan Dela Cruz</td>
							<td>CIT</td> -->
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Name of Personnel Met During Inspection </span>
			<div class="table-responsive mb15">
				<table border="1" class="personel-met tbl_format">
					<thead>
						<tr>
							<td>Name</td>
							<td>Position/Department</td>
						</tr>
					</thead>
					<tbody class="personel_met_tbody">
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Distribution List (except Supplier and GMP Inspection)</span>
			<ol class="distribution-list">
				
			</ol>
		</div>
		<div class="col-md-12 sub-header">D. Templates Answers</div>
		<div class="col-md-12 pad-0">
			<div class="col-md-12 pad-0">
				<span>Product Type: </span><span class="report_producttype"></span>
			</div>
			<div class="col-md-12 pad-0">
				<span>Standard/Reference: </span><span class="report_standardreference"></span>
			</div>
		</div>
		<!-- element tables -->
		<div class="col-md-12 pad-0">
			<div class="col-md-12 pad-0 element_con mb15">
				
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-md-12 audit-header">Part II. Audit Observation</div>
		<div class="col-md-12 pad-0">
			<span>Elements Requiring Recommendation: </span>
			<ol class="element-reco">
				
			</ol>
		</div>
		<div class="col-md-12 pad-0">
			<span>Disposition</span>
			<div class="table-responsive mb15">
				<table border="1" class="scope-disposition tbl_format">
					<thead>
						<tr>
							<td>Scope</td>
							<td>Disposition</td>
						</tr>
					</thead>
					<tbody class="disposition_tbody">
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Other Issue for Audit Report: </span> <span class="other-issue-audit"></span>
		</div>	
		<div class="col-md-12 pad-0">
			<span>Other Issues for Executive Summary: </span> <span class="other-issue-executive"></span>
		</div>	
		<div class="col-md-12 pad-0">
			<span>Audit Closure Date:</span><label class="audit-close-date">02 September 2017 </label>
		</div>	
	</div>
	<hr noshade size=1 width="100%">
	<div class="col-md-12">
		<div class="col-md-12 pad-0">
			<span>REMARKS</span>
			<div class="table-responsive mb15">
				<table border="1" class="report-remarks tbl_format">
					<thead>
						<tr>
							<td>Date</td>
							<td>Name</td>
							<td>Position</td>
							<td>Status</td>
							<td>Remarks</td>
						</tr>
					</thead>
					<tbody class="licesnse_tbody">
						<tr>
							<td>8/1/2017</td>
							<td>Juan Dela Cruz</td>
							<td>Auditor</td>
							<td>Approved</td>
							<th>Good report</th>
						</tr>
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
var id = "<?= $id; ?>";
get_report(id);
function get_report(id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>Preview_report",
		data:{id:id}
	}).done(function(data){
		var obj = JSON.parse(data);
		a = '';
		$.each(obj, function(x,y){
			$('.gmp_report_no').text(y.report_no);
			$('.company_name').text(y.name);
			$('.address1').text(y.address1);
			$('.country').text(y.country);
			get_data(y.address2,'city_id','tbl_city');
			get_data(y.address3,'province_id','tbl_province');
			$('.lead_auditor').text(y.fname+' '+y.mname+' '+y.lname);
			if(y.designation != 0){
				$('.position').text(y.designation);
			}
			$('.auditor-dept').text(y.department+', '+y.company);
			$('.audit_date').text(y.audit_date);
			auditors(y.report_id,'get_co_auditors','Co-auditor');
			auditors(y.report_id,'get_approvers','Approver');
			auditors(y.report_id,'get_reviewer','Reviewer');
			$('.Supplier-history').text(y.background);
			$('.previous-inspection').text(' '+y.previous_inspection);
			inspectors(y.report_id);
			inspection_changes(y.report_id);
			activities(y.report_id);
			$('.other-activities').text(y.other_activities);
			scope_audit(y.report_id);
			if(y.translator != 'None'){
			a += '<tr>';
			a += '<td>'+y.translator+'</td>';
			a += '<td>Translator</td>';
			a += '<td></td>';
			}
			license(y.report_id);
			pre_document(y.report_id);
			$('.audited_areas').text(y.audited_areas);
			$('.wrapup-date').text(y.wrap_up_date);	
			personel_met(y.report_id);
			distribution(y.report_id);
			disposition(y.report_id);
			$('.other-issue-audit').text(y.other_issues_audit);
			$('.other-issue-executive').text(y.other_issues_executive);
			$('.audit-close-date').text(y.closure_date);
			get_template(y.report_id);
			get_element_reco(y.report_id);
			present_during_meeting(y.report_id)
		});
		$('.auditors_tbody').append(a);
	});
}
function get_data(id,field,table){
  $.ajax({
    type: 'post',
    url: '<?=base_url();?>global_controller/get_by_id',        
    data:  { 'table': table, 'field' : field, 'id' : id } ,
  }).done(function(data){       
    var obj = JSON.parse(data);
    $.each(obj, function(x,y){
    	$('.address2').text(y.city_name);
    	$('.address3').text(y.province_name);
    });
  });
}
function auditors(report_id,func,position){
	// $('.auditors_tbody').html('');
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/"+func,
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.fname+' '+y.mname+' '+y.lname+'</td>';
			htm += '<td>'+position+'</td>';
			htm += '<td>'+y.department+', '+y.company+'</td>';
			htm += '</tr>';
		});
		
		$('.auditors_tbody').append(htm);
	});
}
function inspectors(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_inspectors",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<li>'+y.name+'</li>';
		});		
		$('.inspectors').append(htm);
	});
}
function inspection_changes(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_inspection_changes",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<li>'+y.changes+'</li>';
		});		
		$('.changes').append(htm);
	});
}
function activities(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_activities",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<li>'+y.activity_name+'</li>';
		});		
		$('.activities').append(htm);
	});
}
function scope_audit(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_scope_audit",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<li>'+y.scope_name+'</li>';
			scope_product(y.scope_id,report_id);
		});		
		$('.scope_audit').append(htm);
	});
}
function scope_product(scope_id,report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_scope_product",
		data:{scope_id:scope_id,report_id:report_id}
	}).done(function(data){
		var obj =JSON.parse(data);
		var htm = '';
		htm += '<span>Products of Interest (for Insert Scope of Audit Name 1) </span>';
		htm += '<ol class="product_interest">';
		$.each(obj, function(x,y){						
			htm += '<li>'+y.product_name+'</li>';		
		});
		htm += '</ol>';
		$('.scope_audit').after(htm);
	});
}
function license(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_license",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.reference_name+'</td>';
			htm += '<td>'+y.issuer+'</td>';
			htm += '<td>'+y.reference_no+'</td>';
			htm += '<td>'+y.validity+'</td>';
			htm += '</tr>';
		});
		
		$('.license_tbody').append(htm);
	});
}
function pre_document(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_pre_document",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<li>'+y.document_name+'</li>';
		});		
		$('.pre-documents').append(htm);
	});
}
function personel_met(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_personel_met",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.name+'</td>';
			htm += '<td>'+y.designation+'</td>';
			htm += '</tr>';
		});
		
		$('.personel_met_tbody').append(htm);
	});
}
function present_during_meeting(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_present_during_meeting",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		if(obj.length!=0){
			$.each(obj, function(x,y){
				htm += '<tr>';
				htm += '<td>'+y.name+'</td>';
				htm += '<td>'+y.position+'</td>';
				htm += '</tr>';
			});
		}else{
			htm += '<tr><td colspan="2">N/A</td></tr>';
		}
		
		$('.present_met_tbody').append(htm);
	});
}
function distribution(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_distribution",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		if(obj.length == 0){
			htm += 'Not Applicable';
		}else{
			$.each(obj, function(x,y){
				htm += '<li>'+y.distribution_name+'</li>';
			});	
		}	
		$('.distribution-list').append(htm);
	});
}
function disposition(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_disposition",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.scope_name+'</td>';
			htm += '<td>'+y.disposition_name+'</td>';
			htm += '</tr>';
		});
		
		$('.disposition_tbody').append(htm);
	});
}

function get_template(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_template",
		data: {report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		$.each(obj, function(x,y){
			$('.report_producttype').text(y.classification_name);
			$('.report_standardreference').text(y.standard_name);
			get_element(y.template_id,report_id);
		});
	});
}

function get_element(template_id,report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_element",
		data:{template_id:template_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var index = 65; 
		var htm = '';
		$.each(obj, function(x,y){
			htm += '<div class="col-md-12 pad-0 element_item ei_'+y.element_id+'">';
			htm += '<span class="element_name">ELEMENT '+String.fromCharCode(index++)+' - '+y.element_name.toUpperCase();+'</span>';
			htm += '<table border="1" class="element_table_'+y.element_id+' tbl_format">';
			htm += '<thead>';
			htm += '<tr>';
			htm += '<td>Questions</td>';
			htm += '<td>Answer(Yes/No/NA)</td>';
			htm += '<td>Details</td>';
			htm += '<td>Item No.</td>';
			htm += '<td>Category</td>';
			htm += '</tr>';
			htm += '</thead>';
			htm += '<tbody class="element_tbody_'+y.element_id+'">';
			htm += '</tbody>';
			htm += '</table>';
			htm += '</div>';
			get_question_answer(y.element_id,report_id);
		});
		$('.element_con').html(htm);
	});
}
function get_question_answer(element_id,report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_report_answer",
		data:{report_id:report_id,element_id:element_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		var no = 0;
		var category = '';
		$.each(obj, function(x,y){
			if(y.answer_name == 'No'){
				no++;
			}else{
				no = '';
			}
			htm += '<tr>';
			htm += '<td style="width:40%;">'+y.question+'</td>';
			htm += '<td style="width:10%;">'+y.answer_name+'</td>';
			htm += '<td style="width:30%;">'+y.answer_details+'</td>';
			htm += '<td style="width:10%;">'+no+'</td>';
			if(y.category_name == null){
				category = '';
			}else{
				category = y.category_name;
			}
			htm += '<td style="width:10%;">'+category+'</td>';
			htm += '</tr>';
		});
		$('.element_tbody_'+element_id).html(htm);
	});
}

function get_element_reco(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_element_reco",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		var alpha = ["A","B","C","D","E","F","G","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
		if(obj.length == 0){
			htm += 'Not Applicable';
		}else{
			$.each(obj, function(x,y){
				var order = y.order - 1;
				htm += '<li>Element '+alpha[order]+' - '+y.recommendation+'</li>';
			});	
		}	
		$('.element-reco').html(htm);
	});
}
});
</script>