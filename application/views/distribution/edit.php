<?php
  $table = "tbl_distribution";
  $order_by = "distribution_id";
?>

<div class="panel-heading">
    <h4 class="panel-title">
        Edit Distribution
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Distribution name *: </label>
            <div class="col-sm-10">
                <input type = "text" id="distribution" class = "inputcss form-control fullwidth inputs_edit_distribution" >
                <span class = "er-msg">Distribution name should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">
$(document).ready(function(){

  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";
  var current_distribution = "";
  var id = "<?= $id; ?>";
  on_load();

  function on_load(){
    var limit = 1;
    aJax.post(
      "<?=base_url();?>global_controller/edit_global",
      {
        id:id,
        limit:limit,
        table:table,
        field:field
      },
      function(data){
        var obj = JSON.parse(data);
        $.each(obj, function(index, row){
          $('#distribution').val(row.distribution_name);
          current_distribution = row.distribution_name;
        });
      }
    );
  }

  $('.update').off('click').on('click', function(){
    var distribution = $('#distribution').val();
    if(validateFields('.inputs_edit_distribution') == 0){
      var no_click = 0;
      confirm("Are you sure you want to update this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_distribution", "distribution_name = '" + distribution + "' AND distribution_name <> '"+current_distribution+"' AND status >= 0", function(result){
              if(result == "true"){
                $(".inputs_edit_distribution").css('border-color','red');
                $(".inputs_edit_distribution").next().html("This field should contain a unique value.");
                $(".inputs_edit_distribution").next().show();
              } else {
                  $(this).prop('disabled',true);
                  isLoading(true);
                  $.ajax({
                    type: 'Post',
                    url:'<?=base_url();?>global_controller/distribution_array',
                    data:{ distribution:distribution,id:id,table:table, field:field, action: 'update'},
                  }).done( function(data){
                    isLoading(false);
                    $('.update').prop('disabled',false);
                    updateAPI("distribution");
                    insert_audit_trail("Update " + distribution);
                    update_config();
                    bootbox.alert('<b>Record is successfully updated!</b>', function() {
                        location.reload();
                    });
                  });
              }
            });
          }
        }
      });
    }
  });
});
</script>