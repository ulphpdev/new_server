<?php
  $table = "tbl_distribution";
  $order_by = "distribution_id";
?>


<div class="panel-heading">
    <h4 class="panel-title">
        Add Distribution
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Distribution name *: </label>
            <div class="col-sm-10">
                <input type = "text" id="distribution" class = "inputcss form-control fullwidth inputs_distribution" >
                <span class = "er-msg">Distribution name should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">

$(document).ready(function(){

  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";


  $('.save').off('click').on('click', function() {
    var distribution = $('#distribution').val();
    if(validateFields('.inputs_distribution') == 0){
      var no_click = 0;
      confirm("Are you sure you want to save this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_distribution", "distribution_name = '" + distribution + "' AND status >= 0", function(result){
              if(result == "true"){
                $(".inputs_distribution").css('border-color','red');
                $(".inputs_distribution").next().html("This field should contain a unique value.");
                $(".inputs_distribution").next().show();
              } else {
                $(this).prop('disabled',true);
                isLoading(true);
                aJax.post(
                  "<?=base_url('global_controller/distribution_array');?>",
                  {
                    distribution:distribution,
                    table:table,
                    field:field,
                    action: 'save'
                  },
                  function(data){
                    $('.update').prop('disabled',false);
                    updateAPI("distribution");
                    update_config();
                    insert_audit_trail("Create " + distribution);
                    isLoading(false);
                    bootbox.alert('<b>Record is successfully saved!</b>', function() {
                      location.reload();
                    });
                  }
                );
              }
            });
          }
        }
      });

    }
  });
});



</script>