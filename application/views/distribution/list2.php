<?php 
  $table = "tbl_distribution";
  $order_by = "distribution_id";
?>

<ol class="breadcrumb" style="background-color: #fff;">
    <li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
    <li>Data Maintenance</li>
    <li class="active">Distribution List</li>
</ol>

<div class="col-md-4">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Distribution Name" id="txt_search" placeholder="">
    <span class="er-msg file-err">This field is required</span>
    <div class="input-group-btn">
      <button class="btn btn-primary" id="btn_search" type="submit">
          <i class="glyphicon glyphicon-search"> </i> 
          Search
      </button>
    </div>
  </div>
  <span class="max_label"></span>
</div>

<div class="btn-navigation menu-btn col-md-8 pad-0" >
  <button class="add btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-plus"></span> ADD DISTRIBUTION LIST</button>
  <button class="article_list btn-min btn btn-default  bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CLOSE</button>
	<button data-type = "Delete" class="inactive btn-min btn btn-default  bold" style="display: none;"><span class = "glyphicon glyphicon-trash"></span> Delete</button> 
  <button data-type = "Activate" class="inactive activate_btn btn-min btn btn-default  bold"><span class = "glyphicon glyphicon-ok"></span> Activate</button>
  <button data-type = "Deactivate" class="inactive btn-min btn btn-default  bold"><span class = "glyphicon glyphicon-remove-circle"></span> Deactivate</button>
</div>

<div class="row">
	<div class="col-md-12" id="list_data">
		<div class="table-responsive">

            <!-- LIST -->
    		<table class = "table listdata table-striped" style="margin-bottom:0px;">
                <thead>
                    <tr>
                        <th style = "width:10px;"><input class = "selectall" type = "checkbox"></th>
                        <th>Distribution Name</th>
                        <th>Date &amp; Time Modified</th>
                        <th>Status</th>
                        <th style="text-align: right;">Actions</th>
                    </tr>  
                </thead>
                <tbody></tbody>
    		</table>
        </div>

        <!-- PAGINATION -->
        <div style="text-align: center; margin-top: 20px;" class="form-inline pager_div"></div>
	</div>
</div>

<!-- FORM -->
<div class="content-container" id="form_data"  style="margin-top: 30px;"></div>



<script>

$(document).ready(function() {


  <?php $this->load->view('layout/listing_function');?>

  var limit = '10';
  var offset = '0';
  var query = "";

  var count_active_current = 0;
  var self_count = 0;
  var max_current = 10;

  get_list("",limit,offset);
  get_pagination("");


	function get_list(query,limit,offset){
    count_active_current = 0;
    self_count = 0;
    $('.cancel').hide();
    $('.inactive').hide();
    $('.selectall').attr('checked', false);
    var table = "<?=$table;?>";
    var order_by = "<?=$order_by;?>";
    isLoading(true);


      aJax.post(
          "<?=base_url('global_controller/getlist_dm');?>",
          {
              'limit':limit, 
              'offset':offset, 
              'query':query, 
              'table':'tbl_distribution', 
          },
          function(result){ 
              var obj = JSON.parse(result);
              var table_body = new TBody(); //please check helper.js for this function

              if(obj.length!=0){  

                $.each(obj,function(key, row){
                    if(row.status == 1){
                      var status = "Active";
                    } else {
                      var status = "Inactive";
                    }

                    table_body.td("<input class = 'select' data-status="+row.status+" data-id = '"+row.distribution_id+"'  data-name='"+row.distribution_name+"' type ='checkbox'>");
                    table_body.td(row.distribution_name);
                    table_body.td(moment(row.update_date).format("LLL"));
                    table_body.td(status);

                    var action_button = new UList(); //please check helper.js for this function
                    action_button.set_liclass("li-action");
                    action_button.set_ulclass("ul-action");
                    action_button.li("<a class='delete_data action_list action' data-status='' id='"+row.distribution_id+"' data-name='"+row.distribution_name+"' title='Delete'> <span class='glyphicon glyphicon-trash'></span></a>");
                    action_button.li("<a class='edit action_list action' data-status='' id='"+row.distribution_id+"' title='Edit'><span class='glyphicon glyphicon-pencil'></span></a>");

                    table_body.td(action_button.set());
                    table_body.set();
                })
                  
              } else {
                table_body.td_norecord(7);
                table_body.set();
              }
              table_body.append('.listdata tbody');
              isLoading(false);
              check_no_of_active();
          }
      );

  }
  
  function get_pagination(query){
    aJax.post(
      "<?=base_url("global_controller/getlist_dm");?>",
      {
        'limit':9999, 
        'offset':offset, 
        'query':query, 
        'table':'tbl_distribution', 
      },
      function(result){
        var obj = JSON.parse(result);
        var result_count = obj.length;
        var no_of_page = Math.ceil(result_count / limit);
        var pagination = new Pagination(); //helper.js
        pagination.set_total_page(no_of_page);
        pagination.set('.pager_div');
      }
    );
  }

  $(document).on('keypress', '#txt_search', function(e){
    if (e.which == 13) { 
      $('#btn_search').click()
    }
  });

  $(document).on('click', '#btn_search', function(){
    var keyword = $('#txt_search').val();
    if(keyword.trim() == ""){
      query = "";
      get_list(query,limit,offset);
      get_pagination(query);
    } else {
      query = "distribution_name like '%" + keyword + "%'";
      get_list(query,limit,offset);
      get_pagination(query);
    }
  });

  $(document).on('click', '.add', function(){

    aJax.post(
      "<?=base_url('global_controller/count_active_record');?>",
      {
        table : 'tbl_distribution',
        max: 10
      },
      function(result){
        if(result){
          module_action('distribution', 'add', '.content-container', '');
        } else {
          $('.article_list').hide();
          $('.save').hide();
          $('.update').hide();
          $('.add').show();
          $('#form_data').html('');
          bootbox.alert("Maximum Active <b>Distribution</b> Exceeded!");
        }
      }
    );
  })

  $(document).on('click', '.edit', function(){
    var id = $(this).attr('id');
    module_action('distribution', 'edit', '.content-container', id);
  });

  $(document).on('click', '.cancel', function(){
    module_action('distribution', 'cancel', '.content-container', '');
    get_list(query,limit,offset);
    get_pagination(query);
  })


  $(document).on('click', '.inactive', function(){
    isLoading(true);
    var x = 0;
    var data_type = $(this).attr('data-type').toLowerCase();
    aJax.post(
      "<?=base_url('global_controller/count_active_record');?>",
      {
        table : 'tbl_distribution',
        max: 10
      },
      function(result){
        isLoading(false);
        if(data_type == "activate"){
            if(result){
                $('.select').each(function() {                
                  if (this.checked==true) {  x++; } 
                });

                if (x > 0 ) {
                  change_status_modal(
                    "Are you sure you want to "+data_type+" selected record?",
                    data_type,
                    function(result){
                      if(result){
                        isLoading(true);
                        setTimeout(function() {
                          update_satus(data_type);
                        }, 1000);
                      }
                    }
                  );
                }

            } else {
              bootbox.alert("Maximum Active <b>Distribution</b> Exceeded!", function(){
                location.reload();
              });
            }
        } else {
          $('.select').each(function() {                
            if (this.checked==true) {  x++; } 
          });

          if (x > 0 ) {
            change_status_modal(
              "Are you sure you want to "+data_type+" selected record?",
              data_type,
              function(result){
                if(result){
                  isLoading(true);
                  setTimeout(function() {
                    update_satus(data_type);
                  }, 1000);
                }
              }
            );
          }

        }
      }
    );
  });

  $(document).on('click', '.delete_data', function(){
    var x = 0;
    var id = $(this).attr('id');
    var name = $(this).attr('data-name');
    var table = "tbl_distribution";
    var order_by = "distribution_id";
    var dialog = bootbox.confirm({
      message: "Are you sure you want to delete this record?",
      buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
              label: 'No',
              className: 'btn-danger'
          }
      },
      callback: function (result) {
        if(result){
          aJax.post(
            "<?=base_url('global_controller/inactive_global_update');?>",
            {
              id:id,
              type: -2, 
              table:table, 
              order_by: order_by
            },
            function(data){
              updateAPI("distribution");
              insert_audit_trail("Delete " + name);
              dialog.modal('hide');
              location.reload();
            }
          );
        }          
      }
    });      
  });


    function update_satus(type){
      isLoading(true);
      var x = 0;
      var trail_label = "";
      var message = "";
      if (type == 'delete') {
        var type = '-2';
        trail_label = "Delete ";
        message = "<b>Successfully deleted records.</b>";
      }
      if (type == 'deactivate') {
        var type = '0';
        trail_label = "Deactivate ";
        message = "<b>Successfully deactivate records.</b>";
      }
      if (type == 'activate') {
        var type = '1';
        trail_label = "Activate ";
        message = "<b>Successfully activate records. </b>";
      }
      
      var count_to_active = count_status_1('.select');
      if(count_to_active <= 10){
        $('.select').each(function() {                
          if (this.checked==true) {
            var table = "<?=$table;?>";
            var order_by = "<?=$order_by;?>";
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');

            aJax.postasync(
              "<?=base_url('global_controller/inactive_global_update');?>",
              {
                id:id,
                type:type, 
                table:table, 
                order_by: order_by
              },
              function(data){
                updateAPI("distribution");
                insert_audit_trail(trail_label + name);
                update_config();
              }
            );
            x++;
          } 
        });
        
        isLoading(false);
        bootbox.alert(message, function(){
          location.reload();
        });
      } else {
        isLoading(false);
        bootbox.alert("Maximum Active <b>Distribution</b> Exceeded!", function(){
          location.reload();
        });
      }
        
      
      
      
    }
    $('.selectall').click(function(){
      $(".select").each(function() { 
        //get all checked 0 status
        if($(this).attr('data-status') == 0){
          if (this.checked==true) {
            self_count++;
          } else {
            self_count--;
          }
        }

          var total_count = parseInt(count_active_current) + parseInt(self_count);

          var label_max = "Total Number of Active Records: "+total_count+" / " + max_current;
          $(".max_label").html(label_max);
          if(total_count > max_current){
            $(".max_label").attr("style","color: red;");
          } else {
            $(".max_label").attr("style","color:#333;");
          }
      });
    });

    $(document).on('change', '.select', function(){
      if($(this).attr('data-status') == 0){
        if (this.checked==true) {
          self_count++;
        } else {
          self_count--;
        }
      }
      
      var total_count = parseInt(count_active_current) + parseInt(self_count);

      var label_max = "Total Number of Active Records: "+total_count+" / " + max_current;
      $(".max_label").html(label_max);
      if(total_count > max_current){
        $(".max_label").attr("style","color: red;");
      } else {
        $(".max_label").attr("style","color:#333;");
      }
    });

    function check_no_of_active(){

      aJax.postasync(
        "<?=base_url('global_controller/no_of_active_record');?>",
        {
          table : 'tbl_distribution',
        },
        function(result){
          count_active_current = result;
        }
      );
      $(".select").each(function() { 
        //get all checked 0 status
        if($(this).attr('data-status') == 0){
          if (this.checked==true) {
            count_active_current++;
          } 
        }
      });

        
      var label_max = "Total Number of Active Records: "+count_active_current+" / " + max_current;
      $(".max_label").html(label_max);

      if(count_active_current > max_current){
        $(".max_label").attr("style","color: red;");
      } else {
        $(".max_label").attr("style","color:#333;");
      }

    }
  });

  function count_status_1(element){
    var count_active = 0;
    aJax.postasync(
      "<?=base_url('global_controller/no_of_active_record');?>",
      {
        table : 'tbl_distribution',
      },
      function(result){
        count_active = result;
      }
    );

    $(element).each(function() { 
      //get all checked 0 status
      if($(this).attr('data-status') == 0){
        if (this.checked==true) {
          count_active++;
        }
      }
    });

    return count_active;
  }

  

 
</script>