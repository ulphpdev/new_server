<div class="sidebar-nav">
  <div class="navbar navbar-default" role="navigation" id="main-menu-nav">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <span class="navbar-brand hidden-md visible-xs">
        <b> Hi  </b> <?php  echo ucwords($this->session->userdata('name')); ?>!  |  <a href="#" class="btn-logout" style="color:#000">Log-out</a>
      </span>
    </div>
    <div class="navbar-collapse collapse sidebar-navbar-collapse">
      <ul class="nav navbar-nav main-menu test" >
        <li class="main-menu-a home"><a href="<?= base_url().'home'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dashboard_icon.png"></span> <span class="menu_label">Dashboard</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
        <li class="main-menu-a manage_template"><a href="<?= base_url().'home/manage_template'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/template_icon.png"></span> <span class="menu_label">Manage Template</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
        <li class="main-menu-a auditreport" ><a href="<?= base_url().'auditreport'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/report_icon.png"></span> <span class="menu_label">Audit Reports</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
        <li class="main-menu-a audit_report_analysis2" ><a href="<?= base_url().'audit_report_analysis2'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/report_analysis.png"></span> <span class="menu_label">Audit Report Analysis</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
        <?php if($this->session->userdata('sess_role') == 1) { ?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_icon.png"></span> <span class="menu_label">Data Maintenance</span> <span class="caret"></span></a>
          <ul class="dropdown-menu main-menu-child">
            <li  class="main-menu-a auditor"><a href="<?= base_url().'data_maintenance/auditor'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_auditors.png"></span> <span class="menu_label">Auditor</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
            <li class="main-menu-a reviewer"><a href="<?= base_url().'data_maintenance/reviewer'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_reviewers.png"></span> <span class="menu_label">Reviewer</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
            <li class="main-menu-a approver"><a href="<?= base_url().'data_maintenance/approver'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_approvers.png"></span> <span class="menu_label">Approver</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
            <li role="separator" class="divider"></li>
            <li class="main-menu-a site"><a href="<?= base_url().'data_maintenance/site'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_site.png"></span> <span class="menu_label">Supplier</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
            <li class="main-menu-a audit_history">
              <a href="<?= base_url().'audit_history'; ?>">
                  <img class="menu-icon" src="<?= base_url();?>asset/img/icon/audit_history.png">
                  <span class="menu_label">Audit History</span>
                  
              </a>
              <span class="hidden-xs hidden-sm hidden-md arrowsprite"></span>
            </li>
            <li class="main-menu-a products"><a href="<?= base_url().'data_maintenance/product'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_product_materials.png"></span> <span class="menu_label">Products/Materials</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
            <li class="main-menu-a audit_type"><a href="<?= base_url().'data_maintenance/type_of_audit'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_type_of_audit.png"></span> <span class="menu_label">Type of Audit</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>                <li class="main-menu-a category"><a href="<?= base_url().'data_maintenance/category'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_category.png"></span> <span class="menu_label">Category</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
            <li class="main-menu-a product_type"><a href="<?= base_url().'data_maintenance/prodtype'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_product_type.png"></span> <span class="menu_label">Product Type</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
            <li class="main-menu-a standard_ref"><a href="<?= base_url().'data_maintenance/standard'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_standard.png"></span> <span class="menu_label">Standard/Reference</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
            <li class="main-menu-a disposition"><a href="<?= base_url().'data_maintenance/disposition'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_disposition_icon.png"></span> <span class="menu_label">Disposition</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
            <li class="main-menu-a distribution"><a href="<?= base_url().'data_maintenance/distribution'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/dm_distribution.png"></span> <span class="menu_label">Distribution List</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/archive_icon.png"></span> <span class="menu_label">Archive</span> <span class="caret"></span></a>
          <ul class="dropdown-menu main-menu-child">
            <li  class="main-menu-a arc_template"><a href="<?= base_url().'archive2/archive_template2'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/achv_template.png"></span> <span class="menu_label">Template</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
            <li class="main-menu-a arc_report"><a href="<?= base_url().'archive/archive_report'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/achv_report.png"></span> <span class="menu_label">Audit Report</span></a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>

          </ul>
        </li>
        <li class="main-menu-a audit_trail2" ><a href="<?= base_url().'audit_trail2'; ?>"><span><img class="menu-icon" src="<?= base_url();?>asset/img/icon/audit_trail_icon.png"></span> Audit Trail</a><span class="hidden-xs hidden-sm hidden-md arrowsprite"></span></li>
        
        <?php } ?>
      </ul>
    </div>
  </div>
 </div> 