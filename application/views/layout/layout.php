
<?php $this->load->view('layout/header');?>
<script type="text/javascript">
  var active_link = window.location.pathname.split("/").pop();
  $('.'+active_link).addClass('active-main-menu');
  $('.main-menu-a').click(function(e) {
    $('.main-menu-a').each(function() {
      $(this).removeClass('active-main-menu');
    });
    $(this).addClass('active-main-menu');
  });
</script>

<div class="home-container">
  <div id="main-header" class="col-md-12 col-sm-12">
  	<?php $this->load->view('layout/navigation');?>
  </div>

  <div class="col-sm-2 col-md-2 pad-0 side_menu">
    <?php $this->load->view('layout_back/navigation');?>
  </div>

  <div class="col-xs-12 col-sm-10 col-md-10" id="content_gmp">
    <div  class="col-md-12 content_inside">
      <?php $this->load->view($content); ?>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>