
$('.selectall').click(function(){
  var del = 0;
  if(this.checked) { 
    $('.select').each(function() { 
      this.checked = true;  
      $('.delete').show();
      $('.inactive').show();           
    });
  }else{
    $('.select').each(function() { 
      $('.delete').hide();
      $('.inactive').hide();
      this.checked = false;                 
    });         
  }

  $('.select').each(function() {  
    var ischecked =  $(this).is(":checked");
    if(ischecked){
      var data_delete =  $(this).attr("data-delete");
      if(data_delete == 1){
        del ++;
      }
    }
  });
  
  if(del > 0){
    $('.trash').hide();
  }

});



$(document).on('change', '.select', function(){
  var del = 0;
  var x = 0;
  $('.select').each(function() {  
    var ischecked =  $(this).is(":checked");
    if(ischecked){
      var data_delete =  $(this).attr("data-delete");
      if(data_delete == 1){
        del ++;
      }
    }

    if (this.checked==true) { x++; } 
    if (x > 0 ) {
      $('.delete').show();
      $('.inactive').show(); 
    } else {
      $('.delete').hide();
      $('.inactive').hide();
      $('.selectall').attr('checked', true);
    }
  });

  console.log(del);
  if(del > 0){
    $('.trash').hide();
  }
});


$(document).on('change','.pager_number', function() {
      var page_number = parseInt($(this).val());
      var page = (page_number - 1) * limit;
      get_list(query, limit, page);
      $('.pager_no').html("Page " + numeral(page_number).format('0,0'));
  });

  $(document).on('click','.first-page', function() {
    var page_number = parseInt($('.page_number').val());
    if(page_number!=first()){
      var page = (first() - 1) * limit;
      get_list(query, limit, page);
        $('.pager_number').val($('.pager_number option:first').val());
        $('.pager_no').html("Page " + numeral(first()).format('0,0'));
    }
  });


  $(document).on('click','.prev-page', function() {
      var page_number = parseInt($('.pager_number').val());
      if(page_number!=first()){
        var next = page_number - 2;
        console.log(next * limit);
        get_list(query, limit, next * limit);
        $('.pager_number').val(next + 1);
        $('.pager_no').html("Page " + numeral(next + 1).format('0,0'));
    }
  });


  $(document).on('click','.next-page', function() {
      var page_number = parseInt($('.pager_number').val());
      if(page_number!=last()){
        console.log(query);
        var next = page_number + 1;
        get_list(query, limit, (next - 1) * limit);
        $('.pager_number').val(next);
        $('.pager_no').html("Page " + numeral(next).format('0,0') );
      }
  });


  $(document).on('click','.last-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=last()){
      var page = (last() - 1) * limit;
      get_list(query, limit, page);
      $('.pager_number').val($('.pager_number option:last').val());
      $('.pager_no').html("Page " + numeral(last()).format('0,0'));
    }
  });

  function first(){
     return parseInt($('.pager_number option:first').val());
  }

  function last(){
     return parseInt($('.pager_number option:last').val());
  }
