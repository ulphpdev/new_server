<?php 
  $table = "tbl_cms_login";
  $order_by = "id";
?>
<div class="col-md-12" id="form-content">

<div class="col-md-12 pad-5">
   <div class="col-md-2">Name *: </div>
    <div class = "col-md-4"><input type = "text" id="name" class = "inputcss form-control fullwidth inputs"><span class = "er-msg">Name should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
   <div class="col-md-2">Email *: </div>
    <div class = "col-md-4"><input type = "text" id="email" class = "inputcss form-control fullwidth inputs" disabled><span class = "er-msg er-msg-email">Email should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
    <div class="col-md-2 bold">Status *: </div>
     <div class = "col-md-4">
          <select id="status" class="inputcss_blue type_event form-control">
          <option value="1" class="">Active</option> 
          <option value="0" class="">Inactive</option>       
          </select>
     </div>
</div>

</div>


<script type="text/javascript">
$(document).ready(function(){

var table = "<?=$table;?>";
var field = "<?=$order_by;?>";
var id = "<?= $id; ?>";
	on_load();
      function on_load(){

        var limit = 1;
        $.ajax({
        type: 'Post',
        url:'<?=base_url();?>global_controller/edit_global',
        data:{id:id,limit:limit, table:table, field:field},
      }).done( function(data){

          var obj = JSON.parse(data);
          $.each(obj, function(index, row){        
          		$('#name').val(row.name);
          		$("select#status option[value='"+row.status+"']").prop("selected", true);
          		$('#email').val(row.email);
                // $('.selected-image').html(' <img class="imgsrc_image img-responsive" src="../'+row.image+'" width="100%" /><input style = "display:none" class = "imgsrc inputs" value="'+row.image+'"/>');
          });

        

     });      

}


/* Start of update */

 $(document).on('click', '.update', function(){

  
    var name = $('#name').val();
    var status = $('#status').val();
    var email = $('#email').val();
    var role = 2;

    $(this).prop('disabled',true);
	    $('.loader_gif').show();
	    $.ajax({

	            type: 'Post',
	            url:'<?=base_url();?>global_controller/user_login',
	            data:{  id:id,name:name,role:role,status:status,email:email,table:table, field:field, action: 'update'},
	          }).done( function(data){
	            $('.loader_gif').hide();
	            $('.update').prop('disabled',false);
	            bootbox.alert('<b>Successfully Updated.</b>', function() {
                location.reload();
              }).off("shown.bs.modal");
	           // $('.article_list').click();
	    });


 });

/* End of update */

/* Start of Validation */

function validateFields() {

    var counter = 0;
    $('.inputs').each(function(){
          var input = $(this).val();
          if (input.length == 0) {
          	$(this).css('border-color','red');
            $(this).next().show();
            counter++;
          }else{
          	$(this).css('border-color','#ccc');
            $(this).next().hide();
          }
      });

    if($('#administrator').is(':checked')){
          	$('.er-msg-admin').hide();
    } else {
    	    $('.er-msg-admin').show();
    	    counter++;
    }

    return counter;
}

function isValidEmailAddress(email) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(email);
};

/* End of Validation */

/* start file manager */
$('.btn-select-image').click(function(){
  $('.c-copy').hide();
  filemanager('');
  $('#filemanager-image-modal').modal('show');
  $('.btn-copy-file').attr('attr-src','imgsrc_image');
});
$(document).on('click','.btn-copy-file', function(){
  var src = $('.check_active').attr('file-name');
  var old_src = $('.imgsrc').val();
  var type = $(this).siblings('.pathtocopy').attr('data-type'); 
  var path = $(this).attr('attr-src');
  ext = src.split('.').pop();
  ext = ext.toLowerCase();
 	if(type == 'file'){
		bootbox.alert('Please select image only.');
		if(old_src){
  		$('.selected-image').html(' <img class="imgsrc_image img-responsive" src="<?= base_url() ?>'+old_src+'" width="100%" /><input type="text"  class = "imgsrc inputs none filename" name="image"  value="'+old_src+'" hidden/>');
  		}
	 }else{
	  $('.selected-image').html(' <img class="imgsrc_image img-responsive" src="<?= base_url() ?>'+src+'" width="100%" /><input type="text"  class = "imgsrc inputs none filename" name="image"  value="'+src+'" hidden/>');
	 }
});
function filemanager(folder){
  $.ajax({
    type: 'Post',
	url: "<?=base_url()?>global_controller/getimages",
    data:{folder:folder},
  }).done( function(data){
    $('.file-manager').html(data);
  }); 
}
/* End of file manager */




})	

</script>