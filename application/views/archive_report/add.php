<?php 
  $table = "tbl_approver_info";
  $order_by = "approver_id";
?>
<div class="col-md-12" id="form-content">

<div class="col-md-12 pad-5">
   <div class="col-md-2">First name *: </div>
    <div class = "col-md-4"><input type = "text" id="fname" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg">First name should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
   <div class="col-md-2">Middle name *: </div>
    <div class = "col-md-4"><input type = "text" id="mname" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg">Middle name should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
   <div class="col-md-2">Last name *: </div>
    <div class = "col-md-4"><input type = "text" id="lname" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg">Last name should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
   <div class="col-md-2">Designation *: </div>
    <div class = "col-md-4"><input type = "text" id="designation" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg">Designation should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
    <div class="col-md-2">Department/Division *: </div>
    <div class = "col-md-4"><input type = "text" id="division" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg">Department/Division should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
   <div class="col-md-2">Company *: </div>
    <div class = "col-md-4"><input type = "text" id="company" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg">Company should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
   <div class="col-md-2">Email *: </div>
    <div class = "col-md-4"><input type = "text" id="email" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg er-msg-email">Email should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
    <div class="col-md-2">e-Signature *:</div>
    <div class = "col-md-4">
    <div class="col-md-12 pad-0">
      <button class = "btn btn-success btn-standard btn-select-image"><span class = "glyphicon glyphicon-pencil  right-5"></span> Choose file</button>
      </div>
      <div class="col-md-12 pad-0 selected-image">
        <input style = "" class = "imgsrc inputs none" value=""/><span class = "er-msg">e-Signature should not be empty *</span></div>
    </div>
</div>

<div class="col-md-12 pad-5">
    <div class="col-md-2 bold">Administrator: </div>
     <div class = "col-md-4">
		<input type="checkbox" id="administrator" name="administrator" value=""> <span class = "er-msg er-msg-admin">Administrator should not be empty *</span>
     </div>
  </div>
</div>


<script type="text/javascript">
$(document).ready(function(){

var table = "<?=$table;?>";
var field = "<?=$order_by;?>";

/* Start of update */

 $(document).on('click', '.save', function(){

  
  var fname = $('#fname').val();
  var mname = $('#mname').val();
  var lname = $('#lname').val();
  var designation = $('#designation').val();
  var division = $('#division').val();
  var company = $('#company').val();
  var email = $('#email').val();
  var signature = $('#e-Signature').val();
  if($('#administrator').is(':checked')){
  	var administrator = 1;
  } else {
  	var administrator = 0;
  }

  $('.er-msg-email').html("Email should not be empty *");
  $('#email').css('border-color','#ccc');
  if(validateFields() == 0){
  	if(isValidEmailAddress(email)){
    $(this).prop('disabled',true);
	    $('.loader_gif').show();
	    $.ajax({

	            type: 'Post',
	            url:'<?=base_url();?>global_controller/user_array',
	            data:{ fname:fname,mname:mname,lname:lname, designation: designation, division:division, division:division, company:company, email:email, signature:signature,table:table, field:field, action: 'save'},
	          }).done( function(data){
	            $('.loader_gif').hide();
	            $('.update').prop('disabled',false);
	            bootbox.alert('<b>Successfully Saved.</b>', function() {
                location.reload();
              }).off("shown.bs.modal");
	       


	    }); 
		} else {
		  	$('.er-msg-email').show();
		  	$('#email').css('border-color','red');
		  	$('.er-msg-email').html("Invalid email.");
		}

  	}


 });

/* End of update */

/* Start of Validation */

function validateFields() {

    var counter = 0;
    $('.inputs').each(function(){
          var input = $(this).val();
          if (input.length == 0) {
          	$(this).css('border-color','red');
            $(this).next().show();
            counter++;
          }else{
          	$(this).css('border-color','#ccc');
            $(this).next().hide();
          }
      });

    if($('#administrator').is(':checked')){
          	$('.er-msg-admin').hide();
    } else {
    	    $('.er-msg-admin').show();
    	    counter++;
    }

    return counter;
}

function isValidEmailAddress(email) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(email);
};

/* End of Validation */

/* start file manager */
$('.btn-select-image').click(function(){
  $('.c-copy').hide();
  filemanager('');
  $('#filemanager-image-modal').modal('show');
  $('.btn-copy-file').attr('attr-src','imgsrc_image');
});
$(document).on('click','.btn-copy-file', function(){
  var src = $('.check_active').attr('file-name');
  var old_src = $('.imgsrc').val();
  var type = $(this).siblings('.pathtocopy').attr('data-type'); 
  var path = $(this).attr('attr-src');
  ext = src.split('.').pop();
  ext = ext.toLowerCase();
 	if(type == 'file'){
		bootbox.alert('Please select image only.');
		if(old_src){
  		$('.selected-image').html(' <img class="imgsrc_image img-responsive" src="<?= base_url() ?>'+old_src+'" width="100%" /><input type="text"  class = "imgsrc inputs none filename" name="image"  value="'+old_src+'" hidden/>');
  		}
	 }else{
	  $('.selected-image').html(' <img class="imgsrc_image img-responsive" src="<?= base_url() ?>'+src+'" width="100%" /><input type="text"  class = "imgsrc inputs none filename" name="image"  value="'+src+'" hidden/>');
	 }
});
function filemanager(folder){
  $.ajax({
    type: 'Post',
	url: "<?=base_url()?>global_controller/getimages",
    data:{folder:folder},
  }).done( function(data){
    $('.file-manager').html(data);
  }); 
}
/* End of file manager */




})	

</script>