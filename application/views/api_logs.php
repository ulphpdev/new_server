<style type="text/css">
	
	table {
	    border-collapse: collapse;
	    width: 100%;
	    font-size: 14px;
	}

	table, th, td {
	    border: 1px solid black;
	    padding: 3px;
	}

</style>

<div style="width: 100%; min-height: 700px; padding:25px;" class="bg-white">
<h2>API DART Audit Trail</h2>
	<form class="navbar-form" style="padding:0px;" action="" method="GET">
      	<div class="form-group">
        	<input type="text" class="form-control" name="report_id" placeholder="Report ID">
      	</div>
      	<button type="submit" class="btn btn-primary">Submit</button>
      	<a href="<?= base_url("api/logs");?>" class="btn btn-default">Reset</a>
    </form>
	

	<table>
		<thead style="background-color: #fff; color: black;">
			<tr>
				<th>Report ID</th>
				<th >Action</th>
				<!-- <th style="width: 60%;">API</th> -->
				<th>User</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($logs as $key => $value) { ?>

            <?php 
            
                $array  = json_decode($value->json);
                $return_array = array();
                foreach ($array as $x => $y) {
                    $count_record = count(json_decode($y));
                    if($count_record > 0){
                        $return_array[$x] = json_decode($y);
                    } else {
                        $return_array[$x] = $y;
                    }
                }
        
            ?>
				<tr>
					<td>
						<?php if($value->action == "EmailtoLead") {  ?>
							<a href="<?= base_url("api/dart_email_log?id=") . $value->id;?>" target="_blank">
								<?= $value->report_id;?>
							</a>
						<?php } else { ?>
							<a href="<?= base_url("api/dart_log?id=") . $value->id;?>" target="_blank">
								<?= $value->report_id;?>
							</a>
						<?php } ?>
					</td>
					<td><?= $value->action;?></td>
					<!-- <td><textarea style="white-space: nowrap; width: 100%;" data-role="none" id="myTextArea" rows="10"><?= json_encode($return_array);?></textarea></td> -->
					<td><?= $value->Name;?></td>
					<td><?= date("F j, Y",strtotime($value->submission_date)) . " " . date("g:i a",strtotime($value->submission_date));?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<a href="<?= $ahref ;?>" class="btn btn-sm btn-block btn-primary" >Load More</a>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		prettyPrint();
	});
	function prettyPrint() {
		$('textarea').each(function(){
			var ugly = $(this).val();
		    var obj = JSON.parse(ugly);
		    var pretty = JSON.stringify(obj, undefined, 4);
		    $(this).val(pretty)
		})
	    
	}
</script>