<?php 
  $table = "tbl_approver_info";
  $order_by = "approver_id";
?>
<ol class="breadcrumb" style="background-color: #fff;">
    <li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
    <li class="active">Audit Report Analysis</li>
</ol>
<div class="form-inline">
    <div class="form-group">
      <label for="email">Filter : </label>
      <select class="form-control filter">
        <option value="" selected disabled>Please select.</option>
          <option value="report_no">Audit Report No</option>
          <option value="audit_site">Audited Site (Company Name)</option>
          <option value="date_of_audit">Date of Audit</option>
      </select>
    </div>
    <span id="audit_site" class="filtering filtering-div" hidden>
        <div class="form-group" >
          <label for="email">Audited Site (Company Name) : </label>
          <input type="text" class="form-control filter-audited-site " placeholder="Audited Site (Company Name)">
        </div>
        <div class="form-group">
          <button class="btn btn-default  btn-audited-site">Show</button>
          <button class="btn btn-primary  unfiltered">Show All</button>
        </div>
    </span>
    <span id="report_no" class="filtering filtering-div" hidden>
        <div class="form-group" >
          <label for="email">Audit Report No : </label>
          <input type="text" class="form-control filter-report-no" placeholder="Audit Report Number">
        </div>
        <div class="form-group">
          <button class="btn btn-default  btn-report-no">Show</button>
          <button class="btn btn-primary  unfiltered">Show All</button>
        </div>
    </span>
    <span id="date_of_audit" class="filtering filtering-div" hidden>
      <div class="form-group">
        <label>Date from : </label>
        <input type="text" id="datepicker1" class="form-control audit-date-start" />
      </div>
      <div class="form-group">
        <label>to </label>
        <input type="text" id="datepicker2" class="form-control audit-date-end" />
      </div>
      <div class="form-group">
        <button class="btn btn-default btn-date-of-audit">Show</button>
        <button class="btn btn-primary  unfiltered">Show All</button>
      </div>
    </span>
</div>
<div class="btn-navigation menu-btn col-md-12 pad-0" >
  <button class="article_list btn-min btn btn-default btn-sm bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CLOSE</button>
</div>
<div class="row">
  <div class="col-md-12" id="list_data">
    <div class="table-responsive">
      <table class = "table listdata table-striped" style="margin-bottom:0px;">
          <thead>
            <tr>
            <th>Audit Report Number</th>
            <th>Site Name</th>
            <th>Audit Date</th>
            <th>GMP Rating</th>
            <th>% Coverage</th>
            <th style="text-align: right;">Actions</th>
          </tr>  
          </thead>
          <tbody class="table_body">
          </tbody>
      </table>
    </div>
    <div style="text-align: center; margin-top: 20px;"  class="form-inline pager_div"></div>
  </div>
  <div class="col-md-12 content-container" id="form_data">
  </div>
</div>
<script>

  $(function(){
    $("#datepicker1").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#datepicker2").datepicker({ dateFormat: 'yy-mm-dd' });
  });

  var limit = '10';
  var offset = '0';
  var query = ""

  $(document).ready(function() {
    get_list("",limit,offset);
    get_pagination("");

  });


  $(document).on('click', '.btn-audited-site', function() {
    query = "site_name like '%" + $('.filter-audited-site').val() + "%'";
    get_list(query,limit,offset);
    get_pagination(query);
  });

  $(document).on('click', '.btn-report-no', function() {
    query = "report_no like '%" + $('.filter-report-no').val() + "%'";
    get_list(query,limit,offset);
    get_pagination(query);
  });

  $(document).on('click', '.btn-date-of-audit', function() {
    query = "first_audit_date >= '" + $('.audit-date-start').val() + "' AND last_audit_date <='" + $('.audit-date-end').val() + "'";
    get_list(query,limit,offset);
    get_pagination(query);
  });


  $(document).on('click', '.unfiltered', function() {
    query  = "";
    get_list("",limit,offset);
    get_pagination("");
  });


  $(document).on('change', '.filter', function() {
    var selected = $(this).val();
    console.log(selected);
    $('.filtering').hide();
    $('#' + selected).show();
  });

  <?php $this->load->view('layout/listing_function');?>

  function isNumber(number)
  {
    if(isNaN(number)){
      return number;
    } else {
      return 0;
    }
  }

  function get_list(query, limit, offset){
    isLoading(true);
    $(".content-container").html("");
    
    aJax.post(
      "<?=base_url('Audit_report_analysis/get_list_report');?>",
      {
        'limit':limit, 
        'offset':offset, 
        'query':query
      },
      function(data){
        var obj = isJson(data);
        var table_body = new TBody(); //please check helper.js for this function
        if(obj.length!=0){
          $.each(obj, function(index, row){ 
            table_body.td(row.report_no);
            table_body.td(row.site_name);
            table_body.td(row.audit_date_formatted);
            table_body.td(parseFloat(row.gmp_score) + "%");
            table_body.td(parseFloat(row.gmp_coverage) + "%");
            // table_body.td((parseInt(row.yes_answer_cnt) + parseInt(row.no_answer_cnt)));

            var list_data = new UList();
            list_data.set_liclass("li-action");
            list_data.set_ulclass("ul-action");
            list_data.li("<a class='view_report_analysis action_list action' data-status='' data-id='"+row.report_id+"' title='View Report' style='cursor:pointer;'><span class='glyphicon glyphicon-eye-open'></span></a>");

            table_body.td(list_data.set());
            table_body.set();

          });
        } else {
          table_body.td_norecord(6);
          table_body.set();
        }
        table_body.append(".table_body");
        isLoading(false);

      }
    );
  }

  function get_pagination(query){
    aJax.post(
      "<?=base_url('Audit_report_analysis/get_pagination');?>",
      {
        'query':query
      },
      function(data){
        var no_of_page = Math.ceil(data / limit);
        var pagination = new Pagination(); //please check helper.js for this function
        pagination.set_total_page(no_of_page);
        pagination.set('.pager_div');
      }
    );
  }

  $(document).on('click', '.view_report_analysis', function(){
    isLoading(true);
    var id = $(this).attr('data-id');
    aJax.post(
      "<?=base_url('global_controller/action_global');?>",
      {
        id:id, 
        module:'audit_report_analysis', 
        type:'view'
      },
      function(data){
        $('.content-container').html(data);
        $('html, body').animate({
          scrollTop: $(".content-container").offset().top
        }, 1000);
        isLoading(false);
      }
    );
  });

</script>