<?php
  $table = "tbl_auditor_info";
  $order_by = "auditor_id";
?>
<div class="panel-heading">
    <h4 class="panel-title">
        Add Auditor
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">First Name *: </label>
            <div class="col-sm-6">
                <input type = "text" id="fname" class = "auditor inputcss form-control fullwidth" >
                <span class = "er-msg">First name should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Middle Name *: </label>
            <div class="col-sm-6">
                <input type = "text" id="mname" class = "auditor inputcss form-control fullwidth" >
                <span class = "er-msg">Middle name should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Last Name *: </label>
            <div class="col-sm-6">
                <input type = "text" id="lname" class = "auditor inputcss form-control fullwidth" >
                <span class = "er-msg">Last name should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Designation *: </label>
            <div class="col-sm-6">
                <input type = "text" id="designation" class = "auditor inputcss form-control fullwidth" >
                <span class = "er-msg">Designation should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Department/Division *: </label>
            <div class="col-sm-6">
                <input type = "text" id="division" class = "auditor inputcss form-control fullwidth" >
                <span class = "er-msg">Department/Division should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Company *: </label>
            <div class="col-sm-6">
                <input type = "text" id="company" class = "auditor inputcss form-control fullwidth" >
                <span class = "er-msg">Company should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Email *: </label>
            <div class="col-sm-6">
                <input type = "text" id="email" class = "auditor inputcss form-control fullwidth" >
                <span class = "er-msg er-msg-email">Email should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"></label>
            <div class="col-sm-10">
                <input type="checkbox" id="administrator" name="administrator" value=""> SAMS Administrator
                <input style="margin-left: 10px;" type="checkbox" id="department_head" value=""> Acting Department Head
                <input style="margin-left: 10px;" type="checkbox" id="division_head" value=""> Acting Division Head
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>


<script type="text/javascript">

$(document).ready(function(){
  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";

  $('.save').off('click').on('click', function() {

    var fname = $('#fname').val();
    var mname = $('#mname').val();
    var lname = $('#lname').val();
    var designation = $('#designation').val();
    var division = $('#division').val();
    var company = $('#company').val();
    var email = $('#email').val();
    var administrator = 0;
    var act_reviewer = 0;
    var act_approver = 0;

    if($('#administrator').is(':checked')){
      var administrator = 1;
    }

    if($('#department_head').is(':checked')){
      var act_reviewer = 1;
    }

    if($('#division_head').is(':checked')){
      var act_approver = 1;
    }

    $('.er-msg-email').html("Email should not be empty *");
    $('#email').css('border-color','#ccc');

    if(validateFields('.auditor') == 0){
      if(isValidEmailAddress(email)){
        var no_click = 0;
        confirm("Are you sure you want to save this record?",function(result){
          if(result){
            no_click ++;
            if(no_click <= 1){
                isduplicate("qv_approvr_reviewer_auditor", "email = '" + email + "' AND status >= 0", function(result){
                  if(result == "true"){
                    $("#email").css('border-color','red');
                    $("#email").next().html("This field should contain a unique value.");
                    $("#email").next().show();
                  } else {
                    isLoading(true);
                    $(this).prop('disabled',true);
                    aJax.post(
                      "<?=base_url('global_controller/user_array');?>",
                      {
                        fname:fname,
                        mname:mname,
                        lname:lname,
                        designation: designation,
                        division:division,
                        company:company,
                        email:email,
                        // signature:signature,
                        table:table,
                        field:field,
                        administrator:administrator,
                        reviewer:act_reviewer,
                        approver:act_approver,
                        status:'1',
                        type:'auditor',
                        status:'1',
                        action: 'save'
                      },
                      function(data){
                        isLoading(false);
                        $('.update').prop('disabled',false);
                        update_config();
                        insert_audit_trail("Create " + fname + " " + lname);
                        updateAPI("auditor");
                        bootbox.alert('<b>Record is successfully saved!</b>', function() {
                          location.reload();
                        });

                      }
                    );
                  }
                });
            }
          }
        });
      } else {
          $('.er-msg-email').show();
          $('#email').css('border-color','red');
          $('.er-msg-email').html("Invalid email.");
      }
    }
  });
});
</script>