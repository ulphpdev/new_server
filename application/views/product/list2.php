<?php 
  $table = "tbl_product";
  $order_by = "product_id";
?>

<ol class="breadcrumb" style="background-color: #fff;">
    <li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
    <li>Data Maintenance</li>
    <li class="active">Products/Materials</li>
</ol>

<div class="col-md-4">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Product Name or Supplier Name" id="txt_search" placeholder="">
    <span class="er-msg file-err">This field is required</span>
    <div class="input-group-btn">
      <button class="btn btn-primary" id="btn_search" type="submit">
          <i class="glyphicon glyphicon-search"> </i> 
          Search
      </button>
    </div>
  </div>
</div>

<div class="btn-navigation menu-btn col-md-8 pad-0" >
  <button data-type = "Delete" class="trash inactive btn-min btn btn-default  bold" style="display: none;"><span class = "glyphicon glyphicon-trash"></span> Delete</button> 
  <button data-type = "Activate" class="inactive btn-min btn btn-default   bold"><span class = "glyphicon glyphicon-ok"></span> Activate</button>
  <button data-type = "Deactivate" class="inactive btn-min btn btn-default   bold"><span class = "glyphicon glyphicon-remove-circle"></span> Deactivate</button>
  <button class="add btn-min btn btn-success   bold"><span class = "glyphicon glyphicon-plus"></span> ADD PRODUCT/MATERIAL <span></button>
  <button class="article_list btn-min btn btn-default   bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CLOSE</button>
</div>

<div class="row">
  <div class="col-md-12" id="list_data">
    <div class="table-responsive">

      <!-- LIST -->
      <table class = "table listdata table-striped" style="margin-bottom:0px;">
        <thead>
          <tr>
          <th style = "width:10px;"><input class = "selectall" type = "checkbox"></th>
          <th>Supplier</th>
          <th>Product Name</th>
          <th>Date &amp; Time Modified</th>
          <th>Status</th>
          <th style="text-align: right;">Actions</th>
        </tr>  
        </thead>
        <tbody></tbody>
      </table>
    </div>

    <!-- PAGINATION -->
    <div style="text-align: center; margin-top: 20px;" class="form-inline pager_div"></div>
  </div>
</div>

<!-- FORM -->
<div class="content-container" style="margin-top: 30px;"></div>


<script>
$(document).ready(function() {
  <?php $this->load->view('layout/listing_function');?>
  var limit = '10';
  var offset = '0';
  var query = "";

  get_list("",limit,offset);
  get_pagination("");

  function get_list(query,limit,offset){
    $('.cancel').hide();
    $('.inactive').hide();
    $('.selectall').attr('checked', false);
    var table = "<?=$table;?>";
    var order_by = "<?=$order_by;?>";
    isLoading(true);
    aJax.post(
          "<?=base_url('global_controller/getlist_dm');?>",
          {
              'limit':limit, 
              'offset':offset, 
              'query':query, 
              'table':'qv_product', 
          },
          function(result){
              var obj = JSON.parse(result);
              var table_body = new TBody(); //please check helper.js for this function
              if(obj.length!=0){  
                $.each(obj,function(key, row){

                  var inuse = 0;
                  check_inuse("product = " + row.product_id, function(result){
                      inuse = result
                  });

                    if(row.status == 1){
                      var status = "Active";
                    } else {
                      var status = "Inactive";
                    }
                    if(inuse > 0){
                        table_body.td("");
                    } else {
                        table_body.td("<input class = 'select check_"+row.product_id+"' data-id = '"+row.product_id+"' data-name='"+row.product_name+"' type ='checkbox'>");
                    }

                    table_body.td(row.name);
                    table_body.td(row.product_name);
                    table_body.td(moment(row.update_date).format("LLL"));
                    table_body.td(status);
                    var action_button = new UList(); //please check helper.js for this function
                    action_button.set_liclass("li-action");
                    action_button.set_ulclass("ul-action");
                    if(inuse > 0){
                        action_button.li("");
                    } else {
                        action_button.li("<a class='delete_data action_list action action_"+row.product_id+"' data-status='' id='"+row.product_id+"' data-name='"+row.product_name+"' title='Delete'> <span class='glyphicon glyphicon-trash'></span></a>");
                    }
                    action_button.li("<a class='edit action_list action ' data-status='' id='"+row.product_id+"' title='Edit'><span class='glyphicon glyphicon-pencil'></span></a>");
                    table_body.td(action_button.set());
                    table_body.set();

                })
              } else {
                table_body.td_norecord(7);
                table_body.set();
              }
              table_body.append('.listdata tbody');
              isLoading(false);
          }
      );
  }
  function get_pagination(query){
    aJax.post(
        "<?=base_url("global_controller/getlist_dm");?>",
        {
          'limit':9999, 
          'offset':offset, 
          'query':query, 
          'table':'qv_product', 
        },
        function(result){
          var obj = JSON.parse(result);
          var result_count = obj.length;
          var no_of_page = Math.ceil(result_count / limit);
          var pagination = new Pagination(); //helper.js
          pagination.set_total_page(no_of_page);
          pagination.set('.pager_div');
        }
    );
  }

  $(document).on('keypress', '#txt_search', function(e){
    if (e.which == 13) { 
      $('#btn_search').click()
    }
  });

  $(document).on('click', '#btn_search', function(){
    var keyword = $('#txt_search').val();
    if(keyword.trim() == ""){
      query = "";
      get_list(query,limit,offset);
      get_pagination(query);
    } else {
      query = "product_name like '%" + keyword + "%' OR name like '%" + keyword + "%'";
      get_list(query,limit,offset);
      get_pagination(query);
    }
  });

  $(document).on('click', '.add', function(){
    module_action('product', 'add', '.content-container', '');
  });

  $(document).on('click', '.edit', function(){
    var id = $(this).attr('id');
    module_action('product', 'edit', '.content-container', id);
  });

  $(document).on('click', '.cancel', function(){
    module_action('product', 'cancel', '.content-container', '');
    get_list(query,limit,offset);
    get_pagination(query);
  });

  $(document).on('click', '.inactive', function(){
    var x = 0;
    var data_type = $(this).attr('data-type').toLowerCase();
    $('.select').each(function() {                
      if (this.checked==true) {  x++;   } 
    });

    if (x > 0 ) {
      change_status_modal(
        "Are you sure you want to "+data_type+" selected record?",
        data_type,
        function(result){
          if(result){
            isLoading(true);
            setTimeout(function() {
              update_satus(data_type);
            }, 1000);
          }
        }
      );
    }

  });



  $(document).on('click', '.delete_data', function(){
    var x = 0;
    var id = $(this).attr('id');
    var name = $(this).attr('data-name');
    var table = "tbl_product";
    var order_by = "product_id";
    var dialog = bootbox.confirm({
      message: "Are you sure you want to delete this record?",
      buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
              label: 'No',
              className: 'btn-danger'
          }
      },
      callback: function (result) {
        if(result){
          aJax.post(
            "<?=base_url('global_controller/inactive_global_update');?>",
            {
              id:id,
              type: -2, 
              table:table, 
              order_by: order_by
            },
            function(data){
              updateAPI("product");
              insert_audit_trail("Delete " + name);
              get_list(query, limit, offset);
              get_pagination(query);
              bootbox.alert("Successfully deleted record.");
              update_config();
              updateAPI("product");
            }
          );
        }          
      }
    });
  });

  function update_satus(type){
    var x = 0;
    var trail_label = "";
    var message = "";
    if (type == 'delete') {
      var type = '-2';
      trail_label = "Delete ";
      message = "<b>Successfully deleted records.</b>";
    }
    if (type == 'deactivate') {
      var type = '0';
      trail_label = "Deactivate ";
      message = "<b>Successfully deactivate records.</b>";
    }
    if (type == 'activate') {
      var type = '1';
      trail_label = "Activate ";
      message = "<b>Successfully activate records. </b>";
    }

    $('.select').each(function() {                
      if (this.checked==true) {
        var table = "<?=$table;?>";
        var order_by = "<?=$order_by;?>";
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');

        aJax.postasync(
          "<?=base_url('global_controller/inactive_global_update');?>",
          {
            id:id,
            type:type, 
            table:table, 
            order_by: order_by
          },
          function(data){
            updateAPI("product");
            insert_audit_trail(trail_label + name);
          }
        );
        x++; 
      } 
    });


    isLoading(false);
    bootbox.alert(message, function(){
      location.reload();
    });
  }
});
</script>