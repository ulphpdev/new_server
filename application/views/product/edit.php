<?php
  $table = "tbl_product";
  $order_by = "product_id";
?>

<div class="panel-heading">
    <h4 class="panel-title">
        Edit Product
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Name of Site *: </label>
            <div class="col-sm-6">
                <select id="name_site" class="inputcss form-control fullwidth inputs_edit_productmaterial"></select>
                <span class = "er-msg">Name of site should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Name *: </label>
            <div class="col-sm-6">
                <input type = "text" id="name" class = "inputcss form-control fullwidth inputs_edit_productmaterial" >
                <span class = "er-msg">Name should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">
$(document).ready(function(){

  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";
  var id = "<?= $id; ?>";
  var current_product = "";
  var current_site ="";
  get_company();

  function on_load(){
    var limit = 1;
    aJax.post(
      "<?=base_url('product/edit_product');?>",
      {
        id:id,
        limit:limit,
        table:table,
        field:field
      },
      function(data){
        var obj = JSON.parse(data);
        $.each(obj, function(index, row){
          $('#name_site option[value="'+row.company_id+'"]').attr('selected','selected');
          $('#name').val(row.product_name);
          current_product = row.product_name;
          current_site = row.company_id;
          // $('#type option[value="'+row.type+'"]').attr('selected','selected');
        });
      }
    );
  }

  function get_company(){
    $.ajax({
      type: 'Post',
      url:'<?=base_url();?>global_controller/getlist_global_dm',
      data:{limit:'9999', offset:'1',table:'tbl_company', order_by:'company_id'},
    }).done( function(data){
        var obj = JSON.parse(data);
        var htm = '';
        htm += '<option value="">--Select--</option>';
        $.each(obj, function(index, row){
            htm += '<option value="'+row.company_id+'">'+row.name+'</option>';
        });
        $('#name_site').html(htm);
        on_load();
      });
  }

  $('.update').off('click').on('click', function() {
    var name_site = $('#name_site').val();
    var name = $('#name').val();
    var type = $('#type').val();
    if(validateFields('.inputs_edit_productmaterial') == 0){
      var no_click = 0;
      confirm("Are you sure you want to update this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
              isduplicate("tbl_product", "company_id=" + name_site + " AND product_name = '" + name + "' AND product_name <> '"+current_product+"' AND status >= 0", function(result){
                if(result == "true"){
                  $("#name").css('border-color','red');
                  $("#name").next().html("This field should contain a unique value.");
                  $("#name").next().show();
                } else {
                  isLoading(false);
                  $(this).prop('disabled',true);
                  aJax.post(
                    "<?=base_url('product/product_array');?>",
                    {
                      id:id,
                      table:table,
                      field:field,
                      name_site:name_site,
                      name:name,
                      action: 'update'
                    },
                    function(data){
                      isLoading(false);
                      $('.update').prop('disabled',false);
                      updateAPI("product");
                      update_config();
                      insert_audit_trail("Update " + name);
                      bootbox.alert('<b>Record is successfully updated!</b>', function() {
                        location.reload();
                      });

                    }
                  );
                }
              });
          }
        }
      });
    }
  });
});
</script>