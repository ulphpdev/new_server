<div class="report_view">
</div>
<script type="text/javascript">
$(document).ready(function(){
	var id = <?= $_GET['report_id'];?>;
	$.ajax({
	type: 'Post',
	url:'<?=base_url();?>c_auditreport/audit_report_view',
	data:{id:id, div : 'remarks-reviewer'},
	}).done( function(data){
	  	$('.report_view').html(data);
	})
});

//submit remarks
$(document).on('click','.remarks_submit', function(e){
   e.preventDefault();
   	var remarks = $('#txt_remarks').val();
   	var id = "<?=$_GET['report_id']?>"
   	var newdate = new Date();
   	var create_date = moment(newdate).format('YYYY-MM-DD hh:mm:ss');
   	var reviewer_id = "<?=  $this->session->userdata('userid') ?>"
   	var type = 'reviewer';
   	var data =  {'id':id,'remarks':remarks,'create_date':create_date,'reviewer_id':reviewer_id,'type':type};
	$.ajax({
	    url:'<?php echo base_url("Audit_report_link/add_new_remarks_departmenthead"); ?>',
	    type: 'POST',
	    data: data,
	}).done(function(data){

		$.ajax({
       		type: 'Post',
	        url:'<?=base_url("Audit_report_link/add_report_submission_status");?>',
	        data:{report_id:id, remarks : remarks, status:"Approved",position:"Department Head"},
        }).done(function(data){

    		$.ajax({
		      	type: 'Post',
		      	url: "http://sams.ecomqa.com/api/audit_report_list",
		    }).done( function(data){

		    });

	        $.ajax({
	            type: 'Post',
	            url:'<?=base_url("smtp/send/email_to_co_auditor_approve_from_reviewer");?>',
	            data:{report_id:id, remarks : "Approved : " + remarks},
            }).done(function(data){
            	console.log(data);
            });

	        $.ajax({ 
	            type: 'Post',
	            url:'<?=base_url("smtp/send/email_to_division_head");?>',
	            data:{report_id:id},
	        }).done(function(data){
	            	console.log(data);
	        });

	        aJax.get(
	        	"<?=base_url("api/generate_audit_report_json");?>/" + id,
	        	function()
	        	{
	        		console.log("audit report updated");
	        	}
	        );

		    aJax.post(
		    	"<?=base_url("Audit_report_link/signature_stamp");?>",
		    	{
		    		'report_id' : id,
		    		'field' : "review_date"
		    	},
		    	function(data){
		    		console.log("Timestamp Recorded");
		    		bootbox.alert("Successfully approved!", function(){
						location.reload();
					});
		    	}
		    );
        
       	 	update_audit_report_json();
       	});
	});
	
});

$(document).on('click','.reject_submit', function(e){
   	e.preventDefault();
   	var remarks = $('#txt_remarks').val();

   	if(remarks == ""){
   		bootbox.alert("Remarks field is required", function(){});
   	} else {
	   	var id = "<?=$_GET['report_id']?>"
	   	var newdate = new Date();
	   	var create_date = moment(newdate).format('YYYY-MM-DD hh:mm:ss');
	   	var reviewer_id = "<?=  $this->session->userdata('userid') ?>"
	   	var type = 'reviewer';
	   	var data =  {'id':id,'remarks':remarks,'create_date':create_date,'reviewer_id':reviewer_id,'type':type};
		$.ajax({
		    url:'<?php echo base_url("Audit_report_link/reject_remarks_departmenthead"); ?>',
		    type: 'POST',
		    data: data,
		}).done(function(data){

			$.ajax({
	            type: 'Post',
	            url:'<?=base_url("Audit_report_link/add_report_submission_status");?>',
	            data:{report_id:id, remarks : remarks, status:"",position:"Department Head"},
	            }).done(function(data){


	            });

			$.ajax({
	            type: 'Post',
	            url:'<?=base_url("smtp/send/email_to_co_auditor_rejected");?>',
	            data:{report_id:id, remarks : remarks},
	            }).done(function(data){
	            	console.log(data);
	            	bootbox.alert("Successfully sent an email to Lead Auditor.", function(){
						location.reload();
					});
	            });

		});
	}
});

</script> 