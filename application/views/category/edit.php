<?php
  $table = "tbl_question_category";
  $order_by = "create_date";
?>

<div class="panel-heading">
    <h4 class="panel-title">
        Edit Category
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Category Name *: </label>
            <div class="col-sm-6">
                <input type = "text" id="txt-name" class = "inputcss form-control fullwidth inputs_edit_category" >
                <span class = "er-msg"> should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>


<script type="text/javascript">
$(document).ready(function(){
  var table = "<?=$table;?>";
  var field = "category_id";
  var id = "<?= $id; ?>";
  var current_category = "";
  on_load();

  function on_load(){
    var limit = 1;
    aJax.post(
      "<?=base_url('global_controller/edit_global');?>",
      {
        id:id,
        limit:limit,
        table:table,
        field:field
      },
      function(data){
        var obj = JSON.parse(data);
        $.each(obj, function(index, row){
          $('#txt-name').val(row.category_name);
          current_category = row.category_name;
        });
      }
    );
  }


  $('.update').off('click').on('click', function() {
    var catname = $('#txt-name').val();
    var get_time = func_time();
    if(validateFields('.inputs_edit_category') == 0){
      var no_click = 0;
      confirm("Are you sure you want to update this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_question_category", "category_name = '" + catname + "' AND category_name <> '"+current_category+"' AND status >= 0", function(result){
              if(result == "true"){
                $(".inputs_edit_category").css('border-color','red');
                $(".inputs_edit_category").next().html("This field should contain a unique value.");
                $(".inputs_edit_category").next().show();
              } else {
                $(this).prop('disabled',true);
                isLoading(true);
                $.ajax({
                  type: 'Post',
                  url:'<?=base_url();?>global_controller/save_category',
                  data:{id:id,table:table, field:'category_id', catname:catname, get_time: get_time, action: 'update'},
                }).done( function(data){
                  isLoading(false);
                  $('.update').prop('disabled',false);
                  updateAPI("question_category");
                  insert_audit_trail("Update " + catname);
                  update_config();
                  bootbox.alert('<b>Record is successfully updated!</b>', function() {
                    location.reload();
                  });
                });
              }
            });
          }
        }
      });
    }
  });

  function func_time(){
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var get_time = date+' '+time;
    return get_time;
  }
})
</script>