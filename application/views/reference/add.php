<?php 
  $table = "tbl_template";
  $order_by = "create_date";
?>

<div class="col-md-12" id="form-content">

<!-- <div class="col-md-12 pad-5">
   <div class="col-md-2">Name of Site*: </div>
    <div class = "col-md-4"><input type = "text" id="temp_name" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg"> should not be empty *</span></div>
</div> -->
<!-- <div class="col-md-12 pad-5">
    <div class="col-md-2 bold">Name of Site*: </div>
     <div class = "col-md-4">
          <select id="designation" class="inputcss_blue type_event form-control">
          <option value="0" class="">Please Select</option>      
          </select>
     </div>
</div>-->

<div class="col-md-12 pad-5">
    <div class="col-md-2 bold">Name of Site*: </div>
     <div class = "col-md-4">
          <select id="company" class="inputcss_blue type_event form-control">
          <!-- <option value="0" class="">Please Select</option>    -->   
          </select>
     </div>
</div>


<div class="col-md-12 pad-5">
   <div class="col-md-2">Licenses/Certification*: </div>
    <div class = "col-md-4"><input type = "text" id="txt-license" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg"> should not be empty *</span></div>
</div>


<div class="col-md-12 pad-5">
   <div class="col-md-2">Issuing Regulatory/Certification*: </div>
    <div class = "col-md-4"><input type = "text" id="txt-regu" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg"> should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
   <div class="col-md-2">License/Certificate Number*: </div>
    <div class = "col-md-4"><input type = "text" id="txt-cert" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg"> should not be empty *</span></div>
</div>

<div class="col-md-12 pad-5">
   <div class="col-md-2">Validity*: </div>
    <div class = "col-md-4"><input type = "text" id="txt-vali" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg"> should not be empty *</span></div>
</div>

<script type="text/javascript">
$(document).ready(function(){

var table = "<?=$table;?>";
var field = "<?=$order_by;?>";

/* Start of update */



get_data('company','null','null','');

 $(document).on('click', '.save', function(){

  
  var company = $('#company').val();
  var txt_license = $('#txt-license').val();
  var txt_reg = $('#txt-regu').val();
  var txt_cert = $('#txt_cert').val();
  var txt_vali = $('#txt-vali').val();
 
  // $('.er-msg-email').html("Email should not be empty *");
  // $('#email').css('border-color','#ccc');
  if(validateFields() == 0){
  	// if(isValidEmailAddress(email)){
    $(this).prop('disabled',true);
	    $('.loader_gif').show();
	    $.ajax({

	            type: 'Post',
	            url:'<?=base_url();?>global_controller/save_ref',
	            data:{company:company,txt_license:txt_license,txt_reg:txt_reg, txt_cert: txt_cert, txt_vali:txt_vali, action: 'save'},
	          }).done( function(data){
	            $('.loader_gif').hide();
	            $('.update').prop('disabled',false);
	            bootbox.alert('<b>Successfully Saved.</b>', function() {
                location.reload();
              }).off("shown.bs.modal");
	       


	    }); 
		// } else {
		//   	$('.er-msg-email').show();
		//   	$('#email').css('border-color','red');
		//   	$('.er-msg-email').html("Invalid email.");
		// }

  	}


 });

/* End of update */

/* Start of Validation */


function get_data(table,field,id,selected){
  $.ajax({
    type: 'post',
    url: '<?=base_url();?>global_controller/get_data',        
    data:  { 'table': table, 'field' : field, 'id' : id } ,
  }).done(function(data){       
    var callback = JSON.parse(data);    
    var opt = '';
    opt += '<option value="" selected hidden>--SELECT '+table.toUpperCase()+'--</option>'
    $.each(callback, function(x,y) {
        opt += "<option value='"+y.company_id+"'>"+y.name+"</option>";
        
    });
    $('#'+table).html(opt);
  });
}

function validateFields() {

    var counter = 0;
    $('.inputs').each(function(){
          var input = $(this).val();
          if (input.length == 0) {
          	$(this).css('border-color','red');
            $(this).next().show();
            counter++;
          }else{
          	$(this).css('border-color','#ccc');
            $(this).next().hide();
          }
      });

    if($('#administrator').is(':checked')){
          	$('.er-msg-admin').hide();
    } else {
    	    $('.er-msg-admin').show();
    	    counter++;
    }

    return counter;
}

function isValidEmailAddress(email) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(email);
};

/* End of Validation */

/* start file manager */
$('.btn-select-image').click(function(){
  $('.c-copy').hide();
  filemanager('');
  $('#filemanager-image-modal').modal('show');
  $('.btn-copy-file').attr('attr-src','imgsrc_image');
});
$(document).on('click','.btn-copy-file', function(){
  var src = $('.check_active').attr('file-name');
  var old_src = $('.imgsrc').val();
  var type = $(this).siblings('.pathtocopy').attr('data-type'); 
  var path = $(this).attr('attr-src');
  ext = src.split('.').pop();
  ext = ext.toLowerCase();
 	if(type == 'file'){
		bootbox.alert('Please select image only.');
		if(old_src){
  		$('.selected-image').html(' <img class="imgsrc_image img-responsive" src="<?= base_url() ?>'+old_src+'" width="100%" /><input type="text"  class = "imgsrc inputs none filename" name="image"  value="'+old_src+'" hidden/>');
  		}
	 }else{
	  $('.selected-image').html(' <img class="imgsrc_image img-responsive" src="<?= base_url() ?>'+src+'" width="100%" /><input type="text"  class = "imgsrc inputs none filename" name="image"  value="'+src+'" hidden/>');
	 }
});
function filemanager(folder){
  $.ajax({
    type: 'Post',
	url: "<?=base_url()?>global_controller/getimages",
    data:{folder:folder},
  }).done( function(data){
    $('.file-manager').html(data);
  }); 
}
/* End of file manager */




})	

</script>