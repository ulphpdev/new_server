<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a>
  </li>
  <li class="breadcrumb-item active">Dashboard</li>
</ol>
<div class="row">

  <div class="col-md-6">
    <a href="<?= base_url().'auditreport'; ?>" style="text-decoration: none;">
      <div class="col-md-12 dashboard_card" style="background-image: url('./asset/img/icon/dashboard_bg.png');background-size: cover;">
          <div class="dashboard-font"> 
            Audit Reports<br /><?= $audit_reports_cnt; ?>
          </div>
          <div class="div-holder-menu">
            <img class="dashboard_img" src="./asset/img/icon/dashboard_report.png" height="200" style="padding: 30px;">
          </div>
      </div>
    </a>
  </div>
  <div class="col-md-6">
    <a href="<?= base_url().'manage_template'; ?>" style="text-decoration: none;">
      <div class="col-md-12 dashboard_card" style="background-image: url('./asset/img/icon/dashboard_bg.png');background-size: cover;">
          <div class="dashboard-font">
            Templates<br /><?= $templates_cnt; ?>
          </div>
          <div class="div-holder-menu">
            <img class="dashboard_img" src="./asset/img/icon/dashboard_notepad.png" height="200" style="padding: 30px; ">
          </div>
      </div>
    </a>
  </div> 

  <div class="col-md-6">
      <a href="<?= base_url('audit_report_analysis'); ?>" style="text-decoration: none;">
        <div class="col-md-12 dashboard_card" style="background-image: url('./asset/img/icon/dashboard_bg.png');background-size: cover;">
            <div class="dashboard-font">
              Audit Report Analysis<br /><?= $audit_report_analysis; ?>
            </div>
            <div class="div-holder-menu"> 
              <img class="dashboard_img" src="./asset/img/icon/dashboard_analysis.png" height="200" style="padding: 30px;">
            </div>
        </div>
      </a>
  </div>

  <div class="clearfix"></div>
</div>


    

<style type="text/css">
.dash_pic {
    height: 200px;
    width: 100%;
    margin-bottom: 15px;
}
.dash_title {
    padding: 10px 0px 10px 0px;
    text-align: center;
    position: absolute;
    top: 60px;
    text-transform: uppercase;
    color: #fff;
    font-weight: bold;
    font-size: 13pt;
    cursor: pointer;
}

</style>


<script type="text/javascript">
  
  var server = window.location.hostname;
  console.log(server);
</script>