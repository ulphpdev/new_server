<?php
$table = "tbl_standard_reference";
$order_by = "standard_id";
?>

<div class="panel-heading">
    <h4 class="panel-title">
        Edit Standard / Reference
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Standard/Reference *: </label>
            <div class="col-sm-10">
                <textarea id="txt-name" style="width: 100%;" class = "inputcss form-control fullwidth inputs_edit_standard" rows=3></textarea>
                <span class = "er-msg">Reference should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">

$(document).ready(function(){

  var table = "<?=$table;?>";
  var field = "<?=$order_by; ?>";
  var id = "<?= $id; ?>";
  var limit = 1;
  var current_standard = "";
  on_load();

  $("textarea").keydown(function(e){
    if (e.keyCode == 13 && !e.shiftKey){
      e.preventDefault();
    }
  });

  function on_load(){
    aJax.post(
      "<?=base_url('global_controller/edit_global');?>",
      {
        id:id,
        limit:limit,
        table:table,
        field:field
      },
      function(data){
        var obj = JSON.parse(data);
        $.each(obj, function(index, row){
          $('#txt-name').val(row.standard_name);
          current_standard = row.standard_name;
        });
      }
    );
  }

  $('.update').off('click').on('click', function() {
    var catname = $('#txt-name').val();
    var get_time = func_time();

    if(validateFields('.inputs_edit_standard') == 0){
      var no_click = 0;
      confirm("Are you sure you want to update this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_standard_reference", "standard_name = '" + catname + "' AND standard_name <> '"+current_standard+"' AND status >= 0", function(result){
              if(result == "true"){
                $(".inputs_edit_standard").css('border-color','red');
                $(".inputs_edit_standard").next().html("This field should contain a unique value.");
                $(".inputs_edit_standard").next().show();
              } else {
                $(this).prop('disabled',true);
                isLoading(true);
                aJax.post(
                  "<?=base_url('global_controller/save_temp');?>",
                  {
                    table:table,
                    field:'standard_id',
                    id:id,
                    catname:catname,
                    get_time: get_time,
                    action: 'update'
                  },
                  function(data){
                    isLoading(false);
                    updateAPI("standard_reference");
                    update_config();
                    insert_audit_trail("Update " + catname);
                    bootbox.alert('<b>Record is successfully updated!</b>', function() {
                      location.reload();
                    });
                  }
                );
              }
            });
          }
        }
      });
    }
  });

  function func_time(){
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var get_time = date+' '+time;
    return get_time;
  }
});

</script>