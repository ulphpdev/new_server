<?php
  $table = "tbl_standard_reference";
  $order_by = "standard_id";
?>

<div class="panel-heading">
    <h4 class="panel-title">
        Add Standard / Reference
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Standard/Reference *: </label>
            <div class="col-sm-10">
                <textarea id="txt-name" style="width: 100%;" class = "inputcss form-control fullwidth inputss" rows=3></textarea>
                <span class = "er-msg">Standard/Reference should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">

$(document).ready(function(){

  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";
  get_data('classification','null','null','');

  $("textarea").keydown(function(e){
    if (e.keyCode == 13 && !e.shiftKey){
      e.preventDefault();
    }
  });


  $('.save').off('click').on('click', function() {
    var catname = $('#txt-name').val();
    var get_time = func_time();

    if(validateFields('.inputss') == 0){
      var no_click = 0;
      confirm("Are you sure you want to save this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_standard_reference", "standard_name = '" + catname + "' AND status >= 0", function(result){
              if(result == "true"){
                $(".inputss").css('border-color','red');
                $(".inputss").next().html("This field should contain a unique value.");
                $(".inputss").next().show();
              } else {
                $(this).prop('disabled',true);
                isLoading(true);
                aJax.post(
                  "<?=base_url('global_controller/save_temp');?>",
                  {
                    table:table,
                    catname:catname,
                    field:field,
                    get_time: get_time,
                    action: 'save'
                  },
                  function(data){
                    isLoading(false);
                    $('.update').prop('disabled',false);

                    updateAPI("standard_reference");
                    update_config();
                    insert_audit_trail("Create " + catname);
                    bootbox.alert('<b>Record is successfully saved!</b>', function() {
                      location.reload();
                    }).off("shown.bs.modal");
                  }
                );
              }
            });
          }
        }
      });
    }
  });


  function func_time(){
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var get_time = date+' '+time;
    return get_time;
  }

  function get_data(table,field,id,selected){
    $.ajax({
      type: 'post',
      url: '<?=base_url();?>global_controller/get_data',
      data:  { 'table': table, 'field' : field, 'id' : id } ,
    }).done(function(data){
      var callback = JSON.parse(data);
      var opt = '';
      opt += '<option value="" selected hidden>--Select--</option>'
      $.each(callback, function(x,y) {
          opt += "<option value='"+y.classification_id+"'>"+y.classification_name+"</option>";
      });
      $('#'+table).html(opt);
    });
  }

})



</script>