<style type="text/css">
	
	table {
	    border-collapse: collapse;
	    width: 100%;
	    font-size: 14px;
        margin-bottom : 20px;
	}

	table, th, td {
	    border: 1px solid black;
	    padding: 3px;
	}

    .label-head {
        background-color: #4b4b4b;
        color: #fff;
    }

</style>

<?php foreach ($logs as $key => $value) {
    $values = json_decode($value->json);
    $action = $value->action;
    $submission_date = $value->submission_date;
    $report_id = $value->report_id;
    $Name = $value->Name;
}

?>
<div style="width: 100%; min-height: 700px; padding: 25px;" class="bg-white">
    <a href="#" class="pull-right" onclick="window.print()">Print</a>
	<table>
        <tbody>
            <tr>
                <td style="width: 40%;" class="label-head">Action</td>
                <td><?= $action;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Date</td>
                <td><?= $submission_date;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Report ID</td>
                <td><?= $report_id;?></td>
            </tr>
            <tr>
                <td style="width:40%;" class="label-head">User</td>
                <td><?= $Name;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Token</td>
                <td><?= $values->token;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Event</td>
                <td><?= $values->cmdEvent;?></td>
            </tr>
        </tbody>
	</table>

    <table>
        <tbody>
            <tr>
                <td style="width: 40%;" class="label-head">Report ID</td>
                <td><?= $values->report_id;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Report No</td>
                <td><?= $values->report_no;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Company ID</td>
                <td><?= $values->company_id;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Other Activities</td>
                <td><?= $values->other_activities;?></td>
            </tr>
            <?php
                
                $json = stripslashes(htmlspecialchars($values->audit_date));
                $json = str_replace(array("[","]","{","}"), '', $json);
                $json = str_replace(array('&quot;', '"'), '', $json); 
                $audit_date_array = explode(",",$json);
                $auditcount = 0;
                foreach ($audit_date_array as $key => $value) {
                    $auditcount++;
                    echo "<tr>";
                    if($auditcount == 1){
                        echo '<td style="width: 40%;" class="label-head">Audit Dates</td>';
                    } else {
                        echo '<td style="width: 40%;" class="label-head"></td>';
                    }
                    echo '<td>'. date_format(date_create($value),"Y-m-d") .'</td>';
                    echo "</tr>";
                }

            ?>
            <tr>
                <td style="width: 40%;" class="label-head">Template ID</td>
                <td><?= $values->template_id;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Auditor ID</td>
                <td><?= $values->auditor_id;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Reviewer ID</td>
                <td><?= $values->reviewer_id;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Approver ID</td>
                <td><?= $values->approver_id;?></td>
            </tr>
            <?php

                $coauditors = json_decode($values->co_auditor_id);  
                $coauditorscount = 0;
                foreach ($coauditors as $a => $b) {
                    $coauditorscount++;
                    echo "<tr>";
                    if($coauditorscount == 1){
                        echo '<td style="width: 40%;" class="label-head">Co-Auditor ID</td>';
                    } else {
                        echo '<td style="width: 40%;" class="label-head"></td>';
                    }
                    echo '<td>'.$b->auditor_id .'</td>';
                    echo "</tr>";
                }

            ?>

            <?php

            $translator = json_decode($values->translator);  
            $translatorcount = 0;
            foreach ($translator as $a => $b) {
                $translatorcount++;
                echo "<tr>";
                if($translatorcount == 1){
                    echo '<td style="width: 40%;" class="label-head">Transaltor</td>';
                } else {
                    echo '<td style="width: 40%;" class="label-head"></td>';
                }
                echo '<td>'.$b->translator .'</td>';
                echo "</tr>";
            }

            ?>

            <?php
                
                $json = stripslashes(htmlspecialchars($values->other_issues_audit));
                $json = str_replace(array("[","]","{","}"), '', $json);
                $json = str_replace(array('&quot;', '"'), '', $json); 
                $record_array = explode(",",$json);
                $countrecord = 0;
                foreach ($record_array as $key => $value) {
                    $countrecord++;
                    echo "<tr>";
                    if($countrecord == 1){
                        echo '<td style="width: 40%;" class="label-head">Other Issue Audit</td>';
                    } else {
                        echo '<td style="width: 40%;"></td>';
                    }
                    echo '<td>'. $value .'</td>';
                    echo "</tr>";
                }

            ?>

            <?php
                
                $json = stripslashes(htmlspecialchars($values->other_issues_executive));
                $json = str_replace(array("[","]","{","}"), '', $json);
                $json = str_replace(array('&quot;', '"'), '', $json); 
                $record_array = explode(",",$json);
                $countrecord = 0;
                foreach ($record_array as $key => $value) {
                    $countrecord++;
                    echo "<tr>";
                    if($countrecord == 1){
                        echo '<td style="width: 40%;" class="label-head">Other Issue Executive</td>';
                    } else {
                        echo '<td style="width: 40%;"></td>';
                    }
                    echo '<td>'. $value .'</td>';
                    echo "</tr>";
                }

            ?>
            <tr>
                <td style="width: 40%;" class="label-head">Audited Area</td>
                <td><?= $values->audited_areas;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Area to Consider</td>
                <td><?= $values->areas_to_consider;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Wrap up date</td>
                <td><?= $values->wrap_up_date;?></td>
            </tr>

             <?php

            $pre_audit_documents = json_decode($values->pre_audit_documents);  
            $pre_audit_documentscount = 0;
            foreach ($pre_audit_documents as $a => $b) {
                $pre_audit_documentscount++;
                echo "<tr>";
                if($pre_audit_documentscount == 1){
                    echo '<td style="width: 40%;" class="label-head">Pre Audit Documents</td>';
                } else {
                    echo '<td style="width: 40%;"></td>';
                }
                echo '<td>'.$b->document_name .'</td>';
                echo "</tr>";
            }

            ?>
            <tr>
                <td style="width: 40%;" class="label-head">Version</td>
                <td><?= $values->version;?></td>
            </tr>
        </tbody>
	</table>

    <table>
        <thead>
            <tr>
                <th colspan="4">Scope</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $record_array = json_decode($values->scope);  
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td style="width: 40%;" class="label-head">Scope ID</td>';
                    echo '<td colspan="3">'.$b->scope_id .'</td>';
                echo "</tr>";
                echo "<tr>";
                    echo '<td style="width: 40%;" class="label-head">Scope Remarks</td>';
                    echo '<td colspan="3">'.$b->scope_remarks .'</td>';
                echo "</tr>";
            
                foreach ($b->scope_product as $x => $y) {
                    echo "<tr>";
                        echo '<td style="width: 40%;" class="label-head">Product ID</td>';
                        echo '<td>'.$y->product_id .'</td>';
                        echo '<td style="width: 20%;" class="label-head">Disposition ID</td>';
                        echo '<td>'.$y->disposition_id .'</td>';
                    echo "</tr>";
                }
            }

            ?>
        </tbody>
    </table>

    <table>
        <thead>
            <tr>
                <th>Question ID</th>
                <th>Answer ID</th>
                <th>Category ID</th>
                <th>Answer Details</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $record_array = json_decode($values->question);  
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td>'.$b->question_id .'</td>';
                    echo '<td>'.$b->answer_id .'</td>';
                    echo '<td>'.$b->category_id .'</td>';
                    echo '<td>'.$b->answer_details .'</td>';
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

    <table>
        <tbody>
        <?php
            $record_array = json_decode($values->activities);  
            $count = 0;
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td style="width:20%;" class="label-head">Activity ID</td>';
                    echo '<td>'.$b->activity_id .'</td>';
                echo "</tr>";
                foreach ($b->sub_activities as $x => $y) {
                    echo "<tr>";
                    echo '<td style="width:20%;" class="label-head">&nbsp;&nbsp;Sub Activity ID</td>';
                        echo '<td style="width:50%;">'.$y->sub_item_id .'</td>';
                    echo "</tr>";
                }
                    
                
            }

            ?>
        </tbody>
    </table>

     <table>
        <thead>
            <tr>
                <th>Reference Name</th>
                <th>Issuer</th>
                <th>Reference Number</th>
                <th>Validity</th>
                <th>Issued</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $record_array = json_decode($values->references);  
            $count = 0;
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td>'.$b->reference_name .'</td>';
                    echo '<td>'.$b->issuer .'</td>';
                    echo '<td>'.$b->reference_no .'</td>';
                    echo '<td>'.$b->validity .'</td>';
                    echo '<td>'.$b->issued .'</td>';
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

     <table>
        <thead>
            <tr>
                <th colspan="2">Inspection</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $record_array = json_decode($values->inspection);  
            $count = 0;
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td style="width: 40%;" class="label-head">Changes</td>';
                    echo '<td>'.$b->changes .'</td>';
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

     <table>
        <thead>
            <tr>
                <th colspan="2">Inspector</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $record_array = json_decode($values->inspector);  
            $count = 0;
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td style="width: 40%;" class="label-head">Name</td>';
                    echo '<td>'.$b->name .'</td>';
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

     <table>
        <thead>
            <tr>
                <th colspan="2">Distribution</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $record_array = json_decode($values->distribution);  
            $count = 0;
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td style="width: 40%;" class="label-head">Distribution ID</td>';
                    echo '<td>'.$b->distribution_id .'</td>';
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>
    
    <table>
        <thead>
            <tr>
                <th colspan="2">Other Distribution</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $record_array = json_decode($values->other_distribution);  
            $count = 0;
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td style="width: 40%;" class="label-head">Other</td>';
                    echo '<td>'.$b->others .'</td>';
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

     <table>
        <thead>
            <tr>
                <th colspan="2">Personel</th>
            </tr>
            <tr>
                <th>Name</th>
                <th>Designation</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $record_array = json_decode($values->personnel);  
            $count = 0;
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td>'.$b->name .'</td>';
                    echo '<td>'.$b->designation .'</td>';
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

    <table>
        <thead>
            <tr>
                <th colspan="2">Recommendation</th>
            </tr>
            <tr>
                <th>Element ID</th>
                <th>Recommendation</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $record_array = json_decode($values->recommendation);  
            $count = 0;
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td>'.$b->element_id .'</td>';
                    echo '<td>'.$b->recommendation .'</td>';
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

    <table>
        <thead>
            <tr>
                <th colspan="2">Present During Meeting</th>
            </tr>
            <tr>
                <th>Name</th>
                <th>Position</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $record_array = json_decode($values->present_during_meeting);  
            $count = 0;
            foreach ($record_array as $a => $b) {
                echo "<tr>";
                    echo '<td>'.$b->name .'</td>';
                    echo '<td>'.$b->position .'</td>';
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>
</div>
