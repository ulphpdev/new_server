<?php $this->load->view('layout/header'); ?>

<style type="text/css">
    #login-div  {
        background-image: url(asset/login/background.png);
        background-position: center;
        height: 100vh;
        background-size: cover;
        overflow:hidden;
    }

    #login-assets {
        width: 65%;
    }

    .btn-login {
        color: #fff;
        background-color: #2a282b;
        border: 1px solid #fff;
    }
    .btn-login:hover,.btn-login:focus, .btn-login:active {
        color: #fff;
        background-color: #2a282b;
        border: 1px solid #fff;
    }

    .input-login {
        color: #fff !important;
        border: 1px solid #fff !important;
        background: transparent !important;
    }

    @media screen and (min-width: 1025px) {
        #login-form {
            position: absolute;
            width: 250px;
            right: 115px;
            top: 160px;
        }

        #login-form > img {
            width: 100%;
        }
    }

    @media screen and (max-width: 1024px) {
        #login-assets {
            width: 66%;
            position: absolute;
            left: 0px;
            top: 100px;
        }

        #login-form {
            position: absolute;
            width: 207px;
            right: 40px;
            top: 145px;
        }

         #login-form > img {
            width: 100%;
        }
    }

    @media screen and (max-width: 768px) {

        #login-form {
            position: absolute;
            width: 250px;
            right: 75px;
            top: 161px;
        }

        #login-assets {
            display: none;
        }
    }

    @media screen and (max-width: 640px) {
        #login-form {
            position: absolute;
            width: 250px;
            right: 30px;
            top: 161px;
        }

        #login-form > img {
            width: 75%;
        }
    }

    @media screen and (max-width: 480px) {
        #login-form {
            position: absolute;
            width: 100%;
            padding: 20px;
            bottom: 0px !important;
            right: 1px;
        }

        #login-form > img {
            width: 75%;
        }
    }

    @media screen and (max-width: 360px) {
        #login-form {
            position: absolute;
            width: 100%;
            padding: 20px;
            bottom: 0px !important;
            right: 1px;
        }

        #login-form > img {
            width: 75%;
        }
    }

    @media screen and (max-width: 320px) {
        #login-form {
            position: absolute;
            width: 100%;
            padding: 20px;
            bottom: 0px !important;
            right: 1px;
        }

        #login-form > img {
            width: 75%;
        }

    }
</style>
<div class="col-md-12" id="login-div">
    <img id="login-assets" src="<?= base_url();?>asset/login/asset1.png">
    <div id="login-form">
        <img id="login-logo" src="<?= base_url();?>asset/login/logo.png">
        <div class="form-group" style="margin-top: 30px;">
            <input type="text" name="Email" value="" placeholder="Email" class="form-control input-login" id="username" maxlength="80" size="30">
            <span class="email_err error"></span>
        </div>
        <div class="form-group">
            <input type="password" name="password" value="" placeholder="Password" class="form-control input-login" id="password" size="30">
            <span class="password_err err-msg error"></span>
        </div>
        <div class="form-group">
            <input type="submit" name="submit" value="Log in" id="login_submit" class="btn btn-login btn-block btn-flat"> 
            <span class="err-msg login-err error"></span> 
        </div>
    </div>
</div>



<!-- <div class="col-login">
    <div class="login-container">
      <div class="login-form">
        <div class="login-inside">
          <center><img src="<?= base_url();?>asset/img/sams_logo.png" width="130" /></center>
          <div class="form-group" style="margin-top: 30px;">
            <input type="text" name="Email" value="" placeholder="Email" class="form-control input-login" id="username" maxlength="80" size="30">
            <span class="email_err error"></span>
          </div>
          <div class="form-group">
            <input type="password" name="password" value="" placeholder="Password" class="form-control input-login" id="password" size="30">
            <span class="password_err err-msg error"></span>
          </div>
          <div class="form-group">
            <input type="submit" name="submit" value="Log in" id="login_submit" class="btn btn-success btn-block btn-flat"> 
            <span class="err-msg login-err error"></span> 
          </div>
        </div>
      </div>
    </div>
  </div> -->
<script type="text/javascript">


$(document).ready(function() { 

  document.onkeypress = enter;
  function enter(e) {
    if (e.which == 13) { 
      $('#login_submit').click()
    }
  }

  if(isIE()){
    
    $("[placeholder]").focus(function(){
        if($(this).val()==$(this).attr("placeholder")) $(this).val("");
    }).blur(function(){
        if($(this).val()=="") $(this).val($(this).attr("placeholder"));
    }).blur();

    $("[placeholder]").parents("form").submit(function() {
        $(this).find('[placeholder]').each(function() {
            if ($(this).val() == $(this).attr("placeholder")) {
                $(this).val("");
            }
        })
    });
  }

  
    
  
  $(document).unbind('click').on('click', '#login_submit', function(){ 
    $('#login_submit').val("Please wait...");
    var user = $('#username').val();
    var pass = $('#password').val();
    $('#password').removeClass("err-msg");
    $('#username').removeClass("err-msg");
    $('.password_err').html('');
    $('.email_err').html('');
    $('.login-err').html('');

    if(user=='' && pass == ''){ 
      $('.er-msg').show();
      $('.email_err').html('Please provide email.');
      $('.password_err').html('Please provide password.');
      $('#password').addClass("err-msg");
      $('#username').addClass("err-msg");
      $('#login_submit').val("Log in");
    } else if(user=='' && pass != ''){
      $('.er-msg').show();
      $('.email_err').html('Please provide email.');
      $('#password').addClass("err-msg");
      $('#username').addClass("err-msg");
      $('#login_submit').val("Log in");
    } else if(pass =='' && user !=''){
      $('.er-msg').show();
      $('.password_err').html('Please provide password.');
      $('#password').addClass("err-msg");
      $('#username').addClass("err-msg");
      $('#login_submit').val("Log in");
    }else{
      if(isValidEmailAddress(user)){

        aJax.post(
          "<?=base_url('api');?>",
          {
            token : "35ced0a2f0ad35bdc9ae075ee213ea4b8e6c2839",
            cmdEvent : "authenticate_web",
            email : user,
            password : pass
          },
          function(data){
            var obj = isJson(data);
            if(obj.status == "success"){
              var sess_username = "";
              var sess_name = "";
              var sess_role = "";
              var sess_userid ="";

              $.each(obj.data, function(x,y){
                sess_username = user;
                sess_name = y.fname + " " + y.lname;
                sess_role = y.role;
                sess_userid = y.user_id;
              });

              var data_session = {'username': sess_username,'role':sess_role,'userid':sess_userid,'name':sess_name};
              aJax.post(
                "<?=base_url('login/setSession');?>",
                data_session,
                function(result){
                  insert_audit_trail("SAMS Login");
                  update_config();
                  location.href = "<?= base_url(); ?>";  
                }
              );
            } else {
              $('.login-err').text(obj.message).show();
              $('#password').addClass("err-msg");
              $('#login_submit').val("Log in");
            }
          }
        );
      } else {
          $('.email_err').html('You have entered an invalid email address.');
          $('#username').addClass("err-msg");
          $('#login_submit').val("Log in");
      }
    }
  });

  function isValidEmailAddress(email) {
      var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
      return pattern.test(email);
  };

});
</script>