
<ol class="breadcrumb" style="background-color: #fff;">
  	<li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
  	<li class="active">Audit History</li>
</ol>

<div class="btn-navigation menu-btn col-md-12 pad-0" >
	<button class="add btn-min btn btn-success btn-sm bold"><span class = "glyphicon glyphicon-plus"></span> ADD <span></button>
</div>

<div class="row">
    <div class="col-md-12" id="list_data">
        <div class="table-responsive">

            <!-- LIST -->
            <table class = "table listdata table-striped" style="margin-bottom:0px;">
                <thead>
                    <tr>
                        <th style="width: 200px;">Supplier</th>
                        <th style="width: 400px;">Major Changes Since Previous Inspection</th>
                        <th style="width: 200px;">Audit History Date</th>
                        <th style="width: 200px;">Modified Date</th>
                        <th style="text-align: right; width: 100px;">Actions</th>
                    </tr>
                </thead>
                <tbody class="table_body"></tbody>
            </table>
        </div>

        <!-- PAGINATION -->
        <div style="text-align: center; margin-top: 20px;" class="form-inline pager_div"></div>
    </div>
</div>

<div class="row add_new_div" style="margin-top: 20px; display: none">
	<div class="col-md-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"></h4>
            </div>
	        <div class="panel-body"  style="padding-bottom: 17px;">
	        	<div class="form-horizontal">
	  				<div class="form-group">
	    				<label class="control-label col-sm-2" >Supplier:</label>
	    				<div class="col-md-5">
	    					<div class="col-sm-12">
	      						<select type="text" class="form-control inputs-required supplier-input"></select>
	      						<span class = "txt_error er-msg">This field should not be empty *</span>
	    					</div>
	    				</div>
	  				</div>
	  				<div class="form-group">
	    				<label class="control-label col-sm-2" >GMP Report Number* :</label>
	    				<div class="col-md-5">
	    					<div class="col-sm-12">
	      						<input type="text" class="form-control inputs-required report_no" />
	      						<span class = "txt_error er-msg">This field should not be empty *</span>
	    					</div>
	    				</div>
	  				</div>
	  				<div class="form-group">
	    				<label class="control-label col-sm-2" >Major Change(s) Since Previous Inspection* :</label>
	    				<div class="col-md-10 changes_div">

		    			</div>
	  				</div>
	  				<div class="form-group">
	    				<label class="control-label col-sm-2" >Audit History Date* :</label>
	    				<div class="col-md-5 audit_dates_div">

		    			</div>
	  				</div>
	  				<div class="form-group">
	    				<label class="control-label col-sm-2" >Auditors* :</label>
	    				<div class="inspector_div col-md-5">
	    				</div>

	  				</div>
	  			</div>
	        </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary save_history"><span class = "glyphicon glyphicon-floppy-saved"></span> Save</button>
                <button type="submit" class="btn btn-default close_add"><span class = "glyphicon glyphicon-remove-circle"></span> Close</button>
            </div>
	    </div>
	</div>
</div>



<script type="text/javascript">
	var audit_date_count = 1;
	var inspectr_count = 1;
	var changes_count = 1;

	var limit = '10';
  	var offset = '0';
  	var query = "";


  	// Return today's date and time
	var currentTime = new Date()
	var month = currentTime.getMonth()
	var day = currentTime.getDate()
	var year = currentTime.getFullYear() - 1
	var max_date = year + "/12/31";

	$(document).ready(function() {
		get_supplier();
		get_list("",limit,offset);
		get_pagination("");
	});




	<?php $this->load->view('layout/listing_function');?>

  	function get_list(query,limit,offset){
	    isLoading(true);
	    $('.add_new_div').hide();
	    $('.add').show();
	    aJax.post(
	    	"<?=base_url('audit_history/get_list');?>",
	    	{
	    		query: query,
	    		limit: limit,
	    		offset: offset
	    	},
	    	function(data){
	    		var obj = JSON.parse(data);
	    		var table_body = new TBody();
	    		$.each(obj, function(x,y){
	    			table_body.td(y.name);
	    			table_body.td(gmp.changes(y.major_changes));
	    			// table_body.td(gmp.inspectors(y.inspectors));
	    			table_body.td(y.audit_dates);
	    			table_body.td(moment(y.update_date).format("LLL"));


	    			var action_button = new UList();
	    			action_button.set_liclass("li-action");
                    action_button.set_ulclass("ul-action");
                    if(y.from_API != 1){

                    	action_button.li("<a class='edit action_list action' data-status='' data-id='"+y.id+"' title='edit'><span class='glyphicon glyphicon-pencil'></span></a>");
                    	action_button.li("<a class='delete_data action_list action' data-id='"+y.id+"' title='delete'> <span class='glyphicon glyphicon-trash'></span></a>");

                    }

                    table_body.td(action_button.set());

	    			table_body.set();
	    		});

	    		table_body.append('.table_body');
	    		isLoading(false);
	    	}

	    );
	}

	function get_pagination(query){
		aJax.post(
          "<?=base_url("global_controller/getlist_pagination");?>",
          {
              'query':query,
              'table': 'qv_audit_history',
              'status':'status = 1'
          },
          function(result){
              var no_of_page = Math.ceil(result / limit);
              var pagination = new Pagination(); //please check helper.js for this function
              pagination.set_total_page(no_of_page);
              pagination.set('.pager_div');
              isLoading(false);
          }
      	);
	}


	function get_supplier()
	{
		aJax.post(
	        "<?=base_url('global_controller/getlist_global');?>",
	        {
	            'limit':999,
	            'offset':0,
	            'query':"",
	            'table':'tbl_company',
	            'order_by':'update_date',
	            'status':'status = 1'
	        },
	        function(result){
	        	var obj = JSON.parse(result);
          		var html = "";
          		$.each(obj, function(x,y){
          			html += '<option value="'+y.company_id+'">'+y.name+'</option>';
          		});
          		$('.supplier-input').html(html);
          		$('.supplier-input-update').html(html);


          	}
        );
	}

	function validateFields() {
	    var counter = 0;
	    $('.inputs-required').each(function(){
	          var input = $(this).val().trim();
	          if (input.length == 0) {
	            $(this).css('border-color','red');
	            $(this).next().show();
	            counter++;
	          }else{
	            if(input == 0 || input == "0"){
	                $(this).css('border-color','red');
	                $(this).next().html("Invalid Input.");
	                $(this).next().show();
	                counter++;
	            } else {
	              $(this).css('border-color','#ccc');
	              $(this).next().hide();
	            }
	          }
	      });
	    return counter;
	}

	$(document).on('paste', '.audit-date', function(e){
		e.preventDefault();
	})

	$(document).on('click', '.save_history', function(){

        if(validateFields() == 0){

            if(check_unique('.major-changes')  == 0 ){

                if(check_unique('.inspector-input')  == 0 ){

                    if(check_unique('.audit-date')  == 0 ){

				        var no_click = 0;
                        var data_id = $(this).attr('data-id');
                        var dialog = bootbox.confirm({
                            message: "Are you sure you want to add this record?",
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {

                                if(result){
                                    isLoading(true);
					              	no_click ++;
					              	if(no_click <= 1){
	                                    isLoading(true);
	                                    var supplier_id = $('.supplier-input').val();
	                                    var changes = $('.major-changes').val();
	                                    var report_no = $('.report_no').val();

	                                    aJax.post(
	                                        "<?= base_url('audit_history/insert_history');?>",
	                                        {
	                                            'supplier_id':supplier_id,
	                                            'changes':changes,
	                                            'report_no':report_no,
	                                            'datatype': data_id
	                                        },
	                                        function(result){
	                                            var history_id = result;

	                                            if($(this).attr('data-id') == 0){
	                                                insert_audit_trail("Add Audit History");
	                                            } else {
	                                                insert_audit_trail("Update Audit History");
	                                            }

	                                            //INSERT CHANGES
	                                            $('.major-changes').each(function(){
	                                                aJax.post(
	                                                    "<?= base_url('audit_history/insert_history_changes');?>",
	                                                    {
	                                                        'history_id':history_id,
	                                                        'changes':$(this).val(),
	                                                        'orders':$(this).attr("data-order"),
	                                                        'datatype': $(this).attr('data-id'),
	                                                    },
	                                                    function(result){
	                                                    }
	                                                );
	                                            });

	                                            //INSERT AUDIT DATES
	                                            $('.audit-date').each(function(){
	                                                aJax.post(
	                                                    "<?= base_url('audit_history/insert_history_dates');?>",
	                                                    {
	                                                        'history_id':history_id,
	                                                        'date':$(this).val(),
	                                                        'datatype': $(this).attr('data-id'),
	                                                    },
	                                                    function(result){

	                                                    }
	                                                );
	                                            });


	                                            //INSERT INSPECTORS
	                                            $('.inspector-input').each(function(){
	                                                aJax.post(
	                                                    "<?= base_url('audit_history/insert_history_inspector');?>",
	                                                    {
	                                                        'history_id':history_id,
	                                                        'inspector':$(this).val(),
	                                                        'datatype': $(this).attr('data-id'),
	                                                    },
	                                                    function(result){

	                                                    }
	                                                );
	                                            });


	                                            //DONE
	                                            aJax.get(
	                                                "<?=base_url('api/site')?>",
	                                                function(result){
	                                                    bootbox.alert("Audit History Saved!",function(){
	                                                        location.reload();
	                                                    })
	                                                }
	                                            );
	                                    	}
	                                    );
	                                }
	                            }
                            }
                        });

                    }
                }
            }
        }

	});

	$(document).on('click', '.delete_data', function(){

		var id = $(this).attr('data-id');
		console.log(id);
		var dialog = bootbox.confirm({
	        message: "Are you sure you want to delete this record?",
	        buttons: {
	            confirm: {
	                label: 'Yes',
	                className: 'btn-success'
	            },
	            cancel: {
	                label: 'No',
	                className: 'btn-danger'
	            }
	        },
	        callback: function (result) {
	          	if(result){
	          		insert_audit_trail("Delete Audit History");
	          		aJax.post(
	          			"<?= base_url('Under_data_maintenance/data_maintenance_delete');?>",
	          			{
	          				id : id,
	          				status : "-2",
	          				table : "tbl_audit_history",
	          				order_by: "id"
	          			},
	          			function(result){
	          				aJax.get(
            					"<?=base_url('api/site')?>",
            					function(result){
            						bootbox.alert("Successfully deleted record.",function(){
			          					location.reload();
			          				})
            					}
            				);

	          			}
	          		);
	            }
	        }
	    });

	})


	$(document).on('click', '.add', function(){

        isLoading(true);
		$(this).hide();
		$('.add_new_div').show();
		$('.inspector_div').html("");
        $('.audit_dates_div').html("");
        $('.changes_div').html("");
        $('.panel-heading').html("Add Audit History");
        $('.major-changes').val("");
        audit_date_count = 1;
        $(".save_history").attr("data-id",0);




        //CHANGES
        var html = "";
        html += '<div class="col-sm-10">';
		html += '	<textarea type="text" data-id=0 class="form-control inputs-required major-changes" rows="3"></textarea>';
		html += '	<span class = "txt_error er-msg">This field should not be empty *</span>';
		html += '</div>';
		html += '<div class="col-sm-2 inspector_div_0"> ';
		html += '	<button type="submit" class="btn btn-default" position=0 style="border: none; padding: 7px; margin-left: -20px;" id="add_changes">';
		html += '		<i class="glyphicon glyphicon-plus" ></i>';
		html += '	</button>';
		html += '</div>';
		$('.changes_div').append(html);
		order_changes();

		//INSPECTOR
		html =	"";
		html += '<div class="col-sm-9  inspector_div_0">';
		html += '	<input style="margin-top: 5px;" type="text" data-id=0 class=" inputs-required form-control inspector-input" placeholder="" />';
		html += '	<span class = "txt_error er-msg">This field should not be empty *</span>';
		html += '</div>';
		html += '<div class="col-sm-3 inspector_div_0"> ';
		html += '	<button type="submit" class="btn btn-default" position=0 style="border: none; padding: 7px; margin-left: -20px;" id="add_inspector">';
		html += '		<i class="glyphicon glyphicon-plus" ></i>';
		html += '	</button>';
		html += '</div>';
		$('.inspector_div').append(html);


		//AUDIT DATES
		html = "";
		html += '<div class="col-sm-9">';
		html += '	<input type="text" class="audit-date-input-1 inputs-required form-control audit-date" placeholder="" />';
		html += '	<span class = "txt_error er-msg">This field should not be empty *</span>';
		html += '</div>';
		html += '<div class="col-sm-3">';
		html += '	<button type="submit" class="btn btn-default" style="border: none; padding: 7px; margin-left: -20px;" id="add_audit_date">';
	    html += '		<i class="glyphicon glyphicon-plus" ></i>';
	    html += '	</button>';
		html += '</div>';
		$('.audit_dates_div').append(html);
		$(".audit-date-input-1").datepicker({ maxDate: new Date(max_date), dateFormat: 'yy-mm-dd' });

		$('html, body').animate({
      		scrollTop: $(".inspector_div").offset().top
  		}, 1000);

        isLoading(false);

	})

	$(function(){
	    $(".audit-date-input-1").datepicker({ maxDate: new Date(max_date), dateFormat: 'yy-mm-dd' });
	});

	$(document).on('click', '.close_add', function(){
		$('.add_new_div').hide();
		$('.add').show();
	});

	$(document).on('click', '#add_changes', function(){

		if(changes_count != 20){
			changes_count++;
			var html = "";
			html += '<div class="col-sm-10 changes_div_'+ changes_count +'" style="margin-top: 10px;">';
			html += '	<textarea type="text" data-id=0 class="form-control inputs-required major-changes" rows="3"></textarea>';
			html += '	<span class = "txt_error er-msg">This field should not be empty *</span>';
			html += '</div>';
			html += '<div class="col-sm-2 changes_div_'+ changes_count +'"> ';
			html += '	<button type="submit" class="btn btn-default" position=0 style="border: none; padding: 7px; margin-left: -20px;" id="add_changes">';
			html += '		<i class="glyphicon glyphicon-plus" ></i>';
			html += '	</button>';
			html += '	<button type="submit" class="btn btn-default" data-id=0 position='+changes_count+' style="border: none; padding: 7px; margin-left: 0px;" id="remove_changes">';
			html += '		<i class="glyphicon glyphicon-minus" ></i>';
			html += '	</button>';
			html += '</div>';
			$('.changes_div').append(html);
			order_changes();
		}
	})

	function order_changes()
	{
		var count = 1;
		$('.major-changes').each(function(){
			$(this).attr("data-order", count);
			count++;
		});
	}


	$(document).on('click', '#add_audit_date', function(){
		audit_date_count++;
		if(audit_date_count <= 3){
			var html = "";
			html = "";
			html += '<div class="col-sm-9  audit_date_div_'+ audit_date_count +'" style="margin-top: 5px;">';
			html += '	<input type="text" data-id=0 class="audit-date-input-'+audit_date_count+' inputs-required form-control audit-date" placeholder="" />';
			html += '	<span class = "txt_error er-msg">This field should not be empty *</span>';
			html += '</div>';
			html += '<div class="col-sm-3 audit_date_div_'+ audit_date_count +'" style="padding-top: 5px;">';
			html += '	<button type="submit" class="btn btn-default" style="border: none; padding: 7px; margin-left: -20px;" id="add_audit_date">';
		    html += '		<i class="glyphicon glyphicon-plus" ></i>';
		    html += '	</button>';
			html += '	<button type="submit" class="btn btn-default" data-id=0 position='+audit_date_count+' style="border: none; padding: 7px; margin-left: 0px;" id="remove_audit_date">';
		    html += '		<i class="glyphicon glyphicon-minus" ></i>';
		    html += '	</button>';
			html += '</div>';
			$('.audit_dates_div').append(html);
			$(".audit-date-input-"+audit_date_count).datepicker({ maxDate: new Date(max_date), dateFormat: 'yy-mm-dd' });
		}

	})

	$(document).on('click', '#add_inspector', function(){
		inspectr_count ++;
		var position = inspectr_count;
		var html = "";
		html += '<div class="col-sm-9  inspector_div_'+ position +'">';
		html += '	<input style="margin-top: 5px;" type="text" data-id=0 class="form-control inputs-required inspector-input" placeholder="" />';
		html += '	<span class = "txt_error er-msg">This field should not be empty *</span>';
		html += '</div>';
		html += '<div class="col-sm-3 inspector_div_'+ position +'" style="padding-top: 5px;"> ';
		html += '	<button type="submit" class="btn btn-default" position=0 style="border: none; padding: 7px; margin-left: -20px;" id="add_inspector">';
		html += '		<i class="glyphicon glyphicon-plus" ></i>';
		html += '	</button>';
		html += '	<button type="submit" class="btn btn-default" data-id=0  position='+position+' style="border: none; padding: 7px; margin-left: 0px;" id="remove_inspector">';
		html += '		<i class="glyphicon glyphicon-minus" ></i>';
		html += '	</button>';
		html += '</div>';
		$('.inspector_div').append(html);
	});

	$(document).on('click', '#remove_audit_date', function(){
		var position = parseInt($(this).attr("position"));
		var dataid = parseInt($(this).attr("data-id"));
		console.log(dataid);
		if(dataid != 0){
			var dialog = bootbox.confirm({
		        message: "Are you sure you want to delete this record?",
		        buttons: {
		            confirm: {
		                label: 'Yes',
		                className: 'btn-success'
		            },
		            cancel: {
		                label: 'No',
		                className: 'btn-danger'
		            }
		        },
		        callback: function (result) {
		          	if(result){
		          		aJax.post(
		          			"<?= base_url('audit_history/remove_audit_date');?>",
		          			{
		          				id : dataid,
		          			},
		          			function(result){
		          				$('.audit_date_div_' + position).remove();
		          				aJax.get(
	            					"<?=base_url('api/site')?>",
	            					function(){
	            					}
	            				);
		          			}
		          		);
		            }
		        }
		    });
		} else {
			$('.audit_date_div_' + position).remove();
		}


	});

	$(document).on('click', '#remove_changes', function(){
		var position = parseInt($(this).attr("position"));
		var dataid = parseInt($(this).attr("data-id"));

		if(dataid != 0){
			var dialog = bootbox.confirm({
		        message: "Are you sure you want to delete this changes?",
		        buttons: {
		            confirm: {
		                label: 'Yes',
		                className: 'btn-success'
		            },
		            cancel: {
		                label: 'No',
		                className: 'btn-danger'
		            }
		        },
		        callback: function (result) {
		          	if(result){
          			aJax.post(
	          			"<?= base_url('audit_history/remove_changes');?>",
	          			{
	          				id : dataid,
	          			},
	          			function(result){
	          				$('.changes_div_' + position).remove();

	          				aJax.get(
            					"<?=base_url('api/site')?>",
            					function(result){
            					}
            				);
	          			}
	          		);

		            }
		        }
		    });
		} else {
			$('.changes_div_' + position).remove();
		}

		changes_count = changes_count - 1;
	});

	$(document).on('click', '#remove_inspector', function(){
		var position = parseInt($(this).attr("position"));
		var dataid = parseInt($(this).attr("data-id"));

		if(dataid != 0){
			var dialog = bootbox.confirm({
		        message: "Are you sure you want to delete this inspector?",
		        buttons: {
		            confirm: {
		                label: 'Yes',
		                className: 'btn-success'
		            },
		            cancel: {
		                label: 'No',
		                className: 'btn-danger'
		            }
		        },
		        callback: function (result) {
		          	if(result){
		          		aJax.post(
		          			"<?= base_url('audit_history/remove_inspector');?>",
		          			{
		          				id : dataid,
		          			},
		          			function(result){
		          				$('.inspector_div_' + position).remove();
		          				aJax.get(
	            					"<?=base_url('api/site')?>",
	            					function(result){
	            					}
	            				);
		          			}
		          		);
		            }
		        }
		    });
		} else {
			$('.inspector_div_' + position).remove();
		}
	});

	$(document).on('click', '.edit', function(){
		isLoading(true);
		var id = $(this).attr('data-id');
		 $('.changes_div').html("");
        $('.inspector_div').html("");
        $('.audit_dates_div').html("");
		$('.add_new_div').show();
		$(".save_history").attr("data-id",id);
        $('.panel-heading').html("Edit Audit History");

		aJax.post(
			"<?= base_url('audit_history/get_details');?>",
			{
				id: id
			},
			function(result){
				var obj = JSON.parse(result);

				$.each(obj, function(x,y){
					$('.supplier-input option[value="'+y.supplier_id+'"]').attr('selected','selected');
            		// $('.major-changes').val(y.major_changes);
            		$('.inspector_div').html("");
            		$('.audit_dates_div').html("");
            		$('.major-changes').html("");
            		$('.report_no').val(y.report_no);


            		//CHECK CHANGES
            		changes_count = 0;
            		$.each(y.changes, function(l,k){
            			changes_count++;
            			var html = "";
						html += '<div class="col-sm-10  changes_div_'+ k.id +'" style="margin-top: 10px;">';
						html += '	<textarea type="text"  data-id='+k.id+' class="form-control inputs-required major-changes" rows="3">'+k.changes+'</textarea>';
						html += '	<span class = "txt_error er-msg">This field should not be empty *</span>';
						html += '</div>';
						html += '<div class="col-sm-2 changes_div_'+ k.id +'"> ';
						html += '	<button type="submit" class="btn btn-default" style="border: none; padding: 7px; margin-left: -20px;" id="add_changes">';
					    html += '		<i class="glyphicon glyphicon-plus" ></i>';
					    html += '	</button>';
						html += '	<button type="submit" data-id='+k.id+' position='+k.id+' class="btn btn-default" position=0 style="border: none; padding: 7px; margin-left: 0px;" id="remove_changes">';
						html += '		<i class="glyphicon glyphicon-minus" ></i>';
						html += '	</button>';
						html += '</div>';
						$('.changes_div').append(html);
            		});

            		//CHECK AUDIT DATES
            		var date_count = 0 ;
            		audit_date_count = 1;
            		$.each(y.audit_dates, function(a, b){
            			date_count ++;
            			audit_date_count++;
            			var html = "";
						html = "";
						html += '<div class="col-sm-9  audit_date_div_'+ b.id +'" style="margin-top: 5px;">';
						html += '	<input type="text" data-id='+b.id+' class="audit-date-input-'+b.id+' form-control inputs-required audit-date" value="'+b.Date+'" />';
						html += '	<span class = "txt_error er-msg">This field should not be empty *</span>';
						html += '</div>';
						html += '<div class="col-sm-3 audit_date_div_'+ b.id +'" style="padding-top: 5px;">';
						html += '	<button type="submit" class="btn btn-default" style="border: none; padding: 7px; margin-left: -20px;" id="add_audit_date">';
					    html += '		<i class="glyphicon glyphicon-plus" ></i>';
					    html += '	</button>';
					    html += '	<button type="submit" data-id='+b.id+' position='+b.id+' class="btn btn-default" style="border: none; padding: 7px; margin-left: 0px;" id="remove_audit_date">';
				   	 	html += '		<i class="glyphicon glyphicon-minus" ></i>';
				   	 	html += '	</button>';
						html += '</div>';
						$('.audit_dates_div').append(html);
						$(".audit-date-input-"+b.id).datepicker({ maxDate: new Date(max_date), dateFormat: 'yy-mm-dd' });
            		});

            		var inspector_count = 0;
            		$.each(y.inspectors, function(c, d){
            			inspector_count ++;
            			var html = "";
            			html += '<div class="col-sm-9  inspector_div_'+ d.id +'">';
						html += '	<input style="margin-top: 5px;" type="text" data-id='+d.id+' class="form-control inputs-required inspector-input" value="'+d.inspector+'" />';
						html += '	<span class = "txt_error er-msg">This field should not be empty *</span>';
						html += '</div>';
						html += '<div class="col-sm-3 inspector_div_'+ d.id +'" style="padding-top: 5px;"> ';
						html += '	<button type="submit" class="btn btn-default" style="border: none; padding: 7px; margin-left: -20px;" id="add_inspector">';
					    html += '		<i class="glyphicon glyphicon-plus" ></i>';
					    html += '	</button>';
					    html += '	<button type="submit" class="btn btn-default" data-id='+d.id+' position='+d.id+' style="border: none; padding: 7px; margin-left: 0px;" id="remove_inspector">';
						html += '		<i class="glyphicon glyphicon-minus" ></i>';
						html += '	</button>';
						html += '</div>';
						$('.inspector_div').append(html);
            		});
				});

				$('html, body').animate({
              		scrollTop: $(".inspector_div").offset().top
          		}, 1000);

          		isLoading(false);
			}

		);
	});

function check_unique(element)
  {
    var values = {};
    var countUnique = 0;
    var checks = $(element);
    checks.removeClass("error");

    checks.each(function(i, elem)
    {
      if(elem.value in values) {
        $(elem).css('border-color','red');
        $(elem).next().html("Please enter a Unique Value.");
        $(elem).next().show();
        $(values[elem.value]).css('border-color','red');
        $(values[elem.value]).next().html("Please enter a Unique Value.");
        $(values[elem.value]).next().show();
      } else {
        values[elem.value] = elem;
        ++countUnique;
      }
    });

    if(countUnique == checks.size()) {
      return 0;
    } else {
      return 1;
    }
  }



</script>