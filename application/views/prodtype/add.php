<?php
  $table = "tbl_classification";
  $order_by = "create_date";
?>
<div class="panel-heading">
    <h4 class="panel-title">
        Add Product Type
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Product Type *: </label>
            <div class="col-sm-6">
                <input type = "text" id="txt-prod" class = "prodtype inputcss form-control fullwidth inputs_classification" >
                <span class = "er-msg"> Product type should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Limit *: </label>
            <div class="col-sm-2">
                <input min="0" type = "number" id="limit" class = "prodtype input_number inputcss form-control fullwidth inputs_classification" >
                <span class = "er-msg"> Limit should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Category : </label>
            <div class="col-sm-6">
                <select class="form-control" id="ques_category">
                    <option class="hidden selected" disabld selected value="">Select Category</option>
                </select>
                <span class = "er-msg"> Question category should not be empty *</span>
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary" id="add_cat" data-id="" data-text="">ADD</button>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"></label>
            <div class="col-sm-10">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width: 150px;">Category Name</th>
                                <th style="width: 700px !important;">Description</th>
                                <th style="width: 100px;"></th>
                            </tr>
                        </thead>
                        <tbody class="cat_list">
                            <tr  class="cat_item ct_2" data-id="2">
                                <td>Critical</td>
                                <td>
                                    <textarea class="form-control prodtype inputs_classification" placeholder="Input description here. "></textarea>
                                    <span class = "er-msg"> Description should not be empty *</span>
                                </td>
                                <td></td>
                            </tr>
                            <tr  class="cat_item ct_3" data-id="3">
                                <td>Major</td>
                                <td>
                                    <textarea class="form-control prodtype inputs_classification" placeholder="Input description here. "></textarea>
                                    <span class = "er-msg"> Description should not be empty *</span>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4">No. of major to have unacceptable disposition *: </label>
            <div class="col-sm-2">
                <input min="0" type = "number" id="no_major" class = " prodtype input_number inputcss form-control fullwidth inputs_classification" >
                <span class = "er-msg"> No. of major should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4">No. of critical to have unacceptable disposition *: </label>
            <div class="col-sm-2">
                <input min="0" type = "number" id="no_critical" class = "prodtype input_number inputcss form-control fullwidth inputs_classification" >
                <span class = "er-msg"> No. of critical should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">


    function number_only(element){
        $(element).keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }


    $(document).ready(function(){
        var table = "<?=$table;?>";
        var field = "<?=$order_by;?>";
        get_question_category();

        $("body").delegate('.input_number', 'focusout', function(){
            if($(this).val() < 0){
                $(this).val('0');
            }
        });

        number_only("#limit");
        number_only("#no_major");
        number_only("#no_critical");



  $('.save').off('click').on('click', function() {
    var catname = $('#txt-prod').val();
    var limit = $('#limit').val();
    var major = $('#no_major').val();
    var critical = $('#no_critical').val();
    var get_time = func_time();
    if(validateFields('.prodtype') == 0){
      var no_click = 0;
      confirm("Are you sure you want to save this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_classification", "classification_name = '" + catname + "' AND status >= 0", function(result){
              if(result == "true"){
                $("#txt-prod").css('border-color','red');
                $("#txt-prod").next().html("This field should contain a unique value.");
                $("#txt-prod").next().show();
              } else {
                $(this).prop('disabled',true);
                isLoading(true);
                aJax.post(
                  "<?=base_url('global_controller/save_classif');?>",
                  {
                    field:'',
                    table:table,
                    catname:catname,
                    limit:limit,
                    major:major,
                    critical:critical,
                    get_time: get_time,
                    action: 'save'
                  },
                  function(data){
                    var class_id = data;
                    $('.cat_item').each(function(x,y){
                      var catid = $(this).attr('data-id');
                      var catdesc = $(this).find('textarea').val();
                      aJax.postasync(
                        "<?=base_url()?>global_controller/save_classification_category?create_date="+get_time+"&update_date="+get_time,
                        {
                          classification_id:class_id,
                          category_id:catid,
                          description:catdesc
                        },
                        function(data){
                          isLoading(false);
                          updateAPI("classification");


                        }
                      );
                    });
                    
                    updateAPI("classification");
                    insert_audit_trail("Create " + catname);
                    bootbox.alert('<b>Record is successfully saved!</b>', function() {
                        location.reload();
                    });

                  }
                );

               

              }
            });
          }
        }
      });
    }
  });

  function func_time(){
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var get_time = date+' '+time;
    return get_time;
  }

  function get_question_category(){
    aJax.post(
      "<?=base_url('global_controller/get_active_list')?>",
      {
        table:'tbl_question_category'
      },
      function(data){
        var obj = JSON.parse(data);
        var htm = '';
        htm += '<option class="selected" value="">--Select question category--</option>';
        $.each(obj, function(x,y){
          if(y.category_id != 3 && y.category_id != 2){
            if(y.category_name != "Major" || y.category_name != "Critical"){
              htm += '<option value="'+y.category_id+'">'+y.category_name+'</option>';
            }

          }
        });
        $('#ques_category').html(htm);
      }
    );
  }

  $('#ques_category').on('change',function(){
    var cat_id = $(this).val();
    var cat_name = $('#ques_category option:selected').text();
    $('#add_cat').attr('data-id',cat_id).attr('data-text',cat_name);
  });

  $('#add_cat').click(function(){
    var cat_id = $(this).attr('data-id');
    var cat_name = $(this).attr('data-text');

    var htm = '';
    if(cat_id == '' && cat_name == ''){
      bootbox.alert("Select question category to proceed!",function(e){});
    }else{
      if($('.ct_'+cat_id).length == 0){
        get_question_category();

        htm += '<tr class="cat_item ct_'+cat_id+'" data-id="'+cat_id+'">';
        htm += '   <td>'+cat_name+'</td>';
        htm += '   <td>';
        htm += '       <textarea class="form-control prodtype inputs_classification" placeholder="Input description here. "></textarea>';
        htm += '       <span class = "er-msg"> Description should not be empty *</span>';
        htm += '   </td>';
        htm += '   <td>';
        htm += '        <a href="#" class="remove_cat" style="text-align:center;font-size: 10pt;padding-top: 3px;cursor:pointer;">';
        htm += '            <span class="glyphicon glyphicon-remove" style="font-size:18pt;color:#de0000;"></span>';
        htm += '        </a>';
        htm += '   </td>';
        htm += '</tr>';
        $('.cat_list').append(htm);

      }else{
        bootbox.alert("Question category is already in the list, select other category!",function(e){});
      }

    }
    $(this).attr('data-id','').attr('data-text','');
  });


    $(document).on('click','.remove_cat', function(e){
        e.preventDefault();
        $(this).parents('.cat_item').remove();
    });

})
</script>