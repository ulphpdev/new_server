<?php 
  $table = "tbl_disposition";
  $order_by = "disposition_id";
?>
<div class="col-md-12" id="form-content">

<div class="col-md-12 pad-5">
   <div class="col-md-2">Disposition Name *: </div>
    <div class = "col-md-4">
      <input type = "text" id="disposition" class = "inputcss form-control fullwidth inputs" >
      <span class = "er-msg">Disposition name should not be empty *</span>
    </div>
</div>

<div class="col-md-12 pad-5">
   <div class="col-md-2">Disposition Label *: </div>
    <div class = "col-md-10">
      <textarea type = "text" id="disposition_label" class = "inputcss form-control fullwidth inputs" rows="5" ></textarea>
      <span class = "er-msg">Disposition Label should not be empty *</span>
    </div>
</div>

</div>


<script type="text/javascript">
$(document).ready(function(){

var table = "<?=$table;?>";
var field = "<?=$order_by;?>";

/* Start of update */


$('.save').off('click').on('click', function() {

  
 var disposition = $('#disposition').val();
 var disposition_label = $('#disposition_label').val();
 console.log(disposition_label);
  if(validateFields() == 0){
    isduplicate("tbl_distribution", "distribution_name = '" + distribution + "' AND status >= 0", function(result){
      if(result == "true"){
        $("#disposition").css('border-color','red');
        $("#disposition").next().html("Duplicate Value.");
        $("#disposition").next().show();
      } else {
        $(this).prop('disabled',true);
        $('.loader_gif').show();
        $.ajax({
          type: 'Post',
          url:'<?=base_url();?>global_controller/disposition_array',
          data:{ disposition:disposition, disposition_label: disposition_label, table:table, field:field, action: 'save'},
        }).done( function(data){
          $('.loader_gif').hide();
          $('.update').prop('disabled',false);
          updateAPI("disposition");
          update_config();
          bootbox.alert('<b>Record is successfully saved!</b>', function() {
            location.reload();
          }).off("shown.bs.modal");

        }); 
      }

    });

    // if(isValidEmailAddress(email)){
    
    
    }


 });

/* End of update */

/* Start of Validation */



})  

</script>