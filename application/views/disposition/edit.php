<?php 
  $table = "tbl_disposition";
  $order_by = "disposition_id";
?>
<div class="col-md-12" id="form-content">

<div class="col-md-12 pad-5">
   <div class="col-md-2">Disposition name *: </div>
    <div class = "col-md-4"><input type = "text" id="disposition" class = "inputcss form-control fullwidth inputs" ><span class = "er-msg">Disposition name should not be empty *</span></div>
</div>


</div>


<script type="text/javascript">
$(document).ready(function(){

var table = "<?=$table;?>";
var field = "<?=$order_by;?>";
var id = "<?= $id; ?>";
	on_load();
      function on_load(){
        var limit = 1;
        $.ajax({
        type: 'Post',
        url:'<?=base_url();?>global_controller/edit_global',
        data:{id:id,limit:limit, table:table, field:field},
      }).done( function(data){
          var obj = JSON.parse(data);
          $.each(obj, function(index, row){        
          		$('#disposition').val(row.disposition_name);
          });
     });      
}


/* Start of update */

 // $(document).on('click', '.update', function(){
$('.update').off('click').on('click', function(){
 
  var disposition = $('#disposition').val();
 
 
  if(validateFields() == 0){
  	// if(isValidEmailAddress(email)){
    $(this).prop('disabled',true);
	    $('.loader_gif').show();
	    $.ajax({
        type: 'Post',
        url:'<?=base_url();?>global_controller/disposition_array',
        data:{ disposition:disposition,id:id,table:table, field:field, action: 'update'},
      }).done( function(data){
        $('.loader_gif').hide();
        $('.update').prop('disabled',false);
        updateAPI("disposition");
        update_config();
        bootbox.alert('<b>Record is successfully updated!</b>', function() {
          location.reload();
        }).off("shown.bs.modal");
	    }); 

  	}


 });

/* End of update */

/* Start of Validation */

function validateFields() {

    var counter = 0;
    $('.inputs').each(function(){
          var input = $(this).val();
          if (input.length == 0) {
          	$(this).css('border-color','red');
            $(this).next().show();
            counter++;
          }else{
          	if(input == 0 || input == "0"){
                $(this).css('border-color','red');
                $(this).next().html("Invalid Input.");
                $(this).next().show();
                counter++;
            } else {
              $(this).css('border-color','#ccc');
              $(this).next().hide();
            }
          }
      });

    return counter;
}

function isValidEmailAddress(email) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(email);
};

/* End of Validation */

/* start file manager */
$('.btn-select-image').click(function(){
  $('.c-copy').hide();
  filemanager('');
  $('#filemanager-image-modal').modal('show');
  $('.btn-copy-file').attr('attr-src','imgsrc_image');
});
$(document).on('click','.btn-copy-file', function(){
  var src = $('.check_active').attr('file-name');
  var old_src = $('.imgsrc').val();
  var type = $(this).siblings('.pathtocopy').attr('data-type'); 
  var path = $(this).attr('attr-src');
  ext = src.split('.').pop();
  ext = ext.toLowerCase();
 	if(type == 'file'){
		bootbox.alert('Please select image only.');
		if(old_src){
  		$('.selected-image').html(' <img class="imgsrc_image img-responsive" src="<?= base_url() ?>'+old_src+'" width="100%" /><input type="text"  class = "imgsrc inputs none filename" name="image"  value="'+old_src+'" hidden/>');
  		}
	 }else{
	  $('.selected-image').html(' <img class="imgsrc_image img-responsive" src="<?= base_url() ?>'+src+'" width="100%" /><input type="text"  class = "imgsrc inputs none filename" name="image"  value="'+src+'" hidden/>');
	 }
});
function filemanager(folder){
  $.ajax({
    type: 'Post',
	url: "<?=base_url()?>global_controller/getimages",
    data:{folder:folder},
  }).done( function(data){
    $('.file-manager').html(data);
  }); 
}
/* End of file manager */




});	

</script>