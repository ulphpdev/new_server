<ol class="breadcrumb" style="background-color: #fff;">
    <li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
    <li>Data Maintenance</li>
    <li class="active">Disposition</li>
</ol>

<div class="col-md-4">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Disposition Name" id="txt_search" placeholder="">
    <span class="er-msg file-err">This field is required</span>
    <div class="input-group-btn">
      <button class="btn btn-primary" id="btn_search" type="submit">
          <i class="glyphicon glyphicon-search"> </i> 
          Search
      </button>
    </div>
  </div>
  <span class="max_label"></span>
</div>

<div class="btn-navigation menu-btn col-md-8 pad-0" >
	<button class="add btn-min btn btn-success bold "><span class = "glyphicon glyphicon-plus"></span> ADD DISPOSITION <span></button>
	<button class="article_list btn-min btn btn-default bold " ><span class = "glyphicon glyphicon-remove-circle "></span> CLOSE</button>
	<button data-type = "Delete" class="inactive btn-min btn btn-default bold " style="display: none;"><span class = "glyphicon glyphicon-trash"></span> Delete</button> 
  	<button data-type = "Activate" class="inactive activate_btn btn-min btn btn-default bold "><span class = "glyphicon glyphicon-ok"></span> Activate</button>
  	<button data-type = "Deactivate" class="inactive btn-min btn btn-default bold "><span class = "glyphicon glyphicon-remove-circle"></span> Deactivate</button>
</div>

<div class="row">
    <div class="col-md-12" id="list_data">
        <div class="table-responsive">

            <!-- LIST -->
            <table class = "table listdata table-striped" style="margin-bottom:0px;">
                <thead>
                    <tr>
                        <th style = "width:10px;"><input class = "selectall" type = "checkbox"></th>
                        <th style = "width:250px;">Disposition Name</th>
                        <th style = "width:250px;">Disposition Label</th>
                        <th>Date &amp; Time Modified</th>
                        <th>Status</th>
                        <th style="text-align: right; width: 100px;">Actions</th>
                    </tr>  
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <!-- PAGINATION -->
        <div style="text-align: center; margin-top: 20px;" class="form-inline pager_div"></div>
    </div>
</div>


<div class="row add_new_div" style="margin-top: 20px;"> 
	<div class="col-md-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Edit Disposition
                </h4>
            </div>
	        <div class="panel-body"  style="padding-bottom: 17px;">
	        	<div class="form-horizontal">
	  				<div class="form-group">
	    				<label class="control-label col-sm-2">Disposition* :</label>
	    				<div class="col-sm-5">
	      					<input type="text" class="form-control inputs-required disposition-input" placeholder="" />
	      					<span class = "txt_error er-msg">This feild should not be empty *</span>
	    				</div>
	  				</div>
	  				<div class="form-group">
	    				<label class="control-label col-sm-2" >Disposition Label :</label>
	    				<div class="col-sm-10"> 
	      					<textarea type="text" class="form-control disposition-label-input" rows="5"></textarea>
	      					<span class = "txt_error er-msg"></span>
	    				</div>
	  				</div>
	  			</div>
	  			<input type="text" class="form-control disposition-id-input hidden" placeholder="" />
	        </div>
            <div class="panel-footer">
                <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button> 
                <button class="disposition_save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
                <button class="article_list cancel btn-min btn btn-default  bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
            </div>
	    </div>
	</div>
</div>

<script>
	var limit = 10;
	var offset = 0;
	var query = "";
	var current_disposition = "";
	var current_disposition_label = "";

	var count_active_current = 0;
  	var max_current = 10;
	var self_count = 0;

  	$(document).ready(function() {
	    get_list(query, limit, offset);
	    get_pagination(query);
  	});

  	<?php $this->load->view('layout/listing_function');?>

  	function ifEmpty(selector) {
	    var ctr = 0;
	    $('.'+selector).each(function(index) {
	      	if($(this).val()=='') {
		        $(this).addClass('hasError');
		        $(this).next().show();
		        ctr++;
	      	} else {
		        $(this).removeClass('hasError');
		        $(this).next().hide();
		    }
		});
	    return ctr;  
	}
	function get_list(query, limit, offset){
        isLoading(true);
        $('.delete').hide();
        $('.disposition_save').hide();
        $('.update').hide();
        $('.article_list').hide();
        $('.inactive').hide();
        $('.add_new_div').hide();
        $('.selectall').attr('checked', false);

        setTimeout( function() { 
    		count_active_current = 0;
        	self_count = 0;

    	  	
    	    aJax.postasync(
    	        "<?=base_url('global_controller/getlist_dm');?>",
    	        {
    	            'limit':limit, 
    	            'offset':offset, 
    	            'query':query, 
    	            'table':'tbl_disposition', 
    	        },
    	        function(result){
    	            var obj = JSON.parse(result);
    	            var table_body = new TBody(); //please check helper.js for this function
    	            if(obj.length!=0){  
    	              $.each(obj,function(key, row){
    	                  if(row.status == 1){
    	                    var status = "Active";
    	                  } else {
    	                    var status = "Inactive";
    	                  }
    	                  table_body.td("<input class = 'select' data-status="+row.status+" data-id='" + row.disposition_id + "' id = '"+row.disposition_id+"' data-name='"+row.disposition_name+"' type ='checkbox'>");
                          table_body.td(row.disposition_name);
    	                  table_body.td(row.disposition_label);
    	                  table_body.td(moment(row.update_date).format("LLL"));
    	                  table_body.td(status);
    	                  var action_button = new UList(); //please check helper.js for this function
    	                  action_button.set_liclass("li-action");
    	                  action_button.set_ulclass("ul-action");
    	                  action_button.li("<a class='action delete_data action_list' data-status='' id='"+row.disposition_id+"' data-name='"+row.disposition_name+"' Title='Delete'> <span class='glyphicon glyphicon-trash action'></span></a>");
    	                  action_button.li("<a  class='action disposition_edit action_list' href='#' id='"+row.disposition_id+"' data-toggle='tooltip' data-placement='bottom' title='Edit'><span class='glyphicon glyphicon-pencil action'></span></a>");
    	                  table_body.td(action_button.set());
    	                  table_body.set();
    	              })
    	            } else {
    	              table_body.td_norecord(7);
    	              table_body.set();
    	            }
    	            table_body.append('.listdata tbody');
    	            isLoading(false);
    				check_no_of_active();
    	        }
    	    );
        }, 1000);
	}
	function get_pagination(query){
	    aJax.post(
	        "<?=base_url("global_controller/getlist_dm");?>",
	        {
	          'limit':9999, 
	          'offset':offset, 
	          'query':query, 
	          'table':'tbl_disposition', 
	        },
	        function(result){
	          var obj = JSON.parse(result);
	          var result_count = obj.length;
	          var no_of_page = Math.ceil(result_count / limit);
	          var pagination = new Pagination(); //helper.js
	          pagination.set_total_page(no_of_page);
	          pagination.set('.pager_div');
	        }
	    );
	  }
	$(document).on('keypress', '#txt_search', function(e){
	    if (e.which == 13) { 
	      $('#btn_search').click()
	    }
	  });

	  $(document).on('click', '#btn_search', function(){
	    var keyword = $('#txt_search').val();
	    if(keyword.trim() == ""){
	      query = "";
	      get_list(query,limit,offset);
	      get_pagination(query);
	    } else {
	      query = "disposition_name like '%" + keyword + "%'";
	      get_list(query,limit,offset);
	      get_pagination(query);
	    }
	  });

	$(document).on('click', '.inactive', function(){
	    var x = 0;
	    var data_type = $(this).attr('data-type').toLowerCase();
	    $('.select').each(function() {                
	        if (this.checked==true) {  x++;   } 
	    });

	    if (x > 0 ) {
	      change_status_modal(
	        "Are you sure you want to "+data_type+" selected record?",
	        data_type,
	        function(result){
	          if(result){
	            isLoading(true);
				setTimeout(function() {
				update_satus(data_type);
				}, 1000);
	          }
	        }
	      );
	    }

	});


	function update_satus(type){
		var x = 0;
	    var trail_label = "";
	    var message = "";
	    if (type == 'delete') {
	      var type = '-2';
	      trail_label = "Delete ";
	      message = "<b>Successfully deleted records.</b>";
	    }
	    if (type == 'deactivate') {
	      var type = '0';
	      trail_label = "Deactivate ";
	      message = "<b>Successfully deactivate records.</b>";
	    }
	    if (type == 'activate') {
	      var type = '1';
	      trail_label = "Activate ";
	      message = "<b>Successfully activate records. </b>";
	    }
	    
		var count_to_active = count_status_1('.select');
		if(count_to_active <= 10){
			$('.select').each(function() {                
				if (this.checked==true) {
					var table = "tbl_disposition";
					var order_by = "disposition_id";
					var id = $(this).attr('data-id');
					var name = $(this).attr('data-name');
					
					aJax.postasync(
						"<?=base_url('global_controller/inactive_global_update');?>",
						{
							id:id,
							type:type, 
							table:table, 
							order_by: order_by
						},
						function(data){
							updateAPI("disposition");
							insert_audit_trail(trail_label + name);
						}
					); 
					x++;
				} 
			});

			
			isLoading(false);
			bootbox.alert(message, function(){
				location.reload();
			});
		} else {
			isLoading(false);
			bootbox.alert("Maximum Active <b>Disposition</b> Exceeded!", function(){
				location.reload();
			});
		}
	    
	};

    $('.selectall').click(function(){
      $(".select").each(function() { 
        //get all checked 0 status
        if($(this).attr('data-status') == 0){
          if (this.checked==true) {
            self_count++;
          } else {
            self_count--;
          }
        }

          var total_count = parseInt(count_active_current) + parseInt(self_count);

          var label_max = "Total Number of Active Records: "+total_count+" / " + max_current;
          $(".max_label").html(label_max);
          if(total_count > max_current){
            $(".max_label").attr("style","color: red;");
          } else {
            $(".max_label").attr("style","color:#333;");
          }
      });
    });

    $(document).on('change', '.select', function(){
      if($(this).attr('data-status') == 0){
        if (this.checked==true) {
          self_count++;
        } else {
          self_count--;
        }
      }
      
      var total_count = parseInt(count_active_current) + parseInt(self_count);

      var label_max = "Total Number of Active Records: "+total_count+" / " + max_current;
      $(".max_label").html(label_max);
      if(total_count > max_current){
        $(".max_label").attr("style","color: red;");
      } else {
        $(".max_label").attr("style","color:#333;");
      }
    });

    function check_no_of_active(){

      	aJax.postasync(
        	"<?=base_url('global_controller/no_of_active_record');?>",
        	{
          		table : 'tbl_disposition',
        	},
        	function(result){
          		count_active_current = result;
        	}
      	);

      	$(".select").each(function() { 
        //get all checked 0 status
        	if($(this).attr('data-status') == 0){
         	 	if (this.checked==true) {
            		count_active_current++;
          		} 
        	}
      	});

        
      	var label_max = "Total Number of Active Records: "+count_active_current+" / " + max_current;
      	$(".max_label").html(label_max);

	  	if(count_active_current > max_current){
			$(".max_label").attr("style","color: red;");
		} else {
			$(".max_label").attr("style","color:#333;");
		}

    }

	function count_status_1(element){
		var count_active = 0;
		aJax.postasync(
			"<?=base_url('global_controller/no_of_active_record');?>",
			{
				table : 'tbl_disposition',
			},
			function(result){
				count_active = result;
			}
		);

		$(element).each(function() { 
		//get all checked 0 status
		if($(this).attr('data-status') == 0){
			if (this.checked==true) {
			count_active++;
			}
		}
		});

		return count_active;
	}
	
	$(document).on('click', '.add', function(){
        isLoading(true);
		aJax.post(
		    "<?=base_url('global_controller/count_active_record');?>",
		    {
		      table : 'tbl_disposition',
		      max: 10
		    },
		    function(result){
				console.log(result);
		        if(result){
		        	$(this).hide();
					$('.disposition_save').show();
					$('.article_list').show();
					$('.update').hide();
		         	$('.add_new_div').show();
		         	$(".disposition-input").val("")
					$(".disposition-label-input").val("");

                    $(".disposition-label-input").css('border-color','#ccc');
                    $(".disposition-label-input").next().hide();

                    $(".panel-title").html("Add Disposition");

                    $('html, body').animate({
                        scrollTop: $(".add_new_div").offset().top
                    });

                    isLoading(false);
		        } else {
                    isLoading(false);
		          	bootbox.alert("Maximum Active <b>Disposition</b> Exceeded!");

		        }
		    }
		);
	})
	$(document).on('click', '.article_list', function(){
		$(this).hide();
		$('.disposition_save').hide();
		$('.update').hide();
		$('.add').show();
		$('.add_new_div').hide();
		$('.article_list').hide();
		get_list(query, limit, offset);
	    get_pagination(query);
	})

	$(document).on('click', '.disposition_save', function(){
		var ctr = FormValidator('.inputs-required');
		if(ctr==0) {
			bootbox.confirm({
			    message: "Are you sure you want to save this record?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			       if(result){
                    isLoading(true);
			       		var dispotition = $(".disposition-input").val()
						var disposition_label = $(".disposition-label-input").val();
						var data = {"disposition": dispotition, "dispotition_label": disposition_label};
			       		isduplicate("tbl_disposition", "disposition_name = '" + dispotition + "' AND disposition_label = '" + disposition_label + "' AND status >= 0", function(result){
					      	if(result == "true"){
					        	// $(".disposition-input").css('border-color','red');
					        	// $(".disposition-input").next().html("This field should contain a unique value.");
					        	// $(".disposition-input").next().show();

					        	$(".disposition-label-input").css('border-color','red');
					        	$(".disposition-label-input").next().html("This field should contain a unique value for " + dispotition + " disposition.");
					        	$(".disposition-label-input").next().show();
					      	} else {
					      		$.ajax({
						      		type: 'Post',
						      		url:'<?=base_url();?>c_maintenance/disposition_add',
						      		data:data,
						      	}).done( function(result){
					      			updateAPI("disposition");
					      			if(result){
					      				updateAPI("disposition");
					      				insert_audit_trail("Create " + dispotition);
						      			$(".disposition-input").val("")
										$(".disposition-label-input").val("");
                                        bootbox.alert("<b>Record is successfully saved!</b>",function(){
                                            location.reload();
                                        })

						      		}
						      	});
					      	}
					  	});
			       }
			    }
			});
		}
	})
	$(document).on('click','.disposition_edit', function($e) {
		$e.preventDefault();
        isLoading(true);
		var id = $(this).attr('id');
		$.ajax({
	      	type: 'Post',
	      	url:'<?=base_url();?>c_maintenance/dispotition_get_details',
	      	data:{"id": id},
	      	}).done( function(result){
	      		var obj = isJson(result);
				$('.update').show();
				$('.article_list').show();
				$('.save').hide();
				$('.add_new_div').show();
				$.each(obj, function(index, row){
					current_disposition = row.disposition_name;
					current_disposition_label = row.disposition_label;
					$('.disposition-id-input').val(row.disposition_id);
					$('.disposition-input').val(row.disposition_name);
					$('.disposition-label-input').val(row.disposition_label);

                    $(".disposition-label-input").css('border-color','#ccc');
                    $(".disposition-label-input").next().hide();
                    $(".panel-title").html("Edit Disposition");

                    $('html, body').animate({
                        scrollTop: $(".add_new_div").offset().top
                    });

                    isLoading(false);
                    
				})
	      		console.log(result);
	      	});
	}) ;
	$(document).on('click', '.update', function(){
		var ctr = FormValidator('.inputs-required');
		if(ctr==0) {
			bootbox.confirm({
			    message: "Are you sure you want to update this record?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			       if(result){
			       		var dispotition = $(".disposition-input").val()
						var disposition_label = $(".disposition-label-input").val();
						var id = $(".disposition-id-input").val();
						var data = {"disposition": dispotition, "dispotition_label": disposition_label, "id":id};
						isduplicate("tbl_disposition", "disposition_name = '" + dispotition + "' AND disposition_label = '"+disposition_label+"' AND disposition_label <> '"+current_disposition_label+"' AND status >= 0", function(result){
					      	if(result == "true"){
					        	$(".disposition-label-input").css('border-color','red');
					        	$(".disposition-label-input").next().html("This field should contain a unique value for " +dispotition + " disposition.");
					        	$(".disposition-label-input").next().show();
					      	} else {
					      		$.ajax({
						      		type: 'Post',
						      		url:'<?=base_url();?>c_maintenance/disposition_update',
						      		data:data,
						      	}).done( function(result){
						      		if(result){
						      			updateAPI("disposition");
						      			insert_audit_trail("Update " + dispotition);
						      			bootbox.alert('<b>Record is successfully saved!</b>');
						      			$(".disposition-input").val("")
										$(".disposition-label-input").val("");
						      		} else {
						      			console.log("failed!!");
						      		}
						      		get_list(query,limit, offset);
						   		 	get_pagination(query);
						      	});
					      	}
					      });
			       }
			    }
			});
		}
	})
	$(document).on('click', '.dispotition_trash', function($e){
		$e.preventDefault();
		var id = $(this).attr('id');
		bootbox.confirm({
			size: "small",
		    message: "Are you sure you want to delete this record(s)?",
		    buttons: {
		        confirm: {
		            label: 'Yes',
		            className: 'btn-success'
		        },
		        cancel: {
		            label: 'No',
		            className: 'btn-danger'
		        }
		    },
		    callback: function (result) {
		       if(result){
		       		update_list_status("tbl_disposition", "-2", "Successfully deleted!");
		       }
		    }
		});
	})
	$(document).on('click', '.publish', function($e){
		$e.preventDefault();
		var id = $(this).attr('id');
		aJax.post(
		    "<?=base_url('global_controller/count_active_record');?>",
		    {
		      table : 'tbl_disposition',
		      max: 10
		    },
		    function(result){
		        if(result){
		        	bootbox.confirm({
						size: "small",
					    message: "Are you sure you want to activate this record(s)?",
					    buttons: {
					        confirm: {
					            label: 'Yes',
					            className: 'btn-success'
					        },
					        cancel: {
					            label: 'No',
					            className: 'btn-danger'
					        }
					    },
					    callback: function (result) {
					       if(result){
					       		update_list_status("tbl_disposition", "1", "Successful change in Status!");
					       }
					    }
					});
		        } else {
		          	bootbox.alert("Maximum Active <b>Disposition</b> Exceeded!");
		        }
		    }
		);
	})
	$(document).on('click', '.unpublish', function($e){
		$e.preventDefault();
		var id = $(this).attr('id');
		bootbox.confirm({
			size: "small",
		    message: "Are you sure you want to unpublish this record(s)?",
		    buttons: {
		        confirm: {
		            label: 'Yes',
		            className: 'btn-success'
		        },
		        cancel: {
		            label: 'No',
		            className: 'btn-danger'
		        }
		    },
		    callback: function (result) {
		       if(result){
		       		update_list_status("tbl_disposition", "0", "Successful change in Status!");
		       }
		    }
		});
	})
	function update_list_status(table, status, msg) {
	    var s = '';
	      $('.select:checked').each(function(index) { 
	          s+= "'"+$(this).attr('data-id')+"'"+',';
	      });
	      s = s.slice(0,-1);
	      $.ajax({
	        type: "POST",
	        url: '<?=base_url();?>c_maintenance/update_status',
	        data: {table: table, status: status, field: 'disposition_id', where: s}
	        }).done(function(result) {
	          bootbox.alert('<b>' + msg +  '</b>', function() {
	            location.reload();
	          });
	          updateAPI("disposition");
	        });
	  }
	$(document).on('click', '.delete_data', function(){
		var x = 0;
    	var id = $(this).attr('id');
    	var name = $(this).attr('data-name');
    	var table = "tbl_disposition";
  		var order_by = "disposition_id";
          var dialog = bootbox.confirm({
          message: "Are you sure you want to delete this record?",
          buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function (result) {
            if(result){
            $.ajax({
                    type: 'Post',
                    url:'<?=base_url();?>Under_data_maintenance/data_maintenance_delete',
                   data:{id:id, status:"-2",table:table,order_by:order_by},
                    }).done( function(data){
                    	updateAPI("disposition");
                    	insert_audit_trail("Delete " + name);
                        dialog.modal('hide');
                        get_list(query, limit, offset);
                        get_pagination(query);
                    });
            }          
          }
      });
	});
</script>
