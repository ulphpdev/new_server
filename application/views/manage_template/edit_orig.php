<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            Edit Template
        </h4>
    </div>
    <div class="panel-body">
        <input type="hidden" name="success" id="success" value="">
        <div class="col-md-12 col-sm-12 container-template_add">
           <div class="col-md-12 add-template-body">
              <div class="col-md-12 pad-5">
                 <div class="col-md-2">Product Type</div>
                 <div class = "col-md-4">
                    <select id="classification" class = "inputcss form-control fullwidth inputs">
                    </select>
                    <span class = "er-msg">Classification should not be empty *</span>
                 </div>
              </div>
              <div class="col-md-12 pad-5">
                 <div class="col-md-2">Standard/Reference</div>
                 <div class = "col-md-4">
                    <select id="standard_reference" class = "inputcss form-control fullwidth inputs">
                    </select>
                    <span class = "er-msg">Standard/Reference should not be empty *</span>
                 </div>
              </div>
           </div>
           <div class="col-md-12 add-template-body-bottom" style="margin-top: 30px;">
              <div class="col-md-12" id="element-content">
                  <table class="activity_table"></table>
              </div>
           </div>
           <div class="clearfix"></div>
           <hr>
           <div class="col-md-12" style="margin-top: 30px;">
              <div class="col-md-12" id="activity-content">
              </div>
           </div>
           <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-warning set_active update_status" data-status="1" data-id="<?= $id; ?>"> Activate</button>
        <button type="submit" class="btn btn-danger  set_inactive update_status" data-status="-1" data-id="<?= $id; ?>"> Deactivate</button>
        <button type="submit" class="btn btn-info save_final" data-version=1 data-sub-version=0 data-status="1" id=""><span class = "glyphicon glyphicon-paperclip"></span> Save as Final</button>
        <button type="submit" class="btn btn-primary savenewversion" data-version="" data-sub-version="0" id="" ><span class = "glyphicon glyphicon-paperclip"></span> Save as New Version</button>
        <button type="submit" class="btn btn-success update_template" data-version="" data-sub-version="" id=""><span class = "glyphicon glyphicon-floppy-saved"></span> Update</button>
        <button type="submit" class="btn btn-default" onclick="location.reload();" id=""><span class = "glyphicon glyphicon-remove-circle"></span> Close</button>
        <div class="clearfix"></div>
    </div>
</div>





<script type="text/javascript">

  var success_activity = false;
  var success_questions = false;  
  var question_count = 0;
  var q_current_count = 0;

  var current_classification = "";
  var current_standard = "";

  var element = new Template();
  var current_template = "<?= $id; ?>";

  $(document).ready(function() {
      var id = "<?= $id; ?>";
      isLoading(true);
      get_template(id);
  });


  function get_template(id){
      aJax.post(
          "<?=base_url('Manage_template/view_template')?>",
          {
              id:id,
              table:'tbl_template',
              field:'template_id'
          },
          function(result){
              var obj = JSON.parse(result);
              $.each(obj, function(x,y){
                  current_standard = y.standard_id;
                  current_classification = y.classification_id;

                  productType(y.classification_id);
                  standard_reference(y.standard_id);
                  get_activity(y.template_id);
                  get_elements(y.template_id);
                  $('.savenewversion').attr("data-version",parseInt(y.version) + 1);
                  $('.savenewversion').attr("data-sub-version",parseFloat(y.sub_version));
                  $('.update_template').attr("data-version",parseFloat(y.version));
                  $('.update_template').attr("data-sub-version",parseFloat(y.sub_version) + 1);
                  if(y.status == 1 || y.status == -1){
                      $('.savenewversion').show()
                      $('.set_inactive').show()
                      $('.set_active').show()
                      $('.save_final').hide()
                  }else{
                      $('.savenewversion').hide()
                      $('.set_inactive').hide()
                      $('.set_active').hide()
                      $('.save_final').show()
                  }
              });
          }
      );

  };

  function get_elements(template_id){
    aJax.post(
        "<?=base_url('Manage_template/get_elements')?>",
        {
            template_id:template_id
        },
        function(result){
            var obj = JSON.parse(result);
            var element_position = 0;

            $.each(obj, function(x,y){
                element_position++;
                element.set_element_value(y.element_name);
                element.set_element_id(y.element_id);
                element.set_element_position(element_position);
                element.append_element("#element-content",obj.length);
                get_questions(y.element_id,template_id,element_position)
            });

        }
    );
  };

  function get_questions(element_id,template_id,element_position){
    aJax.post(
        "<?=base_url('Manage_template/get_questions')?>",
        {
            element_id:element_id,
            template_id:template_id
        },
        function(result){
            var obj = JSON.parse(result);
            console.log(obj);
            var question_position = 0;
            $.each(obj, function(x,y){
              question_position++ ;
              element.set_question_value(y.question);
              element.set_question_id(y.question_id);
              element.set_question_default_yes(y.default_yes);
              element.set_question_position(question_position);
              element.set_question_element(element_position);
              element.set_question_required_remarks(y.required_remarks);
              element.append_question(obj.length);
            });
            isLoading(false);
        }
    );
  };

  function productType(selected){
    aJax.get(
        "<?=base_url('Manage_template/get_product_type')?>",
        function(result){
            var obj = JSON.parse(result);
            var htm = '';
            $.each(obj, function(x,y){
              htm += '<option value="'+y.classification_id+'">'+y.classification_name+'</option>';
            });
            $('#classification').append(htm);
            $('#classification option[value="'+selected+'"]').attr('selected','selected');
        }
    );
  }

  function standard_reference(selected){
    aJax.get(
        "<?=base_url('Manage_template/get_standard_reference')?>",
        function(result){
          var obj = JSON.parse(result);
          var htm = '';
          $.each(obj, function(x,y){
            htm += '<option class="sr sr-'+y.standard_id+'" value="'+y.standard_id+'">'+y.standard_name+'</option>';
          });
          $('#standard_reference').append(htm);
          $('#standard_reference option[value="'+selected+'"]').attr('selected','selected');
        }
    );
  }

  function get_activity(template_id){
    aJax.post(
        "<?=base_url('Manage_template/get_activity')?>",
        {
          template_id:template_id
        },
        function(result){
          var obj = JSON.parse(result);
          var act_position = 0;
          $.each(obj, function(x,y){
              act_position++;
              element.set_activity_value(y.activity_name);
              element.set_activity_id(y.activity_id );
              element.set_activity_position(act_position);
              element.append_activity("#activity-content", obj.length);

              get_subactivity(y.activity_id,act_position);
          });
        }
    );
  }

  function get_subactivity(activity_id,act_position){
    aJax.post(
        "<?=base_url('Manage_template/get_sub_activity')?>",
        {
            activity_id:activity_id,
        },
        function(result){
            var obj = JSON.parse(result);
            var htm = '';
            var subact_position = 0;

            $.each(obj, function(x,y){
              subact_position++;
              element.set_subactivity_value(y.sub_item_name);
              element.set_subactivity_position(subact_position);
              element.set_subactivity_activity(act_position);
              element.set_subactivity_id(y.sub_item_id);
              element.append_subactivity(obj.length);

            });
        }
    );
  };

    $('.update_template').click(function(){
      var error = 0;
      
      //CHECK SELECT IF VALID
      $('.inputcss').each(function(){
        if($(this).val() == ''){
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        }else{
            $(this).css('border-color', '#ccc');
            $(this).next().hide();
        }
      });

      //CHECK ALL ELEMENT IF NOT NULL
      $('.elem_val').each(function(){
        var elem_id = $(this).attr('data-elem');
        if($(this).val() == ''){
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        }else{
            $(this).css('border-color', '#ccc');
            $(this).next().hide();
            
        }
      });

      //CHECK ALL QUESTION IF NOT NULL
      $('.ques_val').each(function(){
          if($(this).val() == ''){
              $(this).css('border-color', 'red');
              $(this).next().show();
              error++;
          }else{
              $(this).css('border-color', '#ccc');
              $(this).next().hide();
          }
      });

      //CHECK ALL QUESTION DEFAULT YES IF NOT NULL
      $('.ques_val_yes').each(function(){
          if($(this).val() == ''){
              $(this).css('border-color', 'red');
              $(this).next().show();
              error++;
          }else{
              $(this).css('border-color', '#ccc');
              $(this).next().hide();
          }
      });

      //CHECK ALL ACTIVITY IF NOT NULL
      $('.act_val').each(function(){
          if($(this).val() == ''){
              $(this).css('border-color', 'red');
              $(this).next().show();
              error++;
          }else{
              $(this).css('border-color', '#ccc');
              $(this).next().hide();
          }
      });

      //CHECK ALL SUB ACTIVITY IF NOT NULL
      $('.act_sub_val').each(function(){
          if($(this).val() == ''){
              $(this).css('border-color', 'red');
              $(this).next().show();
              error++;
          }else{
              $(this).css('border-color', '#ccc');
              $(this).next().hide();
          }
      });


      if(error == 0){
          isLoading(true);
          var classification_id = $('#classification').val();
          var standard_id = $('#standard_reference').val();
          var new_version = $(this).attr('data-version');
          var sub_version = $(this).attr('data-sub-version');
          var error_result = 0;

          aJax.post(
              "<?=base_url('Manage_template/check_template_standard')?>",
              {
                classification_id:classification_id,
                standard_id:standard_id
              },
              function(result){

                 aJax.post(
                      "<?=base_url('Manage_template/update_template')?>",
                      {
                          classification_id:classification_id,
                          standard_id:standard_id,
                          new_version: new_version,
                          sub_version: sub_version,
                          table: 'tbl_template',
                          field: 'template_id',
                          id: current_template
                      },
                      function(result){
                          insert_audit_trail("Update Template " + current_template + " version " + new_version + '.' + (parseFloat(sub_version) - 1) +  " to " + new_version + '.' + sub_version);
                          //SAVE ACTIVITY
                            var activity_count = $(".act_val").length;
                            var current_count = 0;

                          $('.act_val').each(function(){
                              var activity_value = $(this).val();
                              var activity_position =  $(this).attr('position');
                              var activity_order =  $(this).attr('order');
                              var val_id =  $(this).attr('val-id');
                              var percent = 0;
                             console.log(val_id);
                              if(val_id == 0){
                                  //INSERT ACTIVITY
                                  aJax.post(
                                      "<?=base_url('Manage_template/save_activity')?>",
                                      {
                                          activity_name:activity_value,
                                          order:activity_order,
                                          template_id:current_template
                                      },
                                      function(activity_id){
                                        if(activity_id){
                                          save_subactivity(activity_id,activity_position,current_template);
                                        } else {
                                          error_result ++;
                                        }
                                        
                                      }
                                  );

                              } else {
                                  //UPDATE ACTIVITY
                                  aJax.post(
                                      "<?=base_url('Manage_template/update_activity')?>",
                                      {
                                          activity_id:val_id,
                                          activity_name:activity_value,
                                          order:activity_order,
                                          table:'tbl_activities',
                                          field:'activity_id',
                                          activity_id:val_id,
                                      },
                                      function(activity_id){
                                         if(activity_id){
                                          save_subactivity(val_id,activity_position,current_template);
                                          } else {
                                            error_result ++;
                                          }
                                                              
                                      }
                                  );

                              }
                              current_count++;
                             if(activity_count  ==  current_count) {
                                success_activity = true;
                                if(success_activity == true && success_questions == true){
                                  isLoading(false);
                                  generate_json(template_id);
                                  update_config();
                                  
                                  bootbox.alert("Template is successfully saved.",function(){
                                    $('html, body').animate({
                                        scrollTop: $("html").offset().top
                                    }, 100);
                                    location.reload();
                                  });
                                }
                              }
                                  
                          });

                          var element_pos = 0;

                          //SAVE ELEMENT
                          question_count = $('.question_div').length;
                          $('.elem_val').each(function(){
                                var element_name = $(this).val();
                                var element_order = $(this).attr('data-order');
                                var val_id =  $(this).attr('val-id');
                                element_pos++;
                                console.log(element_pos);
                                if(val_id == 0){
                                    //INSERT ELEMENT
                                    aJax.post(
                                      "<?=base_url('Manage_template/save_element')?>",
                                      {
                                          element_name:element_name,
                                          order:element_pos
                                      },
                                      function(element_id){
                                        if(element_id){
                                          save_questions(element_id,current_template,element_order);
                                        } else {
                                          error_result ++;
                                        }
                                      }
                                    );
                                } else {
                                    //UPDATE ELEMENT
                                    aJax.post(
                                        "<?=base_url('Manage_template/update_element')?>",
                                        {
                                            element_id:val_id,
                                            element_name:element_name,
                                            order:element_pos,
                                            table:'tbl_elements',
                                            field:'element_id'
                                        },
                                        function(element_id){
                                            save_questions(val_id,current_template,element_order);
                                        }
                                    );
                                }
                                    
                            });




                          //CHECK ERROR
                          // if(error_result == 0){
                          //   bootbox.alert("Template was successfully saved", function(){
                          //     update_config();
                          //     generate_json(current_template);
                          //     insert_audit_trail("Update Template " + current_template);
                          //     location.reload();
                          //   });
                          // } else {
                          //   bootbox.alert("Error saving template", function(){});
                          // }
                      }
                  );

            }
            
          );

      }

  });


  function save_subactivity(activity_id,act_ctr,template_id){
    console.log("enter subactivity function - " + activity_id);
    $('.act_sub_val_'+act_ctr).each(function(){
        var subActivity = $(this).val();
        var val_id =  $(this).attr('val-id');
        var order =  $(this).attr('order');

        if(val_id == 0 ){
            //INSERT SUB ACTIVITY
            aJax.post(
                "<?=base_url('Manage_template/save_subactivity')?>",
                {
                    activity_id:activity_id,
                    order:order,
                    subActivity:subActivity
                },
                function(result){
                }
            );
        } else {
            //UPDATE SUB ACTIVITY
            aJax.post(
                "<?=base_url('Manage_template/update_subactivity')?>",
                {
                    sub_item_id:val_id,
                    sub_item_name:subActivity,
                    order:order,
                    table:'tbl_sub_activities',
                    field:'sub_item_id',
                },
                function(result){
                }
            );
        } 
        
    });
  }


  function save_questions(element_id,template_id,elem_ctr){

    console.log("question function entered");
    $('.element_question_'+elem_ctr+' .question_div').each(function(){
      var question_val = $(this).find('.ques_val').val();
      var default_yes = $(this).find('.ques_val_yes').val();
      var required_remarks = $(this).find('.ques_required').is( ':checked' ) ? 1: 0;
      var val_id =  $(this).find('.ques_val').attr('val-id');
      var question_order = $(this).find('.ques_val').attr('data-order');


      if(val_id == 0){
          //INSERT QUESTION
          aJax.post(
              "<?=base_url('Manage_template/save_questions')?>",
              {
                  element_id:element_id,
                  template_id:template_id,
                  question:question_val,
                  default_yes:default_yes,
                  required_remarks:required_remarks,
                  order:question_order
              },
              function(result){
                q_current_count++;
                console.log(q_current_count);
                var percent = (q_current_count / question_count ) * 100;
                console.log(percent);
                $('.progress-bar-question').attr("aria-valuenow",percent);
                $('.progress-bar-question').html(~~percent + "%");
                $('.saving_question_label').html("Saving Element Questions :" + q_current_count + " / " + question_count);
                $('.progress-bar-question').css("width", percent + "%");

                generate_json(template_id);

                if(question_count  ==  q_current_count) {
                  success_questions = true;
                  if(success_activity == true && success_questions == true){
                    isLoading(false);
                    generate_json(template_id);
                    update_config();
                    bootbox.alert("Template is successfully saved.",function(){
                      $('html, body').animate({
                          scrollTop: $("html").offset().top
                      }, 100);
                    });
                    
                  }
                }
              }
          );
      } else {
          //UPDATE QUESTION
          aJax.post(
              "<?=base_url('Manage_template/update_question')?>",
              {
                  question:question_val,
                  default_yes:default_yes,
                  order:elem_ctr,
                  table:'tbl_questions',
                  field:'question_id',
                  required_remarks:required_remarks,
                  question_id:val_id,
                  order:question_order
              },
              function(result){
                q_current_count++;
                console.log(q_current_count);
                var percent = (q_current_count / question_count ) * 100;
                console.log(percent);
                $('.progress-bar-question').attr("aria-valuenow",percent);
                $('.progress-bar-question').html(~~percent + "%");
                $('.saving_question_label').html("Saving Element Questions :" + q_current_count + " / " + question_count);
                $('.progress-bar-question').css("width", percent + "%");

                generate_json(template_id);

                if(question_count  ==  q_current_count) {
                  success_questions = true;
                  if(success_activity == true && success_questions == true){
                    isLoading(false);
                    generate_json(template_id);
                    update_config();
                    bootbox.alert("Template is successfully saved.",function(){
                      $('html, body').animate({
                          scrollTop: $("html").offset().top
                      }, 1000);
                      location.reload();
                    });
                    
                  }
                }
              }
          );
      }

      generate_json(current_template);
      
    });
  }


  function generate_json(template_id){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/api/template_list",
    }).done(function(data){
        $.ajax({
            type: 'post',
            url: "<?=base_url()?>/api/template_info/"+template_id,
        }).done(function(data){
          
        });  
    });
  }

  $(document).on('click','.save_final', function(){  

    var table = "tbl_template";
    var order_by = "template_id";
    var id = <?= $id; ?>;

    aJax.post(
      "<?=base_url('Manage_template/template_as_final')?>",
      {
          table: table,
          field: order_by,
          id: id
      },
      function(result){
        generate_json(id);
                   
        bootbox.alert('<b>Template is successfully saved. </b>', function() {
            insert_audit_trail("Update Template Status "+ current_template +" into Final");
            location.reload();
        }).off("shown.bs.modal");
      }
    );
  });

  function generate_json(template_id){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/api/template_list",
    }).done(function(data){
        $.ajax({
            type: 'post',
            url: "<?=base_url()?>/api/template_info/"+template_id,
        }).done(function(data){
          
        });  
    });
  }


  $(document).on('change', '#classification', function() {
    console.log($(this).val());
    aJax.post(
        "<?=base_url('Manage_template/check_in_use')?>",
        {classification_id : $(this).val()},
        function(result){
          console.log(result);
          var obj = JSON.parse(result);
          
          $('.sr').each(function(){
            $(this).removeAttr("disabled");
          });

          $.each(obj, function(x,y){
              if(current_standard != y.standard_id && current_classification != $(this).val()){
                $('.sr-'+y.standard_id).attr("disabled","disabled");
              }
              
          });

        }

    );

  });


  $('.savenewversion').click(function(){
    console.log($(this).attr('data-version'));
      var error = 0;
      
      //CHECK SELECT IF VALID
      $('.inputcss').each(function(){
        if($(this).val() == ''){
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        }else{
            $(this).css('border-color', '#ccc');
            $(this).next().hide();
        }
      });

      //CHECK ALL ELEMENT IF NOT NULL
      $('.elem_val').each(function(){
        var elem_id = $(this).attr('data-elem');
        if($(this).val() == ''){
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        }else{
            $(this).css('border-color', '#ccc');
            $(this).next().hide();
            
        }
      });

      //CHECK ALL QUESTION IF NOT NULL
      $('.ques_val').each(function(){
          if($(this).val() == ''){
              $(this).css('border-color', 'red');
              $(this).next().show();
              error++;
          }else{
              $(this).css('border-color', '#ccc');
              $(this).next().hide();
          }
      });

      //CHECK ALL QUESTION DEFAULT YES IF NOT NULL
      $('.ques_val_yes').each(function(){
          if($(this).val() == ''){
              $(this).css('border-color', 'red');
              $(this).next().show();
              error++;
          }else{
              $(this).css('border-color', '#ccc');
              $(this).next().hide();
          }
      });

      //CHECK ALL ACTIVITY IF NOT NULL
      $('.act_val').each(function(){
          if($(this).val() == ''){
              $(this).css('border-color', 'red');
              $(this).next().show();
              error++;
          }else{
              $(this).css('border-color', '#ccc');
              $(this).next().hide();
          }
      });

      //CHECK ALL SUB ACTIVITY IF NOT NULL
      $('.act_sub_val').each(function(){
          if($(this).val() == ''){
              $(this).css('border-color', 'red');
              $(this).next().show();
              error++;
          }else{
              $(this).css('border-color', '#ccc');
              $(this).next().hide();
          }
      });


      if(error == 0){
          var activity_count = $(".act_val").length;
          var current_count = 0;
          var percent = 0;
          var classification_id = $('#classification').val();
          var standard_id = $('#standard_reference').val();
          var new_version = $(this).attr('data-version');
          var sub_version = $(this).attr('data-sub-version');
          var error_result = 0;

          aJax.post(
              "<?=base_url('Manage_template/check_template_standard')?>",
              {
                classification_id:classification_id,
                standard_id:standard_id
              },
              function(result){

                 aJax.post(
                      "<?=base_url('Manage_template/update_template')?>",
                      {
                          classification_id:classification_id,
                          standard_id:standard_id,
                          new_version: new_version,
                          sub_version: 0,
                          table: 'tbl_template',
                          field: 'template_id',
                          id: current_template
                      },
                      function(result){

                          insert_audit_trail("Save Template " + current_template + " as new version from " + (parseFloat(new_version) - 1) + '.' + sub_version +  " to " + new_version + '');

                          //SAVE ACTIVITY
                          
                            var activity_count = $(".act_val").length;
                            var current_count = 0;
                          $('.act_val').each(function(){
                              var activity_value = $(this).val();
                              var activity_position =  $(this).attr('position');
                              var val_id =  $(this).attr('val-id');
                                  
                              if(val_id == 0){
                                  //INSERT ACTIVITY
                                  aJax.post(
                                      "<?=base_url('Manage_template/save_activity')?>",
                                      {
                                          activity_name:activity_value,
                                          template_id:current_template
                                      },
                                      function(activity_id){
                                        if(activity_id){
                                            save_subactivity(val_id,activity_position,current_template);
                                        } else {
                                          error_result ++;
                                        }
                                        
                                      }
                                  );

                              } else {
                                  //UPDATE ACTIVITY
                                  aJax.post(
                                      "<?=base_url('Manage_template/update_activity')?>",
                                      {
                                          activity_id:val_id,
                                          activity_name:activity_value,
                                          table:'tbl_activities',
                                          field:'activity_id',
                                          activity_id:val_id,
                                      },
                                      function(activity_id){
                                          if(activity_id){
                                            save_subactivity(val_id,activity_position,current_template);
                                          } else {
                                            error_result ++;
                                          }                    
                                      }
                                  );

                              }
                              // current_count++;
                              // if(activity_count  ==  current_count) {
                              //   success_activity = true;
                              //   if(success_activity == true && success_questions == true){
                              //     isLoading(false);
                              //     generate_json(template_id);
                              //     update_config();
                              //     insert_audit_trail("Save Template " + template_id);
                              //     bootbox.alert("Template is successfully saved.",function(){
                              //       location.reload();
                              //     });
                              //   }
                              // }

                                  
                          });

                          var element_pos = 0;

                          //SAVE ELEMENT
                          question_count = $('.question_div').length;
                          $('.elem_val').each(function(){
                                var element_name = $(this).val();
                                var element_order = $(this).attr("data-order");
                                var val_id =  $(this).attr('val-id');
                                element_pos++;
                                console.log(element_pos);
                                if(val_id == 0){
                                    //INSERT ELEMENT
                                    aJax.post(
                                      "<?=base_url('Manage_template/save_element')?>",
                                      {
                                          element_name:element_name,
                                          order:element_pos
                                      },
                                      function(element_id){
                                        if(element_id){
                                          save_questions(element_id,current_template,element_order);
                                        } else {
                                          error_result ++;
                                        }
                                      }
                                    );
                                } else {
                                    //UPDATE ELEMENT
                                    aJax.post(
                                        "<?=base_url('Manage_template/update_element')?>",
                                        {
                                            element_id:val_id,
                                            element_name:element_name,
                                            order:element_pos,
                                            table:'tbl_elements',
                                            field:'element_id'
                                        },
                                        function(element_id){
                                            save_questions(val_id,current_template,element_order);
                                        }
                                    );
                                }
                                    
                            });




                          //CHECK ERROR
                          // if(error_result == 0){
                          //   bootbox.alert("Template was successfully saved", function(){
                          //     update_config();
                          //     generate_json(current_template);
                          //     insert_audit_trail("Update New Version Template " + current_template);
                          //     location.reload();
                          //   });
                          // } else {
                          //   bootbox.alert("Error saving template", function(){});
                          // }
                      }
                  );

                bootbox.alert("Template is successfully saved.",function(){
                    location.reload();
                });

            }
            
          );

      }

  });

  $(document).on('click', '.btn-done-saving', function() {
    location.reload();
  });


  $(document).on('click', '.update_status', function() {
    var status = $(this).attr('data-status');
    var template_id = $(this).attr('data-id');

    var dialog = bootbox.confirm({
      message: "Are you sure you want to update the status of this record?",
      buttons: {
          cancel: {
              label: 'No',
              className: 'btn-danger'
          },
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          }
      },
      callback: function (result) {
        if(result){
          
          console.log(template_id);
          isLoading(true);
          aJax.post(
            "<?=base_url('global_controller/inactive_global_update');?>",
            {
              id:template_id,
              type:status, 
              table:'tbl_template', 
              order_by: 'template_id'
            },
            function(data){
              isLoading(false);
              if(status == 1){
                insert_audit_trail("Set as Active - " + template_id);
              } else {
                insert_audit_trail("Set as Inactive - " + template_id);
              }
              generate_json(template_id);
              update_config();
              bootbox.alert('<b>Record is successfully updated!</b>', function() {
                location.reload();
              });
            }
          );



          
        }          
      }
    });
  });




</script>