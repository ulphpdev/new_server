<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            Add Template
        </h4>
    </div>
    <div class="panel-body">
        <input type="hidden" name="success" id="success" value="">
        <div class="col-md-12 col-sm-12 container-template_add">
           <div class="col-md-12 add-template-body">
              <div class="col-md-12 pad-5">
                 <div class="col-md-2">Product Type</div>
                 <div class = "col-md-4">
                    <select id="classification" class = "inputcss form-control fullwidth inputs">
                    </select>
                    <span class = "er-msg">Classification should not be empty *</span>
                 </div>
              </div>
              <div class="col-md-12 pad-5">
                 <div class="col-md-2">Standard/Reference</div>
                 <div class = "col-md-4">
                    <select id="standard_reference" class = "inputcss form-control fullwidth inputs">
                    </select>
                    <span class = "er-msg">Standard/Reference should not be empty *</span>
                 </div>
              </div>
           </div>
           <div class="col-md-12 add-template-body-bottom" style="margin-top: 30px;">
              <div class="col-md-12" id="element-content">
                  
              </div>
           </div>
           <div class="clearfix"></div>
              
           <hr>
           <div class="col-md-12" style="margin-top: 30px;">
              <div class="col-md-12" id="activity-content">
                  
              </div>
           </div>
           <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-info save_template" data-status="0" id=""><span class = "glyphicon glyphicon-paperclip"></span> Save as draft</button>
        <button type="submit" class="btn btn-primary save_template" data-status="1" id=""><span class = "glyphicon glyphicon-floppy-saved"></span> Save</button>
        <button type="submit" class="btn btn-default" onclick="location.reload();" id=""><span class = "glyphicon glyphicon-remove-circle"></span> Close</button>
        <div class="clearfix"></div>
    </div>
</div>
<script type="text/javascript">

  var data_status = 0;
  var success_activity = false;
  var success_questions = false;  
  var question_count = 0;
  var q_current_count = 0;

  $(document).ready(function() {

      //helper.js
      var element = new Template();
      element.set_element_value("");
      element.set_element_id(0);
      element.set_element_position(1);
      element.append_element("#element-content",0);

      element.set_question_value("");
      element.set_question_id(0);
      element.set_question_default_yes("");
      element.set_question_position(1);
      element.set_question_element(1);
      element.append_question();

      element.set_activity_value("");
      element.set_activity_id(0);
      element.set_activity_position(1);
      element.append_activity("#activity-content",0);

      aJax.get(
        "<?=base_url('Manage_template/get_product_type')?>",
        function(result){
          var obj = JSON.parse(result);
          var htm = '';
          htm += '<option value="" selected hidden>Select..</option>';
          $.each(obj, function(x,y){
              htm += '<option value="'+y.classification_id+'">'+y.classification_name+'</option>';
          });
          $('#classification').append(htm);
        }
      );


      aJax.get(
        "<?=base_url('Manage_template/get_standard_reference')?>",
        function(result){
          var obj = JSON.parse(result);
          var htm = '';
          htm += '<option value="" selected hidden>Select..</option>';
          $.each(obj, function(x,y){
              htm += '<option class="sr sr-'+y.standard_id+'" value="'+y.standard_id+'">'+y.standard_name+'</option>';
          });
          $('#standard_reference').append(htm);
        }
      );
  });

  $('.save_template').click(function(){
      var error = 0;
      
      //CHECK SELECT IF VALID
      $('.inputcss').each(function(){
        if($(this).val() == ''){
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        }else{
            $(this).css('border-color', '#ccc');
            $(this).next().hide();
        }
      });

      //CHECK ALL ELEMENT IF NOT NULL
      $('.elem_val').each(function(){
        var elem_id = $(this).attr('data-elem');
        var input = $(this).val().trim();
        if (input.length == 0) {
          $(this).css('border-color','red');
          $(this).next().show();
          error++;
        }else{
          if(input == 0 || input == "0"){
              $(this).css('border-color','red');
              $(this).next().html("Invalid Input.");
              $(this).next().show();
              error++;
          } else {
            $(this).css('border-color','#ccc');
            $(this).next().hide();
          }         
        }
      });

      //CHECK ALL QUESTION IF NOT NULL
      $('.ques_val').each(function(){
        var input = $(this).val().trim();
        if (input.length == 0) {
          $(this).css('border-color','red');
          $(this).next().show();
          error++;
        }else{
          if(input == 0 || input == "0"){
              $(this).css('border-color','red');
              $(this).next().html("Invalid Input.");
              $(this).next().show();
              error++;
          } else {
            $(this).css('border-color','#ccc');
            $(this).next().hide();
          }         
        }
      });

      //CHECK ALL QUESTION DEFAULT YES IF NOT NULL
      $('.ques_val_yes').each(function(){
        var input = $(this).val().trim();
        if (input.length == 0) {
          $(this).css('border-color','red');
          $(this).next().show();
          error++;
        }else{
          if(input == 0 || input == "0"){
              $(this).css('border-color','red');
              $(this).next().html("Invalid Input.");
              $(this).next().show();
              error++;
          } else {
            $(this).css('border-color','#ccc');
            $(this).next().hide();
          }         
        }
      });

      //CHECK ALL ACTIVITY IF NOT NULL
      $('.act_val').each(function(){
        var input = $(this).val().trim();
        if (input.length == 0) {
          $(this).css('border-color','red');
          $(this).next().show();
          error++;
        }else{
          if(input == 0 || input == "0"){
              $(this).css('border-color','red');
              $(this).next().html("Invalid Input.");
              $(this).next().show();
              error++;
          } else {
            $(this).css('border-color','#ccc');
            $(this).next().hide();
          }         
        }
      });

      //CHECK ALL SUB ACTIVITY IF NOT NULL
      $('.act_sub_val').each(function(){
        var input = $(this).val().trim();
        if (input.length == 0) {
          $(this).css('border-color','red');
          $(this).next().show();
          error++;
        }else{
          if(input == 0 || input == "0"){
              $(this).css('border-color','red');
              $(this).next().html("Invalid Input.");
              $(this).next().show();
              error++;
          } else {
            $(this).css('border-color','#ccc');
            $(this).next().hide();
          }         
        }
      });


      console.log(error);
      if(error == 0){
        q_current_count = 0;
        isLoading(true);
          //SAVE TEMPLATE
          var classification_id = $('#classification').val();
          var standard_id = $('#standard_reference').val();
          var status = $(this).attr('data-status');
          data_status = $(this).attr('data-status');
          var error_result = 0;
          aJax.post(
              "<?=base_url('Manage_template/check_template_standard')?>",
              {
                  classification_id:classification_id,
                  standard_id:standard_id
              },
              function(result){
                  if(result < 1){

                      aJax.post(
                        "<?=base_url('Manage_template/save_template')?>",
                        {
                            classification_id:classification_id,
                            standard_id:standard_id,
                            status:status
                        },
                        function(template_id){

                            //SAVE ACTIVITY
                            var activity_count = $(".act_val").length;
                            var current_count = 0;

                            $('.act_val').each(function(i){
                                var activity_value = $(this).val();
                                var activity_position =  $(this).attr('position');
                                var order =  $(this).attr('order');
                                
                                aJax.post(
                                  "<?=base_url('Manage_template/save_activity')?>",
                                  {
                                      activity_name:activity_value,
                                      order:order,
                                      template_id:template_id
                                  },
                                  function(activity_id){
                                                                        
                                    if(activity_id){
                                      current_count++;
                                      save_subactivity(activity_id,activity_position,template_id);
                                      if(activity_count  ==  current_count) {
                                        success_activity = true;
                                        if(success_activity == true && success_questions == true){
                                          generate_json(template_id);
                                          update_config();
                                          isLoading(false);
                                          bootbox.alert("Template is successfully saved.",function(){
                                            $('html, body').animate({
                                                scrollTop: $("html").offset().top
                                            }, 100);
                                            location.reload();
                                          });
                                        }
                                      }
                                    } else {
                                      error_result ++;
                                    }
                                    
                                  }
                                );

                            });

                            
                             //SAVE ELEMENTS
                             question_count = $('.question_div').length;

                            $('.elem_val').each(function(){
                                var element_name = $(this).val();
                                var element_order = $(this).attr('data-order');
                                
                                aJax.post(
                                  "<?=base_url('Manage_template/save_element')?>",
                                  {
                                      element_name:element_name,
                                      order:element_order
                                  },
                                  function(element_id){
                                    if(element_id){


                                      save_questions(element_id,template_id,element_order);
                                      

                                    } else {
                                      error_result ++;
                                    }
                                  }
                                );
                            });
                        }
                      );

                  } else {
                    bootbox.alert("Standard and Classification already exist", function(){});
                  }
              }
          );
      }


  });

  function generate_json(template_id){
    console.log(template_id);
    aJax.post(
      "<?= base_url('api/template_list'); ?>",
      {},
      function(data) {}
    );

    aJax.post(
      "<?=base_url('api/template_info'); ?>/" + parseInt(template_id),
      {},
      function(data) {}
    );
  }

  function save_subactivity(activity_id,act_ctr,template_id){
    $('.act_sub_val_'+act_ctr).each(function(){
        var subActivity = $(this).val();
        var order = $(this).attr("order");
        aJax.post(
            "<?=base_url('Manage_template/save_subactivity')?>",
            {
                activity_id:activity_id,
                order:order,
                subActivity:subActivity
            },
            function(result){
                //generate_json(template_id);
            }
        );
    });
  }
  function save_questions(element_id,template_id,elem_ctr){
    

    $('.element_question_'+elem_ctr+' .question_div').each(function(){
      var question = $(this).find('.ques_val').val();
      var default_yes = $(this).find('.ques_val_yes').val();
      var required_remarks = $(this).find('.ques_required').is( ':checked' ) ? 1: 0;
      var question_order = $(this).find('.ques_val').attr('data-order');

      aJax.post(
          "<?=base_url('Manage_template/save_questions')?>",
          {
              element_id:element_id,
              template_id:template_id,
              question:question,
              default_yes:default_yes,
              required_remarks:required_remarks,
              order:question_order
          },
          function(result){
            q_current_count++;

            generate_json(template_id);

            if(question_count  ==  q_current_count) {
              success_questions = true;
              if(success_activity == true && success_questions == true){
                generate_json(template_id);
                update_config();
                if(data_status == 0){
                  insert_audit_trail("Create New Template (Draft) " + template_id);
                } else {
                  insert_audit_trail("Create New Template (Final) " + template_id);
                }
                isLoading(false);
                bootbox.alert("Template is successfully saved.",function(){
                  $('html, body').animate({
                      scrollTop: $("html").offset().top
                  }, 100);
                  location.reload();
                });
              }
            }

          }
      );
    });
  }

  $(document).on('click', '.btn-done-saving', function() {
    location.reload();
  });

  $(document).on('change', '#classification', function() {
    console.log($(this).val());
    aJax.post(
        "<?=base_url('Manage_template/check_in_use')?>",
        {classification_id : $(this).val()},
        function(result){
          console.log(result);
          var obj = JSON.parse(result);
          
          $('.sr').each(function(){
            $(this).removeAttr("disabled");
          });

          $.each(obj, function(x,y){
              $('.sr-'+y.standard_id).attr("disabled","disabled");
          });

        }

    );

  });


  
</script>