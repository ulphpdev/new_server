
<?php
  $table = "tbl_classification";
  $table1 = "tbl_standard_reference";
  $order_by = "template_id";
  $role = $this->session->userdata('sess_role');
?>
<ol class="breadcrumb" style="background-color: #fff;">

    <li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
    <li class="active">Manage Template</li>

</ol>
<div class="col-md-5 man_temp_search">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Product Type or Standard/Reference" id="txt_search" name="fname">
    <div class="input-group-btn">
      <button class="btn btn-primary" id="btn_search" type="submit">
          <i class="glyphicon glyphicon-search"> </i> Search
      </button>
    </div>
  </div>
</div>
<div class="col-md-3 man_temp_filter">
    <select id="btn_filter" class="form-control">
        <option value="" selected disabled>Select</option>
        <option value=0>Draft</option>
        <option value=1>Active</option>
        <option value=-1>Inactive</option>
    </select>
</div>
<div class="col-md-4 man_temp_buttons">
    <?php if($this->session->userdata('sess_role') == 1) { ?>
     <div class="btn-navigation menu-btn  col-md-12 pad-0" >
        <button data-type = "Delete" class="inactive btn-min btn btn-default bold" style="display: none;"><span class = "glyphicon glyphicon-trash"></span> Delete</button>
        <button class="add btn-min  btn-min  btn btn-success bold"><span class = "glyphicon glyphicon-plus"></span> ADD TEMPLATE <span></button>
        <button class="article_list btn-min btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CLOSE</button>

      <!-- <button data-type = "Active" class="inactive btn-min btn btn-default btn-sm bold"><span class = "glyphicon glyphicon-ok"></span>Publish</button> -->
      <!-- <button data-type = "Inactive" class="inactive btn-min btn btn-default btn-sm bold"><span class = "glyphicon glyphicon-remove-circle"></span>Unpublish</button> -->

    </div>
    <?php } ?>
</div>

<div class="row">
	<div class="col-md-12" id="list_data">
		<div class="table-responsive" style="padding-left: 17px;padding-right: 15px;">
			<table class = "table listdata table-striped" style="margin-bottom:0px;">
          <thead>
            <tr>
            <th style = "width:10px;"><input class = "selectall" type = "checkbox"></th>
            <th>Template ID</th>
            <th>Product Type</th>
            <th>Standard/Reference</th>
            <th>Version</th>
            <th>Modified Date</th>
            <th>Status</th>
            <th style="text-align: right; width: 200px;">Actions</th>
          </tr>
          </thead>
          <tbody>

          </tbody>

			</table>

    </div>
      <div style="text-align: center; margin-top: 20px;" class="form-inline pager_div"></div>
	</div>
    <br><br>
	<div class="col-md-12 content-container" id="form_data" style="margin-top: 20px;"></div>
</div>


<!-- email modal -->

<div class="modal fade" id="email-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">

  <div class="modal-dialog" role="document">

    <div class="modal-content" style = "overflow:hidden">

      <div class="modal-header modal-head">

        <button type="button" class="close modal-close btn-close" style = "color:#fff !important; opacity:10" data-dismiss="modal" aria-label="Close"><span style = "color:#fff !important; opacity:10"  aria-hidden="true">&times;</span></button>

        <h4 class="modal-title" id="myModalLabel">Send Template</h4>

      </div>

      <div class="modal-body">
      <div class="col-md-12 pad-0 mb5">
        <div class="col-md-2">To*: </div><div class="col-md-10"><textarea class="form-control inputs" id="to_email_template"></textarea><span style="font-size:10px;">Note: To add (+) more email, please separate with a comma (,)</span><br><span class="er-msg to_email_template_err">Email should not be empty</span></div>
      </div>
      <div class="col-md-12 pad-0 mb5">
        <div class="col-md-2">Subject*: </div><div class="col-md-10"><input type="text"  name="" class="form-control inputs" id="subject_email_template"><span class="er-msg">Subject should not be empty</span></div>
      </div>
      <div class="col-md-12 pad-0 mb5">
        <div class="col-md-2">Message: </div><div class="col-md-10"><textarea class="form-control" id="message_email_template" height="100"></textarea></div>
      </div>
      </div>
      <div class="modal-footer" style = "border:0px">

           <button class = "btn btn-info" style = "padding:2px 10px;" id="send_email_template" >Send <span></button>
          <button class = "btn btn-primary btc2 btn-close" data-dismiss="modal" style = "padding:2px 10px;" >Close</button>



      </div>

    </div>

  </div>

</div>


<script>

$(document).ready(function() {

  CKEDITOR.replace('message_email_template');

/* Start of list */
var role = "<?=$role;?>";
  var limit = '10';
  var offset = '0';
  var query = "";
  get_list("",limit,offset);
  get_pagination("");

  $(document).on('click', '.btn-close2', function(){
    $('.inactive').hide();
    $('.selectall').attr('checked', false);
    $('#success-modal').modal('hide');
  });

  <?php $this->load->view('layout/listing_function');?>

	function get_list(query,limit,offset){
    $('.delete').hide();
    $('.inactive').hide();
    $('.selectall').attr('checked', false);
    var table = "<?=$table;?>";
    var order_by = "<?=$order_by;?>";
    var table1 = "tbl_standard_reference";
    var txt_search  =  $('#txt_search').val();

    isLoading(true);
    aJax.post(
      "<?=base_url('Manage_template/get_list_manage_template');?>",
      {
          'limit':limit,
          'offset':offset,
          'query':query
      },
      function(result){
          var obj = JSON.parse(result);
          var table_body = new TBody();

          if(obj.length > 0){
              $.each(obj, function(index, row){

                  if(row.status == 1){
                    var status = "Active";
                  }

                  if(row.status == 0){
                     var status = "Draft";
                  }

                  if(row.status == -1){
                     var status = "Inactive";
                  }

                  var standard = row.standard_name.substr(0,50);
                  if(standard.length == 50){
                    var standard1 = standard+'...';
                  }else{
                    var standard1 = standard;
                  }

                  if(row.status != 3){
                    table_body.td("<input class = 'select' data-id = '"+row.template_id+"' type ='checkbox'>");
                    table_body.td(row.template_id);
                    table_body.td(row.classification_name);
                    table_body.td(standard1);
                    if(row.sub_version == 0){
                      var version = parseFloat(row.version);
                    } else {
                      var version = parseFloat(row.version) + "." + (parseInt(row.sub_version) + 0);
                    }
                    table_body.td(version);
                    table_body.td(moment(row.update_date).format('LL'));
                    table_body.td(status);

                    var list_data = new UList();
                    list_data.set_liclass("li-action");
                    list_data.set_ulclass("ul-action");
                    <?php if($this->session->userdata('sess_role') == 1) { ?>
                        list_data.li("<a class='delete_data action_list action' data-status='' id='"+row.template_id+"' title='Delete'> <span class='glyphicon glyphicon-trash'></span></a>");
                    <?php } ?>
                    list_data.li("<a class='edit action_list action' data-status='' id='"+row.template_id+"' data-elem='"+row.element_id+"' title='Edit'><span class='glyphicon glyphicon-pencil'></span></a>");
                    list_data.li("<a href='#' class='email_data action' data-status='' id='"+row.template_id+"'  title='Email'> <span class='glyphicon glyphicon-envelope'></span></a>");
                    <?php if($this->session->userdata('sess_role') == 1) { ?>
                    if(row.status > 0){
                      list_data.li("<a class='archive action_list action' data-status='' id='"+row.template_id+"' title='Archive'><span class='glyphicon glyphicon-briefcase'></span></a>");
                    }
                    <?php } ?>
                    list_data.li("<a class='view_template action_list action' data-status='' id='"+row.template_id+"' data-id='"+row.template_id+"' title='Preview'><span class='glyphicon glyphicon-save-file'></span></a>");

                    table_body.td(list_data.set());
                    table_body.set();
                  }
              });
          } else {
              table_body.td_norecord(8);
              table_body.set();
          }
          table_body.append(".listdata tbody");
          isLoading(false);
      }
    );


  }
  function get_pagination(query){

    aJax.post(
      "<?=base_url("Manage_template/get_pagination_template");?>",
      {
          'query': query,
      },
      function(result){
        console.log("Pagination " + result);
          var no_of_page = Math.ceil(result / limit);
          var pagination = new Pagination(); //helper.js
          pagination.set_total_page(no_of_page);
          pagination.set('.pager_div');
      }
    );
  }



/* End of list */
/* Start of Add */

$(document).on('click','.view_template', function(){
  var id = $(this).attr('id');
  insert_audit_trail("Generate Template " + id);
  window.open("<?=base_url()?>generate/preview_template?template_id="+id);
});

$(document).on('click', '.article_list', function(){
$('.article_list').hide();
$('.save').hide();
$('.update').hide();
$('.add').show();
$('#form_data').html('');
get_list(query,limit,offset);
get_pagination('1',limit);
})

$(document).on('click', '.add', function(){
    $('.content-container').empty();
    $(this).hide();
    $('.update').hide();
    $('.article_list').show();
    isLoading(true);
    aJax.post(
        "<?=base_url();?>global_controller/action_global",
        {
            id:'', 
            module:'manage_template', 
            type:'add'
        },
        function(data){
            $('.content-container').html(data);
            $('#success-modal').modal('hide');
            $('html, body').animate({
                scrollTop: $("#form_data").offset().top
            }, 1000);
            setTimeout(function(){
                isLoading(false)
            }, 3000);
        }
    )

});

$(document).on('click', '.edit', function(){
    isLoading(true);
    $('.content-container').empty();
    var id = $(this).attr('id');
    aJax.post(
        "<?=base_url();?>global_controller/action_global",
        {
            id:id, 
            module:'manage_template', 
            type:'edit'
        },
        function(data){
            $('.content-container').html(data);
            $('#success-modal').modal('hide');
            $('html, body').animate({
                scrollTop: $("#form_data").offset().top
            }, 1000);
            setTimeout(function(){
                isLoading(false)
            }, 3000);
        }
    )
});


$(document).on('click', '.article_list', function(){
$(this).hide();
$('.save').hide();
$('.update').hide();
$('.add').show();
})
/* End of update */
/* start of delete */
$('.selectall').click(function(){
     if(this.checked) {
            $('.select').each(function() {
                this.checked = true;
                $('.delete').show();
                $('.inactive').show();
            });
        }else{
            $('.select').each(function() {
              $('.delete').hide();
                $('.inactive').hide();
                this.checked = false;
            });
        }
});
$(document).on('click', '.select', function(){
    var x = 0;
        $('.select').each(function() {
                if (this.checked==true) { x++; }
                if (x > 0 ) {
                   $('.delete').show();
                   $('.inactive').show();
                } else {
                $('.delete').hide();
                $('.inactive').hide();
                $('.selectall').attr('checked', true);
              	}
        });
});
$(document).on('click', '.inactive', function(){
    var x = 0;
    var data_type = $(this).attr('data-type');
    $('.select').each(function() {
      if (this.checked==true) {  x++;   }
    });

    if (x > 0 ) {
      change_status_modal(
        "Are you sure you want to "+data_type+" selected record?",
        data_type,
        function(result){
          if(result){
            isLoading(true);
            setTimeout(function() {
              update_satus(data_type);
            }, 1000);
          }
        }
      );
    }
});
$(document).on('click', '.delete_data', function(){
    var x = 0;
    var id = $(this).attr('id');
    change_status_modal(
      "Are you sure you want to delete selected record?",
      id,
      function(result){
        if(result){
          delete_template(id);
        }
      }
    );
});


  $(document).on('click', '.archive', function(){
    var x = 0;
    var id = $(this).attr('id');
    var table = "tbl_template";
    var order_by = "<?=$order_by;?>";

    var dialog = bootbox.confirm({
      message:"Are you sure you want to archive this template?",
      buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function(result){
            if(result){
              aJax.post(
                "<?=base_url();?>global_controller/inactive_global_update",
                {
                  id:id,
                  type:3,
                  table:table,
                  order_by: order_by,
                },
                function(data){
                  generate_json(id);
                  insert_audit_trail("Archive Template "+ id);
                  bootbox.alert('<b>Successfully moved to archive folder.</b>', function() {
                    location.reload();
                  });
                }
              );
            }
          }
    });
  });


function delete_template(id){
    var x = 0;
    var table = "tbl_template";
    var order_by = "<?=$order_by;?>";
    // alert(table+"-----"+order_by+"---"+id);
    var type = '-2';
      $.ajax({
          type: 'Post',
          url:'<?=base_url();?>global_controller/inactive_global_update',
          data:{id:id,type:type, table:table, order_by: order_by},
        }).done( function(data){
          $('#success-modal').modal('hide');
            generate_json(id);
            get_list(query,limit,offset);
            get_pagination(offset,limit);
          	bootbox.alert('<b>Successfully deleted records.</b>', function() {
              insert_audit_trail("Deleted Template "+ id);
              location.reload();
            }).off("shown.bs.modal");

          });
     x++;
};
function update_satus(type){
    var x = 0;

    if (type == 'Delete') {
      var type = '-2';
      trail_label = "Delete ";
      message = "<b>Successfully deleted records.</b>";
    }
    if (type == 'Inactive') {
      var type = '0';
      trail_label = "Deactivate ";
      message = "<b>Successfully deactivate records.</b>";
    }
    if (type == 'Active') {
      var type = '1';
      trail_label = "Activate ";
      message = "<b>Successfully activate records. </b>";
    }

      $('.select').each(function() {
          if (this.checked==true) {

            var table = "tbl_template";
            // var table1 = "<?=$table;?>";
            var order_by = "template_id"//"<?=$order_by;?>";
            var id = $(this).attr('data-id');


            aJax.postasync(
              "<?=base_url('global_controller/inactive_global_update');?>",
              {
                id:id,
                type:type,
                table:table,
                order_by: order_by
              },
              function(data){
                generate_json(id);
                aJax.get(
                  "<?=base_url()?>/api/template_info/"+id,
                  function(){

                  }
                )

                if (type == '-2') {
                  insert_audit_trail("Deleted Template "+ id);
                }
              }
            );
            x++;
          }

      });

      bootbox.alert(message, function(){
        location.reload();
      });
};

  $(document).on('click', '#btn_search', function(){
    var txt_search = $('#txt_search').val();
    query = "( status = 1 or status = 0 or status = -1 ) AND (classification_name LIKE '%" + txt_search + "%' OR standard_name LIKE '%" + txt_search + "%')";
    get_list(query,limit,offset);
    get_pagination(query);
  })


  $(document).on('change', '#btn_filter', function(){
    var filter = $(this).val();
    query = "status =" + filter;
    get_list(query,limit,offset);
    get_pagination(query);
  });

  function generate_json(template_id){
    aJax.get(
      "<?=base_url()?>/api/template_list",
      function(){

      }
    );
  }
});

  $(document).on('click', '.email_data', function(){
      var template_id = $(this).attr('id');
      $('#send_email_template').attr('data-id',template_id);
      $('#email-modal').modal('show');
  });

  $('#send_email_template').on( "click", function() {
    var to_send = $('#to_email_template').val();
    var subject = $('#subject_email_template').val();
    var message = CKEDITOR.instances.message_email_template.getData();
    var template_id = $(this).attr('data-id');
    var ctr = 0;
    $('.inputs').each(function(){
      if($(this).val() == ''){
        $(this).css('border','1px solid red');
        $(this).siblings('.er-msg').show();
        ctr++;
      }else{
        $(this).siblings('.er-msg').hide();
        $(this).css('border','1px solid #ccc');
      }
    });
    if(ctr == 0){
      var email_error = 0;
      var emails = to_send.split(",");
      var email_with_error = "";
      $.each(emails, function(x,y){
        if(isEmail(y.trim())){
        } else {
          email_error++;
          email_with_error += "<li>" + y + "</li>";
        }
      });
      
      if(email_error == 0){
        $('#email-modal').modal('hide');
        isLoading(true);
        aJax.post(
          "<?=base_url()?>/email/send_template",
          {
            to:to_send,
            subject:subject,
            message:message,
            template_id:template_id
          },
          function(data){
            isLoading(false);
            if(data == 'success'){
              bootbox.alert("Template is successfully sent!",function(e){
                insert_audit_trail("Email Template "+ template_id +" to " + to_send);
                location.reload();
              });
            }else{
              bootbox.alert("Error sending template! Please check your content before sending");
            }
          }
        );
      } else {
        bootbox.alert("Error sending template! Invalid email(s) : <ul>" + email_with_error + "</ul>");
      }
    }
  });

  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }











    //add activity
    $(document).on('click', '.add_activity', function(){ 
        var activity_id = $(this).attr("data-activity");
        if(validate.required(".activity_text") == 0){
            if(validate.unique(".activity_text") == 0){
                add_activity(".activity_"+activity_id);
                sort();
            }
        }
    }); 

    //add subactivity
    $(document).on('click', '.add_subactivity', function(){ 
        var activity_id = $(this).attr("data-activity");
        if(validate.required(".activity_text") == 0){
            if(validate.unique(".activity_text") == 0){
                new_subactivity(".subactivity_" + activity_id);
                sort();
            }
        }
        
    });

    //remove subactivity
    $(document).on('click', '.remove_subactivity', function(){ 
        var subtr = $(this).attr("data-subactivity");
        modal.confirm("Are you sure you want to remove this Sub-activity?",function(result){
            if(result){
                var data_id = $('.data_subactivity_' + subtr).find(".sub_activity_text").attr("data-id");
                if(data_id != 0){
                    var data = {"id" : data_id}
                    subactivity_delete.push(data);
                }
                $('.data_subactivity_' + subtr).remove();
                sort();
            }
        });
    });

    //remove subactivity
    $(document).on('click', '.remove_activity', function(){ 
        var tr = $(this).attr("data-activity");
        modal.confirm("Are you sure you want to remove this activity?<br>Note: All items under it will also be erased.",function(result){
            if(result){
                var data_id = $('.activity_' + tr).find(".activity_text").attr("data-id");
                if(data_id != 0){
                    var data = {"id" : data_id}
                    activity_delete.push(data);
                }
                $('.activity_' + tr).remove();
                $('.subactivity_' + tr).remove();
                sort();
            }
        });
    });

    //insert subactivity
    $(document).on('click', '.insert_subactivity', function(){ 
        var activity_id = $(this).attr("data-subactivity");
        if(validate.required(".sub_activity_text") == 0){
            add_subactivity(".data_subactivity_" + activity_id);
            sort();
        }
        
    });

    //add element
    $(document).on('click', '.element_add', function(){ 
        console.log("click element add");
        $(".template_error").html("");
        var element_id = $(this).attr("element-id");
        var element_question_div = $(this).attr("element_question");
        var question_required = ".required_element_" + element_id; 
        var question_unique = ".unique_element_" + element_id; 
        if(validate.required(".element_text") == 0){
            if(validate.unique(".element_text") == 0){
                if(validate.required(question_required) == 0){
                    if(validate.unique(question_unique) == 0){
                        add_element_new("." + element_question_div);
                        sort();
                    }
                }   
            }
        }
    });

    //remove element
    $(document).on('click', '.element_remove', function(){ 
        var element_id = $(this).attr("element-id");
        
        modal.confirm("Are you sure you want to remove this activity?<br> Note: All items under it will also be erased.",function(result){
            if(result){
                var data_id = $(".element_body_" + element_id).find(".element_text").attr("data-id");
                if(data_id != 0){
                    var data = {"id" : data_id}
                    element_delete.push(data);
                }
                

                $(".element_body_"+ element_id).remove();
                $(".element_question_" + element_id).remove();
                
                sort();
            }
        });
    });

    //add question
    $(document).on('click', '.question_add', function(){ 
        var questiontr = $(this).attr("data-tr");
        var element_id = $(this).attr("data-element-id");
        var question_required = ".required_element_" + element_id; 
        var question_unique = ".unique_element_" + element_id; 
        if(validate.required(question_required) == 0){
            if(validate.unique(question_unique) == 0){
                append_question("." + questiontr, element_id);
                sort();
            }
        }  
        
    });

    //remove question
    $(document).on('click', '.question_remove', function(){ 
        var questiontr = $(this).attr("data-tr");
        modal.confirm("Are you sure you want to remove this question?",function(result){
            if(result){
                var data_id = $('.' + questiontr).find(".question_text").attr("data-id");
                if(data_id != 0){
                    var data = {"id" : data_id}
                    question_delete.push(data);
                }

                $('.' + questiontr).closest("tr").remove();
                sort();
            }
        });
    });


    //question up
    $(document).on('click', '.question_up', function(){ 
        var questiontr = $(this).attr("data-tr");
        var $current = $('.' + questiontr);
        var $elems = $('.question_tr');
        var $previous = $elems.eq($elems.index($current) - 1);
        
        $($current).insertBefore($previous);
        sort();
        $('html, body').animate({
            scrollTop: $current.offset().top
        }, 1000);
    });

    //question down
    $(document).on('click', '.question_down', function(){ 
        var questiontr = $(this).attr("data-tr");
        var $current = $('.' + questiontr);
        var $elems = $('.question_tr');
        var $previous = $elems.eq($elems.index($current) + 1);
        
        $($current).insertAfter($previous);
        sort();
        $('html, body').animate({
            scrollTop: $current.offset().top
        }, 1000);
    });

    //element up
    $(document).on('click', '.element_up', function(){ 
        // element_body_1526541307173
        var elementid = $(this).attr("element-id");
        var $current = $(".element_body_" + elementid); //the element you have
        var $elems = $('.element-tbody'); //the collection of elements

        var $previous = $elems.eq($elems.index($current) - 1); //the one you needed
        var prev_tbody_element = $previous.attr("class").replace("element-tbody", "").trim();
        
        //move element 
        $($current).insertBefore("." + prev_tbody_element);
        $(".element_question_" + elementid).insertAfter($current);
        
        sort();

        $('html, body').animate({
            scrollTop: $current.offset().top
        }, 1000);

        
    });

    //element down
    $(document).on('click', '.element_down', function(){ 
        // element_body_1526541307173
        var elementid = $(this).attr("element-id");
        var $current = $(".element_body_" + elementid); //the element you have
        var $elems = $('.element-tbody'); //the collection of elements

        var $next = $elems.eq($elems.index($current) + 1); //the one you needed
        var next_tbody_element = $next.attr("class").replace("element-tbody", "").trim().replace("element_body_","element_question_");
        
        //move element 

        // alert(next_tbody_element);
        $($current).insertAfter("." + next_tbody_element);
        $(".element_question_" + elementid).insertAfter($current);
        
        sort();

        $('html, body').animate({
            scrollTop: $current.offset().top
        }, 1000);

        
    });
</script>