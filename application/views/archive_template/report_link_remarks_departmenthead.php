<div class="col-md-12 bg-white">
	<div class="col-md-12">
		<div class="header"><h4>GMP AUDIT REPORT</h4><label class="gmp_report_no">Report Number</label></div>
	</div>

	<!-- Part I. Audit Information -->
	<div class="col-md-12">
		<div class="col-md-12 audit-header">Part I. Audit Information</div>
		<div class="col-md-12 sub-header">A. General Information</div>
		<div class="col-md-12 pad-0 part1_content">
			<div class="col-md-12 pad-0">
				<div class="col-md-6 pad-0">
					<div class="col-md-12 pad-0">
						<span>Name of Manufacturer: </span> <label class="company_name"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Site Address 1: </span> <label class="address1"></label>,<label class="address2"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Site Address 2: </span> <label class="address3"></label>, <label class="country"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Date of Audit: </span> <label class="audit_date"></label>
					</div>
				</div>
				<div class="col-md-6 pad-0">
					<div class="col-md-12 pad-0">
						<span>Name of Lead Auditor: </span> <label class="lead_auditor"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Position: </span> <label class="position"></label>
					</div>
					<div class="col-md-12 pad-0">
						<span>Department/Division &amp; Company:  </span> <label class="auditor-dept"></label>
					</div>
				</div>
			</div>
			<div class="col-md-12 pad-0">
				<div class="table-responsive mb15">
					<table border="1" class="auditors_table tbl_format">
						<thead>
							<tr>
								<td>Name</td>
								<td>Position</td>
								<td>Department/Designation</td>
							</tr>
						</thead>
						<tbody class="auditors_tbody">
						
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-md-12 sub-header">B. Company Background/Audit History</div>
		<div class="col-md-12 pad-0 part2_content">
			<div class="col-md-12 pad-0">
				<span>Supplier Background/History: </span>
				<textarea class="form-control Supplier-history mb15" disabled></textarea>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Date of Previous Inspection:</span><span class="previous-inspection"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Inspector(s) Involved:</span>
			<ol class="inspectors">
			</ol>
			<span>Major Changes Since Previous Inspection: </span>
			<ol class="changes">
			</ol>
		</div>
		<div class="col-md-12 sub-header">C. Audit Information</div>
		<div class="col-md-12 pad-0">
			<span>Activities Carried out by the company: </span>
			<ol class="activities">
			</ol>
			<span>Other Activities Carried out by the company: </span><span class="other-activities"></span><br>
			<span>Scope of Audit: </span>
			<ol class="scope_audit">
			</ol>
			<!-- <span>Products of Interest (for Insert Scope of Audit Name 1) </span>
			<ol class="product_interest">
				<li>Product Name</li>
				<li>Product Name</li>
			</ol> -->
		</div>
		<div class="col-md-12 pad-0">
			<div class="table-responsive mb15">
				<table border="1" class="license_table tbl_format">
					<thead>
						<tr>
							<td>Licenses/Certification</td>
							<td>Issuing Regulatory</td>
							<td>License/Certification Number</td>
							<td>Validity</td>
						</tr>
					</thead>
					<tbody class="license_tbody">
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Pre-Audit Documents:</span>
			<ol class="pre-documents">
			</ol>
		</div>
		<div class="col-md-12 pad-0">
			<span>Audited areas:</span> <span class="audited_areas"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Not Audited areas:</span> <span class="not_audited_areas"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Date of Wrap-up or Close out Meeting:</span> <span class="wrapup-date"></span>
		</div>
		<div class="col-md-12 pad-0">
			<span>Name of Present During Close-out Meeting</span>
			<div class="table-responsive mb15">
				<table border="1" class="present-meeting tbl_format">
					<thead>
						<tr>
							<td>Name</td>
							<td>Position/Department</td>
						</tr>
					</thead>
					<tbody class="present_met_tbody">
						<tr>
							<!-- <td>Juan Dela Cruz</td>
							<td>CIT</td> -->
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Name of Personnel Met During Inspection </span>
			<div class="table-responsive mb15">
				<table border="1" class="personel-met tbl_format">
					<thead>
						<tr>
							<td>Name</td>
							<td>Position/Department</td>
						</tr>
					</thead>
					<tbody class="personel_met_tbody">
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<span>Distribution List (except Supplier and GMP Inspection)</span>
			<ol class="distribution-list">
			</ol>
		</div>
		<div class="col-md-12 sub-header">D. Templates Answers</div>
		<div class="col-md-12 pad-0">
			<div class="col-md-12 pad-0">
				<span>Product Type: </span><span class="report_producttype"></span>
			</div>
			<div class="col-md-12 pad-0">
				<span>Standard/Reference: </span><span class="report_standardreference"></span>
			</div>
		</div>
		<!-- element tables -->
		<div class="col-md-12 pad-0">
			<div class="col-md-12 pad-0 element_con mb15"></div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="col-md-12 audit-header">Part II. Audit Observation</div>
		<div class="col-md-12 pad-0">
			<span>Elements Requiring Recommendation: </span>
			<ol class="element-reco"></ol>
		</div>

		<div class="col-md-12 pad-0">
			<span>Audit Team's Findings and Observations Relevant to the Audit</span>
			<ul class="aud_findings"></ul>
		</div>

		<div class="col-md-12 pad-0">
			<span>Definition/Categorization of Audit Observations</span>
			<ol class="aud_definition"></ol>
		</div>

		<div class="col-md-12 pad-0">
			<span>Listing of Audit Observations and Concerns (in decreasing order of criticality)</span>
		</div>

		<div class="col-md-12 pad-0">
			<span>Critical Observation - <i class="critical_observation"></i></span><br>
			<span>Major Observation - <i class="major_observation"></i></span><br>
			<span>Minor Observation - <i class="minor_observation"></i></span>
		</div>
		<div class="clearfix"></div>
		<br>
		<div class="col-md-12 pad-0">
			<span>Disposition</span>
			<div class="table-responsive mb15">
				<table border="1" class="scope-disposition tbl_format">
					<thead>
						<tr>
							<td>Scope</td>
							<td>Disposition and Product</td>
						</tr>
					</thead>
					<tbody class="disposition_tbody">
						
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-md-12 pad-0">
			<span>Other Issue for Audit Report: </span> <span class="other-issue-audit"></span>
		</div>	
		<div class="col-md-12 pad-0">
			<span>Other Issues for Executive Summary: </span> <span class="other-issue-executive"></span>
		</div>	
<!-- 
		<div class="col-md-12 pad-0">

			<span>Audit Closure Date:</span><label class="audit-close-date">02 September 2017 </label>

		</div>	 -->

	</div>
	<hr noshade size=1 width="100%">
	<div class="col-md-12">

		<div class="col-md-12 pad-0 input-remarks">
			<span><b>REMARKS<b></span>
			
					<!-- <button name="subject" type="submit" class="btn btn-success approved_or_not" value="approved">Approved</button>
  					<button name="subject" type="submit" class="btn btn-warning approved_or_not" value="reject">Reject</button> -->
  			
			<div class="">

				<div class="col-md-12">
						<textarea class="form-control" id="txt_remarks" rows="4" cols="50"></textarea><br>
				
						<div class="col-md-6">		
		  				<button name="subject" type="submit" class="btn btn-success remarks_submit" value="approved">SUBMIT TO DIVISION HEAD</button>
		  				</div>
		  				<div class="col-md-6">
		  				<button name="subject" type="submit" class="btn btn-warning reject_submit" value="reject">SUBMIT TO LEAD-AUDITOR</button>
		  				</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){

var id = <?=$_GET['report_id']?>;

get_report(id);

function get_report(id){
	isLoading(true);
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>Preview_report",
		data:{id:id}
	}).done(function(data){
		var obj = JSON.parse(data);
		a = '';
		$.each(obj, function(x,y){
			$('.gmp_report_no').text(y.report_no);
			$('.company_name').text(y.name);
			$('.address1').text(y.address1);
			$('.country').text(y.country);
	    	$('.address2').text(y.address2);
	    	$('.address3').text(y.address3);
			$('.lead_auditor').text(y.fname+' '+y.mname+' '+y.lname);
			if(y.designation != 0){
				$('.position').text(y.designation);
			}
			$('.auditor-dept').text(y.department+', '+y.company);
			// $('.audit_date').text(y.audit_date);
			// $('.audit_date').text(moment(y.audit_date).format('LLL'));
			auditors(y.report_id,'get_co_auditors','Co-auditor');
			auditors(y.report_id,'get_approvers','Approver');
			auditors(y.report_id,'get_reviewer','Reviewer');
			$('.Supplier-history').text(y.background);
			qv_insepection_date(y.report_id);
			inspectors(y.report_id);
			inspection_changes(y.report_id);
			activities(y.report_id);
			$('.other-activities').text(y.other_activities);
			scope_audit(y.report_id);
			if(y.translator != 'None'){
			a += '<tr>';
			a += '<td>'+y.translator+'</td>';
			a += '<td>Translator</td>';
			a += '<td></td>';
			}
			license(y.report_id);
			pre_document(y.report_id);
			$('.audited_areas').text(y.audited_areas);
			$('.wrapup-date').text(y.wrap_up_date);	
			personel_met(y.report_id);
			distribution(y.report_id);
			disposition(y.report_id);
			other_issues_audit(y.report_id)
			other_issues_exec(y.report_id)		
			// $('.audit-close-date').text(y.closure_date);
			get_template(y.report_id);
			get_element_reco(y.report_id);
			present_during_meeting(y.report_id)
			remarks(y.report_id);
			qv_observation_yes(y.report_id);
			qv_audit_observation(y.report_id);
			qv_audit_observation_count(y.template_id, y.report_id);
			last_inspection(y.company_id);
		});

		$('.auditors_tbody').append(a);

	});

}


function remarks(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_remarks",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		var fname = ""
		
		$.each(obj, function(row,data2){
			htm += "<tr>";
			htm += "<td>" + moment(data2.date).format("LL") + "</td>";
			htm += "<td>" + data2.name + "</td>";
			htm += "<td>" + data2.position + "</td>";
			htm += "<td>" + data2.status + "</td>";
			htm += "<td>" + data2.remarks + "</td>";
			htm += "</tr>";	
		});		

		$(".remarks_tbody").append(htm);
	});
}

function get_element_reco(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_element_reco",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		var alpha = ["A","B","C","D","E","F","G","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];

		if(obj.length == 0){
			htm += 'Not Applicable';
		}else{
			$.each(obj, function(x,y){
				var order = y.order - 1;
				htm += '<li>Element '+alpha[order]+' - '+y.recommendation+'</li>';
			});	
		}	
		$('.element-reco').html(htm);
	});
}

function present_during_meeting(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_present_during_meeting",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		if(obj.length!=0){
			$.each(obj, function(x,y){
				htm += '<tr>';
				htm += '<td>'+y.name+'</td>';
				htm += '<td>'+y.position+'</td>';
				htm += '</tr>';
			});
		}else{
			htm += '<tr><td colspan="2">N/A</td></tr>';
		}
		
		$('.present_met_tbody').append(htm);
	});
}

function other_issues_exec(report_id){
	aJax.post(
		"<?=base_url('global_controller/get_by_id');?>",
		{
			'table': "tbl_report_other_issues_executive", 
			'field' : 'report_id', 
			'id' : report_id
		},
		function(result){
			var obj = JSON.parse(result);
			var issues = "<ul>";
			$.each(obj, function(x,y){
				issues += "<li>"  + y.other_issues_executive + "</li>"
			});
			issues += "</ul>"; 
			$('.other-issue-executive').html(issues);
		}
	);
}
function other_issues_audit(report_id){
	aJax.post(
		"<?=base_url('global_controller/get_by_id');?>",
		{
			'table': "tbl_report_other_issues_audit", 
			'field' : 'report_id', 
			'id' : report_id
		},
		function(result){
			var obj = JSON.parse(result);
			var issues = "<ul>";
			$.each(obj, function(x,y){
				issues += "<li>" + y.other_issues_audit + "</li>"
			});
			issues += "</ul>"; 
			$('.other-issue-audit').html(issues);
		}
	);
}

function last_inspection(company_id){
	aJax.post(
		"<?=base_url('global_controller/get_by_id');?>",
		{ 'table': "tbl_audit_history", 'field' : 'supplier_id', 'id' : company_id },
		function(result){
			var obj = JSON.parse(result);
			var last_history = obj[obj.length-2];
			var history_id = last_history.id;
			aJax.post(
				"<?=base_url('global_controller/get_by_id');?>",
				{
					'table': "tbl_audit_history_dates", 
					'field' : 'history_id', 
					'id' : history_id
				},
				function(audit_dates){
					var obj_dates = JSON.parse(audit_dates);
					var audit_date = "";
				    var audit_month = "";
				    var audit_year = "";
    				audit_month = ""; audit_date = ""; audit_year = "";
    				var len = obj_dates.length;

				    $.each(obj_dates, function(x,y){
				    	audit_month = moment(y.Date).format('MMMM');
        				audit_year = moment(y.Date).format('Y');
        				if (x === len - 1) {
					      audit_date += " & " + moment(y.Date).format('D') + "";
					    } else {
					      audit_date +=  ", " + moment(y.Date).format('D');
					    }

				    })

				    if(audit_date.trim().charAt(0) == "&") {
					    audit_date = audit_date.trim().substr(1);
					}

					$('.previous-inspection').html(audit_month + " " + audit_date.substr(1) + ", " + audit_year);

					isLoading(false);
				}
			);

			//INSPECTOR
			aJax.post(
				"<?=base_url('global_controller/get_by_id');?>",
				{
					'table': "tbl_audit_history_inspectors", 
					'field' : 'history_id', 
					'id' : history_id
				},
				function(inspectors){
					var obj_inspectors = JSON.parse(inspectors);
					var html ="";
					$.each(obj_inspectors, function(a,b){
						html += '<li>' + b.inspector + '</li>';
					});
					$('.inspectors').html(html);

				}
			);
		}

	);
}

function qv_insepection_date(reportid){
	aJax.post(
		"<?=base_url('global_controller/get_by_id');?>",
		{ 'table': "tbl_report_inspection_date", 'field' : 'report_id', 'id' : reportid },
		function(result){
			var obj = JSON.parse(result);
			 	var inspection_date = "";
			    var inspection_month = "";
			    var inspection_year = "";

			    var len = obj.length;

			    $.each(obj, function(index, row){
				    inspection_month = moment(row.inspection_date).format('MMMM');
				    inspection_year = moment(row.inspection_date).format('Y');

				    if (index === len - 1) {
				      inspection_date += " & " + moment(row.inspection_date).format('D') + "";
				    } else {
				        inspection_date +=  ", " + moment(row.inspection_date).format('D');
				    }
				});

				if(inspection_date.trim().charAt(0) == "&") {
				    inspection_date = inspection_date.trim().substr(1);
				}
	
			    var return_date = inspection_month + " " + inspection_date.substr(1) + ", " + inspection_year;
			    $('.previous-inspection').text(" " + return_date);
			}

	);
}
function qv_audit_observation_count(templateid,reportid){

	var ctrx = 1;
    var critical = "";
    var major = "";
    var minor = "";

	$.ajax({
    type: 'post',
    url: '<?=base_url();?>global_controller/get_by_id',        
    data:  { 'table': "qv_elements_observation", 'field' : 'template_id', 'id' : templateid } ,
  	}).done(function(data){  
  		var obj = JSON.parse(data);
  		$.each(obj, function(x,y){
  			var pass_data = {'report_id':reportid,'element_id':y.element_id};
  			$.ajax({
			    type: 'post',
			    url: '<?=base_url();?>global_controller/get_no_answer',        
			    data:  pass_data,
			  	}).done(function(result){  
			  		var no_answer = JSON.parse(result);
			  		if(no_answer != null){
				  		$.each(no_answer, function(index,value){
				  			switch(value.category_name) {
							    case 'Critical':
							        critical += ctrx + ',';
							        break;
							    case 'Major':
							        major += ctrx + ',';
							        break;
							    case 'Minor':
							        minor += ctrx + ',';
							        break;
							}
							ctrx +=1;
							if(critical == ""){
					  			$('.critical_observation').html("None");
					  		} else {
					  			$('.critical_observation').html("Please refer to item no(s) " + critical);
					  		}

					  		if(major == ""){
					  			$('.major_observation').html("None");
					  		} else {
					  			$('.major_observation').html("Please refer to item no(s) " + major);
					  		}

					  		if(minor == ""){
					  			$('.minor_observation').html("None");
					  		} else {
					  			$('.minor_observation').html("Please refer to item no(s) " + minor);
					  		}
				  		});
				  	}
			  	});

  		});

  		
	});

}

function qv_audit_observation(reportid){
	$.ajax({
    type: 'post',
    url: '<?=base_url();?>global_controller/get_by_id',        
    data:  { 'table': "qv_audit_observation", 'field' : 'report_id', 'id' : reportid } ,
  	}).done(function(data){  
  		var obj = JSON.parse(data);
  		var htm_critical = '';
  		var htm_major = '';
  		var htm_minor = '';
		$.each(obj, function(x,y){
			if(y.category_name == 'Critical') {
				htm_critical = '<u>Critical Observation</u><br>';
				htm_critical += y.description + '<br>';
			}

			if(y.category_name == 'Major') {
				htm_major = '<u>Major Observation</u><br>';
				htm_major += y.description + '<br>';
			}

			if(y.category_name == 'Minor') {
				htm_minor = '<u>Minor Observation</u><br>';
				htm_minor += y.description + '<br>';
			}
			
		});

		$('.aud_definition').html(htm_critical + htm_major + htm_minor);
	});

}
 
function qv_observation_yes(reportid){
	$.ajax({
    type: 'post',
    url: '<?=base_url();?>global_controller/get_by_id',        
    data:  { 'table': "qv_observation_yes", 'field' : 'report_id', 'id' : reportid } ,
  	}).done(function(data){  
  		var obj = JSON.parse(data);
  		var htm = '';
		$.each(obj, function(x,y){
			htm += '<li>' + y.element_name + '-' + y.answer_details + '</li>';

		});

		$('.aud_findings').html(htm);
  	});
}
function auditors(report_id,func,position){
	// $('.auditors_tbody').html('');
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/"+func,
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.fname+' '+y.mname+' '+y.lname+'</td>';
			htm += '<td>'+position+'</td>';
			htm += '<td>'+y.department+', '+y.company+'</td>';
			htm += '</tr>';
		});
		
		$('.auditors_tbody').append(htm);
	});
}
function inspectors(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_inspectors",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<li>'+y.fname+' '+y.lname+'</li>';
		});		
		$('.inspectors').append(htm);
	});
}
function inspection_changes(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_inspection_changes",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<li>'+y.changes+'</li>';
		});		
		$('.changes').append(htm);
	});
}
function activities(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_activities",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<li>'+y.activity_name+'</li>';
		});		
		$('.activities').append(htm);
	});
}
function scope_audit(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_scope_audit",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<li>'+y.scope_name+'</li>';
			scope_product(y.scope_id,report_id);
		});		
		$('.scope_audit').append(htm);
	});
}
function scope_product(scope_id,report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_scope_product",
		data:{scope_id:scope_id,report_id:report_id}
	}).done(function(data){
		var obj =JSON.parse(data);
		var htm = '';
		htm += '<span>Products of Interest (for Insert Scope of Audit Name 1) </span>';
		htm += '<ol class="product_interest">';
		$.each(obj, function(x,y){						
			htm += '<li>'+y.product_name+'</li>';		
		});
		htm += '</ol>';
		$('.scope_audit').after(htm);
	});
}
function license(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_license",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.reference_name+'</td>';
			htm += '<td>'+y.issuer+'</td>';
			htm += '<td>'+y.reference_no+'</td>';
			htm += '<td>'+y.validity+'</td>';
			htm += '</tr>';
		});
		
		$('.license_tbody').append(htm);
	});
}
function pre_document(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_pre_document",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<li>'+y.document_name+'</li>';
		});		
		$('.pre-documents').append(htm);
	});
}
function personel_met(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_personel_met",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.fname+' '+y.lname+'</td>';
			htm += '<td>'+y.designation+'</td>';
			htm += '</tr>';
		});
		
		$('.personel_met_tbody').append(htm);
	});
}
function distribution(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_distribution",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		if(obj.length == 0){
			htm += 'Not Applicable';
		}else{
			$.each(obj, function(x,y){
				htm += '<li>'+y.distribution_name+'</li>';
			});	
		}	
		$('.distribution-list').append(htm);
	});
}
function disposition(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_disposition",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		
		$.each(obj, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.scope_name+'</td>';
			htm += '<td>'+y.disposition_name+'</td>';
			htm += '</tr>';
		});
		
		$('.disposition_tbody').append(htm);
	});
}

function get_template(report_id){
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_template",
		data: {report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		$.each(obj, function(x,y){
			$('.report_producttype').text(y.classification_name);
			$('.report_standardreference').text(y.standard_name);
		});
	});
}
});


//submit remarks
$(document).on('click','.remarks_submit', function(e){
   e.preventDefault();
   	var remarks = $('#txt_remarks').val();
   	var id = "<?=$_GET['report_id']?>"
   	var newdate = new Date();
   	var create_date = moment(newdate).format('YYYY-MM-DD hh:mm:ss');
   	var reviewer_id = "<?=  $this->session->userdata('userid') ?>"
   	var type = 'reviewer';
   	var data =  {'id':id,'remarks':remarks,'create_date':create_date,'reviewer_id':reviewer_id,'type':type};
	$.ajax({
	    url:'<?php echo base_url("Audit_report_link/add_new_remarks_departmenthead"); ?>',
	    type: 'POST',
	    data: data,
	}).done(function(data){

		$.ajax({
	            type: 'Post',
	            url:'<?=base_url("Audit_report_link/add_report_submission_status");?>',
	            data:{report_id:id, remarks : remarks, status:"Approved",position:"Department Head"},
	            }).done(function(data){

	            		$.ajax({
					      	type: 'Post',
					      	url: "http://sams.ecomqa.com/api/audit_report_list",
					      	}).done( function(data){});

	            });

        $.ajax({
            type: 'Post',
            url:'<?=base_url("smtp/send/email_to_co_auditor_approve_from_reviewer");?>',
            data:{report_id:id, remarks : "Approved : " + remarks},
            }).done(function(data){
            	console.log(data);
            });

        $.ajax({
            type: 'Post',
            url:'<?=base_url("smtp/send/email_to_division_head");?>',
            data:{report_id:id},
            }).done(function(data){
            	console.log(data);
            	bootbox.alert("Remarks was successfully saved and sent to Division head", function(){
					// location.reload();
				});
            });

        update_audit_report_json();

	});
	
});

$(document).on('click','.reject_submit', function(e){
   	e.preventDefault();
   	var remarks = $('#txt_remarks').val();

   	if(remarks == ""){
   		bootbox.alert("Remarks field is required", function(){});
   	} else {
	   	var id = "<?=$_GET['report_id']?>"
	   	var newdate = new Date();
	   	var create_date = moment(newdate).format('YYYY-MM-DD hh:mm:ss');
	   	var reviewer_id = "<?=  $this->session->userdata('userid') ?>"
	   	var type = 'reviewer';
	   	var data =  {'id':id,'remarks':remarks,'create_date':create_date,'reviewer_id':reviewer_id,'type':type};
		$.ajax({
		    url:'<?php echo base_url("Audit_report_link/reject_remarks_departmenthead"); ?>',
		    type: 'POST',
		    data: data,
		}).done(function(data){

			$.ajax({
	            type: 'Post',
	            url:'<?=base_url("Audit_report_link/add_report_submission_status");?>',
	            data:{report_id:id, remarks : remarks, status:"",position:"Department Head"},
	            }).done(function(data){


	            });

			$.ajax({
	            type: 'Post',
	            url:'<?=base_url("smtp/send/email_to_co_auditor_rejected");?>',
	            data:{report_id:id, remarks : remarks},
	            }).done(function(data){
	            	console.log(data);
	            	bootbox.alert("Remarks was successfully saved and sent to Lead Auditor", function(){
						location.reload();
					});
	            });

		});
	}
});

</script>