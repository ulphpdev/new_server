<?php echo $template_id ?>

<!-- <div class="col-md-12 bg-white">
	<div class="col-md-12">
		<h5>PRODUCT TYPE: <span id="classification"></span></h5>
		<h5>STANDARD/REFERENCE: <span id="standard_reference"></span></h5>
	</div>
	<div class="col-md-12 element_con">
		
	</div>
</div>


<script type="text/javascript">
$(document).ready(function(){
var template_id = "<?=$id;?>";

get_template(template_id);

function get_template(template_id){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/Manage_template/get_data_by_id",
        data:{id:template_id,table:'tbl_template',field:'template_id'}
    }).done(function(data){
        var obj = JSON.parse(data);
        $.each(obj, function(x,y){
            productType(y.classification_id);
            standard_reference(y.standard_id);
            get_elements(y.template_id);
            // get_activity(y.template_id);
        });
    });
}

function productType(selected){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/Manage_template/get_product_type"
    }).done(function(data){
        var obj = JSON.parse(data);
        var htm = '';
        // htm += '<option value="" selected hidden>--Select--</option>';
        $.each(obj, function(x,y){
            $('#classification').text(y.classification_name);
        });
    });
}

function standard_reference(selected){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/Manage_template/get_standard_reference"
    }).done(function(data){
        var obj = JSON.parse(data);
        var htm = '';
        // htm += '<option value="" hidden>--Select--</option>';
        $.each(obj, function(x,y){
            $('#standard_reference').text(y.standard_name);
        });
    });
}


function get_elements(template_id){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/Manage_template/get_elements",
        data:{template_id:template_id}
    }).done(function(data){
        var obj = JSON.parse(data);
        var el = '';
        var que = '';
        var el_ctr = 1;
        var index = 65;
        
        $.each(obj, function(x,y){
        	var letter = String.fromCharCode(index++);

        	el += '<div class="item_'+y.element_id+'">';
            el += '<div class="col-md-12 element_'+y.element_id+' elem_div_preview_head">ELEMENT '+letter+'. '+y.element_name+'</div>';
            el += '<div class="col-md-12 table-responsive">';
            el += '<table class="table listdata">';
            el += '<thead><th style="width:10%;"></th><th style="width:45%;">Questions</th><th style="width:45%;">Input Template (Default Output for Yes)</th></thead>';
            el += '<tbody class="element_content el_'+y.element_id+'"></tbody>';
            el += '</table>';
            el += '</div>';
            el += '</div>';
            get_questions(y.element_id,template_id,letter);
        });
        $('.element_con').append(el);
    });
}


function get_questions(element_id,template_id,letter){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/Manage_template/get_questions",
        data:{element_id:element_id,template_id:template_id}
    }).done(function(data){
        var obj = JSON.parse(data);
        var que = ''
        var que_ctr = 1;
        
        $.each(obj, function(x,y){
            que += '<tr><td>'+letter+'.'+que_ctr+'</td><td>'+y.question+'</td><td>'+y.default_yes+'</td>';
            que += '</tr>';
            que_ctr++;
        });
        $('.el_'+element_id).append(que);


    })
}

});
</script> -->