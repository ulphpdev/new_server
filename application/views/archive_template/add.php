<input type="hidden" name="success" id="success" value="">
<div class="col-md-12 col-sm-12 container-template_add">
   <div class="col-md-12 add-template-body">
      <div class="col-md-12 pad-5">
         <div class="col-md-2">Product Type</div>
         <div class = "col-md-4">
            <select id="classification" class = "inputcss form-control fullwidth inputs">
            </select>
            <span class = "er-msg">Classification should not be empty *</span>
         </div>
      </div>
      <div class="col-md-12 pad-5">
         <div class="col-md-2">Standard/Reference</div>
         <div class = "col-md-4">
            <select id="standard_reference" class = "inputcss form-control fullwidth inputs">
            </select>
            <span class = "er-msg">Standard/Reference should not be empty *</span>
         </div>
      </div>
   </div>
   <div class="col-md-12 add-template-body-bottom">
      <div class="col-md-12">
          <button type="submit" class="btn btn-primary button-template" id="add_element" data-id='1' data-cnt="1"><i class="glyphicon glyphicon-plus"></i> Add Element</button>
      </div>
      <div class="col-md-12" id="main-content" style="padding:0px 15px;">
         <div class="element_item col-md-12 pad-0">
            <div class="col-md-1"><button class="btn btn-default up hidden" order="1"><span class="glyphicon glyphicon-chevron-up"></span></button><button class="btn btn-default down" order="1"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
            <div class="col-md-11 pad-0">
                <div class="col-md-12 element_1 elem_div pad-5">
                   <div class="col-md-2 el_text">Element 1</div>
                   <div class="col-md-4">
                        <input type="text" id="elem_1" data-elem="1" data-id="1" class="elem_1 elem_val form-control">
                        <span class = "er-msg">This should not be empty *</span>                    
                   </div>
                 </div>
                 <div class="question_div_parent" id="element_question_1">
                 <div class="col-md-12 question_div  question_1 pad-5">
                       <div class="col-md-2 pad-0 quest-title-ctr">
                            <div class="col-md-4 pad-0">
                                <button class="btn btn-default up_q hidden" order="1">
                                    <span class="glyphicon glyphicon-chevron-up" ></span>
                                </button>
                                <button class="btn btn-default down_q" order="1">
                                    <span class="glyphicon glyphicon-chevron-down"></span>
                                </button>
                            </div>
                            <div class="col-md-8 pad-0"><span class="qtext">Question 1</span></div>
                       </div>
                       <div class="col-md-4">
                           <textarea id="ques_1"  data-elem= 1 class="ques_1 ques_val form-control"></textarea>
                           <span class = "er-msg">This should not be empty *</span>
                           <div>Default Answer for yes:    <input id="default_yes_1"  data-elem="1" class="default_yes_1 default_yes_val form-control df_element_1" type="text" value="" name="fname">                         
                           </div>
                           <form>
                              <input class="mandatory" id="mandatory_1"  type="radio" name="mandatory" value="yes" checked> Yes 
                              <input id="mandatory_1" type="radio" name="mandatory" value="no"> No <br>
                            </form>
                       </div>
                        <div class="col-md-1 qbtn pad-0"><button type="submit" data-id= "1" data-cnt= "1" id="add_question" element="1" class="parent_elem_1 btn btn-default"><i class="glyphicon glyphicon-plus " ></i></button></div>
                    </div>
                </div>
            </div>
         </div>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary button-template" data-id="1" id="add_activity" data-cnt="1"><i class="glyphicon glyphicon-plus"></i> ADD ACTIVITY</button>
     </div>
    <div class="col-md-12 activity_container"></div>
   </div>
   <div class="col-md-12">
      <div class="col-md-12 template_action">
          <button  class="btn btn-danger button-template cancel"><span class = "glyphicon glyphicon-remove-circle"></span> Cancel</button>
          <button type="submit" class="btn btn-info button-template save_template" data-status="0" id=""><span class = "glyphicon glyphicon-paperclip"></span> Save as draft</button>
          <button type="submit" class="btn btn-primary button-template save_template" data-status="1" id=""><span class = "glyphicon glyphicon-floppy-saved"></span> Save</button>
      </div>
   </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    //add question
    $(document).on('click', '#add_question', function() {
        var htm =''
        var id = $(this).attr('data-id');
        var fcnt = $(this).attr('data-cnt');
        var cnt = parseInt($(this).attr('data-cnt'))+1;
        // alert(cnt);
        var error = 0;
        var ctr =parseInt($(this).attr('element'));

       
             $('#element_question_'+ctr+' .ques_val').each(function() {
                 if ($(this).val() == 0) {
                $(this).css('border-color', 'red');
                $(this).next().show();
                error = 1;
                }else {
                    $(this).css('border-color', '#ccc');
                    $(this).next().hide();
                    error = 0;
                }
             });
             if(error == 0){
                htm += '<div class="4remove pad-5 col-md-12 question_div question_'+cnt+'">';
                htm += '<div class="col-md-2 pad-0 quest-title-ctr">';
                htm += '<div class="col-md-4 pad-0">';
                htm += '<button class="btn btn-default up_q" order="'+cnt+'">';
                htm += '<span class="glyphicon glyphicon-chevron-up" ></span>';
                htm += '</button>';
                htm += '<button class="btn btn-default down_q" order="'+cnt+'">';
                htm += '<span class="glyphicon glyphicon-chevron-down"></span>';
                htm += '</button>';
                htm += '</div>';
                htm += '<div class="col-md-8 pad-0"><span class="qtext">Question '+cnt+'</span></div>';
                htm += '</div>';
                htm += '<div class="col-md-4"><textarea id="ques_'+cnt+'" data-elem="'+id+'" class="form-control ques_val ques_' + cnt + '"></textarea><span class = "er-msg">This should not be empty *</span>';
                htm += '<div>Default Answer for yes:<input type="text" value="" name="fname" id="default_yes_' + cnt + '" data-elem="' + id + '" class="default_yes_val default_yes_' + cnt + ' form-control df_element_'+id+'"></div>';
                htm += '<form > ';
                htm += '<input class="mandatory_1"   type="radio" name="mandatory" value="yes"  id="mandatory_' + cnt + '" data-elem="' + cnt + '" checked> Yes ';
                htm += '<input class="mandatory_1" type="radio" name="mandatory" value="no" id="mandatory_' + cnt + '" data-elem="' + cnt + '"> No <br>';
                htm += '</form>';
                htm += '</div>';
                htm += '<div class="col-md-1 qbtn pad-0">'
                htm += '<button type="submit" class="remove btn btn-default" data-elem="'+id+'"><i class="glyphicon glyphicon-minus "></i></button>';
                htm += '</div>';
                htm += '</div>';
                $('#element_question_'+ctr).append(htm);
                $(this).attr('data-cnt',cnt);
                checkorder_q('',ctr);
             } 
    }); 
//remove quetion
$(document).on('click', '.remove', function() {
    $(this).parents('.4remove').remove();
    var element = $(this).attr('data-elem');
    var cnt = parseInt($('#add_question.parent_elem_'+element).attr('data-cnt'))-1;
    var htm ="";
    var ctr = 1;
    // var element_ctr =1;
    // alert(element_ctr);
    $('#add_question.parent_elem_'+element).attr('data-cnt',cnt);
    
    $('#element_question_'+element+ ' .quest-title-ctr .qtext').each(function(){
        $(this).text('Question '+ctr);
        ctr++;
    });
});

$(document).on('click', '#add_element', function() {
    var htm = '';
    var id = $(this).attr('data-id');
    var cnt = $(this).attr('data-cnt');
    $('.ques_val').css('border-color', '#ccc');
    $('.elem_val').css('border-color', '#ccc');
    $('.er-msg').hide();
    var error = 0;
    id++;
    $('.elem_val').each(function() {
        if ($(this).val() == 0) {
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        } else {
            $(this).css('border-color', '#ccc');
            $(this).next().hide();
        }
    })
    if (error == 0) {
        htm += '<div class="element_item col-md-12 pad-0">';
        htm += '<div class="col-md-1"><button class="btn btn-default up" order="'+id+'"><span class="glyphicon glyphicon-chevron-up"></span></button><button class="btn btn-default down" order="'+id+'"><span class="glyphicon glyphicon-chevron-down"></span></button></div>';
        htm += '<div class="col-md-11 pad-0">';
        htm += '<div class="col-md-12 element_' + id + ' elem_div pad-5">';
        htm += '<div class="col-md-2 el_text">Element ' + id + '</div>';
        htm += '<div class="col-md-4">   <input type="text" id="elem_' + id + '" data-elem="' + id + '" data-id="' + id + '" class="elem_1 elem_val form-control"><span class = "er-msg">This should not be empty *</span></div>';
        htm += '<div class="col-md-1 pad-0"><button type="submit" data-id="' + id + '" class="btn btn-default" data-cnt="'+id+'" id="remove_element" title="Remove Element"><i class="glyphicon glyphicon-minus" ></i></button></div>';
        htm += '</div><div class="question_div_parent" id="element_question_'+id+'">';
        htm += '<div class="col-md-12 question_div question_' + id + '">';
        htm += '<div class="col-md-2 pad-0 quest-title-ctr">';
        htm += '<div class="col-md-4 pad-0">';
        htm += '<button class="btn btn-default up_q hidden" order="1">';
        htm += '<span class="glyphicon glyphicon-chevron-up" ></span>';
        htm += '</button>';
        htm += '<button class="btn btn-default down_q" order="1">';
        htm += '<span class="glyphicon glyphicon-chevron-down"></span>';
        htm += '</button>';
        htm += '</div>';
        htm += '<div class="col-md-8 pad-0"><span class="qtext">Question 1</span></div>';
        htm += '</div>';
        htm += '<div class="col-md-4"><textarea id="ques_' + id + '" data-elem="' + id + '" class="ques_val ques_' + id + ' form-control"></textarea><span class = "er-msg">This should not be empty *</span>';
        htm += '<div>Default Answer for yes:<input type="text" value="" name="fname" id="default_yes_' + id + '" data-elem="' + id + '" class="default_yes_val default_yes_' + id + ' form-control"></div>';
        htm += '<form>';
        htm += '<input  class="mandatory' + id + '"  type="radio" name="mandatory" value="yes"  id="mandatory_' + id + '" data-elem="' + id + '" checked> Yes ';
        htm += '<input class="mandatory' + id + '" type="radio" name="mandatory" value="no" id="mandatory_' + id + '" data-elem="' + id + '"> No <br>';
        htm += '</form>';
        htm += '</div>';
        htm += '<div class="col-md-1 qbtn pad-0"><button type="submit" data-id="' + id + '" data-cnt="1" id="add_question" element='+id+' class="parent_elem_'+id+' btn btn-default"><i class="glyphicon glyphicon-plus " ></i></button>';
        htm += '</div></div></div>';
        $('#main-content').append(htm);
        $(this).attr('data-id', id);
    } else {
        // bootbox.alert('<b>Fill out required fields.</b>', function() {}).off("shown.bs.modal");
    }
});

productType();
standard_reference();


$(document).on('click', '#add_activity', function() {
    var htm = '';
    var id = $(this).attr('data-id');
    var ctr = $(this).attr('data-cnt');
    var error = 0;
    if(ctr != 1){
        $('.act_val').each(function(){
            if($(this).val() == ''){
                $(this).css('border-color', 'red');
                $(this).next().show();
                error++;
            }else{
                $(this).css('border-color', '#ccc');
                $(this).next().hide();
            }
        });
    }
   
    if (error == 0) {
        htm += '<div class="col-md-12 activity_' + id + ' activity_div pad-5">';
        htm += '<div class="col-md-2">Activity ' + id + '</div>';
        htm += '<div class="col-md-4"><input type="text" id="activity_' + id + '" data-activity="' + id + '" data-id="' + id + '" class="act_1 act_val form-control"><span class = "er-msg">This should not be empty *</span></div>';
        htm += '<div class="col-md-1 pad-0"><button type="submit" class="btn btn-default" data-id="' + id + '" data-cnt="0" id="add_sub_activity" title="Add Sub-activity"><i class="glyphicon glyphicon-plus " ></i></button></div>';
        htm += '</div>';
        $('.activity_container').append(htm);
        $(this).attr('data-cnt', parseInt(ctr)+1);
        $(this).attr('data-id', parseInt(id)+1);
    } 
});

$(document).on('click','#add_sub_activity', function(){
    // alert('a')
    var ctr = $(this).attr('data-cnt');
    var id = $(this).attr('data-id');
    var fcnt = parseInt($(this).attr('data-cnt'))+1;
    var error = 0;
    if(fcnt != 1){
        $('.subact_'+id).each(function(){
            if($(this).val() == ''){
                $(this).css('border-color', 'red');
                $(this).next().show();
                error++;
            }else{
                $(this).css('border-color', '#ccc');
                $(this).next().hide();
            }
        });
    }
    if(error == 0){
        htm = '';
        htm += '<div class="col-md-12 subact_div subActivity_' + fcnt + ' pad-0" style="margin-top:5px;">';
        htm += '<div class="col-md-2 subtxt" style="text-align:right;">Sub-activity ' + fcnt + '</div>';
        htm += '<div class="col-md-4"><input type="text" id="subactivity_' + fcnt + '" data-subactivity="' + fcnt + '" data-id="' + fcnt + '" class="subact_'+id+' subact_val form-control"><span class = "er-msg">This should not be empty *</span></div>';
        htm += '<div class="col-md-1 pad-0"><button type="submit" data-id="' + id + '" class="btn btn-default" data-cnt="'+fcnt+'" id="remove_sub_activity" title="Remove Sub-activity"><i class="glyphicon glyphicon-minus" ></i></button></div>';
        htm += '</div>';
        $('.activity_'+id).append(htm);
        $(this).attr('data-cnt', parseInt(ctr)+1);
    }
});

$(document).on('click','#remove_sub_activity', function(){
    var ctr = $(this).attr('data-cnt');
    var id = $(this).attr('data-id');
    var ctr1 = 1;

    $(this).parent('div').parent('.subActivity_'+ctr).remove();
    $('.activity_'+id+' #add_sub_activity').attr('data-cnt',parseInt($('.activity_'+id+' #add_sub_activity').attr('data-cnt'))-1);

    $('.activity_'+id+ ' .subact_div .subtxt').each(function(){
        $(this).text('Sub-activity '+ctr1);
        ctr1++;
    });

});

$('.save_template').click(function(){
    var status = $(this).attr('data-status');
    var error = 0;
    $('.inputcss').each(function(){
        if($(this).val() == ''){
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        }else{
            $(this).css('border-color', '#ccc');
            $(this).next().hide();
        }
    });

    $('.elem_val').each(function(){
        var elem_id = $(this).attr('data-elem');
        if($(this).val() != ''){
            $('.ques_'+elem_id).each(function(){
                if($(this).val() == ''){
                    $(this).css('border-color', 'red');
                    $(this).next('.er-msg').show();
                    error++;
                }else{
                    $(this).css('border-color', '#ccc');
                    $(this).next('.er-msg').hide();
                }
            });
            $(this).css('border-color', '#ccc');
            $(this).next().hide();
        }else{
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        }
    });

    $('.act_val').each(function(){
        var act_id = $(this).attr('data-id');
        if($(this).val() != ''){
            $('.subact_'+act_id).each(function(){
                if($(this).val() == ''){
                    $(this).css('border-color', 'red');
                    $(this).next('.er-msg').show();
                    error++;
                }else{
                    $(this).css('border-color', '#ccc');
                    $(this).next('.er-msg').hide();
                }
            });
            $(this).css('border-color', '#ccc');
            $(this).next().hide();
        }else{
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        }
    });

    if(error == 0){
        // SAVE CLASSIFICATION and STANDARD
        var classification_id = $('#classification').val();
        var standard_id = $('#standard_reference').val();
        $.ajax({
            type: 'post',
            url: "<?=base_url()?>/Manage_template/save_classification_standard",
            data:{classification_id:classification_id,standard_id:standard_id,status:status}
        }).done(function(data){
            var template_id = data;
            // FOREACH ELEMENT
            $('.elem_val').each(function(){
                var element_name = $(this).val();
                var elem_ctr = $(this).attr('data-elem');
                var order = $(this).parents('.element_item').find('.up').attr('order');
                $.ajax({
                    type: 'post',
                    url: "<?=base_url()?>/Manage_template/save_element",
                    data:{element_name:element_name,order:order}
                }).done(function(element_id){
                    generate_json(template_id);
                    save_questions(element_id,template_id,elem_ctr);
                });
            });
            // FOREACH ACTIVITY
            var act_count = $('.act_val').length;
            if(act_count != 0){
                $('.act_val').each(function(){
                    var activity_name = $(this).val();
                    var act_ctr = $(this).attr('data-id');
                    $.ajax({
                        type: 'post',
                        url: "<?=base_url()?>/Manage_template/save_activity",
                        data:{activity_name:activity_name,template_id:template_id}
                    }).done(function(activity_id){
                        generate_json(template_id);
                        save_subactivity(activity_id,act_ctr,template_id);
                    });
                });
            }else{
                bootbox.alert("Please add at least one (1) activity for this template", function(){
                });
            }
            


        });
        
    }

    function save_questions(element_id,template_id,elem_ctr){
        $('#element_question_'+elem_ctr+' .question_div').each(function(){
           var question = $(this).find('.ques_val').val();
           var default_yes = $(this).find('.default_yes_val').val();
           var mandatory = $(this).find('input[name="mandatory"]:checked').val();
           var order =  $(this).find('.up_q').attr('order');
           $.ajax({
                type: 'post',
                url: "<?=base_url()?>/Manage_template/save_questions",
                data:{element_id:element_id,template_id:template_id,question:question,default_yes:default_yes,mandatory:mandatory,order:order}
           }).done(function(data){
                generate_json(template_id);
                $('#success').val('success');
           });
        });
    }

    function save_subactivity(activity_id,act_ctr,template_id){
        var sub_count = $('.subact_div').length;
        if(sub_count != 0){
            $('.subact_'+act_ctr).each(function(){
                var subActivity = $(this).val();
                $.ajax({
                    type: 'post',
                    url: "<?=base_url()?>/Manage_template/save_subactivity",
                    data:{activity_id:activity_id,subActivity:subActivity}
                }).done(function(data){
                    generate_json(template_id);
                    bootbox.alert("Template was successfully saved", function(){
                        location.reload();
                    });
                });
            });
        }else{
            generate_json(template_id);
            bootbox.alert("Template was successfully saved", function(){
                location.reload();
            });
        }
    }

    function generate_json(template_id){
        $.ajax({
            type: 'post',
            url: "<?=base_url()?>/api/template_list",
        }).done(function(data){
            $.ajax({
                type: 'post',
                url: "<?=base_url()?>/api/template_info/"+template_id,
            }).done(function(data){
              
            });  
        });
    }

});


$(document).on('click','#remove_element', function(){
    var id = $(this).attr('data-id');
    var ctr = 1;

    bootbox.confirm("Are you sure you want to remove this element?<br> Note: All items under it will also be erased.", function(result){
        if(result == true){
            $('#add_element').attr('data-id',parseInt($('#add_element').attr('data-id'))-1);
            $('.element_'+id).remove();
            $('#element_question_'+id).remove();

            $('.elem_div').each(function(){
                $(this).find('div:first-child').text('Element '+ctr);
                ctr++;
            });
        }
    });

});


$(document).on('click','.down', function(){
    var $parent = $(this).parents(".element_item");
    if($parent.hasClass('hidden') == false){
        var current_order = $(this).attr('order');
        var prev_order = $parent.next().find('.down').attr('order');
        $(this).attr('order',prev_order);
        $(this).siblings('.up').attr('order',prev_order);

        $parent.next().find('.down').attr('order',current_order);
        $parent.next().find('.up').attr('order',current_order);

        // $parent.find('.el_text').text('Element ' + prev_order);
        // $parent.prev('.el_text').text('Element ' + prev_order);
        $parent.insertAfter($parent.next()); 

        checkorder(current_order);
    }
    
});

$(document).on('click','.up', function(){  
    $('.element_item').each(function(){
        $(this).find('.down').removeClass('disabled');
    });

    var $parent = $(this).parents(".element_item");

        var current_order = $(this).attr('order');
        var prev_order = $parent.prev().find('.up').attr('order');

        $(this).attr('order',prev_order);
        $(this).siblings('.down').attr('order',prev_order);

        $parent.prev().find('.down').attr('order',current_order);
        $parent.prev().find('.up').attr('order',current_order);

        $parent.insertBefore($parent.prev()); 

        checkorder(current_order);
});

function checkorder(current_order){
    $('.up').each(function(x,index){
        if($(this).attr('order') == 1){
            $(this).addClass('hidden');
            $(this).parents('.element_item').find('#remove_element').addClass('hidden');
        }else{
            $(this).removeClass('hidden');
            $(this).parents('.element_item').find('#remove_element').removeClass('hidden');
        }
        
    });
    var ctr = 1;
 
    $('.element_item').each(function(){
        $(this).find('.el_text').text('Element '+ ctr);
        ctr++;
         
    }); 
    // alert(ctr);
     $('.element_item').each(function(){
            $(this).find('.down').removeClass('disabled');
        });
    
   
    $('.element_item:last-child').each(function(){
            $(this).find('.down').addClass('disabled');
        });
    
}


$(document).on('click','.down_q', function(){
    var $parent = $(this).parents(".question_div");
    if($parent.hasClass('hidden') == false){
        var current_order = $(this).attr('order');
        var prev_order = $parent.next().find('.down_q').attr('order');
        var elem = $(this).parents('.question_div').find('.ques_val').attr('data-elem');
        $(this).attr('order',prev_order);
        $(this).siblings('.up_q').attr('order',prev_order);

        $parent.next().find('.down_q').attr('order',current_order);
        $parent.next().find('.up_q').attr('order',current_order);

        // $parent.find('.el_text').text('Element ' + prev_order);
        // $parent.prev('.el_text').text('Element ' + prev_order);
        $parent.insertAfter($parent.next()); 

        checkorder_q(current_order,elem);
    }
    
});

$(document).on('click','.up_q', function(){  
    $('.question_div').each(function(){
        $(this).find('.down_q').removeClass('disabled');
    });

    var $parent = $(this).parents(".question_div");
        var elem = $(this).parents('.question_div').find('.ques_val').attr('data-elem');
        var current_order = $(this).attr('order');
        var prev_order = $parent.prev().find('.up_q').attr('order');

        $(this).attr('order',prev_order);
        $(this).siblings('.down_q').attr('order',prev_order);

        $parent.prev().find('.down_q').attr('order',current_order);
        $parent.prev().find('.up_q').attr('order',current_order);

        $parent.insertBefore($parent.prev()); 

        checkorder_q(current_order,elem);
});

function checkorder_q(current_order,element){
    $('#element_question_'+element+' .up_q').each(function(x,index){
        if($(this).attr('order') == 1){
            $(this).addClass('hidden');
            var elem = $(this).parents('.question_div_parent').find('#add_question').attr('data-id');
            var ctr = $(this).parents('.question_div_parent').find('#add_question').attr('data-cnt');
            // alert(elem +'-'+ ctr);
            $(this).parents('.question_div').find('.qbtn').html('<button type="submit" data-id= "'+elem+'" data-cnt= "'+ctr+'" id="add_question" element="'+elem+'" class="parent_elem_'+elem+' btn btn-default"><i class="glyphicon glyphicon-plus " ></i></button>');

            // $(this).parents('.question_div').find('.remove').addClass('hidden');
        }else{
            $(this).removeClass('hidden');
            $(this).parents('.question_div').find('.qbtn').html('<button type="submit" class="remove btn btn-default" data-elem="'+element+'"><i class="glyphicon glyphicon-minus "></i></button>');
            // $(this).parents('.question_div').find('.remove').removeClass('hidden');
        }
        
    });
    var ctr = 1;
 
    $('#element_question_'+element+' .question_div').each(function(){
        $(this).find('.qtext').text('Question '+ ctr);
        ctr++;
         
    }); 
    // alert(ctr);
     $('#element_question_'+element+' .question_div').each(function(){
            $(this).find('.down_q').removeClass('disabled');
        });
    
   
    $('#element_question_'+element+' .question_div:last-child').each(function(){
            $(this).find('.down_q').addClass('disabled');
        });
    
}


});

function productType(){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/Manage_template/get_product_type"
    }).done(function(data){
        var obj = JSON.parse(data);
        var htm = '';
        htm += '<option value="" selected hidden>--Select--</option>';
        $.each(obj, function(x,y){
            htm += '<option value="'+y.classification_id+'">'+y.classification_name+'</option>';
        });
        $('#classification').append(htm);
    });
}

function standard_reference(){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/Manage_template/get_standard_reference"
    }).done(function(data){
        var obj = JSON.parse(data);
        var htm = '';
        htm += '<option value="" selected hidden>--Select--</option>';
        $.each(obj, function(x,y){
            htm += '<option value="'+y.standard_id+'">'+y.standard_name+'</option>';
        });
        $('#standard_reference').append(htm);
    });
}
</script>
