<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Audit_report_analysis_model2 extends CI_Model{
	function __construct() {
        parent::__construct();
    }

    // public function get_report_analysis_list($limit,$offset){
    //     $query = $this->db->query('SELECT DISTINCT(a.report_id),a.report_no,b.name,
    //                                 CONCAT(DATE_FORMAT(a.audit_date_1,"%M")," ",DATE_FORMAT(a.audit_date_1,"%e"),"-",DATE_FORMAT(a.audit_date_2,"%e"),", ",DATE_FORMAT(a.audit_date_2,"%Y")) as audit_date,(SELECT COUNT(question_id) FROM tbl_report_answers where report_id = a.report_id GROUP BY report_id) as question_count 
    //                                 FROM tbl_report_summary as a
    //                                 LEFT JOIN tbl_company as b ON a.company_id = b.company_id
    //                                 LEFT JOIN tbl_report_answers as c ON a.report_id = c.report_id WHERE a.status <> -2 AND a.status <> 3 ORDER BY a.report_id DESC LIMIT '.$offset.','.$limit);
    //     $result = $query->result();
    //     return $result;
    // }

    public function get_report_analysis_view($report_id){
        $query = $this->db->query('SELECT DISTINCT(a.report_id),a.report_no,b.name,
                                    CONCAT(DATE_FORMAT(a.audit_date_1,"%M")," ",DATE_FORMAT(a.audit_date_1,"%e"),"-",DATE_FORMAT(a.audit_date_2,"%e"),", ",DATE_FORMAT(a.audit_date_2,"%Y")) as audit_date,
                                    (SELECT COUNT(question_id) FROM tbl_report_answers where report_id = a.report_id GROUP BY report_id) as question_count,
                                    (SELECT COUNT(answer_id) FROM tbl_report_answers where report_id = '.$report_id.' AND answer_id = 2) as yes,
                                    (SELECT COUNT(answer_id) FROM tbl_report_answers where report_id = '.$report_id.' AND answer_id = 1) as no,
                                    (SELECT COUNT(answer_id) FROM tbl_report_answers where report_id = '.$report_id.' AND answer_id = 3) as NA,
                                    (SELECT COUNT(answer_id) FROM tbl_report_answers where report_id = '.$report_id.' AND answer_id = 4) as NC,
                                    (SELECT COUNT(category_id) FROM tbl_report_answers where report_id = '.$report_id.' AND category_id = 21) as minor,
                                    (SELECT COUNT(category_id) FROM tbl_report_answers where report_id = '.$report_id.' AND category_id = 22) as major,
                                    (SELECT COUNT(category_id) FROM tbl_report_answers where report_id = '.$report_id.' AND category_id = 23) as critical,
                                    e.limit,e.no_major,e.no_critical
                                    FROM tbl_report_summary as a
                                    LEFT JOIN tbl_company as b ON a.company_id = b.company_id
                                    LEFT JOIN tbl_template as d ON a.template_id = d.template_id
                                    LEFT JOIN tbl_classification as e ON e.classification_id = d.classification_id
                                    LEFT JOIN tbl_report_answers as c ON a.report_id = c.report_id WHERE a.report_id = '.$report_id .'
                                    WHERE a.status >=0');
        $result = $query->result();
        return $result;
    }


        //pagination audit_report_analysis
    public function get_pagination($query = null){



        $this->db->select("*");

        $this->db->from("tbl_report_listing");

        if($query != null) {

            $this->db->where($query);

        }
        $this->db->where("status >= 0");
        $q = $this->db->get();

        return $q->num_rows();



    }

    public function get_report_analysis_list($query = null, $limit, $offset){
        $this->db->limit($limit,$offset);
        $this->db->select("*");
        $this->db->from("tbl_report_listing");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status >= 0");
        $this->db->group_by("report_id");
        $this->db->order_by("report_id", "desc");
        $q = $this->db->get();
        return $q->result();
    }

    public function get_report_analysis_list_api($query = null, $limit, $offset){
        $this->db->limit($limit,$offset);
        $this->db->select("*");
        $this->db->select('question_id, COUNT(question_id) as question_count');
        $this->db->from("audit_report_analysis_view1");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status >= 0");
        $this->db->group_by("report_id");
        $this->db->order_by("report_id", "desc");
        $q = $this->db->get();
        return $q->result();
    }

    public function get_pagination_trail($query = null){



        $this->db->select("*");

        $this->db->from("tbl_audit_trail");

        if($query != null) {

            $this->db->where($query);

        }
        // $this->db->where("status > -1");
        // $this->db->group_by("report_id");

        $this->db->order_by("id", "desc");
        $q = $this->db->get();

        return $q->num_rows();



    }

    public function count_yesno($reportid)
    {
        $this->db->select("*");
        $this->db->from("audit_report_analysis_view1");
        $this->db->where("(answer_id = 1 OR answer_id = 2) AND report_id = " . $reportid);
        $q = $this->db->get();
        return $q->num_rows();
    }   
}