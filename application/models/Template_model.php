<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Template_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    function read_product()
    {   
            $this->db->select("a.classification_name,c.standard_name");
            $this->db->from("tbl_classification as a");
            $this->db->join('tbl_template as b', 'a.classification_id = b.classification_id');
            $this->db->join('tbl_standard_reference as c', 'b.standard_id = c.standard_id');
            return $this->db->get()->num_rows();
    }
     function add_template_solo($data)
   {
        $this->db->set($data);
         $this->db->insert('tbl_template',$data);
        $id = $this->db->insert_id();
        return $id;
    }
   function add_template($table,$data)
   {
        $this->db->insert($table, $data); 
        $id = $this->db->insert_id();
        return $id;
    }
    function get_element(){
        $this->db->select('*');
        $this->db->from('tbl_elements');
        return $id;
     }
    function add_template2($data)
   {    
        $this->db->set($data);
        $this->db->insert('tbl_questions',$data);
    }
    function add_sub_act($data)
   {    
        $this->db->set($data);
        $this->db->insert('tbl_sub_activities',$data);
    }
    function search($data)
   {   
        $query = $this->db->query("SELECT a.*, b.classification_name, c.standard_name 
        FROM tbl_template as a LEFT JOIN tbl_classification as b 
        ON a.classification_id = b.classification_id JOIN tbl_standard_reference as c 
        ON a.classification_id = c.classification_id
        WHERE  (classification_name LIKE '%".$data."%' OR standard_name LIKE '%".$data."%' ESCAPE '!'");
        $result = $query->result();
        // Return the results.
        return $result;
    }
  function edit_template($id){
        $query = $this->db->query("SELECT * from tbl_template where template_id = '".$id."'");
        $result = $query->result();
        return $result;
    }
    public function getdata_model($table){
        $query = $this->db->query('SELECT * FROM tbl_'.$table.'');
        $result = $query->result();
        return $result;
    }
    public function get_elementquestion_model($id,$table){
        // $query = $this->db->query('SELECT  distinct element_id FROM tbl_'.$table.' where template_id = '.$id.'');
        $query = $this->db->query('SELECT DISTINCT(a.element_id),b.element_name  FROM tbl_'.$table.' as a LEFT JOIN tbl_elements as b ON b.element_id = a.element_id where template_id = '.$id.'');
        $result = $query->result();
        return $result;
    }
    public function get_question_model($id){
        $query = $this->db->query('SELECT *  FROM tbl_questions where element_id = '.$id.'');
        $result = $query->result();
        return $result;
    }
    public function get_mandatory_model($id, $question_id){
        $query = $this->db->query('SELECT mandatory, question_id  FROM tbl_questions where element_id = '.$id.' AND question_id = question_id');
        $result = $query->result();
        return $result;
    }
    public function update_element_model($id, $element_name, $update_date){
        // $this->db->select('element_name');
        $this->db->set('element_name', $element_name);
        $this->db->set('update_date', $update_date);
        $this->db->where('element_id' , $id);
        $this->db->update('tbl_elements');
    }
       function edit_template_reference($classification_id, $update_date, $template_id, $standard_id)
   {
        $this->db->set('classification_id', $classification_id);
        $this->db->set('standard_id', $standard_id);
        $this->db->set('update_date', $update_date);
        $this->db->where('template_id' , $template_id);
        $this->db->update('tbl_template');
    }
    public function update_question_model($id, $question, $mandatory ,$question_id, $update_date){
        // if($update_delete == ''){
        $this->db->set('question', $question);
        $this->db->set('mandatory', $mandatory);
        $this->db->set('update_date', $update_date);
        $this->db->where('question_id' , $question_id);
        $this->db->update('tbl_questions');
        // }
        // else{
        //     $this->db->where($question_id, $update_delete);
        //     $this->db->delete($tbl_questions);
        // }
    }
    public function update_default_model($default_yes, $question_id, $update_date){
        // $this->db->select('element_name');
        $this->db->set('default_yes', $default_yes);
        $this->db->set('update_date', $update_date);
        $this->db->where('question_id' , $question_id);
        $this->db->update('tbl_questions');
    }
    public function get_activity_model($id, $table){
        $query = $this->db->query('SELECT a.*, b.activity_name  FROM '.$table.' as a LEFT JOIN tbl_activities as b ON b.activity_id = a.activity_id where template_id = '.$id.'');
        $result = $query->result();
        return $result;
    }

    function get_subactivity_model($id){
        $this->db->select("*");
        $this->db->where("activity_id", $id);
        $this->db->from("tbl_sub_activities");
        $this->db->order_by("order", "asc");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
       public function update_activity_model($activity_name, $activity_id, $update_date){
        // $this->db->select('element_name');
        $this->db->set('activity_name', $activity_name);
        $this->db->set('update_date', $update_date);
        $this->db->where('activity_id' , $activity_id);
        $this->db->update('tbl_activities');
    }
    public function get_sub_activity_model($id, $template_id){
        $query = $this->db->query('SELECT DISTINCT(c.activity_id), c.sub_item_name, sub_item_id FROM tbl_template_activities as a LEFT JOIN tbl_activities as b ON b.activity_id = a.activity_id LEFT JOIN tbl_sub_activities as c ON c.activity_id = c.activity_id where c.activity_id = '.$id.'');
        $result = $query->result();
        return $result;
        // $query = $this->db->query('SELECT *  FROM tbl_sub_activities where activity_id = '.$id.'');
        // $result = $query->result();
        // return $result;
    }
    public function update_sub_activity_model($sub_item_name, $sub_item_id, $update_date){
        $this->db->set('sub_item_name', $sub_item_name);
        $this->db->set('update_date', $update_date);
        $this->db->where('sub_item_id' , $sub_item_id);
        $this->db->update('tbl_sub_activities');
    }
    // tek
    function get_product_type(){
        $query = $this->db->query("SELECT classification_id,classification_name FROM tbl_classification WHERE status = 1");
        $result = $query->result();
        return $result;
    }
    function get_standard_reference(){
        $query = $this->db->query("SELECT standard_id,standard_name FROM tbl_standard_reference WHERE status = 1");
        $result = $query->result();
        return $result;
    }
    function save_data($data,$table){
        $this->db->insert($table, $data); 
        $id = $this->db->insert_id();
        return $id;
    }
    function get_data_by_id($id,$field,$table){
        $query = $this->db->query("SELECT * FROM ".$table." WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }

    function get_data_by_id_order($id,$field,$table,$order){
        $query = $this->db->query("SELECT * FROM ".$table." WHERE ".$field." = '".$id."' ORDER BY orders ASC");
        $result = $query->result();
        return $result;
    }

    function get_elements($template_id){
        $query = $this->db->query("SELECT DISTINCT(a.element_id),b.element_name FROM tbl_questions as a
                                LEFT JOIN tbl_elements as b ON a.element_id = b.element_id
                                WHERE a.template_id = ".$template_id." ORDER BY b.order ASC");
        $result = $query->result();
        return $result;
    }
    function get_questions($template_id,$element_id){
        $query = $this->db->query("SELECT a.*,b.element_name FROM tbl_questions as a
                                LEFT JOIN tbl_elements as b ON a.element_id = b.element_id
                                WHERE a.template_id = ".$template_id." AND a.element_id = ".$element_id." ORDER BY a.order_sort ASC");
        $result = $query->result();
        return $result;
    }
    function get_activity($template_id){
        $query = $this->db->query("SELECT b.* FROM tbl_template_activities as a LEFT JOIN tbl_activities as b ON a.activity_id = b.activity_id 
                                WHERE a.template_id = ".$template_id." ORDER BY b.order ASC");
        $result = $query->result();
        return $result;
    }
    public function update_data($field, $where, $table, $data) { 
        $this->db->where($field, $where);
        $this->db->update($table, $data); 
    }
    function delete_data($id,$field,$table){
        $this->db->where($field, $id);
        $this->db->delete($table);
    }
    // function audit_search($keyword){
    //  $query = $this->db->query("SELECT b.* FROM tbl_template_activities as a LEFT JOIN tbl_activities as b ON a.activity_id = b.activity_id 
    //                             WHERE a.template_id = ".$template_id."");
    //  $result = $query->result();
    //     return $result;
    // }
    function get_data_question_answer($question_id, $report_id)
    {
        $this->db->select("*");
        $this->db->from("tbl_report_answers");
        $this->db->where("question_id",$question_id);
        $this->db->where("report_id",$report_id);
        $q = $this->db->get();
        return $q->result();
    }

    function get_data_by_id_group($id,$field,$table,$group){
        $query = $this->db->query("SELECT * FROM ".$table." WHERE ".$field." = ".$id." GROUP BY " . $group);
        $result = $query->result();
        return $result;
    }
}