<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Q_auditreport extends CI_Model{

	public function get_list($query = null, $limit, $offset, $group = null){

		$this->db->limit($limit,$offset);
		$this->db->select("*");
		$this->db->from("qv_audit_report_with_listing");

		if($query != null) {
			$this->db->where($query);
		}

		$this->db->where("Report_Status >= 0");

		if($group != null) {
			$this->db->group_by($group);
		}

		$this->db->order_by("report_id", "desc");
		$q = $this->db->get();
		return $q->result();

	}


	public function get_pagination($query = null,$group = null){

		$this->db->select("*");
		$this->db->from("qv_audit_report_with_listing");
		if($query != null) {
			$this->db->where($query);
		}

		$this->db->where("Report_Status >= 0");
		
		if($group != null) {
			$this->db->group_by($group);
		}


		$q = $this->db->get();
		return $q->num_rows();

	}


	public function group_query($table, $query = null){

		$this->db->select("*");
		$this->db->from($table);
		if($query != null) {
			$this->db->where($query);
		}

		$q = $this->db->get();
		return $q->result();

	}

	public function get_country(){

		$this->db->select("*");
		$this->db->from("country_list");
		$q = $this->db->get();
		return $q->result();

	}

	public function group_list($table, $group=null){

		$this->db->select("*");
		$this->db->from($table);
		if($group != null) {
			$this->db->group_by($group);
			$this->db->order_by($group, "asc");
		}
		$this->db->where("status", 1);

		$q = $this->db->get();
		return $q->result();

	}

	public function report_status_update($id,$data){

		$this->db->where("report_id",$id);
		$this->db->update("tbl_report_listing",$data);

		$this->db->where("report_id",$id);
		$this->db->update("tbl_report_summary",$data);

		if($this->db->affected_rows() >=0){
		  return true; 
		}else{
		  return false; 
		}

	}

	public function get_report_audit_date($id){

		$this->db->select("tbl_report_audit_date.audit_date as Date");
		$this->db->where("tbl_report_audit_date.report_id",$id);
		$this->db->from("tbl_report_audit_date");
		$this->db->group_by("tbl_report_audit_date.audit_date");
		$this->db->order_by("tbl_report_audit_date.audit_date", "asc");
		$q = $this->db->get();
		return $q->result();

	}

	public function update_date_submission($data,$reportid){
		$this->db->where("report_id",$reportid);
		$this->db->update("tbl_report_summary",$data);
	}

	public function check_if_auditor($table,$query)
	{
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where($query);
		$q = $this->db->get();
		return $q->num_rows();
	}

	public function get_data($query)
	{
		$q = $this->db->query($query);
		return $q->result();
	}



}
