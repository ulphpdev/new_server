<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data_maintenance_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
     public function get_list_data_maintenance($query = null, $limit, $offset, $table ,$order_by){
        $this->db->limit($limit,$offset);
        $this->db->select("*");
        $this->db->from($table);
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->where("status","1")->or_where("status","0");
        $this->db->order_by($order_by, "desc");
        $q = $this->db->get();
        return $q->result();
    }
     public function get_list_data_maintenance_product($query = null, $limit, $offset){
        $this->db->limit($limit,$offset);
        $this->db->select("*");
        $this->db->from('product');
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->where("status","1")->or_where("status","0");
        $this->db->order_by('product_id', "desc");
        $q = $this->db->get();
        return $q->result();
    }
    public function get_list_data_maintenance_standard($query = null, $limit, $offset){
        $this->db->limit($limit,$offset);
        $this->db->select("*");
        $this->db->from('standard');
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->where("status","1")->or_where("status","0");
        $this->db->order_by('standard_id', "desc");
        $q = $this->db->get();
        return $q->result();
    }
    ///
    //pagination
     public function get_pagination_auditor($query = null){
        $this->db->select("*");
        $this->db->from("tbl_auditor_info");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->group_by("report_id");
        $this->db->order_by("auditor_id", "desc");
        $q = $this->db->get();
        return $q->num_rows();
    }
     public function get_pagination_reviewer($query = null){
        $this->db->select("*");
        $this->db->from("tbl_reviewer_info");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->group_by("report_id");
        $this->db->order_by("reviewer_id", "desc");
        $q = $this->db->get();
        return $q->num_rows();
    }
     public function get_pagination_approver($query = null){
        $this->db->select("*");
        $this->db->from("tbl_approver_info");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->group_by("report_id");
        $this->db->order_by("approver_id", "desc");
        $q = $this->db->get();
        return $q->num_rows();
    }
     public function get_pagination_site($query = null){
        $this->db->select("*");
        $this->db->from("tbl_company");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->group_by("report_id");
        $this->db->order_by("company_id", "desc");
        $q = $this->db->get();
        return $q->num_rows();
    }
    public function get_pagination_product_materials($query = null){
        $this->db->select("*");
        $this->db->from("tbl_product");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->group_by("report_id");
        $this->db->order_by("product_id", "desc");
        $q = $this->db->get();
        return $q->num_rows();
    }
    public function get_pagination_audit_scope($query = null){
        $this->db->select("*");
        $this->db->from("tbl_audit_scope");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->group_by("report_id");
        $this->db->order_by("scope_id", "desc");
        $q = $this->db->get();
        return $q->num_rows();
    }
      public function get_pagination_category($query = null){
        $this->db->select("*");
        $this->db->from("tbl_question_category");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->group_by("report_id");
        $this->db->order_by("category_id", "desc");
        $q = $this->db->get();
        return $q->num_rows();
    }
     public function get_pagination_product_type($query = null){
        $this->db->select("*");
        $this->db->from("tbl_classification");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->group_by("report_id");
        $this->db->order_by("classification_id", "desc");
        $q = $this->db->get();
        return $q->num_rows();
    }
     public function get_pagination_standard($query = null){
        $this->db->select("*");
        $this->db->from("tbl_standard_reference");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->group_by("report_id");
        $this->db->order_by("standard_id", "desc");
        $q = $this->db->get();
        return $q->num_rows();
    }
     public function get_pagination_distribution($query = null){
        $this->db->select("*");
        $this->db->from("tbl_distribution");
        if($query != null) {
            $this->db->where($query);
        }
        $this->db->where("status > -1");
        // $this->db->group_by("report_id");
        $this->db->order_by("distribution_id", "desc");
        $q = $this->db->get();
        return $q->num_rows();
    }
     public function data_maintenance_delete($id, $status, $table, $order_by){
        $this->db->set('status', $status);
        $this->db->set('update_date', date("Y-m-d H:i:s"));
        $this->db->where($order_by, $id);
        $this->db->update($table);
    }
    // public function data_maintenance_delete_reviewer($id, $status, $table){
    //     $this->db->set('status', $status);
    //     $this->db->where('reviewer_id' , $id);
    //     $this->db->update($table);
    // }
    // public function data_maintenance_delete_approver($id, $status, $table){
    //     $this->db->set('status', $status);
    //     $this->db->where('approver_id' , $id);
    //     $this->db->update($table);
    // }
    // public function data_maintenance_delete_site($id, $status, $table){
    //     $this->db->set('status', $status);
    //     $this->db->where('company_id' , $id);
    //     $this->db->update($table);
    // }
    //  public function data_maintenance_delete_product($id, $status, $table){
    //     $this->db->set('status', $status);
    //     $this->db->where('product_id' , $id);
    //     $this->db->update($table);
    // }
    //  public function data_maintenance_delete_typeofaudit($id, $status, $table, $order_by){
    //     $this->db->set('status', $status);
    //     $this->db->where($order_by , $id);
    //     $this->db->update($table);
    // }
}