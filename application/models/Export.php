<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Export extends CI_Controller {

		function __construct(){
			parent::__construct();
	        $this->load->model('Template_model');
	        $this->load->helper('url');
	        $this->load->model('Audit_report_model');
	        $this->load->model('Global_model');
	        $this->load->model('Audit_report_analysis_model');
	        $this->load->model('Preview_report_model');
			$this->load->model('q_auditreport');
			$this->load->model('Api_model');

	        $this->load->library('Phpword');
		}


		function copyFiles($dir,$dirNew){
			scandir($dir,1);
			if (is_dir($dir)) {
				if ($dh = opendir($dir)) {
					while (($file = readdir($dh)) !== false) {
						if ($file==".") continue;
						if ($file=="..")continue;
						copy("$dir/$file","$dirNew/$file");
					}
		 
					closedir($dh);
				} else{
					echo "error opendir";
				}
			} else {
				echo "error is_dir";
			}
		}




		function index()
		{
			$id = $_POST['report_id'];
			$status = $_POST['status'];

			if($status == 1){
				$status = "approved";
			} else {
				$status = "archive";
			}

			if (!file_exists('./json/export/' . $status . '/' . $id)) {
			    mkdir('./json/export/' . $status . '/' . $id, 0777, true);
			}

			$this->generate_audit_report($id,$status);
			$this->generate_annexure($id,$status);
			$this->generate_executive_summary($id,$status);
			$this->generate_raw_data_report($id,$status);
			$this->generate_annexure_doc($id,$status);

			$result['content'] = $this->Api_model->get_for_export();
			$result['file'] = 'export/list';
			return $this->create_json($result);       

		}

		function create_json($result){
			$file = getcwd().'/json/'.$result['file'].'.json';
			file_put_contents($file,'');
			chmod($file,0777);
			$fp = fopen($file, 'w');
			fwrite($fp, json_encode($result['content']));
			fclose($fp);
		}

		function get_audit_dates($audit_dates)
        {
                $len = count($audit_dates);
                $auddate = "";
                $audyear = "";
                $audmonth = "";
                foreach ($audit_dates as $key => $value) {
                    $audmonth = date_format(date_create($value->audit_date),"F");
                    $audyear = date_format(date_create($value->audit_date),"Y");
                    if ($key == $len - 1) {
                       $auddate .= ' & ' . date_format(date_create($value->audit_date),"d");
                    } else {
                       $auddate .= ', ' . date_format(date_create($value->audit_date),"d");
                    }
                }
                return $audmonth . " " . substr($auddate,2) . " " . $audyear;
        }

        function text_formatter($text){
                return str_replace('&', '&amp;', $text);
        }

        public function generate_annexure_doc($id, $status)
        {
                ///GATHER DATE ///////////////////////
                $query = "report_id = " . $id;
                $report= $this->Audit_report_model->get_report($id);
                $audit_dates = $this->Audit_report_model->get_audit_dates($id);
                $element = $this->Template_model->get_elements($report[0]->template_id);


                ///GENERATE WORD /////////////////////

                $auditdate = $this->get_audit_dates($audit_dates);
                $phpWord = new \PhpOffice\PhpWord\PhpWord();

                $phpWord->getCompatibility()->setOoxmlVersion(14);
                $phpWord->getCompatibility()->setOoxmlVersion(15);
                $phpWord->setDefaultFontName('courier');

                $targetFile = "./assets/word/";
                $filename = 'Annexure.docx';

                
                $tableStyle = array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  );
                $styleCell = array('borderTopSize'=>1 ,'borderTopColor' =>'black','borderLeftSize'=>1,'borderLeftColor' =>'black','borderRightSize'=>1,'borderRightColor'=>'black','borderBottomSize' =>1,'borderBottomColor'=>'black' );
                $noSpace = array('textBottomSpacing' => -1);        
                
                $headcell = [ 'bgColor'=>'#6086B8','gridSpan' => 7, 'borderTopSize'=>1 ,'borderTopColor' =>'black','borderLeftSize'=>1,'borderLeftColor' =>'black','borderRightSize'=>1,'borderRightColor'=>'black','borderBottomSize' =>1,'borderBottomColor'=>'black'];
                $headcell_blank = [ 'gridSpan' => 7];

                $HeaderFont = array('bold'=>true, 'italic'=> false, 'size'=>16, 'name' => 'Calibri', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);
                $BoldFont = array('bold'=>true, 'italic'=> false, 'size'=>12, 'name' => 'Calibri', 'afterSpacing' => 0, 'Spacing'=> 2, 'cellMargin'=>0);
                $Font = array('bold'=>false, 'italic'=> false, 'size'=>12, 'name' => 'Calibri', 'afterSpacing' => 0, 'Spacing'=> 2, 'cellMargin'=>0);
                $CapsFont = array('bold'=>true, 'allCaps'=>true,'italic'=> false, 'size'=>12, 'name' => 'Calibri', 'afterSpacing' => 0, 'Spacing'=> 2, 'cellMargin'=>0);
                $Center_text = ['align' => \PhpOffice\PhpWord\Style\Alignment::ALIGN_CENTER];
                $spacing = array('align'=>'center','alignment'=>'center','spaceAfter' => 20, 'spaceBefore' => 20);

                $section = $phpWord->createSection(array('orientation'=>'landscape','marginLeft' => 500, 'marginRight' => 500, 'marginTop' => 600, 'marginBottom' => 600));
                $section->addText('ANNEXURE - GMP AUDIT REPORT',$HeaderFont, $Center_text);

                $table = $section->addTable('Header-Table',array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  ));
                $table->addRow(-0.5, array('exactHeight' => -5));
                $table->addCell(2500,$styleCell)->addText('Audited Site :',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
                $table->addCell(6000,$styleCell)->addText($this->text_formatter($report[0]->name),$Font,array('align' => 'left', 'spaceAfter' => 5));
                $table->addCell(2500,$styleCell)->addText('Audit Report No. :',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
                $table->addCell(6000,$styleCell)->addText($this->text_formatter($report[0]->report_no),$Font,array('align' => 'left', 'spaceAfter' => 5));

                $table->addRow();
                $table->addCell(2500,$styleCell)->addText('Site Address :',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
                $table->addCell(6000,$styleCell)->addText($this->text_formatter($report[0]->address1. ', ' .$report[0]->address2 . ',' . $report[0]->address3.', '.$report[0]->country),$Font,array('align' => 'left', 'spaceAfter' => 5));
                $table->addCell(2500,$styleCell)->addText('Audit Date(s) :',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
                $table->addCell(6000,$styleCell)->addText($this->text_formatter($auditdate),$Font,array('align' => 'left', 'spaceAfter' => 5));

                $section->addTextBreak();
                $table = $section->addTable('2nd-Table',array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  ));
                $table->addRow(-0.5, array('exactHeight' => -5));
                $table->addCell(500,$styleCell)->addText('NO.',$BoldFont,$spacing,$Center_text);
                $table->addCell(2500,$styleCell)->addText('CATEGORY',$BoldFont,$spacing,$Center_text);
                $table->addCell(4000,$styleCell)->addText('AUDIT OBSERVATIONS',$BoldFont,$spacing,$Center_text);
                $table->addCell(4000,$styleCell)->addText('ROOT CAUSE',$BoldFont,$spacing,$Center_text);
                $table->addCell(4000,$styleCell)->addText('CORRECTIVE ACTION \ PREVENTIVE ACTION',$BoldFont,$spacing,$Center_text);
                $table->addCell(2000,$styleCell)->addText('TARGET DATE',$BoldFont,$spacing,$Center_text);
                $table->addCell(4000,$styleCell)->addText('REMARKS',$BoldFont,$spacing,$Center_text);
                $table->addRow(-0.5, array('exactHeight' => -5));
                $table->addCell(null, $headcell_blank);
                $ctrx = 1;
                foreach ($element as $key => $value) {
                    $no_answer = $this->Global_model->get_no_answer($id,$value->element_id);
                    if(!empty($no_answer)){
                        $table->addRow(-0.5, array('exactHeight' => -5));
                        $table->addCell(null, $headcell)->addText($this->text_formatter($value->element_name),$CapsFont,array('align' => 'left', 'spaceAfter' => 5));
                        foreach ($no_answer as $key => $value) {
                                $table->addRow(-0.5, array('exactHeight' => -5));
                                $table->addCell(500,$styleCell)->addText($ctrx,$Font,$Center_text);
                                $table->addCell(2500,$styleCell)->addText($this->text_formatter($value->category_name),$Font,$Center_text);
                                $table->addCell(4000,$styleCell)->addText($this->text_formatter($value->answer_details),$Font);
                                $table->addCell(4000,$styleCell)->addText('',$Font,array('spaceAfter' => 300,'spaceBefore' =>300));
                                $table->addCell(4000,$styleCell)->addText('',$Font,$Center_text);
                                $table->addCell(2000,$styleCell)->addText('',$Font,$Center_text);
                                $table->addCell(4000,$styleCell)->addText('',$Font,$Center_text);
                            $ctrx++;
                        }
                    }
                //     $table->addRow();
                }

                $section->addTextBreak();
                $table = $section->addTable('2nd-Table',array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  ));
                
                $table->addRow(-0.5, array('exactHeight' => -5));
                $table->addCell(10000,$headcell_blank)->addText('Prepaired and reviewed by:',$Font);
                $table->addCell(10000,$headcell_blank)->addText('Approved by:',$Font);
                
                // if($report[0]->status == '5'){
                //     $table->addRow(-0.5, array('exactHeight' => -5));
                //     $table->addCell(10000,$headcell_blank)->addImage($report[0]->image,array('width' => 150,'align' => 'center'));
                //     $table->addCell(10000,$headcell_blank)->addImage($report[0]->rev_image,array('width' => 150,'align' => 'center'));
                // }
                $table->addRow();
                $table->addCell(10000,$headcell_blank)->addText($this->text_formatter($report[0]->auditor) . '<w:br />' . $this->text_formatter($report[0]->designation) . '<w:br />' . $this->text_formatter($report[0]->department),$Font,$Center_text,array('spaceAfter' => 5));
                $table->addCell(10000,$headcell_blank)->addText($this->text_formatter($report[0]->reviewer) . '<w:br />' . $this->text_formatter($report[0]->rev_pos) . '<w:br />' . $this->text_formatter($report[0]->rev_dep),$Font,$Center_text);

                $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
                $objWriter->save(__DIR__.'./../../json/export/approved/' . $id .'/' . $filename);
                $objWriter->save(__DIR__.'./../../json/export/archive/' . $id .'/' . $filename);
                exit;


        }


		function generate_raw_data_report($id, $status){

	        $query = "report_id = " . $id;
	        $report = $this->q_auditreport->get_list($query,1,0, "");
	        $audit_dates = $this->Audit_report_model->get_audit_dates($id);
	        $co_auditor = $this->Audit_report_model->get_data_by_auditor($id, 'report_id', 'tbl_co_auditors');
	        $products = $this->Audit_report_model->get_products($report[0]->Company_ID);
	        $product_type = $this->Audit_report_model->get_product_type($report[0]->Template_ID);
	        $scope = $this->Audit_report_model->get_data_by_audit_scope($id, 'report_id', 'tbl_report_audit_scope');
	        $stanard_reference = $this->Audit_report_model->stanard_reference($report[0]->Template_ID);

	        $elements = $this->Audit_report_model->get_elements($id);


	        $row = $this->Audit_report_analysis_model->get_report_analysis_view2($id);

	        $applicable_question = $row[0]->Total_Applicable_Questions;
	        $factor_major = $row[0]->Factor_For_Major;
	        $factor_critical = $row[0]->Factor_For_Critical;
	        $weight_major = $row[0]->Weight_For_Major;
	        $weight_critical = $row[0]->Weight_For_Critical;
	        $weighted_denominator = $row[0]->Weighted_Denominator;
	        $rating = (int)$row[0]->Rating;

	        // $applicable_question = ((int)$row[0]->yes)+((int)$row[0]->no)+((int)$row[0]->major)+((int)$row[0]->minor)+((int)$row[0]->critical); 
	        // $factor_major = ((((int)$row[0]->yes * 100)/(int)$row[0]->limit) - $applicable_question);
	        // $factor_critical = ((((int)$row[0]->yes) * 100/((int)$row[0]->limit)) - $applicable_question)/((int)$row[0]->no_major); 
	        // $weight_major = ((int)$row[0]->major) * $factor_major; 
	        // $weight_critical = ((int)$row[0]->critical) * $factor_critical;
	        // $weighted_denominator = $applicable_question + $weight_major + $weight_critical;
	        // $rating = round((((int)$row[0]->yes) / $weighted_denominator) * 100);



	        $this->load->library("Pdf");
	        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
	        $pdf->setFooterData(array(0,64,0), array(0,64,128));
	        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	        $pdf->SetMargins('20', '32', '20');
	        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	        $pdf->SetFont('gothic', '', 10);
	        $pdf->AddPage();

	        $html = '';
	        $html .='<style>.odd{ background-color:#ccc; } .even { background-color: #bbb;} .bordered { border: .5px solid black;} .bordered-white {border: 1px solid #fff;} .spacer { margin-top : 5px; margin-bottom: 5px; }</style>';
	        $html .='<div>';
	        $html .='<table>';
	        $html .='   <tr>';
	        $html .='       <td width="80%" style="text-align: center;">';
	        $html .='           <label style="vertical-align:middle;"><h3>GMP RAW DATA REPORT</h3></label>';
	        $html .='       </td>';
	        $html .='       <td width="20%" class="bordered" style="text-align: center;">';
	        $html .='           <center><label><h2>Rating:<br />';
	        $html .='           ' . $rating . '%</h2></label></center>';
	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='</table>';
	        $html .='</div>';

	        $html .='<table>';
	        $html .='   <tr>';
	        $html .='       <td width="150">Company Name</td>';
	        $html .='       <td width="200">: ' . $report[0]->name . '</td>';
	        $html .='       <td width="10"></td>';
	        $html .='       <td width="100">Date Audit</td>';
	        $html .='       <td width="200">: ';
	        
	        $len = count($audit_dates);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($audit_dates as $key => $value) {
	            $audmonth = date_format(date_create($value->audit_date),"M");
	            $audyear = date_format(date_create($value->audit_date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($value->audit_date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($value->audit_date),"d");
	            }
	        }

	        $html .= $audmonth . " " . substr($auddate,2) . " " . $audyear;

	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='   <tr>';
	        $html .='       <td width="150">Site Address</td>';
	        $html .='       <td width="200">: '.$report[0]->address1. ', ' .$report[0]->city_name.'</td>';
	        $html .='       <td width="10"></td>';
	        $html .='       <td width="100">Lead Auditor</td>';
	        $html .='       <td width="200">: ' . $report[0]->Auditor_Name . '</td>';
	        $html .='   </tr>';
	        $html .='   <tr>';
	        $html .='       <td width="150"></td>';
	        $html .='       <td width="200">  '.$report[0]->province_name.', '.$report[0]->country_name.'</td>';
	        $html .='       <td width="10"></td>';
	        $html .='       <td width="100">Co-Auditor</td>';
	        $html .='       <td width="200">: ';
	      
	        foreach ($co_auditor as $key => $value) {
	            $html .= '<span>' . $value->fname. ' ' .$value->lname.'</span><br>   ';
	        }

	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='</table>';


	        $html .='<div>';
	        $html .='<table>';
	        $html .='   <tr>';
	        $html .='       <td>';
	        $html .='           <b>Product type : </b>' . $product_type[0]->classification_name;
	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='   <tr>';
	        $html .='       <td>';
	        $html .='           <b>Products : </b>';
	        $html .='           <ol>';
	        $array = array();
	        foreach ($scope as $key => $value) {
	            $scope_product = $this->Audit_report_model->get_scope_product_by_scope($id,$value->audit_scope_id);
	        }
	        foreach ($scope_product as $key => $value) {
	             $html .='    <li> '.$value->product_name.' </li>';
	        }   
	        $html .='           </ol>';
	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='<tr><td></td></tr>';
	        $html .='   <tr>';
	        $html .='       <td>';
	        $html .='           <b>Standard/Reference : </b>'. $stanard_reference[0]->standard_name;
	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='</table>';
	        $html .='</div>';
	        $letters = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
	        $element_number = 0;
	        foreach ($elements as $key => $value) {
	        $element_id = $value->element_id;
	        $html .='<label><h3>Element ' . $letters[$element_number] . ' - ' . $value->element_name . '</h3></label>';
	        $html .='<table cellpadding="10">';
	        $html .='   <tr style="background-color: #4594cd;" > ';
	        $html .='       <td class="bordered" style="text-align: center;" width="10%"></td>';
	        $html .='       <td class="bordered" style="text-align: center;" width="30%"><h4 style="color: #fff;">Questions</h4></td>';
	        $html .='       <td class="bordered" style="text-align: center;" width="15%"><h4 style="color: #fff">Answer</h4></td>';
	        $html .='       <td class="bordered" style="text-align: center;" width="30%"><h4 style="color: #fff">Remarks</h4></td>';
	        $html .='       <td class="bordered" style="text-align: center;" width="15%"><h4 style="color: #fff">Category</h4></td>';
	        $html .='   </tr>';

	        $questions_answer = $this->Audit_report_model->get_elements_q_a($id,$element_id);
	        $Q = 1;
	        foreach ($questions_answer as $key => $value1) {

	            if ( $Q & 1 ) { $tr_class = "odd"; } else { $tr_class = "even"; }
	            $html .='   <tr class='. $tr_class . '> ';
	            $html .='       <td class="bordered" style="text-align: center;">' . $letters[$element_number] . $Q .'</td>';
	            $html .='       <td class="bordered">' . $value1->question . '</td>';
	            $html .='       <td class="bordered" style="text-align: center;">' . $value1->answer_name . '</td>';
	            $html .='       <td class="bordered">' . $value1->answer_details . '</td>';
	            $html .='       <td class="bordered" style="text-align: center;">' . $value1->category_name . '</td>';
	            $html .='   </tr>';
	            $Q += 1;
	        }
	       
	        $html .='</table>';
	        $element_number += 1;
	        }


	        $pdf->writeHTML($html,true,false,true,false,'');
	        ob_clean();
	        $pdf->Output(__DIR__.'./../../json/export/approved/' . $id .'/Raw_Data_Report.pdf', 'F');
	        $pdf->Output(__DIR__.'./../../json/export/archive/' . $id .'/Raw_Data_Report.pdf', 'F');
	        // echo $html;
	    }

		function generate_executive_summary($id, $status){       
	        $id = $id;
	        $report = $this->Audit_report_model->get_report($id);
	        $co_auditor = $this->Audit_report_model->get_data_by_auditor($id, 'report_id', 'tbl_co_auditors');
	        $scope = $this->Audit_report_model->get_data_by_audit_scope($id, 'report_id', 'tbl_report_audit_scope');
	        $element = $this->Template_model->get_elements($report[0]->template_id);
	        $template = $this->Audit_report_model->get_template_reference($id);
	        $answers = $this->Audit_report_analysis_model->get_report_analysis_view($id);
	        $recommendation = $this->Audit_report_model->get_element_recommendation($id);
	        $disposition = $this->Audit_report_model->get_disposition2($id);
	        $distribution = $this->Audit_report_model->get_distribution($id);
	        $other_distribution = $this->Global_model->get_other_distribution($id);

	        $audit_dates = $this->Audit_report_model->get_audit_dates($id);

	        $this->load->library("Pdf");
	        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
	        $pdf->setFooterData(array(0,64,0), array(0,64,128));
	        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	        $pdf->SetMargins('20', '32', '20');
	        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	        $pdf->SetFont('gothic', '', 10);
	        $pdf->AddPage();
	        $html = '';
	        $x = 'A';
	        $html .='<div style="text-align:center"><center><label><h3>GMP AUDIT REPORT EXUCUTIVE SUMMARY REPORT</h3></label></center></div>';
	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="150">Company Name </td><td width="10">:</td><td> '.$report[0]->name.' </td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="150">Site Address </td><td width="10">:</td><td> '.$report[0]->address1.', '.$report[0]->address2.' </td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="150"></td><td width="10"></td><td> '.$report[0]->address3.', '.$report[0]->country.' </td>';
	        $html .='</tr>';
	        $html .='</table>';
	        $html .='</div>';
	        $html .='<div>';
	        $html .='<label><b>Summary Statement: </b></label>';
	        $len = count($audit_dates);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($audit_dates as $key => $value) {
	            $audmonth = date_format(date_create($value->audit_date),"F");
	            $audyear = date_format(date_create($value->audit_date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($value->audit_date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($value->audit_date),"d");
	            }
	        }
	        $html .='<p>An audit was conducted at the above facility on '.$audmonth . " " . substr($auddate,2) . " " . $audyear.'. This document serves to attest that the origninal audit documentation has been reviewed and that this report is an accurate summaty of the original audit documentation.</p><br><br>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="150">Lead Auditor </td><td width="10">:</td><td>'.$report[0]->auditor.'</td>';
	        $html .='</tr>';
	        foreach ($co_auditor as $key => $value){
	            $html .='<tr>';
	            $html .='<td width="150">Audit Team Member/s</td><td width="10">:</td><td>'.$value->fname.' '.$value->lname.'</td>';
	            $html .='</tr>';
	        }
	        $html .='</table>';
	        $html .='</div>';
	        $html .='<div>';
	        $html .='<label><b>Scope: </b></label>';
	        foreach ($scope as $key => $value) {
	            $html .='<p><u>'.$value->scope_name.'</u> '.$value->scope_detail.' for the following products:</p>';
	            $scope_product = $this->Audit_report_model->get_scope_product_by_scope($id,$value->audit_scope_id);
	            $html .='<ul type="square">';
	            foreach ($scope_product as $key => $value) {
	               $html .='<li> '.$value->product_name.' </li>';
	            }
	            $html .='</ul>';
	        }
	        $html .='</div>';
	        $html .='<div>';
	        $html .='<p>The audit consisted of an in-depth review of their quality systems including, but not limited to the following: </p>';
	        $html .='<ul type="square">';
	        foreach ($element as $key => $value){
	            $html .='<li> '.$value->element_name.' </li>';
	        }
	        $html .='<li> Licese(s) Accreditation(s)/ Certification(s) held by supplier and verified during the audit. </li>';
	        $html .='</ul>';
	        $html .='</div>';
	        $html .='<div>';
	        $html .='<p>The audit did not cover the following areas, to be covered during the next audit: </p>';
	        $html .='<ul type="square"><li>'.$report[0]->areas_to_consider.'</li></ul>';
	        $html .='<hr><br>';
	        $html .='</div>';

	        $html .='<div>';
	        $html .='<p>The standard(s) used during the audit included: </p>';
	        $html .='<ul type="square">';
	        foreach ($template as $key => $value){
	            $html .='<li> '.$value->standard_name.' </li>';
	        }
	        $html .='</ul>';
	        $html .='</div>';
	        // $html='';
	        
	        // $pdf->AddPage();
	        $html .='<div>';
	        $html .='<label><b> Audit Results: </b></label>';
	        $html .='<p>The following are the observations, categorized based on critically, noted during '.$audmonth . " " . substr($auddate,2) . " " . $audyear.' inspection.</p>';
	        $html .='<table border="1" style="100%" cellpadding="3">';
	        $html .='<tr style="background-color:#ccc;">';
	        $html .='<td style="font-weight:bold;text-align:center;">No. of Critical <br/> Observations</td>';
	        $html .='<td style="font-weight:bold;text-align:center;">No. of Major <br/> Observations </td>';
	        $html .='<td style="font-weight:bold;text-align:center;">No. of Minor <br/> Observations</td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td style="text-align:center;">'.$answers[0]->critical.'</td>';
	        $html .='<td style="text-align:center;">'.$answers[0]->major.'</td>';
	        $html .='<td style="text-align:center;">'.$answers[0]->minor.'</td>';
	        $html .='</tr>';
	        $html .='</table>';
	        $html .='</div>';
	        $html .='<div>';
	        $html .='<label><b>Observations: </b></label>';
	        $html .='<p>We recommend that all the findings noted should be addressed giving utmost emphasis on the significant [Critical,Major] Observations which can be grouped into the following Quality System and/or GMP elements:</p>';
	        $html .='<ul type="square">';
	        foreach ($recommendation as $key => $value) {
	            $html .='<li><b>'.$value->element_name.'</b> - '.$value->recommendation.'</li>';
	        }
	        $html .='</ul>';
	        $html .='</div>';
	        $html .='<div>';
	        $html .='<p>Based on the results of this asessement, <u>'.$report[0]->name.'</u> is at this time considered: </p>';
	        $html .='<table>';
	        $ctr1 = 1;
	        foreach ($disposition as $key => $value) {
	            $html .='<tr>';
	            $html .='<td width="20%"><b>Disposition '.$ctr1.': </b></td>';
	            $html .='<td width="80%">';
	            $html .='<p><b>'.$value->disposition_name.' ' . $value->disposition_label . '</b></p>';
	            $html .='<ul type="square">';
	            $scope_product = $this->Audit_report_model->get_disposistion_product($id,$value->disposition_id);
	            foreach ($scope_product as $key => $value) {
	                $html .='        <li> '. $this->Audit_report_model->get_productname($value->audit_scope_id).' </li>';
	             }             
	            $html .='</ul>';
	            $html .='<div>&nbsp;</div>';
	            $html .='</td>';
	            $html .='</tr>';
	            $ctr1++;
	            
	        }
	        $html .='</table>';
	        $html .='</div>';
	        $html .='<div>';
	        if($report[0]->closure_date == '' || $report[0]->closure_date == null){
	            $close_date = 'N/A';
	        }else{
	            $close_date = $report[0]->closure_date;
	        }
	        $other_issue = explode(",",$report[0]->other_issues_executive);
	        $html .='<p><b>Audit Closure Date. if applicable: '.$close_date.'</b></p>';
	        $html .='<p><b>OTHER ISSUES:</b></p>';
	        $html .='<ul type="square">';
	        foreach ($other_issue as $key => $value) {
	            $html .='<li>'.$value.'</li>';
	        }
	        $html .='</ul>';
	        $html .='<hr><br>';
	        $html .='</div>';
	        $pdf->writeHTML($html,true,false,true,false,'');
	        $pdf->AddPage();
	        $pdf->SetXY(25,170);
	        $html2 ='';        
	        $html2 .='<table>';
	        $html2 .='<tr>';
	        $html2 .='<td><b>Prepared by: </b></td>';
	        $html2 .='<td><b>Approved by: </b></td>';
	        $html2 .='</tr>';
	        // $html2 .='<tr>';
	        // $html2 .='<td style="text-align:center;">';
	        // if($report[0]->status == '5'){
	        //     $html2 .='<img src="'.$report[0]->image.'" height="80">';
	        // }else{
	        //     $html2 .='&nbsp;';
	        // }
	        // $html2 .='</td>';
	        // $html2 .='<td style="text-align:center;">';
	        // if($report[0]->status == '5'){
	        //     $html2 .='<img src="'.$report[0]->rev_image.'" height="80">';
	        // }else{
	        //     $html2 .='&nbsp;';
	        // }
	        // $html2 .='</td>';
	        // $html2 .='</tr>';
	        $html2 .='<tr>';
	        $html2 .='<td style="text-align:center;">'.$report[0]->auditor.'<br>'.$report[0]->designation.'<br>'.$report[0]->department.'</td>';
	        $html2 .='<td style="text-align:center;">'.$report[0]->reviewer.'<br>'.$report[0]->rev_pos.'<br>'.$report[0]->rev_dep.'</td>';
	        $html2 .='</tr>';
	        $html2 .='</table>';
	        $html2 .='</div>';
	        $html2 .='<div>';
	        $html2 .='<b>Distribution List (include the Supplier Quality representative(s) from ALL locations of use associated with this supplier)</b>';
	        $html2 .='<ul type="square">';
	        $html2 .='<li>Quality Representative, '.$report[0]->name.'</li>';
	        foreach ($distribution as $key => $value){
	            $html2 .='<li>'.$value->distribution_name.'</li>';
	        }
	        if(!empty($other_distribution)){
	            foreach ($other_distribution as $key => $value) {
	                $html2 .='<li>'.$value->other_distribution.'</li>';
	            }
	        }
	        $html2 .='<li>Corporate GMP Department, UNILAB</li>';
	        $html2 .='</ul>';
	        $html2 .='</div>';
	        $pdf->writeHTML($html2,true,false,true,false,'');
	        ob_clean();
	        $pdf->Output(__DIR__.'./../../json/export/approved/' . $id .'/Executive_Report.pdf', 'F');
	        $pdf->Output(__DIR__.'./../../json/export/archive/' . $id .'/Executive_Report.pdf', 'F');
	    }

		function generate_annexure($id, $status){    
	        $report_id = $id;
	        $report= $this->Audit_report_model->get_report($report_id);
	        $lead_auditor = $this->Audit_report_model->get_data_by_executive_lead_auditor($report_id,'report_id','tbl_report_summary');
	        $approver = $this->Audit_report_model->get_data_by_approver($report_id,'report_id','tbl_approver');
	        $answers = $this->Audit_report_model->get_data_by_answers($report_id,'report_id','tbl_report_answers');
	        $element = $this->Template_model->get_elements($report[0]->template_id);

	        $audit_dates = $this->Audit_report_model->get_audit_dates($report_id);
	        
	        $this->load->library("Pdf");
	        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
	        $pdf->setFooterData(array(0,64,0), array(0,64,128));
	        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	        $pdf->SetMargins('20', '32', '20');
	        $pdf->SetMargins( 10, 10, 10, 10);
	        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	        $pdf->SetFont('gothic', '', 10);
	        $pdf->SetPrintHeader(false);    
	        $pdf->AddPage('L', 'LETTER');
	        $html = '';
	        $x = 'A';
	        
	        $html .='<div style="text-align:center">
	        <center><label><h2>ANNEXURE - GMP AUDIT DETAILS</h2></label></center>
	        </div>';
	        $html .='<table style="100%" border="1" cellpadding="">';
	        $html .='<tr>';
	        $html .='<td width="10%"><label><b> Audited Site: </b></label></td>';
	        $html .='<td width="50%"><label> '.$report[0]->name.'</label></td>';
	        $html .='<td width="15%"><label><b> Audit Report No: </b></label></td>';
	        $html .='<td width="25%"><label> '.$report[0]->report_no.'</label></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td><label><b> Site Address: </b></label></td>';
	        $html .='<td><label> '.$report[0]->address1." ".$report[0]->address2. "," .$report[0]->address3. ",".$report[0]->country.'</label></td>';
	        $html .='<td><label><b> Audit Date(s): </b></label></td>';
	        $len = count($audit_dates);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($audit_dates as $key => $value) {
	            $audmonth = date_format(date_create($value->audit_date),"F");
	            $audyear = date_format(date_create($value->audit_date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($value->audit_date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($value->audit_date),"d");
	            }
	        }
	        $html .='<td><label> '.$audmonth . " " . substr($auddate,2) . " " . $audyear.'</label></td>';
	        $html .='</tr>';
	        $html .='</table>';
	        $html .='<table><tr><td></td></tr></table>';
	        $html .='<table style="100%" border="1" cellpadding="3px">';
	        $html .='<tr>';
	        $html .='<td width="3%" align="center"><label><b>No. </b></label></td>';
	        $html .='<td width="12%" align="center"><label><b>CATEGORY</b></label></td>';
	        $html .='<td width="20%" align="center"><label><b>AUDIT OBSERVATIONS</b></label></td>';
	        $html .='<td width="20%" align="center"><label><b>ROOT CAUSE</b></label></td>';
	        $html .='<td width="20%" align="center"><label><b>CORRECTIVE ACTION/ PREVENTIVE ACTION</b></label></td>';
	        $html .='<td width="10%" align="center"><label><b>TARGET DATE</b></label></td>';
	        $html .='<td width="15%" align="center"><label><b>REMARKS</b></label></td>';
	        $html .='</tr>';
	        $html .='</table>';
	        $html .='<table><tr><td></td></tr></table>';
	        $ctrx = 1;
	        foreach ($element as $key => $value) {
	            $html .='<table border="1" cellpadding="3px">';
	            $no_answer = $this->Global_model->get_no_answer($report_id,$value->element_id);
	            if(!empty($no_answer)){
	                $html .='<tr  style="background-color:#ccc;"><td colspan="7" ><b>' . $value->element_name .  '</b></td></tr>';
	                foreach ($no_answer as $key => $value) {
	                    $html .='<tr>';
	                    $html .='<td width="3%" align="center">'.$ctrx.'</td>';
	                    $html .='<td width="12%" align="center">'.$value->category_name.'</td>';
	                    $html .='<td width="20%">'.$value->answer_details.'</td>';
	                    $html .='<td width="20%"></td>';
	                    $html .='<td width="20%"></td>';
	                    $html .='<td width="10%"></td>';
	                    $html .='<td width="15%"></td>';
	                    $html .='</tr>';
	                    $ctrx++;
	                }
	            }
	            $html .='</table>';
	        }
	        $html .='<table><tr><td></td></tr></table>';
	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td><label>Prepared by: </label></td>';
	        $html .='<td><label>Approved by: </label></td>';
	        $html .='</tr>';
	        // $html .='<tr>';
	        // $html .='<td style="text-align:center;">';
	        // if($report[0]->status == '5'){
	        //     $html .='<img src="'.$report[0]->image.'" height="80">';
	        // }else{
	        //     $html .='&nbsp;';
	        // }
	        // $html .='</td>';
	        // $html .='<td style="text-align:center;">';
	        // if($report[0]->status == '5'){
	        //     $html .='<img src="'.$report[0]->rev_image.'" height="80">';
	        // }else{
	        //     $html .='&nbsp;';
	        // }
	        // $html .='</td>';
	        // $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td style="text-align:center;">'.$report[0]->auditor.'<br>'.$report[0]->designation.'<br>'.$report[0]->department.'</td>';
	        $html .='<td style="text-align:center;">'.$report[0]->reviewer.'<br>'.$report[0]->rev_pos.'<br>'.$report[0]->rev_dep.'</td>';
	        $html .='</tr>';
	        $html .='</table>';
	        $html .='</div>';
	          
	        $pdf->writeHTML($html,true,false,true,false,'');   
	        ob_clean();
	        $pdf->Output(__DIR__.'./../../json/export/approved/' . $id .'/Annexure.pdf', 'F');
	        $pdf->Output(__DIR__.'./../../json/export/archive/' . $id .'/Annexure.pdf', 'F');

	    }


		function generate_audit_report($id, $status){

	        $query = "report_id = " . $id;
	        $report = $this->q_auditreport->get_list($query,1,0, "");
	        $report2 = $this->Audit_report_model->get_report($id);
	        $co_auditor = $this->Audit_report_model->get_data_by_auditor($id, 'report_id', 'tbl_co_auditors');
	        $activities = $this->Audit_report_model->get_data_by_activity($report[0]->Template_ID,'template_id','tbl_template_activities');
	        $inspectors = $this->Preview_report_model->get_inspectors($id);
	        $inspection_changes = $this->Preview_report_model->get_inspection_changes($id);
	        $inspection_changes_date_last = $this->Audit_report_model->get_last_inspection_changes($id);
	        $scope = $this->Audit_report_model->get_data_by_audit_scope($id, 'report_id', 'tbl_report_audit_scope');
	        $scope_product = $this->Template_model->get_data_by_id($id,'report_id','qv_scope_products');
	        $present_during_meeting = $this->Template_model->get_data_by_id($id,'report_id','tbl_present_during_meeting');
	        $get_personel_met = $this->Template_model->get_data_by_id($id,'report_id','tbl_personnel');
	        $recommendation = $this->Audit_report_model->get_element_recommendation($id);
	        $answers = $this->Audit_report_analysis_model->get_report_analysis_view($id);
	        $disposition = $this->Audit_report_model->get_disposition2($id);

	        $preaudit_documents = $this->Audit_report_model->preaudit_documents($id);
	        $stanard_reference = $this->Audit_report_model->stanard_reference($report[0]->Template_ID);
	        $license_Accreditation = $this->Audit_report_model->license_Accreditation($id);
	        $audit_dates = $this->Audit_report_model->get_audit_dates($id);

	        $inspection_date = $this->Audit_report_model->get_inspection_dates($report[0]->Company_ID);


	        $reviewer_info = $this->Audit_report_model->get_reviewer($id);
	        $approver = $this->Audit_report_model->get_approver($id);

	        $observation = $this->Audit_report_model->get_audit_observation($id);
	        $observation_yes = $this->Audit_report_model->get_audit_observation_yes($id);

	        $element = $this->Template_model->get_elements($report[0]->Template_ID);

	        $signature_stamp = $this->Template_model->get_data_by_id($id,'report_id','tbl_report_signature_stamp');
	       
	        $params = array("report_no"=>$report[0]->Report_No);
	        $this->load->library("Pdf_audit_report",$params);
	        $pdf = new Pdf_audit_report(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
	        $pdf->setFooterData(array(0,64,0), array(0,64,128));
	        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	        $pdf->SetMargins('20', '32', '20');
	        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	        $pdf->SetFont('gothic', '', 10);
	        $pdf->AddPage();
	        $html = '';
	        $x = 'A'; 
	        $html .='<div style="text-align:center"><center><label><h2>GMP AUDIT REPORT</h2><span style="margin-top:-80px;"><b>'.$report[0]->Report_No.'</b></span></label></center></div>';
	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b>AUDITED SITE:</b> </td>';
	        $html .='<td width="30"></td>';
	        $html .='<td width="500"> '. $report[0]->name.' </td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td><td> '.$report[0]->address1. ', ' .$report[0]->city_name. '</td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td><td> '.$report[0]->province_name.', '.$report[0]->country_name.' </td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';


	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b>ACTIVITIES CARRIED OUT BY THE COMPANY:</b></td>';
	        $html .='<td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<table style="margin-top: 20px;">';
	        foreach ($activities as $key => $value) {
	            $html .='<tr>';
	            $html .='<td style="width: 75%;">';
	            $html .= $value->activity_name;
	            $html .='</td>';
	            $html .='<td style="width: 5%;">';

	            $box_activity = base_url() . 'asset/img/' . $this->Audit_report_model->check_used_activity($value->activity_id,$id);
	        
	            $html .='<img style="position: fixed; right: 0;" src="'.$box_activity.'" width="15">';
	            $html .='</td>';
	            $html .='</tr>';
	            foreach ($this->Audit_report_model->get_sub_activities($value->activity_id) as $key => $value) {
	                $box_subactivity = base_url() . 'asset/img/' . $this->Audit_report_model->check_used_subactivity($value->sub_item_id,$id);
	                $html .='<tr>';
	                $html .='<td style="width: 75%;">';
	                $html .= ' &nbsp; &nbsp; &nbsp; ' . $value->sub_item_name;
	                $html .='</td>';
	                $html .='<td style="width: 5%;">';
	                $html .='<img style="position: fixed; right: 0;" src="'. $box_subactivity.'" width="15">';
	                $html .='</td>';
	                $html .='</tr>';
	            }
	            
	        }
	        $html .='<tr>';
	        $html .='<td style="width: 75%;">Others : ' . $report[0]->other_activities . '</td>';
	        $html .='<td style="width: 5%;">';
	            $html .='<img style="position: fixed; right: 0;" src="'.base_url().'asset/img/checkbox_block.jpg" width="15">';
	            $html .='</td>';
	        $html .='</tr>';
	        $html .='</table>';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b>AUDIT DATE:</b></td><td width="30"></td>';
	        $html .='<td width="500">';

	        $len = count($audit_dates);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($audit_dates as $key => $value) {
	            $audmonth = date_format(date_create($value->audit_date),"F");
	            $audyear = date_format(date_create($value->audit_date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($value->audit_date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($value->audit_date),"d");
	            }
	        }

	        $html .= $audmonth . " " . substr($auddate,2) . ", " . $audyear;
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b>AUDITOR/S:</b></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<table>';
	        $html .='<tbody>';
	        $html .='<tr>';
	        $html .='<td>'.$report[0]->Auditor_Name .'</td><td>' .$report[0]->Auditor_Company. '</td>';
	        $html .='</tr>';
	        foreach ($co_auditor as $key => $value) {
	            $html .='<tr>';
	            $html .='<td>'.$value->fname. ' ' .$value->lname.'</td><td>' .$value->company. '</td>';
	            $html .='</tr>';
	        }
	        $html .='<tr><td></td></tr>';
	        $html .='</tbody>';
	        $html .='</table>';
	        $html .= '<br/><b>translator usage : </b> (Name of employee, or approved transaltor, participating in this audit as determined by the Lead Autditor) : ';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';


	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b>REFERENCE:</b></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<b>License(s)/Accreditation(s)/Certification(s)</b> held by supplier and verified during the audit';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="130"></td>';
	        $html .='<td width="500">';
	        $html .='<ul type="square">';
	        foreach ($license_Accreditation as $key => $value) {
	            $html .= '<li>';
	            $html .= $value->reference_name . '<br />';
	            $html .= $value->issuer . '<br />';
	            $html .= 'License / Certificate No .' . $value->reference_no . '<br />';
	            $html .= 'Validity : ' . date_format(date_create($value->validity),"d F Y") . '<br />';
	            $html .= '</li>';
	        }
	        $html .='</ul>';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<b>Pre-audit documents</b> provided and reviewed.';
	        $html .='</td>';
	        $html .='</tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<ul type="square">';
	        foreach ($preaudit_documents as $key => $value) {
	            $html .= '<li>' . $value->document_name . '</li>';
	        }
	        $html .='</ul>';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<b>Regulatory and UNILAB Standards Used</b>';
	        $html .='</td>';
	        $html .='</tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<ul type="square">';
	        foreach ($stanard_reference as $key => $value) {
	            $html .= '<li>' . $value->standard_name . '</li>';
	        }
	        $html .='</ul>';
	        $html .='</td>';
	        $html .='</tr>';

	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>SUPPLIER BACKGROUND / HISTORY</b></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">' . $report[0]->background . '</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td width="500"><b>Date of previous inspection</b></td>';
	        $html .='</tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td><ul type="square"><li>';

	        $len = count($inspection_date[1]['audit_date']);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($inspection_date[1]['audit_date'] as $key => $value) {
	            $audmonth = date_format(date_create($value->Date),"F");
	            $audyear = date_format(date_create($value->Date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($value->Date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($value->Date),"d");
	            }
	        }

	        $html .= $audmonth . " " . substr($auddate,2) . ", " . $audyear;


	        $html .= '</li></ul></td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td><b>Names of Inspectors invloved in previous inspection</b>';

	        $html .='<ul type="square">';
	        foreach ($inspection_date[1]['inspectors'] as $key => $value) {
	            $html .='<li>'.$value->inspector.'</li>';
	        }
	        $html .='</ul>';

	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td width="100"></td>';
	        foreach ($inspectors as $key => $value) {
	            $html .='   <td><ul type="square"><li>' . $value->name .'</li></ul></td>';
	        }
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td><b>Major change/s since the previous inspection</b></td>';
	        $html .='</tr>';
	        foreach ($inspection_changes as $key => $value) {
	            $html .='<tr>';
	            $html .='   <td width="100"></td>';
	            $html .='   <td><ul type="square"><li>' . $value->changes . '</li></ul></td>';
	            $html .='</tr>';
	        }
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>BRIEF REPORT OF THE AUDIT ACTIVITIES UNDERTAKEN</b></td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2"><b>Scope :</b></td>';
	        $html .='</tr>';
	        
	        foreach ($scope as $key => $value) {
	            $html .='<tr>';
	            $html .='   <td colspan="2">';
	            $html .='<u>'.$value->scope_name.'</u> '.$value->scope_detail.' for the following products:';
	            $scope_product = $this->Audit_report_model->get_scope_product_by_scope($id,$value->audit_scope_id);
	            $html .='   <ul type="square">';
	            foreach ($scope_product as $key => $value) {
	               $html .='    <li> '.$value->product_name.' </li>';
	            }
	            $html .='   </ul>';
	            $html .='</td>';
	            $html .='</tr>';
	            $html .='<tr><td colspan="2"></td></tr>';
	        }
	        
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>Audited Area(s) :</b></td>';
	        $html .='</tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">' . $report[0]->audited_areas . '</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">The audit did not cover the following areas, to be considered during the next audit.</td>';
	        $html .='</tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">' . $report[0]->areas_to_consider . '</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

	        

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>PERSONNEL MET DURING THE AUDIT:</b></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">Issues and audit observations were discussed during the wrap-up meeting held on';
	        // if($present_during_meeting[0]->update_date != "") { 
	        //     $html .= date_format(date_create($present_during_meeting[0]->update_date),"d F, Y"); 
	        // } else { 
	        //     $html .= ''; 
	        // } 
	        $html .='. The audit report will focus on the observations that were discussed during the audit.</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">Present during the close-out meeting :</td>';
	        $html .='</tr>';
	        
	        $present_count = 0;
	        foreach ($present_during_meeting as $key => $value) {
	            $present_count++;
	            $html .='<tr>';
	            $html .='   <td colspan="1">';
	            $html .='&nbsp; &nbsp; &nbsp;'. $present_count . '. &nbsp; &nbsp; &nbsp; ' . $value->name;
	            $html .='   </td>';
	            $html .='   <td colspan="1">';
	            $html .=        $value->position;
	            $html .='   </td>';
	            $html .='</tr>';
	        }


	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">Other personnel met during the inspection:</td>';
	        $html .='</tr>';
	        $present_count = 0;
	        foreach ($get_personel_met as $key => $value) {
	            $present_count++;
	            $html .='<tr>';
	            $html .='   <td colspan="1">';
	            $html .='&nbsp; &nbsp; &nbsp;'. $present_count . '. &nbsp; &nbsp; &nbsp; ' . $value->name;
	            $html .='   </td>';
	            $html .='   <td colspan="1">';
	            $html .=        $value->designation;
	            $html .='   </td>';
	            $html .='</tr>';
	        }
	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'><b>AUDIT TEAM'S FINDINGS AND OBSERVATIONS RELEVANT TO THE AUDIT</b></td>";
	        $html .='</tr>';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'>The audit consisted of an in-depth review of quality and GMP elements including, nut not limited to the following:</td>";
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .="   <td colspan='2'>";
	        $html .="       <ul type='square'>";
	        foreach ($observation_yes as $value) {
	            $html .= "<li><b>" . $value->element_name. "</b> - " ;

	            $questions = $this->Template_model->get_data_by_id($value->element_id, 'element_id', 'tbl_questions');
	            foreach ($questions as $question) {
	                $answers_result = $this->Template_model->get_data_question_answer($question->question_id, $id);
	                foreach ($answers_result as $key => $answer) {
	                    switch ($answer->answer_id) {
	                        case 1: //YES
	                            $html .= $question->default_yes . ', ' . $answer->answer_details . '. ';
	                            break;
	                        case 2: //NO
	                            $html .= $question->default_yes . '. ';
	                            break;
	                        case 3: //NA
	                            $html .= 'Not Applicable.';
	                            break;
	                        case 4: //NC
	                            $html .= 'Not Covered.';
	                            break;
	                    }
	                }
	            }

	            $html .= "</li>";
	        }
	        $html .="       </ul>";
	        $html .="   </td>";
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'><b>DEFINITION / CATEGORIZATION OF AUDIT OBSERVATIONS</b></td>";
	        $html .='</tr>';
	        // $html .='<tr><td colspan="2"></td></tr>';

	        foreach ($observation as $key => $value) {
	           if($value->category_name == "Critical"){
	                $html .='<tr nobr="true">';
	                $html .="   <td colspan='2'><u>Critical Observation</u></td>";
	                $html .='</tr>';
	                $html .='<tr>';
	                $html .="   <td colspan='2'><i>" . $value->description . "</i></td>";
	                $html .='</tr>';
	                $html .="<tr><td colspan='2'></td></tr>";
	           }

	           if($value->category_name == "Major"){
	                $html .='<tr nobr="true">';
	                $html .="   <td colspan='2'><u>Major Observation</u></td>";
	                $html .='</tr>';
	                $html .='<tr>';
	                $html .="   <td colspan='2'><i>" . $value->description . "</i></td>";
	                $html .='</tr>';
	                $html .="<tr><td colspan='2'></td></tr>";
	           }

	           if($value->category_name == "Minor"){
	                $html .='<tr>';
	                $html .="   <td colspan='2'><u>Minor Observation</u></td>";
	                $html .='</tr>';
	                $html .='<tr>';
	                $html .="   <td colspan='2'><i>" . $value->description . "</i></td>";
	                $html .='</tr>';
	                $html .="<tr><td colspan='2'></td></tr>";
	           }
	        }
	        $html .="<tr><td colspan='2'></td></tr>";
	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'><b>LISTING OF AUDIT OBSERVATIONS AND CONCERNS</b>(in decreasing order of criticality)</td>";
	        $html .='</tr>';
	        $html .="<tr><td colspan='2'></td></tr>";

	        $ctrx = 1;
	        $critical = null;
	        $major = null;
	        $minor = null;
	        foreach ($element as $key => $value) {
	            $no_answer = $this->Global_model->get_no_answer($id,$value->element_id);
	            if(!empty($no_answer)){
	                foreach ($no_answer as $key => $value) {

	                    switch ($value->category_name) {
	                        case 'Critical':
	                            $critical .= $ctrx . ",";
	                            break;
	                        
	                        case 'Major':
	                            $major .= $ctrx . ",";
	                            break;
	                        
	                        case 'Minor':
	                            $minor .= $ctrx . ",";
	                            break;
	                    
	                    }
	                    $ctrx++;
	                }
	            }
	        }

	        $html .="<tr nobr='true'><td><b>Critical Observations - </b>";
	        if($critical == null) {
	            $html .= "<i>None</i>";
	        } else {
	            $html .= "<i>Please refer to item no(s) " . $critical . "</i>";
	        }
	        $html .="</td></tr>";

	        $html .="<tr nobr='true'><td><b>Major Observations - </b>";
	        if($major == null) {
	            $html .= "<i>None</i>";
	        } else {
	            $html .= "<i>Please refer to item no(s) " . $major . "</i>";
	        }
	        $html .="</td></tr>";

	        $html .="<tr nobr='true'><td><b>Minor Observations - </b>";
	        if($minor == null) {
	            $html .= "<i>None</i>";
	        } else {
	            $html .= "<i>Please refer to item no(s) " . $minor . "</i>";
	        }
	        $html .="</td></tr>";
	        $html .="<tr><td colspan='2'></td></tr>";
	        $html .="<hr>";
	        $html .="<tr><td colspan='2'></td></tr>";
	        $html .='<tr>';
	        $html .="   <td colspan='2'>";
	        $html .="       <ul>";
	        foreach ($element as $key => $value) {
	            $no_answer = $this->Global_model->get_no_answer($id,$value->element_id);
	            if(empty($no_answer)){
	                $html .='<li><b>'. $value->element_name .' - </b> No observations </li>';
	            }
	        }
	        $html .="       </ul>";
	        $html .="   </td>";
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>SUMMARY AND RECOMMENDATION</b></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">There were';
	        if($answers[0]->critical < 1) { 
	            $html .=' <b><u>No</u></b> Critical, ';
	        } else { 
	            $html .= ' <b><u>' . $this->convertNumberToWord($answers[0]->critical) . '[' . $answers[0]->critical . ']</u></b> Critical';
	        }
	        if($answers[0]->major < 1) { 
	            $html .=' <b><u>No</u></b> Major, ';
	        } else { 
	            $html .= ' <b><u>' . $this->convertNumberToWord($answers[0]->major) . '[' . $answers[0]->major . '] </u></b> Major';
	        }
	        if($answers[0]->minor < 1) { 
	            $html .=' and <b><u>No</u></b> Minor, ';
	        } else { 
	            $html .= ' and <b><u>' . $this->convertNumberToWord($answers[0]->minor) . '[' . $answers[0]->minor . ']</u></b> Minor ';
	        }
	        $html .= 'observations noted at this facility during the inspection.</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        if(count($recommendation) > 0){
	            $html .='<tr nobr="true">';
	            $html .='   <td colspan="2">We recommend that all the findings noted should be addressed giving utmost emphasis on the significant [Critical, Major] observations which can be grouped into the following Quality System and/or GMP elements:</td>';
	            $html .='</tr>';
	        }
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">';
	        $html .='       <ul type="square">';
	        foreach ($recommendation as $key => $value) {
	            $html .='   <li><b>' . $value->element_name .'</b> - ' . $value->recommendation. '</li>';
	        }
	        $html .='       </ul>';
	        $html .='   </td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='</div>';


	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>CONCLUSION</b></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">Based on the results of this assessment, ' . $report[0]->name . ' is at this time considered:</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100">Disposition : </td>';
	        $html .='   <td width="500">';
	         foreach ($disposition as $key => $value) {
	            $html .='<span><b>'.$value->disposition_name.'</b> ' . $value->disposition_label . '</span>';
	            $html .='       <ul type="square">';
	            $scope_product = $this->Audit_report_model->get_disposistion_product($id,$value->disposition_id);
	            foreach ($scope_product as $key => $value) {
	                $html .='        <li> '. $this->Audit_report_model->get_productname($value->audit_scope_id).' </li>';
	             }             
	             $html .='       </ul>';
	            
	        }
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='</div>';

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>OTHER ISSUES :</b></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td>' . $report[0]->other_issues_audit . ' </td>';
	        $html .='</tr>';
	        $html .='<tr><td></td></tr>';
	        $html .='<tr><td></td></tr>';
	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div style="margin-bottom: 50px;">';
	        $html .='<br />';

	        $html .='<div nobr="true">';
	        $html .='<table style="border: 1px solid black;">';
	        $html .='   <tr style="border: 1px solid black;">';
	        $html .='       <td style="text-align: center; border: 1px solid black;"><b>Name</b></td>';
	        $html .='       <td style="text-align: center; border: 1px solid black;"><b>Department / Designation</b></td>';
	        $html .='       <td style="text-align: center; border: 1px solid black;"><b>Date / Time</b></td>';
	        $html .='   </tr>';
	        $html .='   <tr><td style="border: 1px solid black;" colspan="3"><b>Prepaired By</b></td></tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;">';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$report2[0]->auditor.'<br></span></td>';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$report2[0]->designation.', '.$report2[0]->department.'<br></span></td>';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        $html .='   </tr>';
	        foreach ($co_auditor as $key => $value) {
	            $html .='<tr style="padding: 20px;border: 1px solid black;">';
	            $html .='   <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$value->fname. ' ' .$value->lname.'<br></span></td>';
	            $html .='   <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>' . $value->company . '<br></span></td>';
	            $html .='   <td style="border: 1px solid black; text-align: center; "></td>';
	            $html .='</tr>';
	        }
	        $html .='   <tr style="padding: 20px;border: 1px solid black;"><td style="border: 1px solid black;" colspan="3"><b>Reviewed By</b></td></tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;">';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $reviewer_info[0]->fname . ' ' .  $reviewer_info[0]->lname . '<br></span></td>';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$reviewer_info[0]->designation. ', ' . $reviewer_info[0]->company . '<br></span></td>';
	        if(isset($signature_stamp[0]->approved_date)) { 
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->review_date)) . '<br>' . date("H:i",strtotime($signature_stamp[0]->review_date)) .'H<br></span></td>';
	        } else {
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        }
	        $html .='   </tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;"><td style="border: 1px solid black;" colspan="3"><b>Approved By</b></td></tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;">';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '<br></span></td>';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$approver[0]->designation. ', ' . $approver[0]->company . '<br></span></td>';
	        if(isset($signature_stamp[0]->approved_date)) { 
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->approved_date)). '<br>' . date("H:i",strtotime($signature_stamp[0]->review_date)) .'H<br></span></td>';
	        } else {
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        }
	        $html .='   </tr>';
	        $html .='</table>';
	        $html .='</div>';


	        $pdf->writeHTML($html,true,false,true,false,'');
	        ob_clean();
	        $pdf->Output(__DIR__.'./../../json/export/approved/' . $id .'/Audit_Report.pdf', 'F');
	        $pdf->Output(__DIR__.'./../../json/export/archive/' . $id .'/Audit_Report.pdf', 'F');

	        // echo $html;


	    }	


	    function convertNumberToWord($num = false)
	    {
	        $num = str_replace(array(',', ' '), '' , trim($num));
	        if(! $num) {
	            return false;
	        }
	        $num = (int) $num;
	        $words = array();
	        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
	            'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
	        );
	        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
	        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
	            'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
	            'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
	        );
	        $num_length = strlen($num);
	        $levels = (int) (($num_length + 2) / 3);
	        $max_length = $levels * 3;
	        $num = substr('00' . $num, -$max_length);
	        $num_levels = str_split($num, 3);
	        for ($i = 0; $i < count($num_levels); $i++) {
	            $levels--;
	            $hundreds = (int) ($num_levels[$i] / 100);
	            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
	            $tens = (int) ($num_levels[$i] % 100);
	            $singles = '';
	            if ( $tens < 20 ) {
	                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
	            } else {
	                $tens = (int)($tens / 10);
	                $tens = ' ' . $list2[$tens] . ' ';
	                $singles = (int) ($num_levels[$i] % 10);
	                $singles = ' ' . $list1[$singles] . ' ';
	            }
	            $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
	        } //end for loop
	        $commas = count($words);
	        if ($commas > 1) {
	            $commas = $commas - 1;
	        }
	        return implode(' ', $words) ;
	    }


	}


