<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Preview_report_model extends CI_Model{
	function __construct() {
        parent::__construct();
    }
    function get_report_summary($id){
    	$query = $this->db->query("SELECT a.*,CONCAT(DATE_FORMAT(a.audit_date_1,'%b'),'. ',DAY(a.audit_date_1),'-',DAY(a.audit_date_2),', ',YEAR(a.audit_date_2)) as audit_date,CONCAT(DATE_FORMAT(a.p_inspection_date_1,'%b'),'. ',DAY(a.p_inspection_date_1),'-',DAY(a.p_inspection_date_2),', ',YEAR(a.p_inspection_date_2)) as previous_inspection, DATE_FORMAT(a.wrap_up_date,'%d %M %Y') as wrap_up_date, DATE_FORMAT(a.closure_date,'%d %M %Y') as closure_date, b.name,b.address1,b.address2,b.address3,b.country,b.background,c.fname,c.mname,c.lname,c.designation,c.department,c.company FROM tbl_report_summary as a 
						    		LEFT JOIN tbl_company as b ON a.company_id = b.company_id 
						    		LEFT JOIN tbl_auditor_info as c ON a.auditor_id = c.auditor_id
						    		WHERE a.report_id = ".$id);
    	$result = $query->result();
    	return $result;
    }
    function get_co_auditors($report_id){
    	$query = $this->db->query("SELECT c.fname,c.mname,c.lname,c.designation,c.department,c.company FROM tbl_co_auditors as a 
    								LEFT JOIN tbl_auditor_info as c ON a.auditor_id = c.auditor_id WHERE a.report_id = ".$report_id . " AND a.auditor_id <> 0"); 
    	$result = $query->result();
    	return $result;
    }

    function get_translator($report_id){
        $query = $this->db->query("SELECT * FROM tbl_report_translator WHERE report_id = ".$report_id); 
        $result = $query->result();
        return $result;
    }

    function get_approvers($report_id){
    	$query = $this->db->query("SELECT c.fname,c.mname,c.lname,c.designation,c.department,c.company FROM tbl_approver as a 
    								LEFT JOIN tbl_approver_info as c ON a.approver_id = c.approver_id WHERE a.report_id = ".$report_id); 
    	$result = $query->result();
    	return $result;
    }
    function get_reviewer($report_id){
    	$query = $this->db->query("SELECT c.fname,c.mname,c.lname,c.designation,c.department,c.company FROM tbl_reviewer as a 
    								LEFT JOIN tbl_reviewer_info as c ON a.reviewer_id = c.reviewer_id WHERE a.report_id = ".$report_id); 
    	$result = $query->result();
    	return $result;
    }
    function get_inspectors($report_id){
    	$query = $this->db->query("SELECT c.* FROM tbl_inspector as c WHERE c.report_id = ".$report_id); 
    	$result = $query->result();
    	return $result;
    }
    function get_inspection_changes($report_id){
    	$query = $this->db->query("SELECT * FROM tbl_inspection_changes WHERE report_id = ".$report_id); 
    	$result = $query->result();
    	return $result;
    }
    function get_activities($report_id){
    	$query = $this->db->query("SELECT DISTINCT(ra.activity_id), a.activity_name FROM tbl_report_activities as ra 
    								LEFT JOIN tbl_activities as a ON a.activity_id = ra.activity_id WHERE ra.report_id = ".$report_id);
    	$result = $query->result();
    	return $result;
    }
    function get_scope_audit($report_id){
    	$query = $this->db->query("SELECT s.*, ras.scope_detail FROM tbl_report_audit_scope as ras 
    								LEFT JOIN tbl_audit_scope as s ON s.scope_id = ras.audit_scope_id WHERE ras.report_id = ".$report_id);
    	$result = $query->result();
    	return $result;
    }
    function get_scope_product($report_id,$scope_id){
    	$query = $this->db->query("SELECT p.* FROM tbl_report_scope_product as rsp
    								LEFT JOIN tbl_product as p ON p.product_id = rsp.product_id WHERE rsp.report_id = ".$report_id." AND rsp.audit_scope_id = ".$scope_id);
    	$result = $query->result();
    	return $result;
    }
    function get_license($report_id){
    	$query = $this->db->query("SELECT * FROM tbl_references WHERE report_id = ".$report_id); 
    	$result = $query->result();
    	return $result;
    }
    function get_pre_document($report_id){
    	$query = $this->db->query("SELECT * FROM tbl_pre_audit_documents WHERE report_id = ".$report_id); 
    	$result = $query->result();
    	return $result;
    }
    function get_personel_met($report_id){
    	$query = $this->db->query("SELECT * FROM tbl_personnel WHERE report_id = ".$report_id); 
    	$result = $query->result();
    	return $result;
    }
    function get_present_during_meeting($report_id){
        $query = $this->db->query("SELECT * FROM tbl_present_during_meeting WHERE report_id = ".$report_id); 
        $result = $query->result();
        return $result;
    }
    function get_distribution($report_id){
    	$query = $this->db->query("SELECT d.* FROM tbl_report_distribution as rd
    								LEFT JOIN tbl_distribution as d ON d.distribution_id = rd.distribution_id WHERE rd.report_id = ".$report_id."");
    	$result = $query->result();
    	return $result;
    }
    function get_disposition($report_id){
    	// $query = $this->db->query("SELECT s.scope_name,d.disposition_name FROM tbl_report_scope_disposition as a LEFT JOIN tbl_audit_scope as s ON a.audit_scope_id = s.scope_id LEFT JOIN tbl_disposition as d on a.disposition_id = d.disposition_id WHERE report_id = ".$report_id);
        $query = $this->db->query("SELECT * FROM qv_report_scope_product_disposition WHERE report_id =" . $report_id . " GROUP BY disposition_id");
    	$result = $query->result();
    	return $result;
    }

     function get_disposition_product($report_id, $disposition){
        // $query = $this->db->query("SELECT s.scope_name,d.disposition_name FROM tbl_report_scope_disposition as a LEFT JOIN tbl_audit_scope as s ON a.audit_scope_id = s.scope_id LEFT JOIN tbl_disposition as d on a.disposition_id = d.disposition_id WHERE report_id = ".$report_id);
        $query = $this->db->query("SELECT * FROM qv_report_scope_product_disposition WHERE report_id =" . $report_id . " AND disposition_id =" . $disposition);
        $result = $query->result();
        return $result;
    }
    function get_template($report_id){
        $query = $this->db->query("SELECT a.template_id,c.standard_name,d.classification_name FROM tbl_report_summary as a
                                    LEFT JOIN tbl_template as b ON a.template_id = b.template_id
                                    LEFT JOIN tbl_standard_reference as c ON b.standard_id = c.standard_id
                                    LEFT JOIN tbl_classification as d ON b.classification_id = d.classification_id
                                    WHERE a.report_id = ".$report_id."");
        $result = $query->result();
        return $result;
    } 
    function get_element($template_id){
        $query = $this->db->query("SELECT DISTINCT(a.element_id),b.element_name, b.order FROM tbl_questions as a
                                    LEFT JOIN tbl_elements as b ON a.element_id = b.element_id
                                    WHERE a.template_id = ".$template_id."  ORDER BY b.order ASC");
        $result = $query->result();
        return $result;
    }
    function get_report_answer($report_id,$element_id){
        $query = $this->db->query("SELECT a.answer_details,b.question,c.answer_name,d.category_name FROM tbl_report_answers as a 
                                    LEFT JOIN tbl_questions as b ON a.question_id = b.question_id
                                    LEFT JOIN tbl_question_answer as c ON a.answer_id = c.answer_id
                                    LEFT JOIN tbl_question_category as d ON a.category_id = d.category_id
                                    WHERE a.report_id = ".$report_id." AND b.element_id = ".$element_id." GROUP BY b.question_id ORDER BY b.order_sort ASC");
        $result = $query->result();
        return $result;
    }

    function get_element_reco($report_id){
        $query = $this->db->query("SELECT a.recommendation,b.element_name,b.order FROM tbl_report_element_recommendation as a
                                    LEFT JOIN tbl_elements as b ON a.element_id = b.element_id WHERE a.report_id = ".$report_id." ORDER BY b.order ASC");
        $result = $query->result();
        return $result;
    }

    function get_remarks($reportid){
        $this->db->select("*");
        $this->db->from("tbl_report_submission_remarks");
        $this->db->where("report_id",$reportid);
        $this->db->order_by("id", "asc");
        $q = $this->db->get();
        return $q->result();
    }
    function get_man_info($table,$query){
        $this->db->select("*"); 
        $this->db->from($table);
        $this->db->where($query);
        $q = $this->db->get()->result();
        return $q;
    }

    function get_audit_observation($reportid){
        $this->db->select("*");
        $this->db->from("qv_get_audit_observation");
        $this->db->where("report_id",$reportid);
        $q = $this->db->get()->result();
        return $q;
    }

    function get_list($table,$report_id, $sort = null){
        $this->db->select("audit_date");
        $this->db->from($table);
        $this->db->where("report_id",$report_id);
        if($sort != null){
            $this->db->order_by($sort,"asc");
        }
        $this->db->group_by("audit_date");
        $q = $this->db->get()->result();
        foreach ($q as $key => $value) {
            $array[] = array("Date"=>$value->audit_date);
        }
        return $array;
    }

    function get_sub_activities($report_id, $activityid){
        $this->db->select("sub_item_name");
        $this->db->from("qv_report_sub_activity");
        $this->db->where("report_id",$report_id);
        $this->db->where("activity_id",$activityid);
        $q = $this->db->get()->result();
        return $q;
    }

    function get_report_scope_product($report_id){
        $this->db->select("*");
        $this->db->from("qv_report_scope");
        $this->db->where("report_id",$report_id);
        $q = $this->db->get()->result();
        return $q;
    }

     function get_products($scope, $report_id){
        $this->db->select("*");
        $this->db->from("qv_report_scope_product");
        $this->db->where("report_id",$report_id);
        $this->db->where("audit_scope_id",$scope);
        $q = $this->db->get()->result();
        return $q;
    }

    function get_observation_yes_group($report_id){
        $this->db->select("*");
        $this->db->from("qv_observation_yes");
        $this->db->where("report_id",$report_id);
        $this->db->group_by("element_id");
        $q = $this->db->get()->result();
        return $q;
    }

    function get_observation_yes($report_id, $elementid){
        $this->db->select("*");
        $this->db->from("qv_observation_yes");
        $this->db->where("report_id",$report_id);
        $this->db->where("element_id",$elementid);
        $q = $this->db->get()->result();
        return $q;
    }

    function preview_report_get_element($template_id){
       
        $this->db->select("*");
        $this->db->from("report_preview_elements");
        $this->db->where("template_id",$template_id);
        $this->db->group_by("element_id");
        $this->db->order_by("order","asc");
        $elements = $this->db->get()->result();
        return $elements;
    }

    function get_questions_answers($report_id, $element_id, $question_id){
        $this->db->select("*");
        $this->db->from("report_preview_question_answer");
        $this->db->where("report_id",$report_id);
        $this->db->where("element_id",$element_id);
        $this->db->where("question_id",$question_id);
        $questions = $this->db->get()->result();
        return $questions;
    }

    function check_stamp($report_id)
    {   
        $this->db->select("*");
        $this->db->where("report_id", $report_id);
        $this->db->from("tbl_report_signature_stamp");
        $q = $this->db->get()->result();

        return $q;
    }

    function reset_stamp($report_id)
    {
        $this->db->where("report_id",$report_id);
        $this->db->delete("tbl_report_signature_stamp");
    }



}
?>