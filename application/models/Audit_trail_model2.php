<?php 

defined('BASEPATH') OR exit('No direct script access allowed');



class Audit_trail_model2 extends CI_Model{

	function __construct() {

        parent::__construct();

    }



	public function save_data($table, $data) { 

		$this->db->insert($table, $data); 

		$id = $this->db->insert_id();

			return $id;

	}



	// function Audit_trail_list($limit,$offset){

	// 	$query = $this->db->query("SELECT a.* FROM tbl_audit_trail as a ORDER BY a.date DESC LIMIT ".$offset.", ".$limit."");

	// 	$result = $query->result();

	// 	return $result;

 //    }
    public function get_audit_trail_list($query = null, $limit, $offset){



        $this->db->limit($limit,$offset);

        $this->db->select("*");
      
        $this->db->from("tbl_audit_trail");



        if($query != null) {

            $this->db->where($query);

        }

     
        $this->db->group_by("id");

        $this->db->order_by("date", "desc");

        $q = $this->db->get();

        return $q->result();

    }

}



?>