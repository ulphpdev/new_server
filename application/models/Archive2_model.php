<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Manila');



class Archive2_model extends CI_Model{ 



	function __construct() {

        parent::__construct();

        $this->load->helper('array');

    }



	// function getlist_archive_template($table,$table1,$limit,$offset,$sort,$txt_search){

	// 	if($txt_search == ''){

	// 		$query = $this->db->query("SELECT a.*, b.classification_name, c.standard_name FROM tbl_template as a 

	// 									LEFT JOIN tbl_classification as b ON a.classification_id = b.classification_id JOIN tbl_standard_reference as c ON a.standard_id = c.standard_id 

	// 									WHERE a.status = '3' ORDER BY ".$sort." DESC LIMIT ".$offset.",".$limit."");		

	// 	}else{

	// 		$query = $this->db->query("SELECT a.*, b.classification_name, c.standard_name FROM tbl_template as a 

	// 									LEFT JOIN tbl_classification as b ON a.classification_id = b.classification_id JOIN tbl_standard_reference as c ON a.standard_id = c.standard_id 

	// 									WHERE (a.status = '3') AND (b.classification_name LIKE '%".$txt_search."%' OR c.standard_name LIKE '%".$txt_search."%') ORDER BY ".$sort." DESC LIMIT ".$offset.",".$limit."");		

	// 	}

	// 	$result = $query->result();

	//     return $result;

	// }



	function get_report_list_archive($limit,$offset){

        $query = $this->db->query("SELECT DISTINCT(a.report_id),a.template_id, a.report_no, a.status,CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date,b.name,

                                    CONCAT(c.fname,' ',c.lname) as auditor_name 

                                    FROM tbl_report_summary as a 

                                    LEFT JOIN tbl_company as b ON a.company_id = b.company_id 

                                    LEFT JOIN tbl_auditor_info as c ON a.auditor_id = c.auditor_id

                                    

                                    LEFT JOIN tbl_questions as d ON d.template_id = a.template_id

                                    -- LEFT JOIN tbl_elements as e ON e.element_id = d.element_id

                                    WHERE a.status = '3'

                                    LIMIT ".$offset.", ".$limit." ");

        $result = $query->result();

        return $result;

    }

    public function getlist_archive_template2($query = null, $limit, $offset){



        $this->db->limit($limit,$offset);

        $this->db->select("*");
       
        $this->db->from("archive_template");
 


	        if($query != null) {

	            $this->db->where($query);

	        }

	        $this->db->where("status = 3");
	        // $this->db->group_by("report_id");

	        $this->db->order_by("template_id", "desc");

	        $q = $this->db->get();

	        return $q->result();


}
	 public function get_pagination_archive_template($query = null){



        $this->db->select("*");

        $this->db->from("archive_template");

        if($query != null) {

            $this->db->where($query);

        }
        $this->db->where("status  = 3");
        // $this->db->group_by("report_id");

        $this->db->order_by("template_id", "desc");
        $q = $this->db->get();

        return $q->num_rows();



    }

}

?>