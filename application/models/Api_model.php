<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api_model extends CI_Model{
    function get_template_list(){   
	    $query = $this->db->query('SELECT * FROM tbl_template');
    	$result = $query->result();
    		$data_array = array();
	        foreach ($result as $key => $value) {
	           $data_array[]= array(
	           		'template_id' => $value->template_id,
	           		'standard_id' => $value->standard_id,
	           		'classification_id' => $value->classification_id,
	           		'modified_date' => $value->update_date,
	           		'status' => $value->status,
	           	);
	        }
	        $data['template_list'] = $data_array;
	    return $data;
    }
    function get_template_info($id){
    	$query = $this->db->query('SELECT t.*,c.classification_name,s.standard_name, a.activity_id FROM tbl_template as t 
								LEFT JOIN tbl_template_activities as a ON t.template_id = a.template_id
								LEFT JOIN tbl_classification as c ON c.classification_id = t.classification_id
								LEFT JOIN tbl_standard_reference as s ON s.standard_id = t.standard_id
								WHERE activity_id !="" AND t.template_id = "'.$id.'"');
    	$result = $query->result();

    	if($result[0]->sub_version == 0){
        	$version = $result[0]->version;
        } else {
        	$version = $result[0]->version . "." . $result[0]->sub_version;
        }
	        
    	$data = array(           
           		'template_id' => $result[0]->template_id,
           		'standard_name' => $result[0]->standard_name,
           		'product_type' => $result[0]->classification_name,
           		'create_date' => $result[0]->create_date,
           		'update_date' => $result[0]->update_date,
           		'version' => $version,
        );	
    	$data['activities'] = array();
        foreach ($result as $key => $value) {
        	$query2 = $this->db->query('SELECT * FROM tbl_activities WHERE activity_id = "'.$value->activity_id.'"');
        	$result2 = $query2->result();
	        foreach ($result2 as $key => $value) {
                $query3 = $this->db->query("SELECT * FROM tbl_sub_activities WHERE activity_id = '".$value->activity_id."'
                    ");
                $result3 = $query3->result();
                $value->sub_activities = $result3;	
	            array_push($data['activities'] , $value);
	        }
        }
        $data['elements'] = array();
    	$query4 = $this->db->query('SELECT DISTINCT(e.element_id),e.element_name,e.create_date,e.update_date,e.order FROM tbl_questions as q LEFT JOIN tbl_elements as e ON q.element_id = e.element_id WHERE q.template_id = "'.$id.'" ORDER BY e.order ASC');
    	$result4 = $query4->result();
        foreach ($result4 as $key => $value) {
            $query5 = $this->db->query("SELECT question_id,question,default_yes,required_remarks,order_sort,create_date,update_date FROM tbl_questions WHERE element_id = '".$value->element_id."' ORDER BY order_sort ASC");
            $result5 = $query5->result();
            $value->questions = $result5;	
            array_push($data['elements'] , $value);
        }
	    return $data;
    }
    function get_site(){
    	$query = $this->db->query('SELECT * FROM tbl_company');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
	        	$this->db->limit(1);
	        	$this->db->select("tbl_audit_history.*");
	        	$this->db->from("tbl_audit_history");
	        	$this->db->where("tbl_audit_history.supplier_id =". $value->company_id ." AND tbl_audit_history.status = 1");
	        	$this->db->join("tbl_audit_history_dates","tbl_audit_history.id = tbl_audit_history_dates.history_id");
	        	$this->db->order_by("tbl_audit_history.update_date","DESC");
	        	$this->db->order_by("tbl_audit_history_dates.Date","DESC");
	        	$q_audit_history = $this->db->get();
	        	$q = $q_audit_history->result();
	        	$q_result = $q;
	        	$audit_history = array();
	        	foreach ($q_result as $key_ah => $value_ah) {

	        		$q_audit_date = $this->db->query('SELECT Date as inspection_date FROM tbl_audit_history_dates WHERE history_id ='. $value_ah->id);
	        		$q_inspector = $this->db->query('SELECT inspector FROM tbl_audit_history_inspectors WHERE history_id ='. $value_ah->id);
	        		$q_changes = $this->db->query('SELECT changes FROM tbl_audit_history_changes WHERE history_id ='. $value_ah->id);

	        		$audit_history[] = array(
	        			"major_changes"=>$q_changes->result(),
	        			"audit_dates"=>$q_audit_date->result(),
	        			"inspectors"=>$q_inspector->result()
	        		);
	        	}
	        	

	           	$data_array[]= array(
	           		'company_id' => $value->company_id,
	           		'company_name' => $value->name,
	           		'address1' => $value->address1,
	           		'address2' => $value->address2,
	           		'address3' => $value->address3,
	           		'country' => $value->country,
	           		'type' => $value->type,
	           		'background' => $value->background,
	           		'create_date' => $value->create_date,
	           		'update_date' => $value->update_date,
	           		'status' => $value->status,
	           		'audit_history' => $audit_history
	           	);

	           	unset($audit_history);
	        }
	        $data['company_list'] = $data_array;
	    return $data;
    }
    function get_site_history(){
        $query = $this->db->query('SELECT * FROM tbl_company');
        $result = $query->result();
            $data_array= array();
            foreach ($result as $key => $value) {
                $this->db->select("tbl_audit_history.*");
                $this->db->from("tbl_audit_history");
                $this->db->where("tbl_audit_history.supplier_id =". $value->company_id ." AND tbl_audit_history.status = 1");
                $this->db->join("tbl_audit_history_dates","tbl_audit_history.id = tbl_audit_history_dates.history_id");
                $this->db->order_by("tbl_audit_history_dates.Date","DESC");
                $this->db->group_by("tbl_audit_history.id");
                $q_audit_history = $this->db->get();
                $q = $q_audit_history->result();
                $q_result = $q;
                $audit_history = array();
                foreach ($q_result as $key_ah => $value_ah) {

                    $q_audit_date = $this->db->query('SELECT Date as inspection_date FROM tbl_audit_history_dates WHERE history_id ='. $value_ah->id);
                    $q_inspector = $this->db->query('SELECT inspector FROM tbl_audit_history_inspectors WHERE history_id ='. $value_ah->id);
                    $q_changes = $this->db->query('SELECT changes FROM tbl_audit_history_changes WHERE history_id ='. $value_ah->id);

                    $audit_history[] = array(
                        "major_changes"=>$q_changes->result(),
                        "audit_dates"=>$q_audit_date->result(),
                        "inspectors"=>$q_inspector->result()
                    );
                }
                

                $data_array[]= array(
                    'company_id' => $value->company_id,
                    'audit_history' => $audit_history
                );

                unset($audit_history);
            }
        $data['site_history'] = $data_array;
        return $data;
    }
    function get_approver(){
    	$query = $this->db->query('SELECT * FROM tbl_approver_info');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
	           $data_array[]= array(
	           		'approver_id' => $value->approver_id,
	           		'firstname' => $value->fname,
	           		'middlename' => $value->mname,
	           		'lastname' => $value->lname,
	           		'designation' => $value->designation,
	           		'company' => $value->company,
	           		'department' => $value->department,
	           		'create_date' => $value->create_date,
	           		'update_date' => $value->update_date,
	           		'status' => $value->status,
	           		'acting_auditor_id' => $value->acting_id,
	           		'email' => $value->email
	           	);
	        }
	        $data['approver_info'] = $data_array;
	    return $data;
    }
    function get_auditor(){
    	$query = $this->db->query('SELECT * FROM tbl_auditor_info');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
	           $data_array[]= array(
	           		'auditor_id' => $value->auditor_id,
	           		'firstname' => $value->fname,
	           		'middlename' => $value->mname,
	           		'lastname' => $value->lname,
	           		'designation' => $value->designation,
	           		'company' => $value->company,
	           		'department' => $value->department,
	           		'create_date' => $value->create_date,
	           		'update_date' => $value->update_date,
	           		'status' => $value->status,
	           		'email' => $value->email
	           	);
	        }
	        $data['auditor_info'] = $data_array;
	    return $data;
    }
    function get_reviewer(){
    	$query = $this->db->query('SELECT * FROM tbl_reviewer_info');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
	           $data_array[]= array(
	           		'reviewer_id' => $value->reviewer_id,
	           		'firstname' => $value->fname,
	           		'middlename' => $value->mname,
	           		'lastname' => $value->lname,
	           		'designation' => $value->designation,
	           		'company' => $value->company,
	           		'department' => $value->department,
	           		'create_date' => $value->create_date,
	           		'update_date' => $value->update_date,
	           		'status' => $value->status,
	           		'acting_auditor_id' => $value->acting_id,
	           		'email' => $value->email
	           	);
	        }
	        $data['reviewer_info'] = $data_array;
	    return $data;
    }
    function get_question_category(){
    	$query = $this->db->query('SELECT * FROM tbl_question_category');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
	           $data_array[]= array(
	           		'category_id' => $value->category_id,
	           		'category_name' => $value->category_name,
	           		'create_date' => $value->create_date,
	           		'status' => $value->status,
	           		'update_date' => $value->update_date,
	           	);
	        }
	        $data['category_list'] = $data_array;
	    return $data;
    }
    function get_product(){
    	$query = $this->db->query('SELECT * FROM tbl_product WHERE status = 1 OR status = -2');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
	           $data_array[]= array(
	           		'product_id' => $value->product_id,
	           		'company_id' => $value->company_id,
	           		'product_name' => $value->product_name,
	           		'create_date' => $value->create_date,
	           		'update_date' => $value->update_date,
	           		'status' => $value->status,
	           	);
	        }
	        $data['product_list'] = $data_array;
	    return $data;
    }
    function get_classification(){
    	$query = $this->db->query('SELECT * FROM tbl_classification');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
	        	$category_query = $this->db->query('SELECT category_id FROM tbl_classification_category WHERE classification_id = ' . $value->classification_id);
    			$category_result = $category_query->result();

	           	$data_array[]= array(
	           		'classification_id' => $value->classification_id,
	           		'classification_name' => $value->classification_name,
	           		'create_date' => $value->create_date,
	           		'update_date' => $value->update_date,
	           		'status' => $value->status,
	           		'category'=> $category_result
	           	);
	        }
	        $data['classification_list'] = $data_array;
	    return $data;
    }
    function get_standard_reference(){
    	$query = $this->db->query('SELECT * FROM tbl_standard_reference');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
	           $data_array[]= array(
	           		'standard_id' => $value->standard_id,
	           		'standard_name' => $value->standard_name,
	           		// 'classification_id' => $value->classification_id,
	           		'create_date' => $value->create_date,
	           		'update_date' => $value->update_date,
	           		'status' => $value->status,
	           	);
	        }
	        $data['standard_reference'] = $data_array;
	    return $data;
    }
    function get_type_audit(){
    	$query = $this->db->query('SELECT * FROM tbl_audit_scope');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
	           $data_array[]= array(
	           		'scope_id' => $value->scope_id,
	           		'scope_name' => $value->scope_name,
	           		'create_date' => $value->create_date,
	           		'update_date' => $value->update_date,
	           		'status' => $value->status,
	           	);
	        }
	        $data['type_audit'] = $data_array;
	    return $data;
    }
    function get_disposition(){
    	$query = $this->db->query('SELECT * FROM tbl_disposition');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
                if($value->disposition_label != ""){
                    $disposition = $value->disposition_name . "-" . $value->disposition_label;
                } else {
                    $disposition = $value->disposition_name;
                }
	           $data_array[]= array(
	           		'disposition_id' => $value->disposition_id,
                    'disposition_name' => $disposition,
	           		'create_date' => $value->create_date,
	           		'update_date' => $value->update_date,
	           		'status' => $value->status,
	           	);
	        }
	        $data['disposition'] = $data_array;
	    return $data;
    }
    function get_distribution(){
    	$query = $this->db->query('SELECT * FROM tbl_distribution');
    	$result = $query->result();
    		$data_array= array();
	        foreach ($result as $key => $value) {
	           $data_array[]= array(
	           		'distribution_id' => $value->distribution_id,
	           		'distribution_name' => $value->distribution_name,
	           		'create_date' => $value->create_date,
	           		'update_date' => $value->update_date,
	           		'status' => $value->status,
	           	);
	        }
	        $data['distribution'] = $data_array;
	    return $data;
    }
    function checkToken($token){
    	$query = $this->db->query('SELECT count(*) as count FROM tbl_token WHERE token = "'.$token.'"');
    	$result = $query->result();
    	return $result;
    }
    function save_data($data,$table){
    	$this->db->insert($table,$data);
    	if ($this->db->affected_rows() == '1')
	    {
	        return $this->db->insert_id();
	    }
    }
    function save_data2($data,$table){
    	$this->db->insert($table,$data);
    }
    public function update_data($field, $where, $table, $data) { 
		$this->db->where($field, $where);
		$this->db->update($table, $data); 
	}
	

	public function checkexist($field,$where,$table,$report_id){
		$query = $this->db->query("SELECT count(*) FROM ".$table." where ".$field." = ".$where." AND report_id = ".$report_id);
		return $query->result();		
	}

	public function get_audit_report_list(){
		$query = $this->db->query("SELECT report_id,report_no,status,date_modified as modified_date FROM tbl_report_listing WHERE status >= -1");
		return $query->result();
	}

    public function get_lasst_approval_history_date($id, $date_modified){
        $this->db->limit(1);
        $this->db->select("date");
        $this->db->from("tbl_report_submission_remarks");
        $this->db->where("report_id", $id);
        $this->db->order_by("date", "desc");
        $query = $this->db->get();
        $data = $query->result_array();

        if($query->num_rows() > 0){
            return $data[0]['date'];
        } else {
            return $date_modified;
        }
        
    }

	public function DMFilter($table, $fields, $query){
		$query = $this->db->query("SELECT $fields FROM $table WHERE $query");
		return $query->result();
	}

	public function get_report_summary($reportid){
		$this->db->limit(1);
		$this->db->select("*");
		$this->db->from("qv_join_views_1");
		$this->db->where("Report_ID",$reportid);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_report_summary_company($companyid){

		$this->db->select("*");
		$this->db->from("qv_audit_company");
		$this->db->where("company_id",$companyid);
		$this->db->group_by("company_id");
		$query = $this->db->get();
		return $query->result();
	}

	 

	function get_last_update($table, $sort){
		$this->db->limit(1);
		$this->db->select($sort);
		$this->db->from($table);
		$this->db->order_by($sort,"desc");
		$q = $this->db->get();
		$data = $q->result_array();
		if($q->num_rows() > 0 ){
			return $data[0][$sort];
		}
		
	}

	public function audit_report_json($table, $query){

		$this->db->select("*");
		$this->db->from($table);
		$this->db->where($query);
		$query = $this->db->get();
		return $query->result();
	}
	public function audit_report_json_filter($table, $query, $filter){

		$this->db->select($filter);
		$this->db->from($table);
		$this->db->where($query);
		$this->db->group_by($filter);
		$query = $this->db->get();
		return $query->result();
	}
	public function audit_report_json_select($table, $query, $filter){

		$this->db->select($filter);
		$this->db->from($table);
		$this->db->where($query);
		$query = $this->db->get();
		return $query->result();
	}

	public function check_report_id_exist($reportid){
		$this->db->select("");
		$this->db->where("report_id",$reportid);
		$this->db->from("tbl_report_summary");
		$q = $this->db->get();
		return $q->num_rows();
	}


	function get_for_export(){
    	$query = $this->db->query('SELECT * FROM tbl_report_summary WHERE status = 5 OR status = -1');
    	$result = $query->result();
	        foreach ($result as $key => $value) {
	        	if($value->status == 5){
					$status = "approved";
					$status_id = 1;
				} else {
					$status = "archive";
					$status_id = 0;
				}

	           $data_array[]= array(
	           		"report_id"=>$value->report_id,
					"status"=>$status_id,
					"links"=> array(
						"audit_report"=> base_url() . 'json/export/' . $status . '/' . $value->report_id . '/Audit_Report.pdf',
						"annexure"=> base_url() . 'json/export/' . $status . '/' . $value->report_id . '/Annexure.pdf',
						"executive_summary"=> base_url() . 'json/export/' . $status . '/' . $value->report_id . '/Executive_Report.pdf'
					)
	           	);
	        }
	    return $data_array;
    }

    function clear_record($table, $report_id)
    {
    	$this->db->where("report_id",$report_id);
    	$this->db->delete($table); 
    }

    function is_question_answered($report_id,$question_id)
    {
    	$this->db->where("report_id", $report_id);
    	$this->db->where("question_id", $question_id);
    	$this->db->from("tbl_report_answers");
    	$q = $this->db->get();
    	return $q->num_rows();
    }

    public function update_question_answer($report_id,$question_id, $data) 
    { 
		$this->db->where("report_id", $report_id);
    	$this->db->where("question_id", $question_id);
		$this->db->update("tbl_report_answers", $data); 
	}

	public function get_auditor_id_email($email)
	{
		$this->db->select("*");
		$this->db->from("tbl_auditor_info");
		$this->db->where("email",$email);
		$this->db->where("status",1);
		$q = $this->db->get();
		$data = $q->result_array();
		return $data[0]['auditor_id'];
	}

	public function check_coauditor_submission($report_id,$auditor_id)
	{
		$this->db->select("*");
		$this->db->from("tbl_report_coauditor_submission");
		$this->db->where("report_id",$report_id);
		$this->db->where("auditor_id",$auditor_id);
		$q = $this->db->get();
		return $q->num_rows();
	}


	public function update_coauditor_submission($data, $report_id = null, $auditor_id = null)
	{
		if($report_id == null and $auditor_id == null){
			//insert
			$this->db->insert("tbl_report_coauditor_submission", $data);
		} else {
			//update
			$this->db->where("report_id", $report_id);
    		$this->db->where("auditor_id", $auditor_id);
			$this->db->update("tbl_report_coauditor_submission", $data); 
		}
	}
}

