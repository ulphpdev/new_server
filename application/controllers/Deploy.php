
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Deploy extends CI_Controller{

	public function index()
	{
		$dir = "./";
		$output = array();
		chdir($dir);
		exec("git log",$output);
		
		$result = $this->parseLog($output);
		echo '<pre>',print_r($result,1),'</pre>';
	}

	function parseLog($log) {
	    $history = array();
	    foreach($log as $key => $line) {
	        if(strpos($line, 'commit') === 0 || $key + 1 == count($line)){
	            $commit['hash'] = substr($line, strlen('commit') + 1);
	        } else if(strpos($line, 'Author') === 0){
	            $commit['author'] = substr($line, strlen('Author:') + 1);
	        } else if(strpos($line, 'Date') === 0){
	            $commit['date'] = substr($line, strlen('Date:') + 3);
	        } elseif (strpos($line, 'Merge') === 0) {
	            $commit['merge'] = substr($line, strlen('Merge:') + 1);
	            $commit['merge'] = explode(' ', $commit['merge']);
	        } else if(!empty($line)){
	            $commit['message'] = substr($line, 4);
	            array_push($history, $commit);  
	            unset($commit);            
	        }
	    }
	    return $history;
	}

}


