<?php 
class Data_maintenance extends CI_Controller{
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('global_model');
		$this->load->model('Audit_trail_model');
		$this->load->helper('url');

		if($this->session->userdata('sess_email')=='' ) { 
			redirect(base_url("login"));
		} else {
			if($this->session->userdata('type')== 'approver'){
				redirect(base_url("login/unsetSession"));
			}
			if($this->session->userdata('type')== 'reviewer'){
				redirect(base_url("login/unsetSession"));
			}
		}
		
	}
	function auditor(){
		$data['content'] = 'auditor/list2';
		$this->load->view('layout/layout',$data);
	}
	function reviewer(){
		$data['content'] = 'reviewer/list2';
		$this->load->view('layout/layout',$data);
	}
	function approver(){
		$data['content'] = 'approver/list2';
		$this->load->view('layout/layout',$data);
	}
	function site(){
		$data['content'] = 'site/list2';
		$this->load->view('layout/layout',$data);	
	}
	function product(){
		$data['content'] = 'product/list2';
		$this->load->view('layout/layout',$data);
	}
	function category(){
		$data['content'] = 'category/list2';
		$this->load->view('layout/layout',$data);
	}
	function prodtype(){
		$data['content'] = 'prodtype/list2';
		$this->load->view('layout/layout',$data);	
	}
	function standard(){
		$data['content'] = 'standard/list2';
		$this->load->view('layout/layout',$data);	
	}
	function type_of_audit(){
		$data['content'] = 'type_of_audit/list2';
		$this->load->view('layout/layout',$data);	
	}
	function disposition(){
		$data['content'] = 'disposition/list2';
		$this->load->view('layout/layout',$data);
	}
	function distribution(){
		$data['content'] = 'distribution/list2';
		$this->load->view('layout/layout',$data);
	}
	function reference(){
		$data['content'] = 'reference/list';
		$this->load->view('layout/layout',$data);
	}


	public function get_country()
	{
		$selected_country = $_POST['country_id'];
		$result = $this->global_model->get_list_location("country_list", "country_name ASC");
		$html = "";
		$html .= "<option value='' selected disabled>Select..</option>";
		foreach ($result as $key => $value) {
			if($selected_country == $value->country_code){
				$html .= "<option value='".$value->country_code."' selected>" .$value->country_name. "</option>";
			} else {
				$html .= "<option value='".$value->country_code."'>" .$value->country_name. "</option>";
			}
			
		}

		echo $html;
	}

	public function get_province()
	{
		$result = $this->global_model->get_list_location("tbl_province", "province_name ASC", "country_id = '" . $_POST['country_id'] . "'");
		$html = "";
		if(count($result) > 0){
			$html .= '<select id="province" class = "inputcss form-control fullwidth inputs_site inputs_edit_site">';
			$html .= "<option value='' selected disabled>Select..</option>";
			foreach ($result as $key => $value) {
				$html .= "<option value='".$value->id."'>" .$value->province_name. "</option>";
			}
			$html .= "<option value='xx'>Other</option>";
			$html .= '</select>';
		} else {
			$html .= '<input class = "inputcss form-control fullwidth inputs_site inputs_edit_site" id="province" placeholder="Enter Province/Region Name". />';
        	$html .= '<span class = "er-msg">Province should not be empty *</span></div>';
		}
		
		echo $html;
	}

	public function get_city()
	{
		$result = $this->global_model->get_list_location("tbl_city", "city_name ASC", "province_id = '" . $_POST['province_id'] . "'");
		$html = "";
		if(count($result) > 0){
			$html .= '<select id="city" class = "inputcss form-control fullwidth inputs_site inputs_edit_site">';
			$html .= "<option value='' selected disabled>Select..</option>";
			foreach ($result as $key => $value) {
				$html .= "<option value='".$value->id."'>" .$value->city_name. "</option>";
			}
			$html .= "<option value='xx'>Other</option>";
			$html .= '</select>';
		} else {
			$html .= '<input class = "inputcss form-control fullwidth inputs_site inputs_edit_site" id="city" placeholder="Enter City Name". />';
        	$html .= '<span class = "er-msg">Province should not be empty *</span></div>';
		}
		
		echo $html;
	}

	public function save_company(){
	  	$table = $_POST['table'];
	  	$field = $_POST['field'];
	  	$data = array(
	    	'type' => strip_tags($_POST['type']),
	    	'name' => strip_tags($_POST['name']),
	    	'address1' => strip_tags($_POST['street']),
	    	'address2' => strip_tags($_POST['city']),
	    	'address3' => strip_tags($_POST['province']),
	    	'country' => strip_tags($_POST['country']),
	    	'background' => strip_tags($_POST['background']),
	    	'create_date' => date('Y-m-d H:i:s'),
	    	'update_date' => date('Y-m-d H:i:s'),
	  	);
	  	$data = json_decode(json_encode($data), FALSE);
	  	
	  	$province_id_input = $_POST['province_id'];

	  	//save province if 0
	  	if($_POST['province_id'] == 0){
	  		$province = array(
	  			"country_id"=>$_POST['country'],
	  			"province_name"=>strip_tags($_POST['province']),
	  		);
	  		$province_id_input = $this->global_model->save_data("tbl_province", $province);
	  	}

	  	//save city if 0
	  	if($_POST['city_id'] == 0){
	  		$city = array(
	  			"province_id"=>$province_id_input,
	  			"city_name"=>strip_tags($_POST['city']),
	  		);
	  		$this->global_model->save_data("tbl_city", $city);
	  	}



	  	echo $this->global_model->save_data($table, $data);
	}

	public function load_province()
	{
		$result = $this->global_model->get_list_location("tbl_province", "province_name ASC", "country_id = '" . $_POST['country_id'] . "'");
		$html = "";
		if(count($result) > 0){
			$html .= '<select id="province" class = "inputcss form-control fullwidth inputs_site inputs_edit_site">';
			foreach ($result as $key => $value) {
				if($_POST['province_name'] == $value->province_name){
					$html .= "<option value='".$value->id."' selected>" .$value->province_name. "</option>";
				} else {
					$html .= "<option value='".$value->id."'>" .$value->province_name. "</option>";
				}
				
			}
			$html .= "<option value='xx'>Other</option>";
			$html .= '</select>';
		} else {
			$html .= '<input class = "inputcss form-control fullwidth inputs_site inputs_edit_site" id="province" placeholder="Enter Province/Region Name". />';
        	$html .= '<span class = "er-msg">Province should not be empty *</span></div>';
		}
		
		echo $html;
	}

	public function load_city()
	{
		$province = $this->global_model->get_list_location("tbl_province", "province_name ASC", "province_name = '" . $_POST['province_name'] . "'");
		$result = $this->global_model->get_list_location("tbl_city", "city_name ASC", "province_id = '" . $province[0]->id . "'");
		$html = "";
		if(count($result) > 0){
			$html .= '<select id="city" class = "inputcss form-control fullwidth inputs_site inputs_edit_site">';
			foreach ($result as $key => $value) {
				if($_POST['city_name'] == $value->city_name){
					$html .= "<option value='".$value->id."' selected>" .$value->city_name. "</option>";
				} else {
					$html .= "<option value='".$value->id."'>" .$value->city_name. "</option>";
				}
				
			}
			$html .= "<option value='xx'>Other</option>";
			$html .= '</select>';
		} else {
			$html .= '<input class = "inputcss form-control fullwidth inputs_site inputs_edit_site" id="city" placeholder="Enter Province/Region Name". />';
        	$html .= '<span class = "er-msg">Province should not be empty *</span></div>';
		}
		
		echo $html;
	}

	public function update_company(){
	  	date_default_timezone_set('Asia/Taipei');
	  	$table = $_POST['table'];
	  	$field = $_POST['field'];
	  	$data = array(
	    	'company_id' => $_POST['id'],
	    	'type' => strip_tags($_POST['type']),
	    	'name' => strip_tags($_POST['name']),
	    	'address1' => strip_tags($_POST['street']),
	    	'address2' => strip_tags($_POST['city']),
	    	'address3' => strip_tags($_POST['province']),
	    	'country' => strip_tags($_POST['country']),
	    	'background' => strip_tags($_POST['background']),
	    	'update_date' => date('Y-m-d H:i:s')
	  	);


	  	$province_id_input = $_POST['province_id'];

	  	//save province if 0
	  	if($_POST['province_id'] == 0){
	  		$province = array(
	  			"country_id"=>$_POST['country'],
	  			"province_name"=>strip_tags($_POST['province']),
	  		);
	  		$province_id_input = $this->global_model->save_data("tbl_province", $province);
	  	}

	  	//save city if 0
	  	if($_POST['city_id'] == 0){
	  		$city = array(
	  			"province_id"=>$province_id_input,
	  			"city_name"=>strip_tags($_POST['city']),
	  		);
	  		$this->global_model->save_data("tbl_city", $city);
	  	}

	  	
	  	$this->global_model->update_data($field,  $_POST['id'], $table, $data);
	  	echo date('Y-m-d H:i:s');
	}


}
?>