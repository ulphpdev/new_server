<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Export extends CI_Controller {

		function __construct(){
			parent::__construct();
	        $this->load->model('Template_model');
	        $this->load->helper('url');
	        $this->load->model('Audit_report_model');
	        $this->load->model('Global_model');
	        $this->load->model('Audit_report_analysis_model');
	        $this->load->model('Preview_report_model');
			$this->load->model('q_auditreport');
			$this->load->model('Api_model');

	        $this->load->library('Phpword');
		}


		function index()
		{
			$report_id = $_GET['report_id'];

			if (!file_exists('./json/export/approved/' . $report_id)) {
			    mkdir('./json/export/approved/' . $report_id, 0777, true);
			}

			$this->audit_report($report_id);

		}

		function is_date( $str ) {
		    try {
		        $dt = new DateTime( trim($str) );
		    }
		    catch( Exception $e ) {
		        return false;
		    }
		    $month = $dt->format('m');
		    $day = $dt->format('d');
		    $year = $dt->format('Y');
		    if( checkdate($month, $day, $year) ) {
		        return true;
		    }
		    else {
		        return false;
		    }
		}


		function audit_report($id)
		{
			$string = file_get_contents("./listing/".$id.".json");
			$details = json_decode($string, false);

			$anastring = file_get_contents("./listing/".$id."_analysis.json");
			$analysis = json_decode($anastring, false);

			$report = $details->Report_Summary;
			$activities = $details->Report_activities;
			$audit_dates = $details->Audit_Dates;
			$lead_auditor = $details->Lead_Auditor;
			$co_auditor = $details->Co_Auditors;
			$translators = $details->Translator;
			$license_Accreditation = $details->License;
			$preaudit_documents = $details->Pre_Document;
			$stanard_reference = $details->Template;
			$inspection_date = $details->Inspection_Audit_Dates;
			$inspection_inspector = $details->Inspection_Inspector;
			$inspection_changes = $details->Inspection_Changes;
			$scope = $details->Scope_Product;
			$present_during_meeting = $details->Present_During_Meeting;
			$get_personel_met = $details->Personel_Met;
			$observation_findings = $details->Observation_Yes;
			$observation = $details->Audit_Observation;

			$signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
			$co_auditor = $this->Audit_report_model->get_data_by_auditor($report[0]->report_id, 'report_id', 'tbl_co_auditors');
			$reviewer_info = $this->Audit_report_model->get_reviewer($report[0]->report_id);
        	$approver = $this->Audit_report_model->get_approver($report[0]->report_id);
        	$country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');

			$params = array("report_no"=>$report[0]->report_no);

			$this->load->library("Pdf",$params);
	        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	        $pdf->SetTitle("Audit Report  " . $report[0]->report_no );
	        $pdf->setReportNo($report[0]->report_no);
	        $pdf->setFooterRight("Document No. OP-14101/F05");
	        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
	        $pdf->setFooterData(array(0,64,0), array(0,64,128));
	        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	        $pdf->SetMargins('20', '27', '20');
	        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	        $gothic = $pdf->AddFont('gothic');
	        $cgi = $pdf->AddFont('cgi');
	        $pdf->setListIndentWidth(20);
	        $pdf->AddPage();
	        $html = '';
	        $x = 'A';  
	        $html .= '<style> 
	                    * {
	                        font-size: 12px;
	                        font-family: gothic;
	                    }
	                </style>';
	        $html .='<div style="text-align:center"><center><label><h2>GMP AUDIT REPORT</h2><span style="margin-top:-80px;"><b>'.$report[0]->report_no.'</b></span></label></center></div>';
	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b>AUDITED SITE:</b> </td>';
	        $html .='<td width="30"></td>';
	        $html .='<td width="500"> '. $report[0]->name.' </td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td><td> '.$report[0]->address1. ', ' .$report[0]->address2. '</td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td><td> '.$report[0]->address3.', '. strtoupper($country[0]->country_name) .' </td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

////////////////////////////////////////////////

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b>ACTIVITIES CARRIED OUT BY THE COMPANY:</b></td>';
	        $html .='<td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<table style="margin-top: 20px;">';
	        foreach ($activities as $key => $value) { 
	        	$box = str_replace(base_url(),"",$value->activity_box);
	            $html .='<tr>';
	            $html .='<td style="width: 75%;">';
	            $html .= $value->activity_name;
	            $html .='</td>';
	            $html .='<td style="width: 5%;">';
	            $html .='<img style="position: fixed; right: 0;" src="'.$box.'" width="15">';
	            $html .='</td>';
	            $html .='</tr>';
	            foreach ($value->sub_activity as $k => $v) {
	            	$sub_box = str_replace(base_url(),"",$v->sub_box);
	                $html .='<tr>';
	                $html .='<td style="width: 75%;">';
	                $html .= ' &nbsp; &nbsp; &nbsp; ' . $v->sub_name;
	                $html .='</td>';
	                $html .='<td style="width: 5%;">';
	                $html .='<img style="position: fixed; right: 0;" src="'. $sub_box.'" width="15">';
	                $html .='</td>';
	                $html .='</tr>';
	            }
	            
	        }
	        if($report[0]->other_activities != "" || $report[0]->other_activities != null){
	            $html .='<tr>';
	            $html .='<td style="width: 75%;">Others : ' . $report[0]->other_activities . '</td>';
	            $html .='<td style="width: 5%;">';
	                $html .='<img style="position: fixed; right: 0;" src="asset/img/checkbox_block.jpg" width="15">';
	                $html .='</td>';
	            $html .='</tr>';
	        }
	        
	        $html .='</table>';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

/////////////////////////////////////

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b>AUDIT DATE:</b></td><td width="30"></td>';
	        $html .='<td width="500">';

	        $len = count($audit_dates);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($audit_dates as $key => $value) {
	            $audmonth = date_format(date_create($value->Date),"F");
	            $audyear = date_format(date_create($value->Date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($value->Date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($value->Date),"d");
	            }
	        }

	        $html .= substr($auddate,2) . " " . $audmonth . " " . $audyear;
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';


//////////////////////////////////////
	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b>AUDITOR/S:</b></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<table>';
	        $html .='<tbody>';
	        $html .='<tr>';
	        $html .='<td>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'</td><td>' .$lead_auditor[0]->company. '</td>';
	        $html .='</tr>';
	        foreach ($co_auditor as $key => $value) {
	            $html .='<tr>';
	            $html .='<td>'.$value->fname. ' ' .$value->lname.'</td><td>' .$value->company. '</td>';
	            $html .='</tr>';
	        }
	        $html .='<tr><td></td></tr>';
	        $html .='</tbody>';
	        $html .='</table>';
	        $html .= '<br/><b>Translator Usage: </b> (Name of employee, or approved translator, participating in this audit as determined by the Lead Auditor) : ';
	        $translators_name ="";
	        foreach ($translators as $key => $value) {
	            $translators_name .= ", " . $value->translator;
	        }
	        $html .= substr($translators_name, 1);
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

/////////////////////////////////////////////

	        $html .='<div nobr="true">';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b>REFERENCE:</b></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<b>License(s)/Accreditation(s)/Certification(s)</b> held by supplier and verified during the audit';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="130"></td>';
	        $html .='<td width="500">';
	        $html .='<ul type="square">';
	        foreach ($license_Accreditation as $key => $value) {
	            $html .= '<li>';
	            if($value->reference_name != ""){
	            	$html .= $value->reference_name . '<br />';
	            } else {
	            	$html .= 'None.<br />';
	            }

	            if($value->issuer != ""){
	            	$html .= $value->issuer . '<br />';
	            } else {
	            	$html .= 'None.<br />';
	            }

	            if($value->reference_no != ""){
	            	$html .= 'License / Certificate No .' . $value->reference_no . '<br />';
	            } else {
	            	$html .= 'License / Certificate No : None.<br />';
	            }

	            if($this->is_date($value->validity)){
	            	$html .= 'Validity : ' . date_format(date_create($value->validity),"d F Y") . '<br />';
	            } else {
	            	$html .= 'Validity : None. <br />';
	            }
	            
	            
	            $html .= '</li>';
	        }
	        $html .='</ul>';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<b>Pre-audit documents</b> provided and reviewed';
	        $html .='</td>';
	        $html .='</tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<ul type="square">';
	        foreach ($preaudit_documents as $key => $value) {
	            $html .= '<li>' . $value->document_name . '</li>';
	        }
	        $html .='</ul>';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<b>Regulatory and UNILAB Standards Used</b>';
	        $html .='</td>';
	        $html .='</tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<ul type="square">';
	        foreach ($stanard_reference as $key => $value) {
	            $html .= '<li>' . $value->standard_name . '</li>';
	        }
	        $html .='</ul>';
	        $html .='</td>';
	        $html .='</tr>';

	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';

////////////////////////////////////////////////////

	        $html .='<div nobr="true">';
	        $html .='<table nobr="true">';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>SUPPLIER BACKGROUND / HISTORY</b></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2"><span style="text-align:justify;">' . $report[0]->background . '</span></td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td width="500"><b>Date of previous inspection </b><span class="italic">(if applicable)</span></td>';
	        $html .='</tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td><ul type="square"><li>';

	        $len = count($inspection_date);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($inspection_date as $key => $value) {
	            $audmonth = date_format(date_create($value->Date),"F");
	            $audyear = date_format(date_create($value->Date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($value->Date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($value->Date),"d");
	            }
	        }

	        $html .= substr($auddate,2) . " " . $audmonth . ", " . $audyear;


	        $html .= '</li></ul></td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td><b>Names of Inspectors invloved in previous inspection</b> <span class="italic">(if applicable)</span>';

	        $html .='<ul type="square">';
	        foreach ($inspection_inspector as $key => $value) {
	            $html .='<li>'.$value->inspector.'</li>';
	        }
	        $html .='</ul>';

	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td><b>Major change/s since the previous inspection</b> <span class="italic">(if applicable)</span></td>';
	        $html .='</tr>';
	        if(count($inspection_changes) > 0 ){
	        	foreach ($inspection_changes as $key => $value) {
		            $html .='<tr>';
		            $html .='   <td width="100"></td>';
		            $html .='   <td><ul type="square"><li>' . $value->changes . '</li></ul></td>';
		            $html .='</tr>';
		        }
	        } else {
	        	$html .='<tr>';
	            $html .='   <td width="100"></td>';
	            $html .='   <td><ul type="square"><li>None</li></ul></td>';
	            $html .='</tr>';
	        }
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';


///////////////////////////////////////////////////////////////


	        $html .='<div nobr="true">';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>BRIEF REPORT OF THE AUDIT ACTIVITIES UNDERTAKEN</b></td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>SCOPE: </b></td>';
	        $html .='</tr>';
	        
	        foreach ($scope as $key => $value) {
	            $html .='<tr nobr="true">';
	            $html .='   <td colspan="2">';
	            $html .='<u>'.$value->scope.'</u> '.$value->scope_details.' for:';
	            $html .='   <ul type="square">';
	            foreach ($value->products as $key => $value) {
	               $html .='    <li> '.$value->product_name.' </li>';
	            }
	            $html .='   </ul>';
	            $html .='</td>';
	            $html .='</tr>';
	            $html .='<tr><td colspan="2"></td></tr>';
	        }
	        
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>AUDITED AREA(S): </b></td>';
	        $html .='</tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">' . $report[0]->audited_areas . '</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">The audit did not cover the following areas, to be considered during the next audit.</td>';
	        $html .='</tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">' . $report[0]->areas_to_consider . '</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';


///////////////////////////////////////////////////////////////

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>PERSONNEL MET DURING THE AUDIT:</b></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">Issues and audit observations were discussed during the wrap-up meeting held on ' . $report[0]->wrap_up_date;
	        $html .='. The audit report will focus on the observations that were discussed during the audit.</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">Present during the close-out meeting :</td>';
	        $html .='</tr>';
	        
	        $present_count = 0;
	        foreach ($present_during_meeting as $key => $value) {
	            $present_count++;
	            $html .='<tr>';
	            $html .='   <td colspan="1">';
	            $html .='&nbsp; &nbsp; &nbsp;'. $present_count . ') &nbsp; &nbsp; &nbsp; ' . $value->name;
	            $html .='   </td>';
	            $html .='   <td colspan="1">';
	            if($value->position != ""){
	            	$html .=        $value->position;
	            } else {
	            	$html .=        "None.";
	            }
	            
	            $html .='   </td>';
	            $html .='</tr>';
	        }


	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">Other personnel met during the inspection:</td>';
	        $html .='</tr>';

	        if(count($get_personel_met) > 0){
	            $present_count = 0;
	            foreach ($get_personel_met as $key => $value) {
	                $present_count++;
	                $html .='<tr>';
	                $html .='   <td colspan="1">';
	                $html .='&nbsp; &nbsp; &nbsp;'. $present_count . ') &nbsp; &nbsp; &nbsp; ' . $value->name;
	                $html .='   </td>';
	                $html .='   <td colspan="1">';
	                if($value->designation != ""){
		            	$html .=        $value->designation;
		            } else {
		            	$html .=        "None.";
		            }
	                $html .='   </td>';
	                $html .='</tr>';
	            }
	        } else {
	            $html .='<tr>';
	            $html .='   <td colspan="1">None';
	            $html .='   </td>';
	            $html .='   <td colspan="1">';
	            $html .='   </td>';
	            $html .='</tr>';
	        }
	        
	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';


///////////////////////////////////////////////////////////

	        $html .='<div nobr="true">';
	        $html .='<table nobr="true">';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'><b>AUDIT TEAM'S FINDINGS AND OBSERVATIONS RELEVANT TO THE AUDIT</b></td>";
	        $html .='</tr>';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'>The audit consisted of an in-depth review of quality and GMP elements including, nut not limited to the following:</td>";
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .="   <td colspan='2'>";
	        $html .='       <ul type="square">';
	        if(count($observation_findings) > 0){
	            foreach ($observation_findings as $key => $value) {
	            	$observation_x = substr(trim($value->Observation), 0, -1);
	            	if($observation_x != ""){
	               	 	$html .= "<li><b>" . $value->Element. "</b> - " . $observation_x. "</li>";
	            	} else {
	            		$html .= "<li><b>" . $value->Element. "</b> - None.</li>";
	            	}
	            }
	        }
	        $html .="       </ul>";
	        $html .="   </td>";
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

	        $html .='<div nobr="true">';
	        $html .='<table nobr="true">';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'><b>DEFINITION / CATEGORIZATION OF AUDIT OBSERVATIONS</b></td>";
	        $html .='</tr>';
	        // $html .='<tr><td colspan="2"></td></tr>';

	        foreach ($observation as $key => $value) {
        		$html .='<tr nobr="true">';
                $html .="   <td colspan='2'><u>" . $value->category_name . "</u></td>";
                $html .='</tr>';
                $html .='<tr>';
                $html .="   <td colspan='2'><i>" . $value->description . "</i></td>";
                $html .='</tr>';
                $html .="<tr><td colspan='2'></td></tr>";
	        }
	        $html .="<tr><td colspan='2'></td></tr>";
	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';

//////////////////////////////

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'><b>LISTING OF AUDIT OBSERVATIONS AND CONCERNS</b>(in decreasing order of criticality)</td>";
	        $html .='</tr>';
	        $html .="<tr><td colspan='2'></td></tr>";

	        $ctrx = 1;
	        $critical = null;
	        $major = null;
	        $minor = null;
	        foreach ($details->Template_Elements as $key => $value) {
	            foreach ($value->questions as $k => $v) {
	            	switch ($v->category) {
	            		case 'Critical':
                            $critical .= $v->answer_no . ",";
                            break;
                        
                        case 'Major':
                            $major .= $v->answer_no . ",";
                            break;
                        
                        case 'Minor':
                            $minor .= $v->answer_no . ",";
                            break;
	            	}
	            }
	        }
	        $html .= '<style> 
	                    .italic {
	                        font-size: 12px;
	                        font-family: cgi;
	                    } 
	                </style>';
	        $html .="<tr nobr='true'><td><b>Critical Observations - </b>";
	        if($critical == null) {
	            $html .= '<span class="italic">None</span>';
	        } else {
	            $html .= '<span class="italic" style="text-align:justify;">Please refer to item no(s) ' . substr(trim($critical), 0, -1)  . '</span>';
	        }
	        $html .="</td></tr>";

	        $html .="<tr nobr='true'><td><b>Major Observations - </b>";
	        if($major == null) {
	            $html .= '<i class="italic">None</i>';
	        } else {
	            $html .= '<span class="italic" style="text-align:justify;">Please refer to item no(s) ' . substr(trim($major), 0, -1) . '</span>';
	        }
	        $html .="</td></tr>"; 

	        $html .="<tr nobr='true'><td><b>Minor Observations - </b>";
	        if($minor == null) {
	            $html .= '<i class="italic">None</i>';
	        } else {
	            $html .= '<span class="italic" style="text-align:justify;">Please refer to item no(s) ' . substr(trim($minor), 0, -1) . '</span>';
	        }
	        $html .="</td></tr>";
	        $html .="<tr><td colspan='2'></td></tr>";
	        $html .="<hr>";
	        $html .="<tr><td colspan='2'></td></tr>";
	        $html .='<tr>';
	        $html .="   <td colspan='2'>";
	        $html .="       <ul>";
	        foreach ($details->Observation_No as $key => $value) {
	            if(empty($no_answer)){
	                $html .='<li><b>'. $value->element_name .' - </b> No observations </li>';
	            }
	        }
	        $html .="       </ul>";
	        $html .="   </td>";
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';

//////////////////////////////////////////////

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>SUMMARY AND RECOMMENDATION</b></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">There were';
	        if($analysis->no_critical < 1) { 
	            $html .=' <b><u>no</u></b> Critical, ';
	        } else { 
	            $html .= ' <b><u>' . $analysis->no_critical . '</u></b> Critical';
	        }
	        if($analysis->no_major < 1) { 
	            $html .=' <b><u>no</u></b> Major, ';
	        } else { 
	            $html .= ' <b><u>' .  $analysis->no_major . '</u></b> Major';
	        }
	        if($analysis->no_minor < 1) { 
	            $html .=' and <b><u>no</u></b> Minor, ';
	        } else { 
	            $html .= ' and <b><u>' . $analysis->no_minor . '</u></b> Minor ';
	        }
	        $html .= 'observations noted at this facility during the inspection.</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        if(count($details->Recommendation) > 0){
	            $html .='<tr nobr="true">';
	            $html .='   <td colspan="2">We recommend that all the findings noted should be addressed giving utmost emphasis on the significant [Critical, Major] observations which can be grouped into the following Quality System and/or GMP elements:</td>';
	            $html .='</tr>';
	        }
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">';
	        $html .='       <ul type="square">';
	        foreach ($details->Recommendation as $key => $value) {
	            $html .='   <li><b>' . $value->element_name .'</b> - ' . $value->recommendation. '</li>';
	        }
	        $html .='       </ul>';
	        $html .='   </td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='</div>';

///////////////////////////////////////

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>CONCLUSION</b></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">Based on the results of this assessment, ' . $report[0]->name . ' is at this time considered:</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100">Disposition : </td>';
	        $html .='   <td width="500">';
	        foreach ($details->Disposition_Product as $key => $value) {
	            $html .='<span><b>'.$value->disposition_name.'</b> ' . $value->disposition_label . '</span><br>';
	            $html .='       <div>';
	            foreach ($value->disposition_products as $key => $value) {
	                $html .='        <span><img src="asset/img/bullet_black.png"> &nbsp; &nbsp;'.$value->product_name.' </span><br>';
	             }             
	             $html .='       </div>';
	        }
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='</div>';
	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>OTHER ISSUES :</b></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='   <td>';
	        $html .='<ul type="square">';
	        foreach ($details->other_issue_audit as $key => $value){
	            $html .='<li> '.$value->other_issues_audit.' </li>';
	        }
	        $html .='</ul>';
	        $html .='   </td>';
	        $html .='</tr>';
	        $html .='<tr><td></td></tr>';
	        $html .='<tr><td></td></tr>';
	        $html .='</table><br/>';
	        

	        /////////////////////////////////
        	$html .='<br/>';
	        $html .='</div style="margin-bottom: 50px;">';
	        $html .='<br />';
	        $html .='<div nobr="true">';
	        $html .='<table style="border: 1px solid black;">';
	        $html .='   <tr style="border: 1px solid black;">';
	        $html .='       <td style="text-align: center; border: 1px solid black;"><b>Name</b></td>';
	        $html .='       <td style="text-align: center; border: 1px solid black;"><b>Department / Designation</b></td>';
	        $html .='       <td style="text-align: center; border: 1px solid black;"><b>Date / Time</b></td>';
	        $html .='   </tr>';
	        $html .='   <tr><td style="border: 1px solid black;" colspan="3"><b>Prepared By</b></td></tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;">';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br></span></td>';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->designation.', '.$lead_auditor[0]->company.'<br></span></td>';
	        if(isset($signature_stamp[0]->approved_date)) {
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($report[0]->create_date)) . '<br>' . date("H:i",strtotime($report[0]->create_date)) .'H<br></span></td>';
	        } else {
	             $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        }

	        $html .='   </tr>';
	        foreach ($co_auditor as $key => $value) {
	            $html .='<tr style="padding: 20px;border: 1px solid black;">';
	            $html .='   <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>' .$value->fname. ' ' .$value->lname.'<br></span></td>';
	            $html .='   <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>' . $value->company . '<br></span></td>';
	            $coauditor_stamp = $this->Audit_report_model->get_co_auditor_stamp($report[0]->report_id,$value->auditor_id);
	            if(isset($signature_stamp[0]->approved_date)) {
	                if($coauditor_stamp != null){
	                    $html .='   <td style="border: 1px solid black; text-align: center; "><span><br>'. date("d F Y",strtotime($coauditor_stamp)) . '<br>' . date("H:i",strtotime($coauditor_stamp)) .'H<br></span></td>';
	                } else {
	                    $html .='   <td style="border: 1px solid black; text-align: center; "><span><br></span></td>';
	                }
	            } else {
	                $html .='   <td style="border: 1px solid black; text-align: center; "><span><br></span></td>';
	            }
	            $html .='</tr>';
	        }
	        $html .='   <tr style="padding: 20px;border: 1px solid black;"><td style="border: 1px solid black;" colspan="3"><b>Reviewed By</b></td></tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;">';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $reviewer_info[0]->fname . ' ' .  $reviewer_info[0]->lname . '<br></span></td>';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$reviewer_info[0]->designation. ', ' . $reviewer_info[0]->company . '<br></span></td>';
	        if(isset($signature_stamp[0]->approved_date)) { 
	        	if($signature_stamp[0]->review_date != null){
	        		$html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->review_date)) . '<br>' . date("H:i",strtotime($signature_stamp[0]->review_date)) .'H<br></span></td>';
	        	} else {
	        		$html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        	}
	        } else {
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        }
	        $html .='   </tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;"><td style="border: 1px solid black;" colspan="3"><b>Approved By</b></td></tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;">';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '<br></span></td>';
	        $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$approver[0]->designation. ', ' . $approver[0]->company . '<br></span></td>';
	        if(isset($signature_stamp[0]->approved_date)) { 
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->approved_date)). '<br>' . date("H:i",strtotime($signature_stamp[0]->approved_date)) .'H<br></span></td>';
	        } else {
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        }
	        $html .='   </tr>';
	        $html .='</table>';
	        $html .='</div>';

	        $pdf->writeHTML($html,true,false,true,false,'');
	        $pdf->Output( __DIR__ . './../../json/export/approved/' . $id . '/Audit_Report.pdf', 'D');
		}

		function view()
		{
			$report_id = $_POST['id'];
			$data['div'] = $_POST['div'];
			$this->load->view('audit_report/approved_view',$data);
		}

		// function report($report_id)
		// {
		// 	set_time_limit(30); 
		// 	$report = $this->Preview_report_model->get_report_summary($report_id);
		// 	$history = $this->Global_model->get_data_query("tbl_audit_history", "report_id = " . $report_id);

		// 	$data['Report_Summary'] = $this->Preview_report_model->get_report_summary($report_id);
		// 	$data['Audit_Dates'] = $this->Preview_report_model->get_list("tbl_report_audit_date",$report_id);
		// 	$data['Co_Auditors'] = $this->Preview_report_model->get_co_auditors($report_id);
		// 	$data['Approver'] = $this->Preview_report_model->get_approvers($report_id);
		// 	$data['Reviewer'] = $this->Preview_report_model->get_reviewer($report_id);
		// 	$data['Translator'] = $this->Preview_report_model->get_translator($report_id);
		// 	$data['Inspector'] = $this->Preview_report_model->get_inspectors($report_id);
		// 	$data['Inspection_Audit_Dates'] = $this->Global_model->get_data_query("tbl_audit_history_dates", "history_id = " . $history[0]->id);
		// 	$data['Inspection_Inspector'] = $this->Global_model->get_data_query("tbl_audit_history_inspectors", "history_id = " . $history[0]->id);
		// 	$data['Inspection_Changes'] = $this->Preview_report_model->get_inspection_changes($report_id);
		// 	$data['Activities'] = $this->get_activities($report_id);
		// 	$data['Scope_Audit'] = $this->Preview_report_model->get_scope_audit($report_id);
		// 	$data['Scope_Product'] = $this->get_report_scope_product($report_id);
		// 	$data['License'] = $this->Preview_report_model->get_license($report_id);
		// 	$data['Pre_Document'] = $this->Preview_report_model->get_pre_document($report_id);
		// 	$data['Personel_Met'] = $this->Preview_report_model->get_personel_met($report_id);
		// 	$data['Distribution'] = $this->Preview_report_model->get_distribution($report_id);
		// 	$data['Disposition'] = $this->get_disposition($report_id);
		// 	$data['Template'] = $this->Preview_report_model->get_template($report_id);
		// 	$data['Element_Reco'] = $this->Preview_report_model->get_element_reco($report_id);
		// 	$data['Present_During_Meeting'] = $this->Preview_report_model->get_present_during_meeting($report_id);
		// 	$data['Remarks'] = $this->Preview_report_model->get_remarks($report_id);
		// 	$data['Observation_Yes'] = $this->report_observation($report_id);
		// 	$data['Signature_Stamp'] = $this->Preview_report_model->check_stamp($report_id);
			
		// 	return json_encode($data, JSON_PRETTY_PRINT);
		// }


		// public function report_observation($report_id)
	 //    {
	 //      $report= $this->Audit_report_model->get_report($report_id);
	 //      $element = $this->Template_model->get_elements($report[0]->template_id);

	 //      foreach ($element as $key => $value) {
	 //          # code...
	 //        $is_no = 0;
	 //        $is_yes = 0;
	 //        $is_na = 0;
	 //        $is_nc = 0;

	 //        $questions = $this->Global_model->get_element_answers($report_id,$value->element_id);

	 //        //counting all answers
	 //        foreach ($questions as $b => $a) {
	 //            if($a->answer_id == 1){
	 //                $is_yes ++;
	 //            }
	 //            if($a->answer_id == 2){
	 //                $is_no ++;
	 //            }
	 //            if($a->answer_id == 3){
	 //                $is_na ++;
	 //            }
	 //            if($a->answer_id == 4){
	 //                $is_nc ++;
	 //            }
	 //        }

	 //        //generating answers for annexure
	 //        $observation = "";

	 //        //NA only
	 //        if($is_na != 0 && $is_nc == 0 && $is_no == 0 && $is_yes == 0){
	 //            $observation .= "Not Applicable.";
	 //        }

	 //        //NC only
	 //        if($is_na == 0 && $is_nc != 0 && $is_no == 0 && $is_yes == 0){
	 //            $observation .= "Not Covered.";
	 //        }

	 //        //NO ONLy
	 //        if($is_na == 0 && $is_nc == 0 && $is_no != 0 && $is_yes == 0){
	 //            foreach ($questions as $c => $d) {
	 //                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
	 //            }
	 //        }

	 //        //yes and no
	 //        if($is_na == 0 && $is_nc == 0 && $is_no != 0 && $is_yes != 0){
	 //            foreach ($questions as $c => $d) {
	 //                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
	 //            }
	 //        }

	 //        //yes only
	 //        if($is_na == 0 && $is_nc == 0 && $is_no == 0 && $is_yes != 0){
	 //            foreach ($questions as $c => $d) {
	 //                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
	 //            }
	 //        }

	 //        //na and nc
	 //        if($is_na != 0 && $is_nc != 0 && $is_no == 0 && $is_yes == 0){
	 //            $observation = "Not Covered";
	 //        }

	 //        //nc and no
	 //        if($is_na == 0 && $is_nc != 0 && $is_no != 0 && $is_yes == 0){
	 //            foreach ($questions as $c => $d) {
	 //                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
	 //            }
	 //        }

	 //        //nc, no and yes
	 //        if($is_na == 0 && $is_nc != 0 && $is_no != 0 && $is_yes != 0){
	 //            foreach ($questions as $c => $d) {
	 //                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
	 //            }
	 //        }

	 //        //na and no
	 //        if($is_na != 0 && $is_nc == 0 && $is_no != 0 && $is_yes == 0){
	 //            foreach ($questions as $c => $d) {
	 //                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
	 //            }
	 //        }

	 //        //na, no and yes
	 //        if($is_na != 0 && $is_nc == 0 && $is_no != 0 && $is_yes != 0){
	 //            foreach ($questions as $c => $d) {
	 //                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
	 //            }
	 //        }

	 //        //na, nc, no and yes
	 //        if($is_na != 0 && $is_nc != 0 && $is_no != 0 && $is_yes != 0){
	 //            foreach ($questions as $c => $d) {
	 //                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
	 //            }
	 //        }


	 //        $array_result[] = array(
	 //            'Element' => $value->element_name, 
	 //            'Observation'=> $observation
	 //        );
	 //      }

	 //      return $array_result;

	 //    }

		// function get_disposition($report_id){
		// 	$data = $this->Preview_report_model->get_disposition($report_id);

		// 	$result = array();
		// 	foreach ($data as $key => $value) {
		// 		$products = $this->Preview_report_model->get_disposition_product($report_id, $value->disposition_id);
		// 		$result_product = "";
		// 		foreach ($products as $a => $b) {
		// 			$result_product .= ", " . $b->product_name;
		// 		}
		// 		$result[] = array(
		// 			"Disposition" => $value->disposition_name,
		// 			"Products" => substr($result_product, 1)
		// 		);
		// 	}
		// 	return $result;
		// }

		// function get_activities($report_id){
		// 	$data = $this->Preview_report_model->get_activities($report_id);
		// 	$array_return = array();
		// 	foreach ($data as $key => $value) {
		// 		$sub = $this->Preview_report_model->get_sub_activities($report_id,$value->activity_id);
		// 		$array_return[] = array(
		// 			"activity_name" => $value->activity_name,
		// 			"sub_activity" => $sub
		// 		);
		// 	}
		// 	return $array_return;
		// }

		// function get_report_scope_product($report_id){
		// 	$scopes = $this->Preview_report_model->get_report_scope_product($report_id);
		// 	$array = array();
		// 	foreach ($scopes as $key => $value) {
		// 		$product = $this->Preview_report_model->get_products($value->audit_scope_id, $report_id);
		// 		$array[] = array(
		// 					"scope"=>$value->scope_name,
		// 					"products"=>$product
		// 				);
		// 	}
		// 	return $array;
		// } 



		// function standard($template_id, $standard_id)
		// {
		// 	$q = $this->Global_model->get_data_query("tbl_audit_history_dates", "standard_id = " . $standard_id . " and template_id = " . $template_id);
		// 	return $q;
		// }

		// function classification($classification_id)
		// {
		// 	$q = $this->Global_model->get_data_query("tbl_classification", "classification_id = " . $classification_id);
		// 	return $q;
		// }



	}


