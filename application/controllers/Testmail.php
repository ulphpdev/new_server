<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Taipei');

class Testmail extends CI_Controller {

  public function __construct() {
    parent::__construct();

    $this->load->library('session');
    $this->load->model('q_auditreport');
    $this->load->model('Audit_report_model');
    $this->load->model('Global_model');
    $this->load->helper('url');
    $this->load->library('email');
    $this->email->set_newline("\r\n");
  }

    public function sendmail()
    {
        $to_email = "parvillanueva@gmail.com"; 
        $from_name = "King Villanueva";
        $from_email = "c_prvillanueva@unilab.com.ph";
        $subject =  "TEST Sendmail";
        $body ="Test Sendmail Body";

        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($from_email, $from_name); 
        $this->email->to($to_email); 
        $this->email->subject($subject); 
        $this->email->body($body); 
        print_r($this->email->send());

        echo $this->email->print_debugger();

    }

    public function smtp()
    {
        $to_email = "parvillanueva@gmail.com";
        $from_name = "From";
        $from_email = "c_prvillanueva@unilab.com.ph";
        $subject =  "TEST";
        $body ="Test Body";
        
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'phpdev.unilab@gmail.com',
            'smtp_pass' => 'phpdeveloper123',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );

        
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($from_email, $from_name); 
        $this->email->to($to_email); 
        $this->email->subject($subject); 
        $this->email->body($body); 
        print_r($this->email->send());

       echo $this->email->print_debugger();

    }
}