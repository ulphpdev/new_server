<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

    class Download extends CI_Controller {


        function __construct(){
            parent::__construct();
            $this->load->helper('url');
            $this->load->library('Phpword');
            $this->load->model('Template_model');
            $this->load->model('Audit_report_model');
        }   

        function index()
        {
            $id = $_GET['id'];
            $action = $_GET['action'];
            $this->$action($id);
        }

      public function preview_template($template_id)
      {  
            $template = $this->Template_model->get_data_by_id($template_id,'template_id','tbl_template');
            $classification = $this->Template_model->get_data_by_id($template[0]->classification_id,'classification_id','tbl_classification');
            $standard = $this->Template_model->get_data_by_id($template[0]->standard_id,'standard_id','tbl_standard_reference');
            // foreach element
            $element = $this->Template_model->get_elements($template_id);
           
            $this->load->library("Pdf_template");
            $orientation = 'P';
            $unit = 'mm';
            $format = 'LETTER';
            $unicode = true;
            $encoding = 'UTF-8';
            $pdf = new Pdf_template($orientation, $unit, $format, $unicode, $encoding);
            $pdf->SetFont('Helvetica', '', 10);
            $pdf->SetPrintHeader(false);
            $pdf->setFooterRight("CONFIDENTIAL");
            $pdf->AddPage(); 
            $html = '';
            $x = 'A';
           
            $html .= '<div><div><label>PRODUCT TYPE : <span style="color:blue;text-transform:uppercase;">'.$classification[0]->classification_name.'</span></label><br/>
                    <label>STANDARD / REFERENCE : <span style="color:blue;text-transform:uppercase;">'.$standard[0]->standard_name.'</span></label><br/>
                    <label>VERSION : <span style="color:blue;text-transform:uppercase;">'.floatval($template[0]->version).'</span></label></div></div>';
            $html .= '<div style="font-size12px;">';
            foreach($element as $value){
            $ctr = 1;                   
                $html .= '<h4>ELEMENT '.$x.' - '.$value->element_name.'</h4>';
                $html .= '<table border="1" cellpadding="3">';
                $html .= '<tr style="background-color:#5a5a5a;color:#fff;">
                            <td width="10%"></td>
                            <td width="45%">Questions</td>
                            <td width="45%">Input for Template <br/> (Default answer for Yes)</td>
                        </tr>';
                $questions = $this->Template_model->get_questions($template_id,$value->element_id);
            
                foreach ($questions as $key => $value) {
                    $html .= '<tr><td>'.$x.$ctr.'</td><td>'.$value->question.'</td><td>'.$value->default_yes.'</td></tr>';
                    $ctr++;
                } 
                $html .= '</table>';
                $x++;
            }  
            $html .= '</div>';  
            $pdf->writeHTML($html,true,false,true,false,'');   
            $pdf->Output('template_'.$template_id.'.pdf', 'D');
        } 


        function audit_report($id)
        {
            
            $string = file_get_contents("./listing/".$id.".json");
            $details = json_decode($string, false);

            $anastring = file_get_contents("./listing/".$id."_analysis.json");
            $analysis = json_decode($anastring, false);

            $report = $details->Report_Summary;
            $activities = $details->Report_activities;
            $audit_dates = $details->Audit_Dates;
            $lead_auditor = $details->Lead_Auditor;
            $co_auditor = $details->Co_Auditors;
            $translators = $details->Translator;
            $license_Accreditation = $details->License;
            $preaudit_documents = $details->Pre_Document;
            $stanard_reference = $details->Template;
            $inspection_date = $details->Inspection_Audit_Dates;
            $inspection_inspector = $details->Inspection_Inspector;
            $inspection_changes = $details->Inspection_Changes;
            $scope = $details->Scope_Product;
            $present_during_meeting = $details->Present_During_Meeting;
            $get_personel_met = $details->Personel_Met;
            $observation_findings = $details->Observation_Yes;
            $observation = $details->Audit_Observation;

            $signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
            $co_auditor = $this->Audit_report_model->get_data_by_auditor($report[0]->report_id, 'report_id', 'tbl_co_auditors');
            $reviewer_info = $this->Audit_report_model->get_reviewer($report[0]->report_id);
            $approver = $this->Audit_report_model->get_approver($report[0]->report_id);

            $params = array("report_no"=>$report[0]->report_no);

            $this->load->library("Pdf",$params);
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->setReportNo($report[0]->report_no);
            $pdf->setFooterRight("Document No. OP-14101/F05");
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetMargins('20', '27', '20');
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $gothic = $pdf->AddFont('gothic');
            $cgi = $pdf->AddFont('cgi');
            $pdf->setListIndentWidth(20);
            $pdf->AddPage();
            $html = '';
            $x = 'A';  
            $html .= '<style> 
                        * {
                            font-size: 12px;
                            font-family: gothic;
                        }
                    </style>';
            $html .='<div style="text-align:center"><center><label><h2>GMP AUDIT REPORT</h2><span style="margin-top:-80px;"><b>'.$report[0]->report_no.'</b></span></label></center></div>';
            $html .='<div>';
            $html .='<table>';
            $html .='<tr>';
            $html .='<td width="100"><b>AUDITED SITE:</b> </td>';
            $html .='<td width="30"></td>';
            $html .='<td width="500"> '. $report[0]->name.' </td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='<td width="100"></td><td width="30"></td><td> '.$report[0]->address1. ', ' .$report[0]->address2. '</td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='<td width="100"></td><td width="30"></td><td> '.$report[0]->address3.', '.$report[0]->country.' </td>';
            $html .='</tr>';
            $html .='</table><br/>';
            $html .='<hr><br/>';
            $html .='</div>';

////////////////////////////////////////////////

            $html .='<div>';
            $html .='<table>';
            $html .='<tr>';
            $html .='<td width="100"><b>ACTIVITIES CARRIED OUT BY THE COMPANY:</b></td>';
            $html .='<td width="30"></td>';
            $html .='<td width="500">';
            $html .='<table style="margin-top: 20px;">';
            foreach ($activities as $key => $value) { 
                $box = str_replace(base_url(),"",$value->activity_box);
                $html .='<tr>';
                $html .='<td style="width: 75%;">';
                $html .= $value->activity_name;
                $html .='</td>';
                $html .='<td style="width: 5%;">';
                $html .='<img style="position: fixed; right: 0;" src="'.$box.'" width="15">';
                $html .='</td>';
                $html .='</tr>';
                foreach ($value->sub_activity as $k => $v) {
                    $sub_box = str_replace(base_url(),"",$v->sub_box);
                    $html .='<tr>';
                    $html .='<td style="width: 75%;">';
                    $html .= ' &nbsp; &nbsp; &nbsp; ' . $v->sub_name;
                    $html .='</td>';
                    $html .='<td style="width: 5%;">';
                    $html .='<img style="position: fixed; right: 0;" src="'. $sub_box.'" width="15">';
                    $html .='</td>';
                    $html .='</tr>';
                }
                
            }
            if($report[0]->other_activities != "" || $report[0]->other_activities != null){
                $html .='<tr>';
                $html .='<td style="width: 75%;">Others : ' . $report[0]->other_activities . '</td>';
                $html .='<td style="width: 5%;">';
                    $html .='<img style="position: fixed; right: 0;" src="asset/img/checkbox_block.jpg" width="15">';
                    $html .='</td>';
                $html .='</tr>';
            }
            
            $html .='</table>';
            $html .='</td>';
            $html .='</tr>';
            $html .='</table><br/>';
            $html .='<hr><br/>';
            $html .='</div>';

/////////////////////////////////////

            $html .='<div>';
            $html .='<table>';
            $html .='<tr>';
            $html .='<td width="100"><b>AUDIT DATE:</b></td><td width="30"></td>';
            $html .='<td width="500">';

            $len = count($audit_dates);
            $auddate = "";
            $audyear = "";
            $audmonth = "";
            foreach ($audit_dates as $key => $value) {
                $audmonth = date_format(date_create($value->Date),"F");
                $audyear = date_format(date_create($value->Date),"Y");
                if ($key == $len - 1) {
                   $auddate .= ' & ' . date_format(date_create($value->Date),"d");
                } else {
                   $auddate .= ', ' . date_format(date_create($value->Date),"d");
                }
            }

            $html .= substr($auddate,2) . " " . $audmonth . " " . $audyear;
            $html .='</td>';
            $html .='</tr>';
            $html .='</table><br/>';
            $html .='<hr><br/>';
            $html .='</div>';


//////////////////////////////////////
            $html .='<div>';
            $html .='<table>';
            $html .='<tr>';
            $html .='<td width="100"><b>AUDITOR/S:</b></td><td width="30"></td>';
            $html .='<td width="500">';
            $html .='<table>';
            $html .='<tbody>';
            $html .='<tr>';
            $html .='<td>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'</td><td>' .$lead_auditor[0]->company. '</td>';
            $html .='</tr>';
            foreach ($co_auditor as $key => $value) {
                $html .='<tr>';
                $html .='<td>'.$value->fname. ' ' .$value->lname.'</td><td>' .$value->company. '</td>';
                $html .='</tr>';
            }
            $html .='<tr><td></td></tr>';
            $html .='</tbody>';
            $html .='</table>';
            $html .= '<br/><b>Translator Usage: </b> (Name of employee, or approved translator, participating in this audit as determined by the Lead Auditor) : ';
            $translators_name ="";
            foreach ($translators as $key => $value) {
                $translators_name .= ", " . $value->translator;
            }
            $html .= substr($translators_name, 1);
            $html .='</td>';
            $html .='</tr>';
            $html .='</table><br/>';
            $html .='<hr><br/>';
            $html .='</div>';

/////////////////////////////////////////////

            $html .='<div nobr="true">';
            $html .='<table>';
            $html .='<tr>';
            $html .='<td width="100"><b>REFERENCE:</b></td><td width="30"></td>';
            $html .='<td width="500">';
            $html .='<b>License(s)/Accreditation(s)/Certification(s)</b> held by supplier and verified during the audit';
            $html .='</td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='<td width="130"></td>';
            $html .='<td width="500">';
            $html .='<ul type="square">';
            foreach ($license_Accreditation as $key => $value) {
                $html .= '<li>';
                $html .= $value->reference_name . '<br />';
                $html .= $value->issuer . '<br />';
                $html .= 'License / Certificate No .' . $value->reference_no . '<br />';
                $html .= 'Validity : ' . date_format(date_create($value->validity),"d F Y") . '<br />';
                $html .= '</li>';
            }
            $html .='</ul>';
            $html .='</td>';
            $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';

            $html .='<tr>';
            $html .='<td width="100"></td><td width="30"></td>';
            $html .='<td width="500">';
            $html .='<b>Pre-audit documents</b> provided and reviewed';
            $html .='</td>';
            $html .='</tr>';

            $html .='<tr>';
            $html .='<td width="100"></td><td width="30"></td>';
            $html .='<td width="500">';
            $html .='<ul type="square">';
            foreach ($preaudit_documents as $key => $value) {
                $html .= '<li>' . $value->document_name . '</li>';
            }
            $html .='</ul>';
            $html .='</td>';
            $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';

            $html .='<tr>';
            $html .='<td width="100"></td><td width="30"></td>';
            $html .='<td width="500">';
            $html .='<b>Regulatory and UNILAB Standards Used</b>';
            $html .='</td>';
            $html .='</tr>';

            $html .='<tr>';
            $html .='<td width="100"></td><td width="30"></td>';
            $html .='<td width="500">';
            $html .='<ul type="square">';
            foreach ($stanard_reference as $key => $value) {
                $html .= '<li>' . $value->standard_name . '</li>';
            }
            $html .='</ul>';
            $html .='</td>';
            $html .='</tr>';

            $html .='</table><br/>';
            $html .='<br/>';
            $html .='</div>';

////////////////////////////////////////////////////

            $html .='<div nobr="true">';
            $html .='<table nobr="true">';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2"><b>SUPPLIER BACKGROUND / HISTORY</b></td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='   <td colspan="2">' . $report[0]->background . '</td>';
            $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr nobr="true">';
            $html .='   <td width="100"></td>';
            $html .='   <td width="500"><b>Date of previous inspection </b><span class="italic">(if applicable)</span></td>';
            $html .='</tr>';
            $html .='<tr nobr="true">';
            $html .='   <td width="100"></td>';
            $html .='   <td><ul type="square"><li>';

            $len = count($inspection_date);
            $auddate = "";
            $audyear = "";
            $audmonth = "";
            foreach ($inspection_date as $key => $value) {
                $audmonth = date_format(date_create($value->Date),"F");
                $audyear = date_format(date_create($value->Date),"Y");
                if ($key == $len - 1) {
                   $auddate .= ' & ' . date_format(date_create($value->Date),"d");
                } else {
                   $auddate .= ', ' . date_format(date_create($value->Date),"d");
                }
            }

            $html .= substr($auddate,2) . " " . $audmonth . ", " . $audyear;


            $html .= '</li></ul></td>';
            $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr nobr="true">';
            $html .='   <td width="100"></td>';
            $html .='   <td><b>Names of Inspectors invloved in previous inspection</b> <span class="italic">(if applicable)</span>';

            $html .='<ul type="square">';
            foreach ($inspection_inspector as $key => $value) {
                $html .='<li>'.$value->inspector.'</li>';
            }
            $html .='</ul>';

            $html .='</td>';
            $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr nobr="true">';
            $html .='   <td width="100"></td>';
            $html .='   <td><b>Major change/s since the previous inspection</b> <span class="italic">(if applicable)</span></td>';
            $html .='</tr>';
            if(count($inspection_changes) > 0 ){
                foreach ($inspection_changes as $key => $value) {
                    $html .='<tr>';
                    $html .='   <td width="100"></td>';
                    $html .='   <td><ul type="square"><li>' . $value->changes . '</li></ul></td>';
                    $html .='</tr>';
                }
            } else {
                $html .='<tr>';
                $html .='   <td width="100"></td>';
                $html .='   <td><ul type="square"><li>None</li></ul></td>';
                $html .='</tr>';
            }
            $html .='</table><br/>';
            $html .='<hr><br/>';
            $html .='</div>';


///////////////////////////////////////////////////////////////


            $html .='<div nobr="true">';
            $html .='<table>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2"><b>BRIEF REPORT OF THE AUDIT ACTIVITIES UNDERTAKEN</b></td>';
            $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2"><b>SCOPE: </b></td>';
            $html .='</tr>';
            
            foreach ($scope as $key => $value) {
                $html .='<tr nobr="true">';
                $html .='   <td colspan="2">';
                $html .='<u>'.$value->scope.'</u> '.$value->scope_details.' for:';
                $html .='   <ul type="square">';
                foreach ($value->products as $key => $value) {
                   $html .='    <li> '.$value->product_name.' </li>';
                }
                $html .='   </ul>';
                $html .='</td>';
                $html .='</tr>';
                $html .='<tr><td colspan="2"></td></tr>';
            }
            
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2"><b>AUDITED AREA(S): </b></td>';
            $html .='</tr>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2">' . $report[0]->audited_areas . '</td>';
            $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2">The audit did not cover the following areas, to be considered during the next audit.</td>';
            $html .='</tr>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2">' . $report[0]->areas_to_consider . '</td>';
            $html .='</tr>';
            $html .='</table><br/>';
            $html .='<hr><br/>';
            $html .='</div>';


///////////////////////////////////////////////////////////////

            $html .='<div>';
            $html .='<table>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2"><b>PERSONNEL MET DURING THE AUDIT:</b></td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='   <td colspan="2">Issues and audit observations were discussed during the wrap-up meeting held on ' . $report[0]->wrap_up_date;
            $html .='. The audit report will focus on the observations that were discussed during the audit.</td>';
            $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2">Present during the close-out meeting :</td>';
            $html .='</tr>';
            
            $present_count = 0;
            foreach ($present_during_meeting as $key => $value) {
                $present_count++;
                $html .='<tr>';
                $html .='   <td colspan="1">';
                $html .='&nbsp; &nbsp; &nbsp;'. $present_count . ') &nbsp; &nbsp; &nbsp; ' . $value->name;
                $html .='   </td>';
                $html .='   <td colspan="1">';
                $html .=        $value->position;
                $html .='   </td>';
                $html .='</tr>';
            }


            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2">Other personnel met during the inspection:</td>';
            $html .='</tr>';

            if(count($get_personel_met) > 0){
                $present_count = 0;
                foreach ($get_personel_met as $key => $value) {
                    $present_count++;
                    $html .='<tr>';
                    $html .='   <td colspan="1">';
                    $html .='&nbsp; &nbsp; &nbsp;'. $present_count . ') &nbsp; &nbsp; &nbsp; ' . $value->name;
                    $html .='   </td>';
                    $html .='   <td colspan="1">';
                    $html .=        $value->designation;
                    $html .='   </td>';
                    $html .='</tr>';
                }
            } else {
                $html .='<tr>';
                $html .='   <td colspan="1">None';
                $html .='   </td>';
                $html .='   <td colspan="1">';
                $html .='   </td>';
                $html .='</tr>';
            }
            
            $html .='</table><br/>';
            $html .='<br/>';
            $html .='</div>';


///////////////////////////////////////////////////////////

            $html .='<div nobr="true">';
            $html .='<table nobr="true">';
            $html .='<tr nobr="true">';
            $html .="   <td colspan='2'><b>AUDIT TEAM'S FINDINGS AND OBSERVATIONS RELEVANT TO THE AUDIT</b></td>";
            $html .='</tr>';
            $html .='<tr nobr="true">';
            $html .="   <td colspan='2'>The audit consisted of an in-depth review of quality and GMP elements including, nut not limited to the following:</td>";
            $html .='</tr>';
            $html .='<tr>';
            $html .="   <td colspan='2'>";
            $html .='       <ul type="square">';
            if(count($observation_findings) > 0){
                foreach ($observation_findings as $key => $value) {
                    $html .= "<li><b>" . $value->Element. "</b> - " . substr(trim($value->Observation), 0, -1) . "</li>";
                }
            }
            $html .="       </ul>";
            $html .="   </td>";
            $html .='</tr>';
            $html .='</table><br/>';
            $html .='<hr><br/>';
            $html .='</div>';

            $html .='<div nobr="true">';
            $html .='<table nobr="true">';
            $html .='<tr nobr="true">';
            $html .="   <td colspan='2'><b>DEFINITION / CATEGORIZATION OF AUDIT OBSERVATIONS</b></td>";
            $html .='</tr>';
            // $html .='<tr><td colspan="2"></td></tr>';

            foreach ($observation as $key => $value) {
                $html .='<tr nobr="true">';
                $html .="   <td colspan='2'><u>" . $value->category_name . "</u></td>";
                $html .='</tr>';
                $html .='<tr>';
                $html .="   <td colspan='2'><i>" . $value->description . "</i></td>";
                $html .='</tr>';
                $html .="<tr><td colspan='2'></td></tr>";
            }
            $html .="<tr><td colspan='2'></td></tr>";
            $html .='</table><br/>';
            $html .='<br/>';
            $html .='</div>';

//////////////////////////////

            $html .='<div>';
            $html .='<table>';
            $html .='<tr nobr="true">';
            $html .="   <td colspan='2'><b>LISTING OF AUDIT OBSERVATIONS AND CONCERNS</b>(in decreasing order of criticality)</td>";
            $html .='</tr>';
            $html .="<tr><td colspan='2'></td></tr>";

            $ctrx = 1;
            $critical = null;
            $major = null;
            $minor = null;
            foreach ($details->Template_Elements as $key => $value) {
                foreach ($value->questions as $k => $v) {
                    switch ($v->category) {
                        case 'Critical':
                            $critical .= $v->answer_no . ",";
                            break;
                        
                        case 'Major':
                            $major .= $v->answer_no . ",";
                            break;
                        
                        case 'Minor':
                            $minor .= $v->answer_no . ",";
                            break;
                    }
                }
            }
            $html .= '<style> 
                        .italic {
                            font-size: 12px;
                            font-family: cgi;
                        } 
                    </style>';
            $html .="<tr nobr='true'><td><b>Critical Observations - </b>";
            if($critical == null) {
                $html .= '<span class="italic">None</span>';
            } else {
                $html .= '<span class="italic">Please refer to item no(s) ' . substr(trim($critical), 0, -1)  . '</span>';
            }
            $html .="</td></tr>";

            $html .="<tr nobr='true'><td><b>Major Observations - </b>";
            if($major == null) {
                $html .= '<i class="italic">None</i>';
            } else {
                $html .= '<span class="italic">Please refer to item no(s) ' . substr(trim($major), 0, -1) . '</span>';
            }
            $html .="</td></tr>"; 

            $html .="<tr nobr='true'><td><b>Minor Observations - </b>";
            if($minor == null) {
                $html .= '<i class="italic">None</i>';
            } else {
                $html .= '<span class="italic">Please refer to item no(s) ' . substr(trim($minor), 0, -1) . '</span>';
            }
            $html .="</td></tr>";
            $html .="<tr><td colspan='2'></td></tr>";
            $html .="<hr>";
            $html .="<tr><td colspan='2'></td></tr>";
            $html .='<tr>';
            $html .="   <td colspan='2'>";
            $html .="       <ul>";
            foreach ($details->Observation_No as $key => $value) {
                if(empty($no_answer)){
                    $html .='<li><b>'. $value->element_name .' - </b> No observations </li>';
                }
            }
            $html .="       </ul>";
            $html .="   </td>";
            $html .='</tr>';
            $html .='</table><br/>';
            $html .='<br/>';
            $html .='</div>';

//////////////////////////////////////////////

            $html .='<div>';
            $html .='<table>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2"><b>SUMMARY AND RECOMMENDATION</b></td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='   <td colspan="2">There were';
            if($analysis->no_critical < 1) { 
                $html .=' <b><u>no</u></b> Critical, ';
            } else { 
                $html .= ' <b><u>' . $analysis->no_critical . '</u></b> Critical';
            }
            if($analysis->no_major < 1) { 
                $html .=' <b><u>no</u></b> Major, ';
            } else { 
                $html .= ' <b><u>' .  $analysis->no_major . '</u></b> Major';
            }
            if($analysis->no_minor < 1) { 
                $html .=' and <b><u>no</u></b> Minor, ';
            } else { 
                $html .= ' and <b><u>' . $analysis->no_minor . '</u></b> Minor ';
            }
            $html .= 'observations noted at this facility during the inspection.</td>';
            $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
            if(count($details->Recommendation) > 0){
                $html .='<tr nobr="true">';
                $html .='   <td colspan="2">We recommend that all the findings noted should be addressed giving utmost emphasis on the significant [Critical, Major] observations which can be grouped into the following Quality System and/or GMP elements:</td>';
                $html .='</tr>';
            }
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr>';
            $html .='   <td colspan="2">';
            $html .='       <ul type="square">';
            foreach ($details->Recommendation as $key => $value) {
                $html .='   <li><b>' . $value->element_name .'</b> - ' . $value->recommendation. '</li>';
            }
            $html .='       </ul>';
            $html .='   </td>';
            $html .='</tr>';
            $html .='</table><br/>';
            $html .='</div>';

///////////////////////////////////////

            $html .='<div>';
            $html .='<table>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2"><b>CONCLUSION</b></td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='   <td colspan="2">Based on the results of this assessment, ' . $report[0]->name . ' is at this time considered:</td>';
            $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
            $html .='<tr nobr="true">';
            $html .='   <td width="100">Disposition : </td>';
            $html .='   <td width="500">';
            foreach ($details->Disposition_Product as $key => $value) {
                $html .='<span><b>'.$value->disposition_name.'</b> ' . $value->disposition_label . '</span><br>';
                $html .='       <div>';
                foreach ($value->disposition_products as $key => $value) {
                    $html .='        <span><img src="'.base_url().'asset/img/bullet_black.png"> &nbsp; &nbsp;'.$value->product_name.' </span><br>';
                 }             
                 $html .='       </div>';
            }
            $html .='</td>';
            $html .='</tr>';
            $html .='</table><br/>';
            $html .='</div>';
            $html .='<div>';
            $html .='<table>';
            $html .='<tr nobr="true">';
            $html .='   <td colspan="2"><b>OTHER ISSUES :</b></td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='   <td>';
            $html .='<ul type="square">';
            foreach ($details->other_issue_audit as $key => $value){
                $html .='<li> '.$value->other_issues_audit.' </li>';
            }
            $html .='</ul>';
            $html .='   </td>';
            $html .='</tr>';
            $html .='<tr><td></td></tr>';
            $html .='<tr><td></td></tr>';
            $html .='</table><br/>';
            $html .='<br/>';
            $html .='</div style="margin-bottom: 50px;">';
            $html .='<br />';

            /////////////////////////////////

        
            $html .='<div nobr="true">';
            $html .='<table style="border: 1px solid black;">';
            $html .='   <tr style="border: 1px solid black;">';
            $html .='       <td style="text-align: center; border: 1px solid black;"><b>Name</b></td>';
            $html .='       <td style="text-align: center; border: 1px solid black;"><b>Department / Designation</b></td>';
            $html .='       <td style="text-align: center; border: 1px solid black;"><b>Date / Time</b></td>';
            $html .='   </tr>';
            $html .='   <tr><td style="border: 1px solid black;" colspan="3"><b>Prepared By</b></td></tr>';
            $html .='   <tr style="padding: 20px;border: 1px solid black;">';
            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br></span></td>';
            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->designation.', '.$lead_auditor[0]->company.'<br></span></td>';
            if(isset($signature_stamp[0]->approved_date)) {
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($report[0]->create_date)) . '<br>' . date("H:i",strtotime($report[0]->create_date)) .'H<br></span></td>';
            } else {
                 $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
            }

            $html .='   </tr>';
            foreach ($co_auditor as $key => $value) {
                $html .='<tr style="padding: 20px;border: 1px solid black;">';
                $html .='   <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>' .$value->fname. ' ' .$value->lname.'<br></span></td>';
                $html .='   <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>' . $value->company . '<br></span></td>';
                $coauditor_stamp = $this->Audit_report_model->get_co_auditor_stamp($report[0]->report_id,$value->auditor_id);
                if(isset($signature_stamp[0]->approved_date)) {
                    if($coauditor_stamp != null){
                        $html .='   <td style="border: 1px solid black; text-align: center; "><span><br>'. date("d F Y",strtotime($coauditor_stamp)) . '<br>' . date("H:i",strtotime($coauditor_stamp)) .'H<br></span></td>';
                    } else {
                        $html .='   <td style="border: 1px solid black; text-align: center; "><span><br></span></td>';
                    }
                } else {
                    $html .='   <td style="border: 1px solid black; text-align: center; "><span><br></span></td>';
                }
                $html .='</tr>';
            }
            $html .='   <tr style="padding: 20px;border: 1px solid black;"><td style="border: 1px solid black;" colspan="3"><b>Reviewed By</b></td></tr>';
            $html .='   <tr style="padding: 20px;border: 1px solid black;">';
            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $reviewer_info[0]->fname . ' ' .  $reviewer_info[0]->lname . '<br></span></td>';
            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$reviewer_info[0]->designation. ', ' . $reviewer_info[0]->company . '<br></span></td>';
            if(isset($signature_stamp[0]->approved_date)) { 
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->review_date)) . '<br>' . date("H:i",strtotime($signature_stamp[0]->review_date)) .'H<br></span></td>';
            } else {
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
            }
            $html .='   </tr>';
            $html .='   <tr style="padding: 20px;border: 1px solid black;"><td style="border: 1px solid black;" colspan="3"><b>Approved By</b></td></tr>';
            $html .='   <tr style="padding: 20px;border: 1px solid black;">';
            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '<br></span></td>';
            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$approver[0]->designation. ', ' . $approver[0]->company . '<br></span></td>';
            if(isset($signature_stamp[0]->approved_date)) { 
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->approved_date)). '<br>' . date("H:i",strtotime($signature_stamp[0]->approved_date)) .'H<br></span></td>';
            } else {
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
            }
            $html .='   </tr>';
            $html .='</table>';
            $html .='</div>';


            $pdf->writeHTML($html,true,false,true,false,'');

            $pdf->Output($report[0]->report_no.'-Audit_Report.pdf', 'D');

        }



        function convertNumberToWord($num = false)
        {
            $num = str_replace(array(',', ' '), '' , trim($num));
            if(! $num) {
                return false;
            }
            $num = (int) $num;
            $words = array();
            $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
                'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
            );
            $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
            $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
                'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
                'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
            );
            $num_length = strlen($num);
            $levels = (int) (($num_length + 2) / 3);
            $max_length = $levels * 3;
            $num = substr('00' . $num, -$max_length);
            $num_levels = str_split($num, 3);
            for ($i = 0; $i < count($num_levels); $i++) {
                $levels--;
                $hundreds = (int) ($num_levels[$i] / 100);
                $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
                $tens = (int) ($num_levels[$i] % 100);
                $singles = '';
                if ( $tens < 20 ) {
                    $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
                } else {
                    $tens = (int)($tens / 10);
                    $tens = ' ' . $list2[$tens] . ' ';
                    $singles = (int) ($num_levels[$i] % 10);
                    $singles = ' ' . $list1[$singles] . ' ';
                }
                $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
            } //end for loop
            $commas = count($words);
            if ($commas > 1) {
                $commas = $commas - 1;
            }
            return implode(' ', $words) ;
        }


        function executive_summary($id)
        {
            
            $string = file_get_contents("./listing/".$id.".json");
            $details = json_decode($string, false);

            $anastring = file_get_contents("./listing/".$id."_analysis.json");
            $analysis = json_decode($anastring, false);

            $report = $details->Report_Summary;
            $audit_dates = $details->Audit_Dates;
            $co_auditor = $details->Co_Auditors;
            $lead_auditor = $details->Lead_Auditor;
            $reviewer = $details->Reviewer;
            $approver = $details->Approver;
            $signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
            $country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');

            $this->load->library("Pdf");
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->setReportNo($report[0]->report_no);
            $pdf->setFooterRight("CONFIDENTIAL");
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetMargins('20', '32', '20');
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $gothic = $pdf->AddFont('gothic');
            $timesitalic = $pdf->AddFont('timesitalic');
            $italic_font = $pdf->AddFont('helveticaBI');
            $pdf->setListIndentWidth(30);
            $pdf->AddPage();
            $html = '';
            $x = 'A';
            $html .= '<style> 
                        * {
                            font-size: 12px;
                            font-family: gothic;
                        } 
                    </style>';
            $html .='<div style="text-align:center"><center><label><h3>GMP AUDIT REPORT EXUCUTIVE SUMMARY REPORT</h3></label></center></div>';
            $html .='<div>';
            $html .='<table>';
            $html .='<tr>';
            $html .='<td width="150">Company Name </td><td width="10">:</td><td> '.$report[0]->name.' </td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='<td width="150">Site Address </td><td width="10">:</td><td> '.$report[0]->address1.', '.$report[0]->address2.' '.$report[0]->address3.', '. strtoupper($country[0]->country_name).' </td>';
            $html .='</tr>';
            $html .='</table>';
            $html .='</div>';

////////////////////////////////////


            $html .='<div  nobr="true">';
            $html .='<label><b>Summary Statement: </b></label>';
            $len = count($audit_dates);
            $auddate = "";
            $audyear = "";
            $audmonth = "";
            foreach ($audit_dates as $key => $value) {
                $audmonth = date_format(date_create($value->Date),"F");
                $audyear = date_format(date_create($value->Date),"Y");
                if ($key == $len - 1) {
                   $auddate .= ' & ' . date_format(date_create($value->Date),"d");
                } else {
                   $auddate .= ', ' . date_format(date_create($value->Date),"d");
                }
            }

            // $html .= substr($auddate,2) . " " . $audmonth . " " . $audyear;
            $html .='<p>An audit was conducted at the above facility on ' . substr($auddate,2) . " " .$audmonth . " " . $audyear.'. This document serves to attest that the original audit documentation has been reviewed and that this report is an accurate summary of the original audit documentation.</p><br><br>';
            $html .='<table>';
            $html .='<tr>';
            $html .='<td width="150">Lead Auditor </td><td width="10">:</td><td>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'</td>';
            $html .='</tr>';
            $auditteamcount = 1;
            foreach ($co_auditor as $key => $value){
                $html .='<tr>';
                if($auditteamcount == 1){
                    $html .='<td width="150">Audit Team Member/s</td>';
                } else {
                    $html .='<td width="150"></td>';
                }
                
                $html .='<td width="10">:</td><td>'.$value->fname.' '.$value->lname.'</td>';
                $html .='</tr>';
                $auditteamcount++;
            }
            $html .='</table>';
            $html .='</div>';

////////////////////////////////////////

            $html .='<div>';
            $html .='<label><b>Scope: </b></label>';
            foreach ($details->Scope_Product as $key => $value) {
                $html .='<p><u>'.$value->scope.'</u> '.$value->scope_details.' for the following products:</p>';
               $html .='<ul type="square">';
                foreach ($value->products as $key => $value) {
                   $html .='<li style="margin-left: 200px;"> '.$value->product_name.' </li>';
                }
                $html .='</ul>';
            }
            $html .='</div>';

///////////////////////////////////////////////////

            $html .='<div>';
            $html .='<p>The audit consisted of an in-depth review of their quality systems including, but not limited to the following: </p>';
            $html .='<table>';
            foreach ($details->Template_Elements as $key => $value) {
                $html .='<tr>';
                $html .='<td width="15%;"></td>';
                $html .='<td width="85%;">';
                $html .='<img src="'.base_url().'asset/img/bullet.png" width="8"> &nbsp; '.$value->element_name;
                $html .='</td>';
                $html .='</tr>';
            }
            $html .='<tr>';
            $html .='<td width="15%;"></td>';
            $html .='<td width="85%;">';
            $html .='<img src="'.base_url().'asset/img/bullet.png" width="8"> &nbsp; License(s) Accreditation(s)/ Certification(s) held by supplier and verified during the audit. ';
            $html .='</td>';
            $html .='</tr>';
            
            $html .='</table>';
            $html .='</div>';

//////////////////////////////////////


            $html .='<div>';
            $html .='<p>The audit did not cover the following areas, to be covered during the next audit: </p>';
            $html .='<ul type="square"><li>'.$report[0]->areas_to_consider.'</li></ul>';
            $html .='<hr><br>';
            $html .='</div>';

            $html .='<div>';
            $html .='<p>The standard(s) used during the audit include:</p>';
            $html .='<ul type="square">';
            foreach ($details->Template as $key => $value){
                $html .='<li> '.$value->standard_name.' </li>';
            }
            $html .='</ul>';
            $html .='</div>';

////////////////////////////////////////////

    if($analysis->no_critical != ""){
        $critical = $analysis->no_critical;
    } else {
        $critical = 0;
    }

    if($analysis->no_major != ""){
        $no_major = $analysis->no_major;
    } else {
        $no_major = 0;
    }

    if($analysis->no_minor != ""){
        $no_minor = $analysis->no_minor;
    } else {
        $no_minor = 0;
    }


        $html .='<div  nobr="true">';
        $html .='<label><b> Audit Results: </b></label>';
        $html .='<p>The following are the observations, categorized based on critically, noted during '. substr($auddate,2) . " " . $audmonth . " " . $audyear.' inspection.</p>';
        $html .='<table border="1" style="100%" cellpadding="3">';
        $html .='<tr style="background-color:#ccc;">';
        $html .='<td style="font-weight:bold;text-align:center;">No. of Critical <br/> Observations</td>';
        $html .='<td style="font-weight:bold;text-align:center;">No. of Major <br/> Observations </td>';
        $html .='<td style="font-weight:bold;text-align:center;">No. of Minor <br/> Observations</td>';
        $html .='</tr>';
        $html .='<tr>';
        $html .='<td style="text-align:center;">'.$critical.'</td>';
        $html .='<td style="text-align:center;">'.$no_major.'</td>';
        $html .='<td style="text-align:center;">'.$no_minor.'</td>';
        $html .='</tr>';
        $html .='</table>';
        $html .='</div>';
        $html .='<div>';
        $html .='<label><b>Observations: </b></label>';
        $html .='<p>We recommend that all the findings noted should be addressed giving utmost emphasis on the significant [Critical, Major] Observations which can be grouped into the following Quality System and/or GMP elements:</p>';
        $html .='<ul type="square">';
        if(count($details->Recommendation) > 0){
            foreach ($details->Recommendation as $k => $v) {
                $html .='<li><b>'.$v->element_name.'</b> - '.$v->recommendation.'</li>';
            }
        } else {
            $html .='<li>None</li>';
        }
        
        $html .='</ul>';
        $html .='</div>';

/////////////////////////////////////


        $html .='<div>';
        $html .='<p>Based on the results of this asessement, <u>'.$report[0]->name.'</u> is at this time considered: </p>';
        $html .='<table>';
        $ctr1 = 1;
        foreach ($details->Disposition_Product as $key => $value) {
            $html .='<tr>';
            $html .='<td width="10%"></td>';
            if($ctr1 == 1){
                $html .='<td width="20%"><b>Disposition</b></td>';
            } else {
                $html .='<td width="20%"></td>';
            }
            
            $html .='<td width="70%">';
            $html .='<p><b>'.$value->disposition_name.' ' . $value->disposition_label . '</b></p><br>';
            $html .='<div>';
            foreach ($value->disposition_products as $k => $v) {
                $html .='        <span>&nbsp;&nbsp;&nbsp;&nbsp;<img src="asset/img/bullet_black.png"> &nbsp; &nbsp; '. $v->product_name.' </span><br>';
             }             
            $html .='</div>';
            $html .='<div>&nbsp;</div>';
            $html .='</td>';
            $html .='</tr>';
            $ctr1++;
            
        }
        $html .='</table>';
        $html .='</div>';
        $html .='<div>';
        // if($report[0]->closure_date == '' || $report[0]->closure_date == null){
        //     $close_date = 'N/A';
        // }else{
        //     $close_date = $report[0]->closure_date;
        // }
        // $html .='<p><b>Audit Closure Date, if applicable: '.$close_date.'</b></p>';
        $html .='<p><b>OTHER ISSUES:</b></p>';
        $html .='<ul type="square">';
        foreach ($details->other_issue_exec as $key => $value){
            $html .='<li> '.$value->other_issues_executive.' </li>';
        }
        $html .='</ul>';
        $html .='<hr><br>';
        $html .='</div>';
        $pdf->writeHTML($html,true,false,true,false,'');


/////////////////////////////////////
        
        $pdf->AddPage();
        $pdf->SetXY(25,170);
        $html2 ='';        
        $html2 .= '<style> 
                    b {
                        font-size: 12px;
                        font-family: gothic;
                    } 
                    
                    td {
                        font-size: 12px;
                        font-family: gothic;
                    } 

                    li {
                        font-size: 12px;
                        font-family: gothic;
                    } 

                    .italic_font {
                        font-weight: 700;
                        font-size: 12px;
                        font-family: timesitalic;
                    } 
                </style>';

        $html2 .='<div nobr="true">';
        $html2 .='<table style="border: 1px solid black;">';
        $html2 .='   <tr style="border: 1px solid black;">';
        $html2 .='       <td style="text-align: center; border: 1px solid black;"><b>Name</b></td>';
        $html2 .='       <td style="text-align: center; border: 1px solid black;"><b>Department / Designation</b></td>';
        $html2 .='       <td style="text-align: center; border: 1px solid black;"><b>Date / Time</b></td>';
        $html2 .='   </tr>';
        $html2 .='   <tr><td style="border: 1px solid black;" colspan="3"><b>Prepared By</b></td></tr>';
        $html2 .='   <tr style="padding: 20px;border: 1px solid black;">';
        $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br></span></td>';
        $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->designation.', '.$lead_auditor[0]->company.'<br></span></td>';
        if(isset($signature_stamp[0]->approved_date)) {
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($report[0]->create_date)) . '<br>' . date("H:i",strtotime($report[0]->create_date)) .'H<br></span></td>';
        } else {
             $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
        }

        $html2 .='   </tr>';
        $html2 .='   <tr style="padding: 20px;border: 1px solid black;"><td style="border: 1px solid black;" colspan="3"><b>Approved By</b></td></tr>';
        $html2 .='   <tr style="padding: 20px;border: 1px solid black;">';
        $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '<br></span></td>';
        $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$approver[0]->designation. ', ' . $approver[0]->company . '<br></span></td>';
        if(isset($signature_stamp[0]->approved_date)) { 
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->approved_date)). '<br>' . date("H:i",strtotime($signature_stamp[0]->approved_date)) .'H<br></span></td>';
        } else {
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
        }
        $html2 .='   </tr>';
        $html2 .='</table>';
        $html2 .='</div>';
        $html2 .='</div>';

        $html2 .='<div>';
        $html2 .='<b class="italic_font">Distribution List (include the Supplier Quality representative(s) from ALL locations of use associated with this supplier)</b>';
        $html2 .='<ul type="square">';
        $html2 .='<li>Quality Representative, '.$report[0]->name.'</li>';
        foreach ($details->Distribution as $key => $value){
            $html2 .='<li>'.$value->distribution_name.'</li>';
        }
        if(!empty($details->Other_Distribution)){
            foreach ($details->Other_Distribution as $key => $value) {
                $html2 .='<li>'.$value->other_distribution.'</li>';
            }
        }
        $html2 .='<li>Corporate GMP Department, UNILAB</li>';
        $html2 .='</ul>';
        $html2 .='</div>';
        $pdf->writeHTML($html2,true,false,true,false,'');     

        $pdf->Output($report[0]->report_no.'-Executuve_Report.pdf', 'D');

        }


        function annexure_pdf($id)
        {
            
            $string = file_get_contents("./listing/".$id.".json");
            $details = json_decode($string, false);

            $anastring = file_get_contents("./listing/".$id."_analysis.json");
            $analysis = json_decode($anastring, false);

            $report = $details->Report_Summary;
            $audit_dates = $details->Audit_Dates;
            $lead_auditor = $details->Lead_Auditor;
            $reviewer = $details->Reviewer;
            $approver = $details->Approver;
            $signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
            $country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');


            $this->load->library("Pdf");
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->setReportNo($report[0]->report_no);
            $pdf->setFooterRight("Document No. OP-100030/F09");
            $pdf->setCenter(180);
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetMargins('20', '32', '20');
            $pdf->SetMargins( 10, 10, 10, 10);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf->SetFont('gothic', '', 10);
            $pdf->SetPrintHeader(false);    
            $pdf->AddPage('L', 'LETTER');
            $html = '';
            $x = 'A';
            
            $html .='<div style="text-align:center">
            <center><label><h2>ANNEXURE - GMP AUDIT DETAILS</h2></label></center>
            </div>';
            $html .='<table style="100%" border="1" cellpadding="">';
            $html .='<tr>';
            $html .='<td width="10%"><label><b> Audited Site: </b></label></td>';
            $html .='<td width="50%"><label> '.$report[0]->name.'</label></td>';
            $html .='<td width="15%"><label><b> Audit Report No: </b></label></td>';
            $html .='<td width="25%"><label> '.$report[0]->report_no.'</label></td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='<td><label><b> Site Address: </b></label></td>';
            $html .='<td><label> '.$report[0]->address1.", ".$report[0]->address2. " " .$report[0]->address3. ", ".strtoupper($country[0]->country_name).'</label></td>';
            $html .='<td><label><b> Audit Date(s): </b></label></td>';
            $len = count($audit_dates);
            $auddate = "";
            $audyear = "";
            $audmonth = "";
            foreach ($audit_dates as $key => $value) {
                $audmonth = date_format(date_create($value->Date),"F");
                $audyear = date_format(date_create($value->Date),"Y");
                if ($key == $len - 1) {
                   $auddate .= ' & ' . date_format(date_create($value->Date),"d");
                } else {
                   $auddate .= ', ' . date_format(date_create($value->Date),"d");
                }
            }
            $html .='<td><label>'. substr($auddate,2) . " " . $audmonth . " " . $audyear.'</label></td>';
            $html .='</tr>';
            $html .='</table>';

///////////////////////

            $html .='<table><tr><td></td></tr></table>';
            $html .='<table height="30px" style="100%" border="1" cellpadding="3px" >';
            $html .='<tr>';
            $html .='<td width="5%" align="center">';
            $html .='   <table height="1">';
            $html .='   <tr><td>';
            $html .='       <h4><b>No. </b></h4>';
            $html .='   </td></tr>';
            $html .='   </table>';
            $html .='</td>';
            $html .='<td width="10%" align="center">';
            $html .='   <table height="1">';
            $html .='   <tr><td>';
            $html .='       <h4><b>CATEGORY</b></h4>';
            $html .='   </td></tr>';
            $html .='   </table>';
            $html .='</td>';
            $html .='<td width="20%" align="center">';
            $html .='   <table height="1">';
            $html .='   <tr><td>';
            $html .='       <h4><b>AUDIT OBSERVATIONS</b></h4>';
            $html .='   </td></tr>';
            $html .='   </table>';
            $html .='</td>';
            $html .='<td width="20%" align="center">';
            $html .='   <table height="1">';
            $html .='   <tr><td>';
            $html .='       <h4><b>ROOT CAUSE</b></h4>';
            $html .='   </td></tr>';
            $html .='   </table>';
            $html .='</td>';
            $html .='<td width="20%" align="center">';
            $html .='   <table height="1">';
            $html .='   <tr><td>';
            $html .='       <h4><b>CORRECTIVE ACTION/ PREVENTIVE ACTION</b></h4>';
            $html .='   </td></tr>';
            $html .='   </table>';
            $html .='</td>';
            $html .='<td width="10%" align="center">';
            $html .='   <table height="1">';
            $html .='   <tr><td>';
            $html .='       <h4><b>TARGET DATE</b></h4>';
            $html .='   </td></tr>';
            $html .='   </table>';
            $html .='</td>';
            $html .='<td width="15%" align="center">';
            $html .='   <table height="1">';
            $html .='   <tr><td>';
            $html .='       <h4><b>REMARKS</b></h4>';
            $html .='   </td></tr>';
            $html .='   </table>';
            $html .='</td>';
            // $html .='<td width="10%" align="center"><label><b><br>CATEGORY</b></label></td>';
            // $html .='<td width="22%" align="center"><label><b><br>AUDIT OBSERVATIONS</b></label></td>';
            // $html .='<td width="20%" align="center"><label><b><br>ROOT CAUSE</b></label></td>';
            // $html .='<td width="20%" align="center"><label><b>CORRECTIVE ACTION/ PREVENTIVE ACTION</b></label></td>';
            // $html .='<td width="10%" align="center"><label><b><br>TARGET DATE</b></label></td>';
            // $html .='<td width="15%" align="center"><label><b><br>REMARKS</b></label></td>';
            $html .='</tr>';
            $html .='</table>';

/////////////////////////////////////////

            $html .='<table><tr><td></td></tr></table>';
            foreach ($details->Annexure_Observation as $key => $value) {
                $html .='<table border="1" cellpadding="3px">';
                $html .='<tr  style="background-color:#ccc;"><td colspan="7" ><b>' . $value->Element .  '</b></td></tr>';
                if(count($value->Observation) > 0){
                    foreach ($value->Observation as $a => $b) {
                        $html .='<tr>';
                        $html .='<td width="5%" align="center">'.$b->no.'</td>';
                        $html .='<td width="10%" align="center">'.$b->category.'</td>';
                        $html .='<td width="20%">'.$b->observation.'</td>';
                        $html .='<td width="20%"></td>';
                        $html .='<td width="20%"></td>';
                        $html .='<td width="10%"></td>';
                        $html .='<td width="15%"></td>';
                        $html .='</tr>';
                    }
                } else {
                    $html .='<tr>';
                    $html .='<td width="5%" align="center">-</td>';
                    $html .='<td width="10%" align="center">-</td>';
                    $html .='<td width="20%">-</td>';
                    $html .='<td width="20%"></td>';
                    $html .='<td width="20%"></td>';
                    $html .='<td width="10%"></td>';
                    $html .='<td width="15%"></td>';
                    $html .='</tr>';
                }
                
                $html .='</table>';
            }

            $html .='<table><tr><td></td></tr></table>';

/////////////////////////////////////////

            // $html .='<div>';
      //       $html .='<table>';
      //       $html .='<tr><td></td><td></td></tr>';
      //       $html .='<tr>';
      //       $html .='<td><label>Prepared and Reviewed by: </label></td>';
      //       $html .='<td><label>Approved by: </label></td>';
      //       $html .='</tr>';
      //       $html .='<tr><td></td><td></td></tr>';
      //       $html .='<tr><td></td><td></td></tr>';
      //       $html .='<tr>';
      //       if(isset($signature_stamp[0]->approved_date)) { 
      //           $html .='<td style="text-align:center;">'. date("d F Y",strtotime($report[0]->create_date)) . ' ' . date("H:i",strtotime($report[0]->create_date)) . 'H<br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br>'.$lead_auditor[0]->designation.'<br>'.$lead_auditor[0]->department.'</td>';
      //       } else {
      //           $html .='<td style="text-align:center;">'. $lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br>'.$lead_auditor[0]->designation.'<br>'.$lead_auditor[0]->department.'</td>';
      //       }
      //       if(isset($signature_stamp[0]->approved_date)) { 
      //           $html .='<td style="text-align:center;">'. date("d F Y",strtotime($signature_stamp[0]->approved_date)). ' ' . date("H:i",strtotime($signature_stamp[0]->approved_date)) . 'H<br>'.$approver[0]->fname . ' ' . $approver[0]->lname.'<br>'.$approver[0]->designation.'<br>'.$approver[0]->department.'</td>';
      //       } else {
      //           $html .='<td style="text-align:center;">'.$approver[0]->fname . ' ' . $approver[0]->lname .'<br>'.$approver[0]->designation.'<br>'.$approver[0]->department.'</td>';
      //       }

             
      //       $html .='</tr>';
      //       $html .='</table>';
      //       $html .='</div>';

            $pdf->writeHTML($html,true,false,true,false,'');     

            $pdf->Output($report[0]->report_no.'-Annexure.pdf', 'D');
        }

        function raw_data($id)
        {
            
            $string = file_get_contents("./listing/".$id.".json");
            $details = json_decode($string, false);

            $anastring = file_get_contents("./listing/".$id."_analysis.json");
            $analysis = json_decode($anastring, false);

            $report = $details->Report_Summary;
            $audit_dates = $details->Audit_Dates;
            $co_auditor = $details->Co_Auditors;
            $lead_auditor = $details->Lead_Auditor;
            $template = $details->Template;
            $reviewer = $details->Reviewer;
            $signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
            $country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');

            $this->load->library("Pdf");
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->setReportNo($report[0]->report_no);
            $pdf->setFooterRight("CONFIDENTIAL");
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetMargins('20', '32', '20');
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf->SetFont('gothic', '', 10);
            $pdf->AddPage();

            $html = '';
            $html .='<style>.odd{ background-color:#ccc; } .even { background-color: #bbb;} .bordered { border: .5px solid black;} .bordered-white {border: 1px solid #fff;} .spacer { margin-top : 5px; margin-bottom: 5px; }</style>';
            $html .='<div>';
            $html .='<table>';
            $html .='   <tr>';
            $html .='       <td width="80%" style="text-align: center;">';
            $html .='           <label style="vertical-align:middle;"><h3>GMP RAW DATA REPORT</h3></label>';
            $html .='       </td>';
            $html .='       <td width="20%" class="bordered" style="text-align: center;">';
            $html .='           <center>';
            $html .='               <label><h2>Rating:<br />';
            $html .='               ' . (number_format($analysis->rating,2) + 0) . '%</h2></label>';
            $html .='               <label>% Coverage:' . (number_format($analysis->coverage,2) + 0) . '</label><br>';
            $html .='           </center>';
            $html .='       </td>';
            $html .='   </tr>';
            $html .='</table>';
            $html .='</div>';

/////////////////////////////////////////
            $html .='<table>';
            $html .='   <tr>';
            $html .='       <td width="150">Company Name</td>';
            $html .='       <td width="200">: ' . $report[0]->name . '</td>';
            $html .='       <td width="10"></td>';
            $html .='       <td width="100">Date Audit</td>';
            $html .='       <td width="200">: ';
            
            $len = count($audit_dates);
            $auddate = "";
            $audyear = "";
            $audmonth = "";
            foreach ($audit_dates as $key => $value) {
                $audmonth = date_format(date_create($value->Date),"F");
                $audyear = date_format(date_create($value->Date),"Y");
                if ($key == $len - 1) {
                   $auddate .= ' & ' . date_format(date_create($value->Date),"d");
                } else {
                   $auddate .= ', ' . date_format(date_create($value->Date),"d");
                }
            }
            $html .= $audmonth . " " . substr($auddate,2) . " " . $audyear;

            $html .='       </td>';
            $html .='   </tr>';
            $html .='   <tr>';
            $html .='       <td width="150">Site Address</td>';
            $html .='       <td width="500">: '.$report[0]->address1. ', ' .$report[0]->address2.' '.$report[0]->address3.', '.strtoupper($country[0]->country_name).'</td>';
            $html .='       <td width="1"></td>';
            $html .='       <td width="1"></td>';
            $html .='       <td width="1"></td>';
            $html .='   </tr>';
            $html .='   <tr>';
            $html .='       <td width="150">Lead Auditor</td>';
            $html .='       <td width="300">: ' . $lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname . '</td>';
            $html .='       <td width="10"></td>';
            $html .='       <td width="10"></td>';
            $html .='       <td width="10"></td>';
            $html .='   </tr>';
            $html .='   <tr>';
            $html .='       <td width="150">Co-Auditor</td>';
            $html .='       <td width="300">:';
            foreach ($co_auditor as $key => $value) {
                $html .= '  <span>' . $value->fname. ' ' .$value->lname.'</span><br>';
            }
            $html .='       </td>';
            $html .='       <td width="10"></td>';
            $html .='       <td width="10"></td>';
            $html .='       <td width="10"></td>';
            $html .='   </tr>';
            $html .='</table>';

    /////////////////////////////

            $html .='<div>';
            $html .='<table>';
            $html .='   <tr>';
            $html .='       <td>';
            $html .='           <b>Product type : </b>' . $template[0]->classification_name;
            $html .='       </td>';
            $html .='   </tr>';
            $html .='   <tr>';
            $html .='       <td>';
            $html .='           <b>Products : </b>';
            $html .='           <ol>';
            $array = array();
            foreach ($details->Scope_Product as $key => $value) {
                foreach ($value->products as $k => $v) {
                    $html .='    <li> '.$v->product_name.' </li>';
                }  
            }
             
            $html .='           </ol>';
            $html .='       </td>';
            $html .='   </tr>';
            $html .='<tr><td></td></tr>';
            $html .='   <tr>';
            $html .='       <td>';
            $html .='           <b>Standard/Reference : </b>'. $template[0]->standard_name;
            $html .='       </td>';
            $html .='   </tr>';
            $html .='</table>';
            $html .='</div>';

///////////////////////////////////////////

             $letters = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

            foreach ($details->Template_Elements as $key => $value) {
                $html .='<label><h3>Element ' . $letters[$value->order - 1] . ' - ' . $value->element_name . '</h3></label>';  
                $html .='<table cellpadding="10">';
                $html .='   <tr style="background-color: #4594cd;" > ';
                $html .='       <td class="bordered" style="text-align: center;" width="10%"></td>';
                $html .='       <td class="bordered" style="text-align: center;" width="30%"><h4 style="color: #fff;">Questions</h4></td>';
                $html .='       <td class="bordered" style="text-align: center;" width="15%"><h4 style="color: #fff">Answer</h4></td>';
                $html .='       <td class="bordered" style="text-align: center;" width="30%"><h4 style="color: #fff">Remarks</h4></td>';
                $html .='       <td class="bordered" style="text-align: center;" width="15%"><h4 style="color: #fff">Category</h4></td>';
                $html .='   </tr>';
                $question_count = 0;
                foreach ($value->questions as $a => $b) {
                    $question_count++;
                    $html .='   <tr> ';
                    $html .='       <td class="bordered" style="text-align: center;">'. $letters[$value->order - 1]. $question_count . '</td>';
                    $html .='       <td class="bordered">' . $b->question . '</td>';
                    $html .='       <td class="bordered" style="text-align: center;">' . $b->answer_name . '</td>';
                    $html .='       <td class="bordered">' . $b->answer_details . '</td>';
                    $html .='       <td class="bordered" style="text-align: center;">' . $b->category . '</td>';
                    $html .='   </tr>';
                }
                $html .='</table>';
            }
            $pdf->writeHTML($html,true,false,true,false,'');     
            $pdf->Output($report[0]->report_no.'-Raw_Data.pdf', 'D');

        }

        function annexure_doc($id)
        {
            $string = file_get_contents("./listing/".$id.".json");
            $details = json_decode($string, false);

            $anastring = file_get_contents("./listing/".$id."_analysis.json");
            $analysis = json_decode($anastring, false);

            $report = $details->Report_Summary;
            $audit_dates = $details->Audit_Dates;
            $lead_auditor = $details->Lead_Auditor;
            $reviewer = $details->Reviewer;
            $approver = $details->Approver;
            $signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
            $country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');

            $len = count($audit_dates);
            $auddate = "";
            $audyear = "";
            $audmonth = "";
            foreach ($audit_dates as $key => $value) {
                $audmonth = date_format(date_create($value->Date),"F");
                $audyear = date_format(date_create($value->Date),"Y");
                if ($key == $len - 1) {
                   $auddate .= ' & ' . date_format(date_create($value->Date),"d");
                } else {
                   $auddate .= ', ' . date_format(date_create($value->Date),"d");
                }
            }
            $audit_date_formatted = substr($auddate,2) . ' ' . $audmonth .  " " . $audyear;

            $phpWord = new \PhpOffice\PhpWord\PhpWord();

            $phpWord->getCompatibility()->setOoxmlVersion(14);
            $phpWord->getCompatibility()->setOoxmlVersion(15);
            $phpWord->setDefaultFontName('courier');

            $targetFile = "./assets/word/";
            $filename = $report[0]->report_no.'-Annexure' .  '.docx';

            $tableStyle = array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  );
            $styleCell = array('borderTopSize'=>1 ,'borderTopColor' =>'black','borderLeftSize'=>1,'borderLeftColor' =>'black','borderRightSize'=>1,'borderRightColor'=>'black','borderBottomSize' =>1,'borderBottomColor'=>'black' );
            $noSpace = array('textBottomSpacing' => -1);        
            
            $headcell = [ 'bgColor'=>'#6086B8','gridSpan' => 7, 'borderTopSize'=>1 ,'borderTopColor' =>'black','borderLeftSize'=>1,'borderLeftColor' =>'black','borderRightSize'=>1,'borderRightColor'=>'black','borderBottomSize' =>1,'borderBottomColor'=>'black'];
            $headcell_blank = [ 'gridSpan' => 7];

            $HeaderFont = array('bold'=>true, 'italic'=> false, 'size'=>16, 'name' => 'Calibri', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);
            $BoldFont = array('bold'=>true, 'italic'=> false, 'size'=>12, 'name' => 'Calibri', 'afterSpacing' => 0, 'Spacing'=> 2, 'cellMargin'=>0);
            $Font = array('bold'=>false, 'italic'=> false, 'size'=>12, 'name' => 'Calibri', 'afterSpacing' => 0, 'Spacing'=> 2, 'cellMargin'=>0);
            $CapsFont = array('bold'=>true, 'allCaps'=>true,'italic'=> false, 'size'=>12, 'name' => 'Calibri', 'afterSpacing' => 0, 'Spacing'=> 2, 'cellMargin'=>0);
            $Center_text = ['align' => \PhpOffice\PhpWord\Style\Alignment::ALIGN_CENTER];
            $spacing = array('align'=>'center','alignment'=>'center','spaceAfter' => 20, 'spaceBefore' => 20);

            $section = $phpWord->createSection(array('orientation'=>'landscape','marginLeft' => 500, 'marginRight' => 500, 'marginTop' => 600, 'marginBottom' => 600));
            $section->addText('ANNEXURE - GMP AUDIT REPORT',$HeaderFont, $Center_text);

            $table = $section->addTable('Header-Table',array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  ));
            $table->addRow(-0.5, array('exactHeight' => -5));
            $table->addCell(2500,$styleCell)->addText('Audited Site :',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(6000,$styleCell)->addText($this->text_formatter($report[0]->name),$Font,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(2500,$styleCell)->addText('Audit Report No. :',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(6000,$styleCell)->addText($this->text_formatter($report[0]->report_no),$Font,array('align' => 'left', 'spaceAfter' => 5));

            $table->addRow();
            $table->addCell(2500,$styleCell)->addText('Site Address :',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(6000,$styleCell)->addText($this->text_formatter($report[0]->address1. ', ' .$report[0]->address2 . ' ' . $report[0]->address3.', '.strtoupper($country[0]->country_name)),$Font,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(2500,$styleCell)->addText('Audit Date(s) :',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(6000,$styleCell)->addText($this->text_formatter($audit_date_formatted),$Font,array('align' => 'left', 'spaceAfter' => 5));
 
            $section->addTextBreak();
            $table = $section->addTable('2nd-Table',array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  ));
            $table->addRow(-0.5, array('exactHeight' => -5));
            $table->addCell(500,$styleCell)->addText('NO.',$BoldFont,$spacing,$Center_text);
            $table->addCell(2500,$styleCell)->addText('CATEGORY',$BoldFont,$spacing,$Center_text);
            $table->addCell(4000,$styleCell)->addText('AUDIT OBSERVATIONS',$BoldFont,$spacing,$Center_text);
            $table->addCell(4000,$styleCell)->addText('ROOT CAUSE',$BoldFont,$spacing,$Center_text);
            $table->addCell(4000,$styleCell)->addText('CORRECTIVE ACTION \ PREVENTIVE ACTION',$BoldFont,$spacing,$Center_text);
            $table->addCell(2000,$styleCell)->addText('TARGET DATE',$BoldFont,$spacing,$Center_text);
            $table->addCell(4000,$styleCell)->addText('REMARKS',$BoldFont,$spacing,$Center_text);
            $table->addRow(-0.5, array('exactHeight' => -5));
            $table->addCell(null, $headcell_blank);


                foreach ($details->Annexure_Observation as $key => $value) {
                    $table->addRow(-0.5, array('exactHeight' => -5));
                    $table->addCell(null, $headcell)->addText($this->text_formatter($value->Element),$CapsFont,array('align' => 'left', 'spaceAfter' => 5));
                    if(count($value->Observation) > 0){
                        foreach ($value->Observation as $a => $b) {
                            $table->addRow(-0.5, array('exactHeight' => -5));
                            $table->addCell(500,$styleCell)->addText($b->no,$Font,$Center_text);
                            $table->addCell(2500,$styleCell)->addText($this->text_formatter($b->category),$Font,$Center_text);
                            $table->addCell(4000,$styleCell)->addText($this->text_formatter($b->observation),$Font);
                            $table->addCell(4000,$styleCell)->addText('',$Font);
                            $table->addCell(4000,$styleCell)->addText('',$Font,$Center_text);
                            $table->addCell(2000,$styleCell)->addText('',$Font,$Center_text);
                            $table->addCell(4000,$styleCell)->addText('',$Font,$Center_text);
                        }
                    } else {
                        $table->addRow(-0.5, array('exactHeight' => -5));
                        $table->addCell(500,$styleCell)->addText("-",$Font,$Center_text);
                        $table->addCell(2500,$styleCell)->addText($this->text_formatter("-"),$Font,$Center_text);
                        $table->addCell(4000,$styleCell)->addText($this->text_formatter("-"),$Font);
                        $table->addCell(4000,$styleCell)->addText('',$Font);
                        $table->addCell(4000,$styleCell)->addText('',$Font,$Center_text);
                        $table->addCell(2000,$styleCell)->addText('',$Font,$Center_text);
                        $table->addCell(4000,$styleCell)->addText('',$Font,$Center_text);
                    }
                    
                }

                $section->addTextBreak();
                // $table = $section->addTable('2nd-Table',array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  ));
                
                // $table->addRow(-0.5, array('exactHeight' => -5));
                // $table->addCell(10000,$headcell_blank)->addText('Prepared and reviewed by:',$Font);
                // $table->addCell(10000,$headcell_blank)->addText('Approved by:',$Font);

                // $table->addRow();
                // if(isset($signature_stamp[0]->approved_date)) {
                //     $table->addCell(10000,$headcell_blank)->addText($this->text_formatter('<w:br />' . date("d F Y",strtotime($report[0]->create_date)) . ' ' . date("H:i",strtotime($report[0]->create_date))) . 'H<w:br />' . $this->text_formatter($lead_auditor[0]->fname . " " . $lead_auditor[0]->lname) . '<w:br />' . $this->text_formatter($lead_auditor[0]->designation) . '<w:br />' . $this->text_formatter($lead_auditor[0]->department),$Font,$Center_text,array('spaceAfter' => 5));
                // } else {
                //     $table->addCell(10000,$headcell_blank)->addText('<w:br /><w:br />' . $this->text_formatter($lead_auditor[0]->fname . " " . $lead_auditor[0]->lname) . '<w:br />' . $this->text_formatter($lead_auditor[0]->designation) . '<w:br />' . $this->text_formatter($lead_auditor[0]->department),$Font,$Center_text,array('spaceAfter' => 5));
                // }

                // if(isset($signature_stamp[0]->approved_date)) { 
                //     $table->addCell(10000,$headcell_blank)->addText('<w:br />' . $this->text_formatter(date("d F Y",strtotime($signature_stamp[0]->approved_date)). ' ' . date("H:i",strtotime($signature_stamp[0]->approved_date))) . 'H<w:br />' . $this->text_formatter($approver[0]->fname . " " . $approver[0]->lname) . '<w:br />' . $this->text_formatter($lead_auditor[0]->designation) . '<w:br />' . $this->text_formatter($lead_auditor[0]->department),$Font,$Center_text);
                // } else {
                //    $table->addCell(10000,$headcell_blank)->addText('<w:br /><w:br />' . $this->text_formatter($approver[0]->fname . " " . $approver[0]->lname) . '<w:br />' . $this->text_formatter($approver[0]->designation) . '<w:br />' . $this->text_formatter($approver[0]->department),$Font,$Center_text);
                // }

                $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
                $objWriter->save($filename);
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.$filename);
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filename));
                flush();
                readfile($filename);
                unlink($filename); // deletes the temporary file

                redirect(base_url("download/generate_annexure") . '?report_id=' . $id);
                exit;
        }

        function text_formatter($text){
                return str_replace('&', '&amp;', $text);
        }
    }