<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class C_auditreport extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('global_model');
		$this->load->model('Audit_trail_model');
		$this->load->model('Api_model');
		$this->load->model('q_auditreport');
		$this->load->helper('url');
		$this->load->model('Template_model'); 
		$this->load->model('Preview_report_model'); 
	}
	public function get(){
		header('Content-type: Application/JSON');
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$query = $_POST['query'];
		$group = $_POST['group'];
		$result_array = "";
		$report = $this->q_auditreport->get_list($query,$limit,$offset,$group);
		foreach ($report as $key => $value) {
			$report_id = $value->Report_ID;
			$user_id = $this->session->userdata('userid');
			if($this->q_auditreport->check_if_auditor("tbl_report_summary","auditor_id = " . $user_id . " AND report_id = " . $report_id) > 0){
				$report_email = "display";
			} else {
				if($this->q_auditreport->check_if_auditor("tbl_co_auditors","auditor_id = " . $user_id . " AND report_id = " . $report_id) > 0){
					$report_email = "display";
				} else {
					$report_email = "hide";
				}
			}
			$report_audit_date = $this->q_auditreport->get_report_audit_date($report_id);
			$result_array[] = array(
				 	"Report_ID" => $value->Report_ID,
			        "Report_No"=> $value->Report_No,
			        "Template_ID"=> $value->Template_ID,
			        "Audited_Site"=>$value->Audited_Site,
			        "Classification_ID"=> $value->Classification_ID,
			        "Auditor_Name"=> $value->Auditor_Name,
			        "Auditor_ID"=> $value->Auditor_ID,
			        "Report_Status"=> $value->Report_Status,
			        "TemplateID"=> $value->TemplateID,
			        "Template_Created"=> $value->Template_Created,
			        "Template_Updated"=> $value->Template_Updated,
			        "Template_Version"=> $value->Template_Version,
			        "Standard_Name"=> $value->Standard_Name,
			        "Classification_Code"=> $value->Classification_Code,
			        "Classification_Name"=> $value->Classification_Name,
			        "name"=> $value->name,
			        "address1"=> $value->address1,
			        "country"=> $value->country,
			        "zip_code"=> $value->zip_code,
			        "status"=> $value->status,
			        "type"=> $value->type,
			        "background"=> $value->background,
			        "create_date"=> $value->create_date,
			        "update_date"=> $value->update_date,
			        "Country_Code"=> $value->country_code,
			        "Country_Name"=> $value->country_name,
			        "Province_Name"=> $value->province_name,
			        "city_name"=> $value->city_name,
			        "audited_areas"=> $value->audited_areas,
			        "areas_to_consider"=> $value->areas_to_consider,
			        "Auditor_Company"=> $value->Auditor_Company,
			        "other_issues_audit"=> $value->other_issues_audit,
			        "report_submission"=>$value->report_submission,
			        "report_issuance"=>$value->report_issuance,
			        "version"=>$value->version,
			        "modified_date"=>$value->Modified_Date,
			        "display_email"=>$report_email,
			        "Audit_Dates"=>$report_audit_date
				);
		}
		echo json_encode($result_array, JSON_PRETTY_PRINT);
	}
	public function get_pagination(){
		header('Content-type: Application/JSON');
		$query = $_POST['query'];
		$group = $_POST['group'];
		echo $this->q_auditreport->get_pagination($query, $group);
	}
	public function get_group()
	{
		$table = $_POST['table'];
		$query = $_POST['query'];
		echo json_encode($this->q_auditreport->group_query($table,$query), JSON_PRETTY_PRINT);
	}
	public function audit_report_view(){
		$data['id'] = $_POST['id'];
		$data['div'] = $_POST['div'];
  		$this->load->view('audit_report/approved_view',$data);
	}
	public function audit_report_status_update()
	{
		$id = $_POST['id'];	
		$status = $_POST['status'];	
		$data = array("status"=>$status);
		$this->q_auditreport->report_status_update($id,$data);


        $ch9 = curl_init();
        curl_setopt($ch9, CURLOPT_URL, base_url('api/update_listing') . "?report_id=" . $id);
        curl_setopt($ch9, CURLOPT_POST, 1);
        curl_setopt($ch9, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch9);
        curl_close ($ch9);




	}
	function check_auditor_details(){
		$report_id = $_POST['report_id'];
		$report_details = $this->q_auditreport->group_query("tbl_report_summary","report_id = " . $report_id);
		$head_lead = $report_details[0]->head_lead;
		if($head_lead == 1){
			echo 1;
		} else {
			echo 0;
		}
	}
	public function update_date_submission(){
		$reportid =  $_POST['report_id'];
		$data = array("report_submission"=>date("Y-m-d	H:m:s"));
		$this->q_auditreport->update_date_submission($data,$reportid);
	}
	public function check_if_auditor()
	{
		$report_id = $_POST['report_id'];
		$user_id = $this->session->userdata('userid');
		if($this->q_auditreport->check_if_auditor("tbl_report_summary","auditor_id = " . $user_id . " AND report_id = " . $report_id) > 0){
			echo "true";
		} else {
			if($this->q_auditreport->check_if_auditor("tbl_co_auditors","auditor_id = " . $user_id . " AND report_id = " . $report_id) > 0){
				echo "true";
			} 
		}
	}
	public function get_data(){
		header('Content-type: Application/JSON');
		$query = "";
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$select = $_POST['select'];
		$table = $_POST['table'];
		$join = $_POST['join'];
		$where = $_POST['where'];
		$result_array = "";
		if($join != ""){
			if($where != ""){
				$query = "SELECT " . $select . " FROM " . $table . " " . $join . " WHERE ".  $where . " AND a.status >= 0 AND a.status <=5 GROUP BY a.Report_No ORDER BY a.Report_ID DESC LIMIT " . $offset . "," . $limit;
			} else {
				$query = "SELECT " . $select . " FROM " . $table . " " . $join . " WHERE a.status >= 0 AND a.status <=5 GROUP BY a.Report_No ORDER BY a.Report_ID DESC LIMIT " . $offset . "," . $limit;
			}
		} else {
			if($where != ""){
				$query = "SELECT " . $select . " FROM " . $table . " WHERE ".  $where . " AND a.status >= 0 AND a.status <=5 GROUP BY a.Report_No ORDER BY a.Report_ID DESC LIMIT " . $offset . "," . $limit;
			} else {
				$query = "SELECT " . $select . " FROM " . $table . " WHERE a.status >= 0 AND a.status <=5 GROUP BY a.Report_No  ORDER BY a.Report_ID DESC LIMIT " . $offset . "," . $limit;
			}
		}
		$report = $this->q_auditreport->get_data($query);
		foreach ($report as $key => $value) {
			$report_id = $value->Report_ID;
			$user_id = $this->session->userdata('userid');
			if($this->q_auditreport->check_if_auditor("tbl_report_summary","auditor_id = " . $user_id . " AND report_id = " . $report_id) > 0){
				$report_email = "display";
			} else {
				if($this->q_auditreport->check_if_auditor("tbl_co_auditors","auditor_id = " . $user_id . " AND report_id = " . $report_id) > 0){
					$report_email = "display";
				} else {
					$report_email = "hide";
				}
			}
			$report_audit_date = $this->q_auditreport->get_report_audit_date($report_id);
			$result_array[] = array(
				 	"Report_ID" => $value->Report_ID,
			        "Report_No"=> $value->Report_No,
			        "Audited_Site"=>$value->Audited_Site,
			        "Auditor_Name"=> $value->Auditor_Name,
			        "Report_Status"=> $value->status,
			        "version"=>$value->version,
			        "modified_date"=>$value->modified_date,
			        "display_email"=>$report_email,
			        "Audit_Dates"=>$report_audit_date
				);
		}
		echo json_encode($result_array, JSON_PRETTY_PRINT);
	}
	public function get_data_pagination()
	{
		$limit = 9999999;
		$offset = 0;
		$query = $_POST['query'];
		echo count($this->q_auditreport->get_data($query,$limit,$offset));
	}

	function get_csv()
	{
		set_time_limit(30); 
		date_default_timezone_set('Asia/Manila');
		$date = date('Y-m-d');
		$filename = "Audit Report -" .$date. ".xlsx";

		
		if(isset($_POST['query']) || $_POST['query'] == ""){
			$query = $_POST['query'];
		} else {
			$query = null;
		}
    	$result = $this->global_model->report_listing_model(9999,0,$query);

		$this->load->library("PHPExcel/Classes/PHPExcel.php");
		$obj = new PHPExcel();
		$obj->getProperties()->setCreator($this->session->userdata('sess_email'));
		//HEADERS
		$obj->setActiveSheetIndex(0)
            ->setCellValue('A1', 'No.')
            ->setCellValue('B1', 'Site Name')
            ->setCellValue('C1', 'Country')
            ->setCellValue('D1', "Province \n /State")
            ->setCellValue('E1', "Auditor")
            ->setCellValue('F1', "Co-Auditor(s)")
            ->setCellValue('G1', 'Report No')
            ->setCellValue('H1', 'Version No')
            ->setCellValue('I1', "Date \n(Audit)")
            ->setCellValue('J1', "Date \n(Submitted)")
            ->setCellValue('K1', "Lead Time \n(Preparation)")
            ->setCellValue('L1', "Date \n(Reviewed)")
            ->setCellValue('M1', "Lead Time \n(Review)")
            ->setCellValue('N1', "Date \n(Approved)")
            ->setCellValue('O1', "Lead Time \n(Approval)")
            ->setCellValue('P1', "Lead Time \n(Total)")
            ->setCellValue('Q1', 'Type of Audit')
            ->setCellValue('R1', 'Product-Type')
            ->setCellValue('S1', 'Products-of-Interest')
            ->setCellValue('T1', 'Disposition')
            ->setCellValue('U1', 'GMP Score (%)')
            ->setCellValue('V1', 'Coverage (%)')
            ->setCellValue('W1', 'Status');

        $no = 0;
        $total_result = count($result);
        $ave_total = 0;
        foreach ($result as $key => $value) {

        	$lead_auditor = $this->Api_model->audit_report_json("tbl_auditor_info", "auditor_id=" . $value->lead_auditor);
        	$country = $this->Api_model->audit_report_json("country_list", "country_code = '" . $value->country . "'");
        	
        	
			$co_auditor = "";
			if($value->co_auditor != ""){
				if($value->co_auditor != "0"){
					$coauditor_arr = explode(",", $value->co_auditor);
					foreach ($coauditor_arr as $k => $v) {
						$co_audit = $this->Api_model->audit_report_json("tbl_auditor_info", "auditor_id=" . $v);
						if(count($co_audit) > 0){
							$co_auditor .= $co_audit[0]->fname . " " . $co_audit[0]->lname . ", ";
						}
						
					}
					$co_auditor = substr(trim($co_auditor),0,-1);
				} else {
					$co_auditor = "None";
				}
			} else {
				$co_auditor = "None";
			}

        	$arr_type_audit = explode(",", $value->type_of_audit);
        	$typeaudit = "";
        	foreach ($arr_type_audit as $k_type => $v_type) {
        		$type_of_audit = $this->Api_model->audit_report_json("tbl_audit_scope", "scope_id =" . $v_type);
				
				if(count($type_of_audit) > 0){
					foreach ($type_of_audit as $x => $c) {
						$typeaudit .= $c->scope_name . ",";
					}
				}
				
        	}

        	

        	$arr_prodtype = explode(",", $value->product_type);
        	$prod_type = "";
        	foreach ($arr_prodtype as $k_prodtype => $v_prodtype) {
        		$q_prod_type = $this->Api_model->audit_report_json("tbl_classification", "classification_id =" . $v_prodtype);
	        	foreach ($q_prod_type as $e => $f) {
	        		$prod_type .= $f->classification_name . ",";
	        	}
        	}

        	$arr_prod = array_unique(explode(",", $value->product));
        	$prod = "";
        	foreach ($arr_prod as $k_prod => $v_prod) {
        		$q_prod = $this->Api_model->audit_report_json("tbl_product", "product_id =" . $v_prod);
	        	foreach ($q_prod as $g => $h) {
	        		$prod .= $h->product_name . ",";
	        	}
        	}

        	$arr_disposition = array_unique(explode(",", $value->disposition));
			$disposition = "";
			if(count($arr_disposition) > 0){
				foreach ($arr_disposition as $k_disposition => $v_disposition) {
					$q_disposition = $this->Api_model->audit_report_json("tbl_disposition", "disposition_id =" . $v_disposition);
					foreach ($q_disposition as $g => $h) {
						$disposition .= $h->disposition_name . ",";
					}
				}
			}
        	
        	

	        $audit_date = $value->audit_date_formatted;


        	if($value->date_submitted){
				if($value->status == 3 || $value->status == 4 || $value->status == 5){
					$date_submission = date("j F, Y", strtotime($value->date_submitted));
					$lead_time_submission = max(0,$value->lead_time_submission);
				} else {
					$date_submission = "-";
					$lead_time_submission = "0";
				}
        	} else {
				$date_submission = "-";
				$lead_time_submission = "0";
        	}

        	if($value->date_review){
				if($value->status == 4 || $value->status == 5){
					$date_review = date("j F, Y", strtotime($value->date_review));
					$lead_time_review = max(0,$value->lead_time_review);
				} else {
					$date_review = "-";
					$lead_time_review = "0";
				}
        		
        	} else {
				$date_review = "-";
				$lead_time_review = "0";
        	}

        	if($value->date_approved){
				if($value->status == 5){
					$date_approved = date("j F, Y", strtotime($value->date_approved));
					$lead_time_approved = max(0,$value->lead_time_approved);
					$lead_time_total = max(0,$value->lead_time_total);
				} else {
					$date_approved = "-";
					$lead_time_approved = "0";
					$lead_time_total = "0";
				}
        	} else {
				$date_approved = "-";
				$lead_time_approved = "0";
				$lead_time_total = "0";
			}
			

			switch ($value->status) {
				case 0:
					$report_status = "Draft";
					break;
				case 1:
					$report_status = "Co-Auditor's Review";
					break;
				case 2:
					$report_status = "Co-Auditor's Review";
					break;
				case 3:
					$report_status = "Submitted to Department Head";
					break;
				case 4:
					$report_status = "Submitted to Division Head";
					break;
				case 5:
					$report_status = "Approved by Division Head";
					break;
				case 6:
					$report_status = "Returned by Department Head";
					break;
				case 7:
					$report_status = "Returned by Division Head";
					break;
				
				default:
				$report_status = "-";
					break;
			}

        	$state = explode(",", $value->province_state);
        	$no ++;
        	$cell = $no + 1;

        	if(count($country) > 0) {
        		$display_country = $country[0]->country_name;
        	} else {
        		$display_country = "";
        	}
        	$obj->setActiveSheetIndex(0)
	            ->setCellValue('A'.$cell, $no)
	            ->setCellValue('B'.$cell, $value->site_name)
	            ->setCellValue('C'.$cell, $display_country)
	            ->setCellValue('D'.$cell, $state[1])
	            ->setCellValue('E'.$cell, $lead_auditor[0]->fname . " " . $lead_auditor[0]->lname)
	            ->setCellValue('F'.$cell, $co_auditor)
	            ->setCellValue('G'.$cell, $value->report_no)
	            ->setCellValue('H'.$cell, $value->version)
	            ->setCellValue('I'.$cell, $audit_date)
	            ->setCellValue('J'.$cell, $date_submission)
	            ->setCellValue('K'.$cell, $lead_time_submission)
	            ->setCellValue('L'.$cell, $date_review)
	            ->setCellValue('M'.$cell, $lead_time_review)
	            ->setCellValue('N'.$cell, $date_approved)
	            ->setCellValue('O'.$cell, $lead_time_approved)
	            ->setCellValue('P'.$cell,$lead_time_total)
	            ->setCellValue('Q'.$cell, substr($typeaudit, 0, -1))
	            ->setCellValue('R'.$cell, substr($prod_type, 0, -1))
	            ->setCellValue('S'.$cell, substr($prod, 0, -1))
	            ->setCellValue('T'.$cell, substr($disposition, 0, -1))
	            ->setCellValue('U'.$cell, (number_format($value->gmp_score,2) + 0) . "%")
	            ->setCellValue('V'.$cell, (number_format($value->gmp_coverage,2) + 0) . "%")
	            ->setCellValue('W'.$cell, $report_status);
        	$obj->getActiveSheet()->getStyle('A'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('H'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('K'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('M'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('O'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('P'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('U'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('V'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
        }

        $ave_total = $ave_total / $total_result; 
		$count =  $no + 3;
		$no++;
		$xl_leadtime_min = '="Min = " & MIN(K2:K'.$no.') &';
		$xl_leadtime_max = '"Max = " & MAX(K2:K'.$no.') &';
		$xl_leadtime_ave = '"Ave = " & ROUND(AVERAGE(K2:K'.$no.'),2)';
		$xl_leadtime = $xl_leadtime_min . ' CHAR(10) & '. $xl_leadtime_max . ' CHAR(10) & ' . $xl_leadtime_ave;
		$obj->getActiveSheet()->setCellValue('K'.$count, $xl_leadtime);
		$obj->getActiveSheet()->getStyle('K'.$count)
		                              ->getAlignment()
		                              ->setWrapText(true);
		$xl_review_min = '="Min = " & MIN(M2:M'.$no.') &';
		$xl_review_max = '"Max = " & MAX(M2:M'.$no.') &';
		$xl_review_ave = '"Ave = " & ROUND(AVERAGE(M2:M'.$no.'),2)';
		$xl_review = $xl_review_min . ' CHAR(10) & '. $xl_review_max . ' CHAR(10) & ' . $xl_review_ave;
		$obj->getActiveSheet()->setCellValue('M'.$count, $xl_review);
		$obj->getActiveSheet()->getStyle('M'.$count)
		                              ->getAlignment()
		                              ->setWrapText(true);
		$xl_approval_min = '="Min = " & MIN(O2:O'.$no.') &';
		$xl_approval_max = '"Max = " & MAX(O2:O'.$no.') &';
		$xl_approval_ave = '"Ave = " & ROUND(AVERAGE(O2:O'.$no.'),2)';
		$xl_approval = $xl_approval_min . ' CHAR(10) & '. $xl_approval_max . ' CHAR(10) & ' . $xl_approval_ave;
		$obj->getActiveSheet()->setCellValue('O'.$count, $xl_approval);
		$obj->getActiveSheet()->getStyle('O'.$count)
		                              ->getAlignment()
		                              ->setWrapText(true);
		$xl_total_min = '="Min = " & MIN(P2:P'.$no.') &';
		$xl_total_max = '"Max = " & MAX(P2:P'.$no.') &';
		$xl_total_ave = '"Ave = " & ROUND(AVERAGE(P2:P'.$no.'),2)';
		$xl_total = $xl_total_min . ' CHAR(10) & '. $xl_total_max . ' CHAR(10) & ' . $xl_total_ave;
		$obj->getActiveSheet()->setCellValue('P'.$count, $xl_total);
		$obj->getActiveSheet()->getStyle('P'.$count)
		                              ->getAlignment()
		                              ->setWrapText(true);

        $header_style = array(
	    'font'  => array(
	        'color' => array('rgb' => 'FFFFFF'),
	    ));
        $obj->getActiveSheet(0)->getColumnDimension('A')->setWidth(5);    	
        $obj->getActiveSheet(0)->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getActiveSheet(0)->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('5a5a5a');
        $obj->getActiveSheet(0)->getStyle('A1')->applyFromArray($header_style);
        foreach ( range("B","W") as $value) {
        	$obj->getActiveSheet(0)->getColumnDimension($value)->setAutoSize(true);  
        	$obj->getActiveSheet(0)->getStyle($value."1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle($value.'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('5a5a5a');
        	$obj->getActiveSheet(0)->getStyle($value."1")->applyFromArray($header_style);
        	$obj->getActiveSheet(0)->getStyle($value."1")->getAlignment()->setWrapText(true);
        }
        $obj->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $obj->getActiveSheet()->getStyle("A1:W1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $obj->getActiveSheet()->getStyle("A1:W1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getActiveSheet()->setTitle($filename);
        $obj->setActiveSheetIndex(0);
        // DOWNLOAD EXCEL
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		$objWriter->setPreCalculateFormulas(true);
		$objWriter->save('php://output');
		exit();
		
	}

	public function get_csv_details($select,$table,$join,$where)
	{
		set_time_limit(30); 
		date_default_timezone_set('Asia/Manila');
		$date = date('Y-m-d');
		$filename = "Audit Report -" .$date. ".xlsx";
		$query = "";
		$select = $_GET['select'];
		$table = $_GET['table'];
		$join = $_GET['join'];
		$where = $_GET['where'];
		$result_array = "";
		if($join != ""){
			if($where != ""){
				$query = "SELECT " . $select . " FROM " . $table . " " . $join . " WHERE ".  $where . " AND a.status >= 0 AND a.status <=5 GROUP BY a.Report_No";
			} else {
				$query = "SELECT " . $select . " FROM " . $table . " " . $join . " WHERE a.status >= 0 AND a.status <=5 GROUP BY a.Report_No";
			}
		} else {
			if($where != ""){
				$query = "SELECT " . $select . " FROM " . $table . " WHERE ".  $where . " AND a.status >= 0 AND a.status <=5 GROUP BY a.Report_No";
			} else {
				$query = "SELECT " . $select . " FROM " . $table . " WHERE a.status >= 0 AND a.status <=5 GROUP BY a.Report_No";
			}
		}
		// echo $query;
		$report = $this->q_auditreport->get_data($query);
		$data =  array();
		$count = 1;
		foreach ($report as $key => $value) {

			$report_id = $value->Report_ID;

			//co auditors
			$coauditors = $this->Template_model->get_data_by_id($report_id,'report_id','ar_coauditor');
			$co_auditors = "";
			foreach ($coauditors as $a) {
				$co_auditors .= $a->Co_Auditor_Name . ", ";
			}

			//audit dates
			$report_audit_date = $this->q_auditreport->get_report_audit_date($report_id);
			$len = count($report_audit_date);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($report_audit_date as $y) {
	            $audmonth = date_format(date_create($y->Date),"F");
	            $audyear = date_format(date_create($y->Date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($y->Date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($y->Date),"d");
	            }
	        }

	        //date submitted
	        $date_submitted = $this->Template_model->get_data_by_id($report_id,'report_id','tbl_report_submission_date');
			$date_submitted_display = "-";
			if(count($date_submitted) > 0){
				$date_submitted_display = date("d F Y",strtotime($date_submitted[0]->reviewer));
			}

			//lead time submission
			$date_submitted_q = $this->Template_model->get_data_by_id($report_id,'report_id','csv_leadtime_submission');
			$date_submitted_lead_time = "-";
			if(count($date_submitted_q) > 0){
				$date_submitted_lead_time = $date_submitted_q[0]->leadtime;
			}

			 //review date
	        $signature_stamp = $this->Template_model->get_data_by_id($report_id,'report_id','tbl_report_signature_stamp');
			$review_date = "-";
			if(count($signature_stamp) > 0){
				$review_date = date("d F Y",strtotime($signature_stamp[0]->review_date));
			}

			//review leadtime 
			$review_lead_time = $review_date - $date_submitted_display;
			if($review_lead_time < 0 ){
				$review_lead_time = 0;
			}

			//approved date
			$approved_date = "-";
			if(count($signature_stamp) > 0){
				$approved_date = date("d F Y",strtotime($signature_stamp[0]->approved_date));
			}

			//approved leadtime
			$approved_lead_time = $approved_date - $review_date;
			if($approved_lead_time < 0 ){
				$approved_lead_time = 0;
			}


			//type og audit
			$audit_scope = $this->Template_model->get_data_by_id($report_id,'report_id','ar_type_of_audit');
			$type_of_audit = "-";
			foreach ($audit_scope as $j) {
				$type_of_audit = $j->scope_name;
			}

			//product type
			$product_type = $this->Template_model->get_data_by_id($report_id,'report_id','ar_product_type');
			$product_type_display = "";
			foreach ($product_type as $f) {
				$product_type_display .= $f->Product_Type . " ";
			}

			//product of interest
			$product_interest = $this->Template_model->get_data_by_id($report_id,'report_id','ar_product_of_interest');
			$product_of_interest = "";
			foreach ($product_interest as $g) {
				$product_of_interest .= $g->product_of_interest_name . ", ";
			}

			//disposition
			$disposition = $this->Template_model->get_data_by_id_group($report_id,'report_id','ar_disposition','disposition_name');
			$disposition_display = "";
			foreach ($disposition as $g) {
				$disposition_display .= $g->disposition_name . ", ";
			}

			//RATINGS
			$gmp_computation = $this->Template_model->get_data_by_id($report_id,'report_id','gmp_computation_rating');
			$gmp_rate = "0";
			if(count($gmp_computation) > 0){
				$gmp_rate = number_format($gmp_computation[0]->Rating,2)  + 0;
			}
			

			//COVERAGE
			$coverage_computation = $this->Template_model->get_data_by_id($report_id,'report_id','gmp_computation_coverage');
			$coverage_rate = "0";
			if(count($coverage_computation) > 0){
				$coverage_rate = number_format($coverage_computation[0]->Coverage,2)  + 0;
			}
			
			$data[] = (object) array(
				"No"=> $count++,
				"Audited_Site"=> $value->Audited_Site,
				"Country"=> $value->country_name,
				"Province_State"=> $value->address2 . ", " . $value->address3,
				"Lead_Auditor"=> $value->Auditor_Name,
				"Co_Auditors"=>substr(trim($co_auditors), 0, -1),
				"Report_No"=> $value->Report_No,
				"Version"=> $value->version,
				"Audit_Dates"=> substr($auddate,2) . " " . $audmonth . " " . $audyear,
				"Date_Submitted"=>$date_submitted_display,
				"Lead_Time_Submission"=>$date_submitted_lead_time,
				"Review_Date"=>$review_date,
				"Review_Lead_Time"=> $review_lead_time,
				"Approved_Date"=>$approved_date,
				"Approved_Lead_Time"=>$approved_lead_time,
				"Total_Lead_Time"=>$date_submitted_lead_time + $review_lead_time +$approved_lead_time,
				"Type_of_Audit"=>$type_of_audit,
				"Product_Type"=>$product_type_display,
				"Product_of_Interest"=>substr(trim($product_of_interest), 0, -1),
				"Disposition"=>substr(trim($disposition_display), 0, -1),
				"GMP_Score"=> $coverage_rate . "%" ,
				"Ratings"=>$gmp_rate . "%"
			);
		}
		
		return (object) $data;	
	}	
	public function get_csv2()
	{
		date_default_timezone_set('Asia/Manila');
		$date = date('Y-m-d');
		$filename = "Audit Report -" .$date. ".xlsx";
		$query = "";
		$select = $_GET['select'];
		$table = $_GET['table'];
		$join = $_GET['join'];
		$where = $_GET['where'];
		$result_array = "";
		if($join != ""){
			if($where != ""){
				$query = "SELECT " . $select . " FROM " . $table . " " . $join . " WHERE ".  $where . " AND a.status >= 0 AND a.status <=5 GROUP BY a.Report_No";
			} else {
				$query = "SELECT " . $select . " FROM " . $table . " " . $join . " WHERE a.status >= 0 AND a.status <=5 GROUP BY a.Report_No";
			}
		} else {
			if($where != ""){
				$query = "SELECT " . $select . " FROM " . $table . " WHERE ".  $where . " AND a.status >= 0 AND a.status <=5 GROUP BY a.Report_No";
			} else {
				$query = "SELECT " . $select . " FROM " . $table . " WHERE a.status >= 0 AND a.status <=5 GROUP BY a.Report_No";
			}
		}
		// echo $query;
		$report = $this->q_auditreport->get_data($query);
		$this->load->library("PHPExcel/Classes/PHPExcel.php");
		$obj = new PHPExcel();
		$obj->getProperties()->setCreator($this->session->userdata('sess_email'));
		//HEADERS
		$obj->setActiveSheetIndex(0)
            ->setCellValue('A1', 'No.')
            ->setCellValue('B1', 'Site Name')
            ->setCellValue('C1', 'Country')
            ->setCellValue('D1', "Province \n /State")
            ->setCellValue('E1', "Auditor \n(Lead)")
            ->setCellValue('F1', "Auditor's \n(Co-Auditor)")
            ->setCellValue('G1', 'Report No')
            ->setCellValue('H1', 'Version No')
            ->setCellValue('I1', "Date \n(Audit)")
            ->setCellValue('J1', "Date \n(Submitted)")
            ->setCellValue('K1', "Lead Time \n(Submission)")
            ->setCellValue('L1', "Date \n(Reviewed)")
            ->setCellValue('M1', "Lead Time \n(Review)")
            ->setCellValue('N1', "Date \n(Approved)")
            ->setCellValue('O1', "Lead Time \n(Approval)")
            ->setCellValue('P1', "Lead Time \n(Total)")
            ->setCellValue('Q1', 'Type of Audit')
            ->setCellValue('R1', 'Product-Type')
            ->setCellValue('S1', 'Products-of-Interest')
            ->setCellValue('T1', 'Disposition')
            ->setCellValue('U1', 'GMP Score (%)')
            ->setCellValue('V1', 'Coverage (%)');
        //GET DATA
        $no = 0;
        $total_result = count($report);
        $ave_total = 0;
        foreach ($report as $key => $value) {
        	$no ++;
        	$cell = $no + 1;
        	$report_id = $value->Report_ID;
			$report_audit_date = $this->q_auditreport->get_report_audit_date($report_id);
			$coauditors = $this->Template_model->get_data_by_id($report_id,'report_id','ar_coauditor');
			$co_auditors = "";
			foreach ($coauditors as $a) {
				$co_auditors .= $a->Co_Auditor_Name . ", ";
			}

			$date_submitted = $this->Template_model->get_data_by_id($report_id,'report_id','tbl_report_submission_date');
			$date_submitted_display = "-";
			if(count($date_submitted) > 0){
				$date_submitted_display = date("d F Y",strtotime($date_submitted[0]->reviewer));
			}

			$date_submitted_q = $this->Template_model->get_data_by_id($report_id,'report_id','csv_leadtime_submission');
			$date_submitted_lead_time = "-";
			if(count($date_submitted_q) > 0){
				$date_submitted_lead_time = $date_submitted_q[0]->leadtime;
			}

			$date_review = "-";

			$approval = $this->Template_model->get_data_by_id($report_id,'report_id','ar_lead_time_approval');
			$date_submitted_approval_lead_time = "0";
			$date_approved = "-";
			foreach ($approval as $c) {
				$date_submitted_approval_lead_time = $c->Lead_Time_Approval;
				$date_approved = date("d F Y",strtotime($c->approved_date));
			}
			$approval = $this->Template_model->get_data_by_id($report_id,'report_id','ar_lead_time_preparation');
			$preparation_lead_time = "0";
			foreach ($approval as $d) {
				$preparation_lead_time = $d->Lead_Time_Preparation;
			}
			$lead_time = $this->Template_model->get_data_by_id($report_id,'report_id','ar_lead_time_total');
			$lead_time_total = "0";
			foreach ($lead_time as $e) {
				if($e->Lead_Time_Total < 0){
					$lead_time_total =0;
					$ave_total += 0;
				} else {
					$lead_time_total = $e->Lead_Time_Total;
					$ave_total += $e->Lead_Time_Total;
				}
				
			}
			$product_type = $this->Template_model->get_data_by_id($report_id,'report_id','ar_product_type');
			$product_type_display = "";
			foreach ($product_type as $f) {
				$product_type_display .= $f->Product_Type . " ";
			}
			$product_interest = $this->Template_model->get_data_by_id($report_id,'report_id','ar_product_of_interest');
			$product_of_interest = "";
			foreach ($product_interest as $g) {
				$product_of_interest .= $g->product_of_interest_name . ", ";
			}
			$disposition = $this->Template_model->get_data_by_id($report_id,'report_id','ar_disposition');
			$disposition_display = "";
			foreach ($disposition as $g) {
				$disposition_display .= $g->disposition_name . ", ";
			}
			$gmp_computation = $this->Template_model->get_data_by_id($report_id,'report_id','gmp_computation_rating');
			$gmp_rate = "-";
			$gmp_rate = number_format($gmp_computation[0]->Rating,2);

			$coverage_computation = $this->Template_model->get_data_by_id($report_id,'report_id','gmp_computation_coverage');
			$coverage_rate = "-";
			$coverage_rate = number_format($coverage_computation[0]->Coverage,2);

			$audit_scope = $this->Template_model->get_data_by_id($report_id,'report_id','ar_type_of_audit');
			$type_of_audit = "-";
			foreach ($audit_scope as $j) {
				$type_of_audit = $j->scope_name;
			}
        	$len = count($report_audit_date);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($report_audit_date as $y) {
	            $audmonth = date_format(date_create($y->Date),"F");
	            $audyear = date_format(date_create($y->Date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($y->Date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($y->Date),"d");
	            }
	        }

			$obj->setActiveSheetIndex(0)
	            ->setCellValue('A'.$cell, $no)
	            ->setCellValue('B'.$cell, $value->Audited_Site)
	            ->setCellValue('C'.$cell, $value->country_name)
	            ->setCellValue('D'.$cell, $value->address2 . ", " . $value->address3)
	            ->setCellValue('E'.$cell, $value->Auditor_Name)
	            ->setCellValue('F'.$cell, substr(trim($co_auditors), 0, -1))
	            ->setCellValue('G'.$cell, $value->Report_No)
	            ->setCellValue('H'.$cell, $value->version)
	            ->setCellValue('I'.$cell, substr($auddate,2) . " " . $audmonth . " " . $audyear)
	            ->setCellValue('J'.$cell, $date_submitted_display)
	            ->setCellValue('K'.$cell, $preparation_lead_time)
	            ->setCellValue('L'.$cell, $date_review)
	            ->setCellValue('M'.$cell, $date_submitted_lead_time)
	            ->setCellValue('N'.$cell, $date_approved)
	            ->setCellValue('O'.$cell, $date_submitted_approval_lead_time)
	            ->setCellValue('P'.$cell, $lead_time_total)
	            ->setCellValue('Q'.$cell, $type_of_audit)
	            ->setCellValue('R'.$cell, $product_type_display)
	            ->setCellValue('S'.$cell, $product_of_interest)
	            ->setCellValue('T'.$cell, $disposition_display)
	            ->setCellValue('U'.$cell, $gmp_rate)
	            ->setCellValue('V'.$cell, $coverage_rate);
        	$obj->getActiveSheet()->getStyle('A'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('H'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('K'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('M'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('O'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('P'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('U'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle('V'.$cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		// print_r($report);
		// echo $total_result;
		$ave_total = $ave_total / $total_result; 
		$count =  $no + 3;
		$xl_leadtime_min = '="Min = " & MIN(K2:K'.$no.') &';
		$xl_leadtime_max = '"Max = " & MAX(K2:K'.$no.') &';
		$xl_leadtime_ave = '"Ave = " & AVERAGE(K2:K'.$no.')';
		$xl_leadtime = $xl_leadtime_min . ' CHAR(10) & '. $xl_leadtime_max . ' CHAR(10) & ' . $xl_leadtime_ave;
		$obj->getActiveSheet()->setCellValue('K'.$count, $xl_leadtime);
		$obj->getActiveSheet()->getStyle('K'.$count)
		                              ->getAlignment()
		                              ->setWrapText(true);
		$xl_review_min = '="Min = " & MIN(M2:M'.$no.') &';
		$xl_review_max = '"Max = " & MAX(M2:M'.$no.') &';
		$xl_review_ave = '"Ave = " & AVERAGE(M2:M'.$no.')';
		$xl_review = $xl_review_min . ' CHAR(10) & '. $xl_review_max . ' CHAR(10) & ' . $xl_review_ave;
		$obj->getActiveSheet()->setCellValue('M'.$count, $xl_review);
		$obj->getActiveSheet()->getStyle('M'.$count)
		                              ->getAlignment()
		                              ->setWrapText(true);
		$xl_approval_min = '="Min = " & MIN(O2:O'.$no.') &';
		$xl_approval_max = '"Max = " & MAX(O2:O'.$no.') &';
		$xl_approval_ave = '"Ave = " & AVERAGE(O2:O'.$no.')';
		$xl_approval = $xl_approval_min . ' CHAR(10) & '. $xl_approval_max . ' CHAR(10) & ' . $xl_approval_ave;
		$obj->getActiveSheet()->setCellValue('O'.$count, $xl_approval);
		$obj->getActiveSheet()->getStyle('O'.$count)
		                              ->getAlignment()
		                              ->setWrapText(true);
		$xl_total_min = '="Min = " & MIN(P2:P'.$no.') &';
		$xl_total_max = '"Max = " & MAX(P2:P'.$no.') &';
		$xl_total_ave = '"Ave = " & AVERAGE(P2:P'.$no.')';
		$xl_total = $xl_approval_min . ' CHAR(10) & '. $xl_total_max . ' CHAR(10) & ' . $xl_total_ave;
		$obj->getActiveSheet()->setCellValue('P'.$count, $xl_total);
		$obj->getActiveSheet()->getStyle('P'.$count)
		                              ->getAlignment()
		                              ->setWrapText(true);
        //SETUP STYLE, WIDTH AND HEIGHT
        $header_style = array(
	    'font'  => array(
	        'color' => array('rgb' => 'FFFFFF'),
	    ));
        $obj->getActiveSheet(0)->getColumnDimension('A')->setWidth(5);    	
        $obj->getActiveSheet(0)->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getActiveSheet(0)->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('5a5a5a');
        $obj->getActiveSheet(0)->getStyle('A1')->applyFromArray($header_style);
        foreach ( range("B","V") as $value) {
        	$obj->getActiveSheet(0)->getColumnDimension($value)->setAutoSize(true);  
        	$obj->getActiveSheet(0)->getStyle($value."1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	$obj->getActiveSheet()->getStyle($value.'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('5a5a5a');
        	$obj->getActiveSheet(0)->getStyle($value."1")->applyFromArray($header_style);
        	$obj->getActiveSheet(0)->getStyle($value."1")->getAlignment()->setWrapText(true);
        }
        $obj->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $obj->getActiveSheet()->getStyle("A1:V1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $obj->getActiveSheet()->getStyle("A1:V1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getActiveSheet()->setTitle($filename);
        $obj->setActiveSheetIndex(0);
        // DOWNLOAD EXCEL
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		$objWriter->setPreCalculateFormulas(true);
		$objWriter->save('php://output');
		exit();
	}
	public function count_total_question()
	{
		$id = $_POST['report_id'];
        $query = "report_id = " . $id;
        $report = $this->q_auditreport->get_list($query,1,0, "");
        $template_id = $report[0]->Template_ID;
        //get number of questions
        $questions = count($this->Template_model->get_data_by_id($template_id,'template_id','tbl_questions'));
        //get number of answers
        $answers = count($this->Template_model->get_data_by_id($id,'report_id','tbl_report_answers'));
        $unanswered = (int)$questions - (int)$answers;
        echo json_encode(array("count"=>$unanswered, "template_id"=>$template_id));
	}

	public function unanswered_question()
	{
		$id = $_GET['report_id'];
		$template = $_GET['template_id'];
		$element_order =1 ;
		$count_no = 0;
		$elements_result = $this->Preview_report_model->preview_report_get_element($template);
		$element_array = array();
        foreach ($elements_result as $key => $value) {

        	$questions = $this->Template_model->get_data_by_id($value->element_id,'element_id','tbl_questions');
            $questions_array = array();
        	foreach ($questions as $k => $v) {

        		$answer = count($this->Template_model->get_data_question_answer($v->question_id, $id));
                $questions_array[] = array(
                    "question_id"=> $v->question_id,
                    "question"=> $v->question,
                    "answered"=> $answer,
                );
            }



            $element_array[] = array(
                "element_id"=> $value->element_id,
                "element_name"=> $value->element_name,
                "order"=> $element_order,
                "questions" => $questions_array
            );

            $element_order++;
        }

		echo json_encode($element_array);
	}
}
