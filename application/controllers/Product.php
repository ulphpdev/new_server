<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Taipei');
class Product extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('product_model');
        $this->load->model('global_model');
        $this->load->model('Audit_trail_model');
        $this->load->helper('url');
    }  
    function get_productList(){
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
	  	$offset = ($_POST['offset']-1)* $limit;
		$sort = $_POST['order_by'];
		$data = $this->product_model->getlist_product_model($limit,$offset,$sort);
		echo json_encode($data);
	}
	function edit_product(){
		$id = $_POST['id'];
		$data = $this->product_model->get_product($id);
		echo json_encode($data);
	}
	function product_array(){
		$table = $_POST['table'];
		$field = $_POST['field'];
		$data = array(
			'product_id' => $_POST['id'],
			'company_id' => $_POST['name_site'],
			// 'type' => $_POST['type'],
			'product_name' => strip_tags($_POST['name']),
			'update_date' => date('Y-m-d H:i:s')
		);
		$data = json_decode(json_encode($data), FALSE);
		$this->global_model->update_data($field,  $_POST['id'], $table, $data);
	}
	function save_product(){ 
		$table = $_POST['table'];
		$field = $_POST['field'];
		$data = array(
			'company_id' => $_POST['name_site'],
			// 'type' => $_POST['type'],
			'product_name' => strip_tags($_POST['name']),
			'create_date' => date('Y-m-d H:i:s'),
			'update_date' => date('Y-m-d H:i:s'),
		);
		$data = json_decode(json_encode($data), FALSE);
		$this->global_model->save_data($table, $data);
	}
	public function save_audit_trail($action){
	    $data['user'] = $this->session->userdata('userid');
	    $data['page'] = $this->agent->referrer();
	    $data['type'] = $this->session->userdata('type');
	    $data['role'] = $this->session->userdata('sess_role');
	    $data['email'] = $this->session->userdata('sess_email');
	    $data['action'] = ucwords($action);
	    $data['date'] = date('Y-m-d H:i:s');
	    $table = 'tbl_audit_trail';
	    $this->Audit_trail_model->save_data($table,$data);
	}
}
?>