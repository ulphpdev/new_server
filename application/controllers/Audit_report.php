<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Audit_report extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		  $this->load->library('Pdf');
		$this->load->model('Audit_report_model');
		$this->load->helper('url');
	}
	public function index()
	{	
	}
	function get_report_list(){
		$limit = $_POST['limit'];
		$offset = ($_POST['offset']-1)* $limit;
		$data = $this->Audit_report_model->get_report_list_model($limit,$offset);
		echo json_encode($data);
	}
	public function get_pdf()
    {
        $report_id = $_POST['report_id'];
        $data  = $this->Audit_report_model->get_pdf($report_id);
        echo json_encode($data);
        $this->load->view('edit');
    }
    function get_report() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_report($id);
	  echo json_encode($data);
	}
	function get_report_table() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_report_table($id);
	  echo json_encode($data);
	}
	function get_supplier_history() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_supplier_history($id);
	  echo json_encode($data);
	}
	function get_inspector() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_inspector($id);
	  echo json_encode($data);
	}
	function get_changes_report() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_changes_report($id);
	  echo json_encode($data);
	}
	function get_activity_name() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_activity_name($id);
	  echo json_encode($data);
	}
	function get_scope_name() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_scope_name($id);
	  echo json_encode($data);
	}
	function get_product_name() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_product_name($id);
	  echo json_encode($data);
	}
	function get_audit_references() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_audit_references($id);
	  echo json_encode($data);
	}
	function get_audit_document() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_audit_document($id);
	  echo json_encode($data);
	}
	function get_audit_area() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_audit_area($id);
	  echo json_encode($data);
	}
	function get_not_audit_area() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_not_audit_area($id);
	  echo json_encode($data);
	}
	function get_name_closeup_meeting() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_name_closeup_meeting($id);
	  echo json_encode($data);
	}
	function get_name_inspection() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_name_inspection($id);
	  echo json_encode($data);
	}
	function get_distribution() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_distribution($id);
	  echo json_encode($data);
	}
	function get_template_reference() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_template_reference($id);
	  echo json_encode($data);
	}
	function get_template_element() {
	  $id = $_POST['id'];
	  // $element_id = $_POST['element_id'];
	  $data = $this->Audit_report_model->get_template_element($id);
	  echo json_encode($data);
	}
	function get_question() {
	  // $id = $_POST['id'];
	  // $element_id = $_POST['element_id'];
	  $element_id = $_POST['element_id'];
	  $template_id = $_POST['template_id'];
	  $data = $this->Audit_report_model->get_question($element_id, $template_id);
	  echo json_encode($data);
	}
	function get_element_noanswer() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_element_noanswer($id);
	  echo json_encode($data);
	}
	function get_disposition() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_disposition($id);
	  echo json_encode($data);
	}
	function get_other_audit_report() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_other_audit_report($id);
	  echo json_encode($data);
	}
	function get_executive_summary() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_executive_summary($id);
	  echo json_encode($data);
	}
	function get_remarks() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_remarks($id);
	  echo json_encode($data);
	}
	//start of audit email getting credencials
	function get_email_lead_auditor() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_email_lead_auditor($id);
	  echo json_encode($data);
	}
	function get_email_co_auditor() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_email_co_auditor($id);
	  echo json_encode($data);
	   // echo "<pre>";
    // 	print_r($data);
    // 	die();
	}
	function get_email_department_head() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_email_department_head($id);
	  echo json_encode($data);
	}
	function get_email_division_head() {
	  $id = $_POST['id'];
	  $data = $this->Audit_report_model->get_email_division_head($id);
	  echo json_encode($data);
	}
	// query modal for status
	function check_status() {
	  $report_id = $_POST['report_id'];
      $data = $this->Audit_report_model->check_status($report_id);
	  $co_auditor_count = $this->Audit_report_model->count_co_auditor($report_id);
	  $reviewer = $this->Audit_report_model->get_email_department_head($report_id);
	  $approver = $this->Audit_report_model->get_email_division_head($report_id);
	  	$result[] = array(
            "status"=> $data[0]->status,
	  		"co_auditor_count"=> $co_auditor_count[0]->co_auditor,
	  		"reviewer"=> "Department Head : " . $reviewer[0]->fname . " " . $reviewer[0]->lname,
	  		"approver"=> "Division Head : " . $approver[0]->fname . " " . $approver[0]->lname,
	 	);
	  echo json_encode($result);
	  // return;
	}
	//nolie 9/8/17
	function getcount_search_classification() {
	  $classification_id = $_POST['id'];
	  $table = $_POST['table'];
	  $order_by = $_POST['order_by'];
	  $per_page = $_POST['limit'];
	  $limit = '9999999';
	  $offset = '0';
	  $data = count($this->Audit_report_model->get_search_classification($classification_id, $offset, $limit));
	  // $data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));
	  $page_count = ceil($data/$per_page);
	  echo $page_count;
	}
	function getcount_search_disposition() {
	  $disposition_id = $_POST['id'];
	  $table = $_POST['table'];
	  $order_by = $_POST['order_by'];
	  $per_page = $_POST['limit'];
	  $limit = '9999999';
	  $offset = '0';
	  $data = count($this->Audit_report_model->get_search_disposition($disposition_id, $offset, $limit));
	  // $data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));
	  $page_count = ceil($data/$per_page);
	  echo $page_count;
	}
	function getcount_search_country() {
	  $id = $_POST['id'];
	  $table = $_POST['table'];
	  $order_by = $_POST['order_by'];
	  $per_page = $_POST['limit'];
	  $limit = '9999999';
	  $offset = '0';
	  $data = count($this->Audit_report_model->get_search_country($id, $offset, $limit));
	  // $data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));
	  $page_count = ceil($data/$per_page);
	  echo $page_count;
	}
	function getcount_search_province() {
	  $province_id = $_POST['id'];
	  $table = $_POST['table'];
	  $order_by = $_POST['order_by'];
	  $per_page = $_POST['limit'];
	  $limit = '9999999';
	  $offset = '0';
	  $data = count($this->Audit_report_model->get_search_country($province_id, $offset, $limit));
	  // $data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));
	  $page_count = ceil($data/$per_page);
	  echo $page_count;
	}
	function getcount_search_product() {
	  $product_id = $_POST['id'];
	  $table = $_POST['table'];
	  $order_by = $_POST['order_by'];
	  $per_page = $_POST['limit'];
	  $limit = '9999999';
	  $offset = '0';
	  $data = count($this->Audit_report_model->get_search_country($product_id, $offset, $limit));
	  // $data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));
	  $page_count = ceil($data/$per_page);
	  echo $page_count;
	}
	function getcount_search_lead_auditor() {
	  $auditor_id = $_POST['id'];
	  $table = $_POST['table'];
	  $order_by = $_POST['order_by'];
	  $per_page = $_POST['limit'];
	  $limit = '9999999';
	  $offset = '0';
	  $data = count($this->Audit_report_model->audit_search_auditor($auditor_id, $limit, $offset));
	  // $data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));
	  $page_count = ceil($data/$per_page);
	  echo $page_count;
	}
	function getcount_search_co_auditor() {
	  $auditor_id = $_POST['id'];
	  $table = $_POST['table'];
	  $order_by = $_POST['order_by'];
	  $per_page = $_POST['limit'];
	  $limit = '9999999';
	  $offset = '0';
	  $data = count($this->Audit_report_model->audit_search_auditor($auditor_id, $limit, $offset));
	  // $data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));
	  $page_count = ceil($data/$per_page);
	  echo $page_count;
	}
	function getcount_search_date_of_audit() {
	  $table = 'tbl_report_summary';
	  // $table = $_POST['table'];
	  $date1 = $_POST['date1'];
	  $date2 = $_POST['date2'];
	  $order_by = $_POST['order_by'];
	  $per_page = $_POST['limit'];
	  $limit = '9999999';
	  $offset = '0';
	  $data = count($this->Audit_report_model->get_search_date_of_audit($date1,$date2,$limit,$offset));
	  // $data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));
	  $page_count = ceil($data/$per_page);
	  echo $page_count;
	}
	function getcount_search_date_of_report_submission() {
	  $table = 'tbl_report_summary';
	  // $table = $_POST['table'];
	  $create_date = $_POST['create_date'];
	  // $date2 = $_POST['date2'];
	  $order_by = $_POST['order_by'];
	  $per_page = $_POST['limit'];
	  $limit = '9999999';
	  $offset = '0';
	  $data = count($this->Audit_report_model->get_search_date_of_audit($create_date,$limit,$offset));
	  // $data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));
	  $page_count = ceil($data/$per_page);
	  echo $page_count;
	}
	function audit_search() {
	  $table = $_POST['table'];
	  $data = $this->Audit_report_model->audit_search($table);
	  echo json_encode($data);
	  // return;
	}
	function audit_search_keyword()
    {
    	$table = $_POST['table'];
    	$field_name = $_POST['field_name'];
        $keyword = $_POST['search_input'];
       $data = $this->Audit_report_model->audit_search_keyword($table, $field_name, $keyword);
        // echo 'success';
        echo json_encode($data);
    }
    function audit_search_auditor()
    {
    	// $table = $_POST['table'];
    	$limit = $_POST['limit'];
    	$offset = $_POST['offset'];
    	$auditor_id = $_POST['auditor_id'];
        // $keyword = $_POST['search_input'];
       $data = $this->Audit_report_model->audit_search_auditor($auditor_id, $limit, $offset);
        // echo 'success';
        echo json_encode($data);
    }
    function get_search_classification()
    {
    	$classification_id = $_POST['classification_id'];
    	$limit = $_POST['limit'];
    	$offset = $_POST['offset'];
       $data = $this->Audit_report_model->get_search_classification($classification_id, $offset, $limit);
        // echo 'success';
        echo json_encode($data);
    }
    function get_search_disposition()
    {	
    	$limit = $_POST['limit'];
    	$offset = $_POST['offset'];
    	$disposition_id = $_POST['disposition_id'];
       $data = $this->Audit_report_model->get_search_disposition($disposition_id, $offset, $limit);
        // echo 'success';
        echo json_encode($data);
    }
    //nolie 9/11/17
    function get_search_country()
    {
    	$limit = $_POST['limit'];
    	$offset = $_POST['offset'];
    	$id = $_POST['id'];
       $data = $this->Audit_report_model->get_search_country($id, $offset, $limit);
        // echo 'success';
        echo json_encode($data);
    }
    function get_search_province()
    {
    	$limit = $_POST['limit'];
    	$offset = $_POST['offset'];
    	$province_id = $_POST['province_id'];
       $data = $this->Audit_report_model->get_search_province($province_id, $offset, $limit);
        // echo 'success';
        echo json_encode($data);
    }
    function get_search_auditor()
    {
    	$auditor_id = $_POST['auditor_id'];
       $data = $this->Audit_report_model->get_search_auditor($auditor_id);
        // echo 'success';
        echo json_encode($data);
    }
     function get_search_co_auditor()
    {
    	$limit = $_POST['limit'];
    	$offset = $_POST['offset'];
    	$auditor_id = $_POST['auditor_id'];
       $data = $this->Audit_report_model->get_search_co_auditor($auditor_id, $limit, $offset);
        // echo 'success';
        echo json_encode($data);
    }
     function get_search_product_interest()
    {
    	$limit = $_POST['limit'];
    	$offset = $_POST['offset'];
    	$product_id = $_POST['product_id'];
       $data = $this->Audit_report_model->get_search_product_interest($product_id, $limit, $offset);
        // echo 'success';
        echo json_encode($data);
    }
    function get_search_date_of_audit()
    {
    	$limit = $_POST['limit'];
    	$offset = $_POST['offset'];
    	$date1 = $_POST['date1'];
    	$date2 = $_POST['date2'];
       $data = $this->Audit_report_model->get_search_date_of_audit($date1, $date2, $limit, $offset);
        // echo 'success';
        echo json_encode($data);
    }
    function date_of_report_submission()
    {
    	$limit = $_POST['limit'];
    	$offset = $_POST['offset'];
    	$table = $_POST['table'];
    	$create_date = $_POST['create_date'];
       $data = $this->Audit_report_model->date_of_report_submission($table, $create_date, $limit, $offset);
        // echo 'success';
        echo json_encode($data);
    }
	}
