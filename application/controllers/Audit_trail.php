<?php 

defined('BASEPATH') OR exit('No direct script access allowed');



class Audit_trail extends CI_Controller{

	public function __construct() {

		parent::__construct();

		$this->load->library('session');

		$this->load->model('Audit_trail_model2');

		$this->load->helper('url');

		$this->load->model('global_model');



	}



	public function index()

	{

		if($this->session->userdata('sess_email')=='' ) { 

			$this->load->view('login');

		} else {

			$data['content' ]= 'audit_trail/list2';

			$this->load->view('layout/layout',$data);

		}

	}



	




	public function get_list_audit_trail(){

		// header('Content-type: Application/JSON');

		$limit = $_POST['limit'];

		$offset = $_POST['offset'];

		$query = $_POST['query'];


		echo json_encode($this->Audit_trail_model2->get_audit_trail_list($query,$limit,$offset), JSON_PRETTY_PRINT);

	}
}



?>