<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: origin, x-requested-with, content-type");
header("Access-Control-Allow-Methods: PUT, GET, POST");

class Api extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Api_model');
		$this->load->model('Global_model');
		$this->load->model('q_auditreport');
		$this->load->model('Preview_report_model');
		$this->load->model('Audit_report_model');
		$this->load->model('Template_model');
		$this->load->model('Audit_report_analysis_model');
        $this->load->model('Audit_report_analysis_model2');
        $this->load->model('Audit_trail_model');
		$this->load->model('Audit_report_model');
		$this->load->helper('url');
		$this->load->library('email');
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        date_default_timezone_set("Asia/Manila");
	}



	public function index()
	{
		$data = $_POST;
        $token = $this->Api_model->checkToken($data['token']);
        if($token[0]->count == 0){
            echo "Invalid Token";
        } else {
            switch ($data['cmdEvent']) {
                case 'postInput':

                    if($data['report_id'] == ''){
                        $this->saveReport($data);

                    }else{

                        if($this->check_report_status($data['report_id']) == 0){
                            $this->updateReport($data);
                        } else if($this->check_report_status($data['report_id']) == 1){
                            $this->updateReport($data);
                        } else if($this->check_report_status($data['report_id']) == 2){
                            $this->updateReport($data);
                        } else if($this->check_report_status($data['report_id']) == 6){
                            $this->updateReport($data);
                        } else if($this->check_report_status($data['report_id']) == 7){
                            $this->updateReport($data);
                        } else {
                            echo json_encode(array('status' => 'failed','message'=> 'Report is already for approval or already approved.'));
                            die();
                        }
                    } 

                    break;

                case 'CheckAccount':

                    if($data['email'] == ''){
                        $status = 'failed';
                        $msg = 'No email address.';
                    }else{
                        $status = 'success';
                        $msg = $this->checkemail($data['email']);
                    }
                    echo json_encode(array('status' => $status,'message'=>$msg));

                    break;
                    
                case 'modifiedList':

                    if(isset($data['date'])){
                        if($data['date'] != ""){
                            $this->DMFilter($data['date']);
                        } else {
                            echo json_encode(array('status' => 'failed','message'=>'Date is Empty'));
                        }
                    } else {
                        echo json_encode(array('status' => 'failed','message'=>'Date not Set'));
                    }

                    break;            

                case 'EmailtoLead':
                    if(isset($data['report_id']) && isset($data['co_auditor_email']) && isset($data['co_auditor_name'])){
                        if($data['report_id'] != ""){
                            if($data['co_auditor_email'] != ""){
                                if($data['co_auditor_name'] != ""){
                                    //API LOGS
                                    $api_logs = array(
                                        "json"              => json_encode($data),
                                        "report_id"         => $data['report_id'],
                                        "action"            => "EmailtoLead",
                                        "auditor_id"        => 0,
                                        "submission_date"   => date("Y-m-d H:i:s")
                                    );
                                    $this->Api_model->save_data($api_logs,"tbl_api_logs");

                                    //ADD STATUS
                                    $this->Audit_report_model->add_report_submission_status_coauditor($data['report_id'],"Revision by Co-Auditor","Submitted Audit Report Revision","Co-Auditor",$data['co_auditor_name']);

                                    //audit trail
                                    $this->update_audit_trail($data['co_auditor_email'], "UPDATE REPORT", $data['report_id']);

                                    $this->Email_to_Lead($data['report_id'],$data['co_auditor_email'],$data['co_auditor_name']);
                                    echo json_encode(array('status' => 'success','message'=>'Email Sent'));
                                } else {
                                    echo json_encode(array('status' => 'failed','message'=>'Co-Auditor Name is Empty'));
                                }
                            } else {
                                echo json_encode(array('status' => 'failed','message'=>'Co-Auditor Email is Empty'));
                            }
                        } else {
                            echo json_encode(array('status' => 'failed','message'=>'Report ID is Empty'));
                        }
                    } else {
                        echo json_encode(array('status' => 'failed','message'=>'Required Field is not Set'));
                    }
                    break;        
                                
				case 'authenticate':
								
                    $this->login($data);
                    //audit trail
                    $this->login_audit_trail($data['email'], "API Login");
                    break;   

                case 'authenticate_web':
                    $this->login($data);
                    break;

                default:
                    echo json_encode(array('status' => 'failed','message'=>'Invalid Event'));
                    break;
            }
        }
	}
	
	function checkemail($email){
		$where = "email = '".$email."' ";
		$data = $this->Global_model->check_exist('tbl_cms_login', $where);
		$count = $data[0]->count;
		if($count == 0){
			return false;
		}else{
			return true;
		} 
	}
	public function template_list(){
		$result['content'] = $this->Api_model->get_template_list();
		$result['file'] = 'template/template_list';
		return $this->create_json($result);
	}
	public function audit_report_list(){
		$array = $this->Api_model->get_audit_report_list();

        $listing = array();
        foreach ($array as $key => $value) {
            $listing[] = array(
                "report_id"         => $value->report_id,
                "report_no"         => $value->report_no,
                "status"            => $value->status,
                "modified_date"     => $this->Api_model->get_lasst_approval_history_date($value->report_id, $value->modified_date)
            );
        }


		$data = array("audit_report"=>$listing);
		$result['content'] = $data;
		$result['file'] = 'audit_report/audit_report_list';
		return $this->create_json($result);
	}
	public function template_info($id=null){
		$result['content'] = $this->Api_model->get_template_info($id);
		$result['file'] = 'template/'.$id;
		return $this->create_json($result);
	}
    public function site(){
        $result['content'] = $this->Api_model->get_site();
        $result['file'] = 'data_maintenance/site';
        return $this->create_json($result);

    }
	public function audit_history(){
		$result['content'] = $this->Api_model->get_site_history();
		$result['file'] = 'data_maintenance/audit_history';
		return $this->create_json($result);

	}
	public function approver(){
		$result['content'] = $this->Api_model->get_approver();
		$result['file'] = 'data_maintenance/approver';
		return $this->create_json($result);
	}
	public function auditor(){
		$result['content'] = $this->Api_model->get_auditor();
		$result['file'] = 'data_maintenance/auditor';
		return $this->create_json($result);
	}
	public function reviewer(){
		$result['content'] = $this->Api_model->get_reviewer();
		$result['file'] = 'data_maintenance/reviewer';
		return $this->create_json($result);
	}
	public function question_category(){
		$result['content'] = $this->Api_model->get_question_category();
		$result['file'] = 'data_maintenance/category';
		return $this->create_json($result);
	}
	public function product(){
		$result['content'] = $this->Api_model->get_product();
		$result['file'] = 'data_maintenance/product';
		return $this->create_json($result);
	}
	public function classification(){
		$result['content'] = $this->Api_model->get_classification();
		$result['file'] = 'data_maintenance/classification';
		return $this->create_json($result);
	}
	public function standard_reference(){
		$result['content'] = $this->Api_model->get_standard_reference();
		$result['file'] = 'data_maintenance/standard_reference';
		return $this->create_json($result);
	}
	public function type_audit(){
		$result['content'] = $this->Api_model->get_type_audit();
		$result['file'] = 'data_maintenance/type_audit';
		return $this->create_json($result);
	}
	public function disposition(){
		$result['content'] = $this->Api_model->get_disposition();
		$result['file'] = 'data_maintenance/disposition';
		return $this->create_json($result);
	}
	public function distribution(){
		$result['content'] = $this->Api_model->get_distribution();
		$result['file'] = 'data_maintenance/distribution';
		return $this->create_json($result);
	}

	public function update_dm()
	{
		$this->site();
		$this->approver();
		$this->auditor();
		$this->reviewer();
		$this->question_category();
		$this->product();
		$this->classification();
		$this->standard_reference();
		$this->type_audit();
		$this->disposition();
		$this->distribution();
	}

	function create_json($result){
		$file = getcwd().'/json/'.$result['file'].'.json';
		file_put_contents($file,'');
		chmod($file,0777);
		$fp = fopen($file, 'w');
		fwrite($fp, json_encode($result['content']));
		fclose($fp);
	}
	
	function saveReport($post){
		if(isset($post['status'])){
			echo json_encode(array('status' => 'failed','message'=>'Status Exist'));
			exit;  
		}
		$in_array = array(
            "co_auditor_id",
            "scope",
            "pre_audit_documents",
            "references",
            "inspection",
            "inspector",
            "personnel",
            "activities",
            "question",
            "distribution"
        );

		$status ='';
		$x = 0;
		$error_message = "";
		foreach ($post as $key => $value) {
			if(in_array($key,$in_array)){
				if(!is_array(json_decode($value))){
					$x++;
					$error_message .= $key . " ";				
				}
			}
		}
		$key_error = array(
            'status'        => 'failed', 
            'message'       => $error_message . ' not in array', 
            'report_id'     => '', 
            'report_no'     => '', 
            'report_status' => ''
        );

		if($x > 0){
			echo json_encode($key_error);
		}else{
    		// REPORT_SUMMARY
    		$rsummary = array(
    			'template_id'        => $post['template_id'],
    			'company_id'         => $post['company_id'],
    			'other_activities'   => $post['other_activities'],
    			'auditor_id'         => $post['auditor_id'],
    			'audited_areas'      => $post['audited_areas'],
    			'areas_to_consider'  => $post['areas_to_consider'],
    			'wrap_up_date'       => $post['wrap_up_date'],
    			'create_date'        => date("Y-m-d H:i:s"),
    			'update_date'        => date("Y-m-d H:i:s"),
    			'status'             => 0,
    			'version'            => $post['version'],
    			'head_lead'          => $post['head_lead']
    		);
    		$table_summary = 'tbl_report_summary';
    		$report_id = $this->Api_model->save_data($rsummary,$table_summary);
    		

    		//API LOGS
    		$api_logs = array(
    			"json"               => json_encode($post),
    			"report_id"          => $report_id,
    			"action"             => "Save",
    			"auditor_id"         => $post['auditor_id'],
    			"submission_date"    => date("Y-m-d H:i:s")
    		);
    		$this->Api_model->save_data($api_logs,"tbl_api_logs");

    		//insert to Audit Date Table 
    		$this->update_audit_date($report_id,$post['audit_date']);
    		$this->other_issues_audit($report_id,$post['other_issues_audit']);
    		$this->other_issues_executive($report_id,$post['other_issues_executive']);

    		// $this->update_wrapup_date($report_id,$post['wrap_up_dates']);
    		$report_no = sprintf("%03s",$report_id);
    		$dateYear = date('y');
    		$report_no_format = 'GMP-'.$dateYear.'-'.$report_no;
    		$data1 = array(
    			'report_no'  => $report_no_format,
    		);
    		$tbl1 = 'tbl_report_summary';
    		$this->Api_model->update_data('report_id',$report_id,$tbl1,$data1);

            //audit trail
            $this->add_audit_trail($post['auditor_id'], "CREATE REPORT", $report_no_format);
    		

    		// CO-AUDITOR
    		$co_auditor_id = $post["co_auditor_id"];
    		$a = json_decode($co_auditor_id);
    		foreach ($a as $key => $value) {
    			$data = array(
    				'report_id'     => $report_id,
    				'auditor_id'    => $value->auditor_id,
    				'create_date'   => date('Y-m-d H:i:s'),
    				'update_date'   => date('Y-m-d H:i:s'),
    			);
    			$table = 'tbl_co_auditors';
    			$this->Api_model->save_data($data,$table);
    		}	


    		// REVIEWER
    		$reviewer_id = $post["reviewer_id"];
    		$b = json_decode($reviewer_id);
    		$data = array(
    			'report_id'      => $report_id,
    			'reviewer_id'    => $reviewer_id,
    			'create_date'    => date('Y-m-d H:i:s'),
    			'update_date'    => date('Y-m-d H:i:s'),
    		);
    		$table = 'tbl_reviewer';
    		$this->Api_model->save_data($data,$table);


    		// APPROVER
    		$approver_id = $post["approver_id"];
    		$c = json_decode($approver_id);
    		$data = array(
    			'report_id'      => $report_id,
    			'approver_id'    => $approver_id,
    			'create_date'    => date('Y-m-d H:i:s'),
    			'update_date'    => date('Y-m-d H:i:s'),
    		);
    		$table = 'tbl_approver';
    		$this->Api_model->save_data($data,$table);


    		// SCOPE
    		$scope = $post["scope"];
    		$d = json_decode($scope);
    		foreach ($d as $key => $value) {
    			$data = array(
    				'audit_scope_id'    => $value->scope_id,
    				'report_id'         => $report_id,				
    				'scope_detail'      => $value->scope_remarks,
    				'create_date'       => date('Y-m-d H:i:s'),
    				'update_date'       => date('Y-m-d H:i:s'),
    			);

    			$table = 'tbl_report_audit_scope';
    			$this->Api_model->save_data($data,$table);
    			$audit_scope_id = $value->scope_id;
    			foreach ($value->scope_product as $key => $value) {
    				$data = array(
    					'audit_scope_id'   => $audit_scope_id,
    					'report_id'        => $report_id,
    					'product_id'       => $value->product_id,
    					'disposition_id'   => $value->disposition_id,
    					'create_date'      => date('Y-m-d H:i:s'),
    					'update_date'      => date('Y-m-d H:i:s'),
    				);
    				$table = 'tbl_report_scope_product';
    				$this->Api_model->save_data($data,$table);
    			}
    		}

    		//TRANSLATOR
    		if($post["translator"] != null || $post["translator"] != ""){
    			$translator = $post["translator"];
    			$x = json_decode($translator);
    			foreach ($x as $key => $value) {
    				$data = array(
    					'report_id'    => $report_id,
    					'translator'   => $value->translator,
    					);
    				$table = 'tbl_report_translator';
    				$this->Api_model->save_data($data,$table);
    			} 
    		}


    		//PRE-DOCUMENTS
    		$document = $post["pre_audit_documents"];
    		$f = json_decode($document);
    		foreach ($f as $key => $value) {
    			$data = array(
    				'report_id'         => $report_id,	
    				'document_name'     => $value->document_name,			
    				'create_date'       => date('Y-m-d H:i:s'),
    				'update_date'       => date('Y-m-d H:i:s'),
    			);
    			$table = 'tbl_pre_audit_documents';
    			$this->Api_model->save_data($data,$table);
    		} 

    		//REFERENCES
    		$references = $post["references"];
    		$g = json_decode($references);
    		foreach ($g as $key => $value) {
    			$data = array(
    				'report_id'         => $report_id,	
    				'reference_name'    => $value->reference_name,	
    				'issuer'            => $value->issuer,
    				'reference_no'      => $value->reference_no,
    				'validity'          => $value->validity,		
    				'create_date'       => date('Y-m-d H:i:s'),
    				'update_date'       => date('Y-m-d H:i:s'),
    				'issued'            => $value->issued
    			);
    			$table = 'tbl_references';
    			$this->Api_model->save_data($data,$table);
    		} 

    		// INSPECTION CHANGES
    		$inspection = $post["inspection"];
    		$h = json_decode($inspection);
    		foreach ($h as $key => $value) {
    			$data = array(
    				'report_id'     => $report_id,	
    				'changes'       => $value->changes,		
    				'create_date'   => date('Y-m-d H:i:s'),
    				'update_date'   => date('Y-m-d H:i:s'),
    				);
    			$table = 'tbl_inspection_changes';
    			$this->Api_model->save_data($data,$table);
    		} 

    		// PERSONNEL
    		$personnel = $post["personnel"];
    		$k = json_decode($personnel);
    		foreach ($k as $key => $value) {
    			$data = array(
    				'report_id'     => $report_id,	
    				'name'          => $value->name,		
    				'designation'   => $value->designation,
    				'create_date'   => date('Y-m-d H:i:s'),
    				'update_date'   => date('Y-m-d H:i:s'),
    			);
    			$table = 'tbl_personnel';
    			$this->Api_model->save_data($data,$table);
    		} 


    		// PRESENT DURING MEETING
    		$present = $post["present_during_meeting"];
    		$pre = json_decode($present);
    		foreach ($pre as $key => $value) {
    			$data = array(
    				'report_id'     => $report_id,	
    				'name'          => $value->name,		
    				'position'      => $value->position,
    				'create_date'   => date('Y-m-d H:i:s'),
    				'update_date'   => date('Y-m-d H:i:s'),
    			);
    			$table = 'tbl_present_during_meeting';
    			$this->Api_model->save_data($data,$table);
    		} 

    		// ACTIVITIES
    		$activities = $post["activities"];
    		$l = json_decode($activities);
    		foreach ($l as $key => $value) {
    			$activity_id = $value->activity_id;
    			if(count($value->sub_activities) > 0){
    				foreach ($value->sub_activities as $key => $value) {
    					$data = array(
    						'sub_item_id'     => $value->sub_item_id,
    						'report_id'       => $report_id,
    						'activity_id'     => $activity_id,
    						'create_date'     => date('Y-m-d H:i:s'),
    						'update_date'     => date('Y-m-d H:i:s'),
    					);
    					$table = 'tbl_report_activities';
    					$this->Api_model->save_data($data,$table);
    				}
    			}else {
    				$data = array(
    					'sub_item_id'      => "",
    					'report_id'        => $report_id,
    					'activity_id'      => $activity_id,
    					'create_date'      => date('Y-m-d H:i:s'),
    					'update_date'      => date('Y-m-d H:i:s'),
    				);
    				$table = 'tbl_report_activities';
    				$this->Api_model->save_data($data,$table);
    			}
    		}


    		// QUESTION_ANSWER
    		$question = $post["question"];
            
    		$m = json_decode($question);
    		foreach ($m as $key => $value) {
    			$cat_id = null;
                if($value->answer_id == 2){
                    $cat_id = $value->category_id;
                }
    			$data = array(
    				'report_id'         => $report_id,	
    				'question_id'       => $value->question_id,
    				'answer_id'         => $value->answer_id,		
    				'answer_details'    => $value->answer_details,
    				'category_id'       => $cat_id,
    				'create_date'       => date('Y-m-d H:i:s'),
    				'update_date'       => date('Y-m-d H:i:s'),
    			);
    			$table = 'tbl_report_answers';
    			$this->Api_model->save_data($data,$table);
    		} 



    		//DISTRIBUTION
    		$distribution = $post["distribution"];
    		$dis = json_decode($distribution);
    		foreach ($dis as $key => $value) {
    			$data = array(
    				'distribution_id'   => $value->distribution_id,
    				'report_id'         => $report_id,
    				'create_date'       => date('Y-m-d H:i:s'),
    				'update_date'       => date('Y-m-d H:i:s'),
    			);
    			$table = 'tbl_report_distribution';
    			$this->Api_model->save_data($data,$table);
    		} 


    		// ELEMENT RECOMMENDATION
    		if($post["recommendation"] != null || $post["recommendation"] != ""){
    			$recommendation_ = $post["recommendation"];
    			$reco = json_decode($recommendation_);
    			foreach ($reco as $key => $value){
    				$data = array(
    					'element_id'       => $value->element_id,
    					'report_id'        => $report_id,
    					'recommendation'   => $value->recommendation,
    					'create_date'      => date('Y-m-d H:i:s'),
    					'update_date'      => date('Y-m-d H:i:s'),
    				);
    				$table = 'tbl_report_element_recommendation';
    				$this->Api_model->save_data($data,$table);
    			}
    		}


    		//OTHER DISTRIBUTION
    		$other_distribution = $post["other_distribution"];
    		$otherdistri = json_decode($other_distribution);
    		foreach ($otherdistri as $key => $value) {
    			$data = array(
    				'report_id'             => $report_id,	
    				'other_distribution'    => $value->others,			
    				'create_date'           => date('Y-m-d H:i:s'),
    				'update_date'           => date('Y-m-d H:i:s'),
    				);
    			$table = 'tbl_report_other_distribution';
    			$this->Api_model->save_data($data,$table);
    		}  


            //MAJOR CHANGES
            $major_changes = $post["inspection"];
            $majorchanges = json_decode($major_changes);
            foreach ($majorchanges as $key => $value) {
                $data = array(
                    'report_id'     => $report_id,  
                    'changes'       => $value->changes,          
                );
                $table = 'tbl_report_major_changes';
                $this->Api_model->save_data($data,$table);
            }  

    		//GENERATE AUDIT REPORT.JSON
    		$this->generate_audit_report_json($report_id);
    	

    		$query = $this->db->query("SELECT report_no,status from tbl_report_summary where report_id = ".$report_id."");
    		$query_res = $query->result();
    		$result = array(
                'status'        => 'success',
                'message'       => 'Successfully saved',
                'report_id'     => "$report_id",
                'report_no'     => $query_res[0]->report_no,
                'report_status' => $query_res[0]->status
            );

            $this->audit_report_list();

            //add to report listing
            $this->report_listing($report_id, "Save", $post);

            // $ch9 = curl_init();
            // curl_setopt($ch9, CURLOPT_URL, base_url('api/update_listing') . "?report_id=" . $report_id);
            // curl_setopt($ch9, CURLOPT_POST, 1);
            // curl_setopt($ch9, CURLOPT_RETURNTRANSFER, true);
            // $server_output = curl_exec ($ch9);
            // curl_close ($ch9);

            $this->samscurl->load(base_url('api/update_listing') . "?report_id=" . $report_id);

    		echo json_encode($result);
		}
	}

	function check_datetime($x) {

		if($x != "0000-00-00 00:00:00"){
			return $x;
		} else {
			return "None";
		}
	    
	}

	function updateReport($post){
		if(isset($post['status'])){
			echo json_encode(array('status' => 'failed','message'=>'Status Exist'));
			exit;  
		}

		$in_array = array(
            "co_auditor_id",
            "scope",
            "disposition",
            "pre_audit_documents",
            "references",
            "inspection",
            "inspector",
            "personnel",
            "activities",
            "question",
            "distribution",
            "other_distribution",
            "present_during_meeting"
        );

		$status ='';
		$x = 0;
		$error_message = "";
		foreach ($post as $key => $value) {
			if(in_array($key,$in_array)){
				if(!is_array(json_decode($value))){
					$x++;
					$error_message .=  $key . " ";
				}
			}
		}
		$key_error = array(
            'status'        => 'failed', 
            'message'       => $error_message . ' not in array', 
            'report_id'     => '', 'report_no'=> '', 
            'report_status' => ''
        );

		if($x > 0){
			echo json_encode($key_error);
		}else{
			//CHECK IF REPORT ID EXIST
			if(substr($post['report_id'], 0, 1) == '0'){
				$reportid_fromatter = 'x' . $post['report_id'];
			} else {
				$reportid_fromatter = $post['report_id'];
			};
			if($this->Api_model->check_report_id_exist($reportid_fromatter) > 0) {

				// REPORT_SUMMARY
				$rsummary = array(
					'template_id'          => $post['template_id'],
					'company_id'           => $post['company_id'],
					'other_activities'     => $post['other_activities'],
					'auditor_id'           => $post['auditor_id'],
					'audited_areas'        => $post['audited_areas'],
					'areas_to_consider'    => $post['areas_to_consider'],
					'wrap_up_date'         => $post['wrap_up_date'],
					'update_date'          => date("Y-m-d H:i:s"),
					'version'              => $post['version'],
					'head_lead'            => $post['head_lead']
				);
				$table_summary = 'tbl_report_summary';
				$this->Api_model->update_data('report_id',$post['report_id'],$table_summary,$rsummary);

				$report_id = $post['report_id'];

				
				//API LOGS
				$api_logs = array(
					"json"             =>json_encode($post),
					"report_id"        =>$post['report_id'],
					"action"           =>"Update",
					"auditor_id"       =>$post['auditor_id'],
					"submission_date"  =>date("Y-m-d H:i:s")
				);
				$this->Api_model->save_data($api_logs,"tbl_api_logs");

				
				//insert to Audit Date Table
				$this->Global_model->delete_data($post['report_id'],'tbl_report_audit_date','report_id');
				$this->update_audit_date($post['report_id'],$post['audit_date']);

				$this->Global_model->delete_data($post['report_id'],'tbl_report_other_issues_audit','report_id');
				$this->other_issues_audit($report_id,$post['other_issues_audit']);

				$this->Global_model->delete_data($post['report_id'],'tbl_report_other_issues_executive','report_id');
				$this->other_issues_executive($report_id,$post['other_issues_executive']);

                

                // CO AUDITOR
				$co_auditor_id = $post["co_auditor_id"];
				$a = json_decode($co_auditor_id);
				$this->Global_model->delete_data($post['report_id'],'tbl_co_auditors','report_id');
				foreach ($a as $key => $value) {
					$data = array(
					   'report_id'    => $report_id,
					   'auditor_id'   => $value->auditor_id,
					   'create_date'  => date('Y-m-d H:i:s'),
					   'update_date'  => date('Y-m-d H:i:s'),
					);
					$table = 'tbl_co_auditors';
					$this->Api_model->save_data($data,$table);			
				}	



				// REVIEWER
				$reviewer_id = $post["reviewer_id"];
				$b = json_decode($reviewer_id);
				$data = array(
					'reviewer_id'  => $reviewer_id,
					'update_date'  => date('Y-m-d H:i:s'),
				);
				$table = 'tbl_reviewer';
				$this->Api_model->update_data('report_id',$post['report_id'],$table,$data);


				// APPROVER
				$approver_id = $post["approver_id"];
				$c = json_decode($approver_id);
				$data = array(
					'approver_id'  => $approver_id,
					'update_date'  => date('Y-m-d H:i:s'),
				);
				$table = 'tbl_approver';
				$this->Api_model->update_data('report_id',$post['report_id'],$table,$data);



				// SCOPE
				$scope = $post["scope"];
				$d = json_decode($scope);
				$this->Global_model->delete_data($post['report_id'],'tbl_report_audit_scope','report_id');
				$this->Global_model->delete_data($post['report_id'],'tbl_report_scope_product','report_id');
				foreach ($d as $key => $value) {
					$data = array( 
						'audit_scope_id'  => $value->scope_id,
						'report_id'       => $report_id,				
						'scope_detail'    => $value->scope_remarks,
						'create_date'     => date('Y-m-d H:i:s'),
						'update_date'     => date('Y-m-d H:i:s'),
						);
					$table = 'tbl_report_audit_scope';
					$this->Api_model->save_data($data,$table);
					$audit_scope_id = $value->scope_id;
					foreach ($value->scope_product as $key => $value) {
						$data = array(
							'audit_scope_id'     => $audit_scope_id,
							'report_id'          => $report_id,
							'product_id'         => $value->product_id,
							'disposition_id'     => $value->disposition_id,
							'create_date'        => date('Y-m-d H:i:s'),
							'update_date'        => date('Y-m-d H:i:s'),
							);
						$table = 'tbl_report_scope_product';
						$this->Api_model->save_data($data,$table);
					}
				}



				//TRANSLATOR
				if($post["translator"] != null || $post["translator"] != ""){
					$translator = $post["translator"];
					//clear translator
					$this->Api_model->clear_record("tbl_report_translator",$post['report_id']);
					//save new translator
					$x = json_decode($translator);
					foreach ($x as $key => $value) {
						$data = array(
							'report_id'      => $post['report_id'],
							'translator'     => $value->translator,
							);
						$table = 'tbl_report_translator';
						$this->Api_model->save_data($data,$table);
					} 
				}



				//PRE-DOCUMENTS
				$document = $post["pre_audit_documents"];
				$f = json_decode($document);
				$this->Global_model->delete_data($post['report_id'],'tbl_pre_audit_documents','report_id');
				foreach ($f as $key => $value) {
					$data = array(
						'report_id'       => $report_id,	
						'document_name'   => $value->document_name,			
						'create_date'     => date('Y-m-d H:i:s'),
						'update_date'     => date('Y-m-d H:i:s'),
						);
					$table = 'tbl_pre_audit_documents';
					$this->Api_model->save_data($data,$table);
				} 




				//REFERENCES
				$references = $post["references"];
				$g = json_decode($references);
				$this->Global_model->delete_data($post['report_id'],'tbl_references','report_id');
				foreach ($g as $key => $value) {
					$data = array(
						'report_id'       => $report_id,	
						'reference_name'  => $value->reference_name,	
						'issuer'          => $value->issuer,
						'reference_no'    => $value->reference_no,
						'validity'        => $value->validity,		
						'create_date'     => date('Y-m-d H:i:s'),
						'update_date'     => date('Y-m-d H:i:s'),
						'issued'          => $value->issued
					);
					$table = 'tbl_references';
					$this->Api_model->save_data($data,$table);
				} 



				// INSPECTION CHANGES
				$inspection = $post["inspection"];
				$h = json_decode($inspection);
				$this->Global_model->delete_data($post['report_id'],'tbl_inspection_changes','report_id');
				foreach ($h as $key => $value) {
					$data = array(
						'report_id'       => $report_id,	
						'changes'         => $value->changes,		
						'create_date'     => date('Y-m-d H:i:s'),
						'update_date'     => date('Y-m-d H:i:s'),
						);
					$table = 'tbl_inspection_changes';
					$this->Api_model->save_data($data,$table);
				} 


				// PERSONNEL
				$personnel = $post["personnel"];
				$k = json_decode($personnel);
				$this->Global_model->delete_data($post['report_id'],'tbl_personnel','report_id');
				foreach ($k as $key => $value) {
					$data = array(
						'report_id'       => $report_id,	
						'name'            => $value->name,		
						'designation'     => $value->designation,
						'create_date'     => date('Y-m-d H:i:s'),
						'update_date'     => date('Y-m-d H:i:s'),
						);
					$table = 'tbl_personnel';
					$this->Api_model->save_data($data,$table);
				} 



				// PRESENT DURING MEETING
				$present = $post["present_during_meeting"];
				$pre = json_decode($present);
				$this->Global_model->delete_data($post['report_id'],'tbl_present_during_meeting','report_id');
				foreach ($pre as $key => $value) {
					$data = array(
						'report_id'       => $report_id,	
						'name'            => $value->name,		
						'position'        => $value->position,
						'create_date'     => date('Y-m-d H:i:s'),
						'update_date'     => date('Y-m-d H:i:s'),
						);
					$table = 'tbl_present_during_meeting';
					$this->Api_model->save_data($data,$table);
				} 



				// ACTIVITIES
				$activities = $post["activities"];
				$l = json_decode($activities);
				$this->Global_model->delete_data($post['report_id'],'tbl_report_activities','report_id');
				foreach ($l as $key => $value) {
					$activity_id = $value->activity_id;
					if(count($value->sub_activities) > 0){
						foreach ($value->sub_activities as $key => $value) {
							$data = array(
								'sub_item_id'   => $value->sub_item_id,
								'report_id'     => $report_id,
								'activity_id'   => $activity_id,
								'create_date'   => date('Y-m-d H:i:s'),
								'update_date'   => date('Y-m-d H:i:s'),
							);
							$table = 'tbl_report_activities';
							$this->Api_model->save_data($data,$table);
						}
					}else {
						$data = array(
							'sub_item_id'    => "",
							'report_id'      => $report_id,
							'activity_id'    => $activity_id,
							'create_date'    => date('Y-m-d H:i:s'),
							'update_date'    => date('Y-m-d H:i:s'),
						);
						$table = 'tbl_report_activities';
						$this->Api_model->save_data($data,$table);
					} 
				}


				// QUESTION_ANSWER
				$question = $post["question"];
				$m = json_decode($question);

                //REMOVE QUESTIONS
                $this->Global_model->delete_data($report_id,'tbl_report_answers','report_id');

				foreach ($m as $key => $value) {
                    $cat_id = null;
					$table = 'tbl_report_answers';

					//SAVE NEW SET OF QUESTIONS
                    if($value->answer_id == 2){
                        $cat_id = $value->category_id;
                    }
                    $data = array(
                        'report_id'          => $report_id, 
                        'question_id'        => $value->question_id,
                        'answer_id'          => $value->answer_id,      
                        'answer_details'     => $value->answer_details,
                        'category_id'        => $cat_id,
                        'create_date'        => date('Y-m-d H:i:s'),
                        'update_date'        => date('Y-m-d H:i:s'),
                    );
                    $this->Api_model->save_data($data,$table);
				} 


				//DISTRIBUTION
				$distribution = $post["distribution"];
				$dis = json_decode($distribution);
				$this->Global_model->delete_data($post['report_id'],'tbl_report_distribution','report_id');
				foreach ($dis as $key => $value) {
					$data = array(
						'distribution_id'     => $value->distribution_id,
						'report_id'           => $report_id,
						'create_date'         => date('Y-m-d H:i:s'),
						'update_date'         => date('Y-m-d H:i:s'),
					);
					$table = 'tbl_report_distribution';
					$this->Api_model->save_data($data,$table);
				}


				// ELEMENT RECOMMENDATION
				if($post["recommendation"] != null || $post["recommendation"] != ""){
					$recommendation_ = $post["recommendation"];
					$reco = json_decode($recommendation_);
					$this->Global_model->delete_data($post['report_id'],'tbl_report_element_recommendation','report_id');
					foreach ($reco as $key => $value){
						$data = array(
							'element_id'         => $value->element_id,
							'report_id'          => $report_id,
							'recommendation'     => $value->recommendation,
							'create_date'        => date('Y-m-d H:i:s'),
							'update_date'        => date('Y-m-d H:i:s'),
						);
						$table = 'tbl_report_element_recommendation';
						$this->Api_model->save_data($data,$table);
					}
				} 




				//OTHER DISTRIBUTION
				$other_distribution = $post["other_distribution"];
				$otherdistri = json_decode($other_distribution);
				$this->Global_model->delete_data($post['report_id'],'tbl_report_other_distribution','report_id');
				foreach ($otherdistri as $key => $value) {
					$data = array(
						'report_id'           => $report_id,	
						'other_distribution'  => $value->others,			
						'create_date'         => date('Y-m-d H:i:s'),
						'update_date'         => date('Y-m-d H:i:s'),
						);
					$table = 'tbl_report_other_distribution';
					$this->Api_model->save_data($data,$table);
				}  


                //MAJOR CHANGES
                $major_changes = $post["inspection"];
                $majorchanges = json_decode($major_changes);
                $this->Global_model->delete_data($post['report_id'],'tbl_report_major_changes','report_id');
                foreach ($majorchanges as $key => $value) {
                    $data = array(
                        'report_id'     => $report_id,  
                        'changes'       => $value->changes,          
                    );
                    $table = 'tbl_report_major_changes';
                    $this->Api_model->save_data($data,$table);
                }  


				//GENERATE AUDIT REPORT.JSON
				$this->generate_audit_report_json($report_id);
			

				$query = $this->db->query("SELECT report_no,status from tbl_report_summary where report_id = ".$post['report_id']."");
				$query_res = $query->result();
				$result = array(
                    'status'        => 'success',
                    'message'       => 'Successfully updated',
                    'report_id'     => $post['report_id'],
                    'report_no'     => $query_res[0]->report_no,
                    'report_status' => $query_res[0]->status
                );

				//add to report listing
				$this->report_listing($report_id, "Update", $post);

                $this->audit_report_list();

                // $ch9 = curl_init();
                // curl_setopt($ch9, CURLOPT_URL, base_url('api/update_listing') . "?report_id=" . $report_id);
                // curl_setopt($ch9, CURLOPT_POST, 1);
                // curl_setopt($ch9, CURLOPT_RETURNTRANSFER, true);
                // $server_output = curl_exec ($ch9);
                // curl_close ($ch9);

                $this->samscurl->load(base_url('api/update_listing') . "?report_id=" . $report_id);

				echo json_encode($result);
			} else {
				echo json_encode(array("status"=>"failed","message"=>"Report ID not found"));
			}
		}
	}

	function update_audit_date($report_id,$auditdate)
	{
		$json = stripslashes(htmlspecialchars($auditdate));
		$json = str_replace(array("[","]","{","}"), '', $json);
		$json = str_replace(array('&quot;', '"'), '', $json); 
		$audit_date_array = explode(",",$json);
		foreach ($audit_date_array as $key => $value) {
			$data = array(
				"report_id"     => $report_id,
				"audit_date"    => date_format(date_create($value),"Y-m-d"),
				"created_date"  => date('Y-m-d H:i:s')
			);
			$this->Api_model->save_data($data,"tbl_report_audit_date");
		}
	}
 
    public function samplle()
    {
		$issues = '[{"co test 1, not posting"},{"co test 2 not posting"}]';
		$json = stripslashes(htmlspecialchars($issues));
		$json = str_replace(array('{', '}', '&quot;'), array('','','"'), $json);
        $json = json_decode($json);
		$other_issues = $json;
        // $json = json_encode(array_values($issues));
        print_r($other_issues);
    }

	function other_issues_audit($report_id, $issues)
	{
		$this->Global_model->delete_data($report_id,'tbl_report_other_issues_audit','report_id');
		$json = stripslashes(htmlspecialchars($issues));
		$json = str_replace(array('{', '}', '&quot;'), array('','','"'), $json);
        $json = json_decode($json);
		$other_issues = $json;
		foreach ($other_issues as $key => $value) {
			$data = array(
				"report_id"             => $report_id,
				"other_issues_audit"    => $value
			);
			$this->Api_model->save_data($data,"tbl_report_other_issues_audit");
		}
	}

	function other_issues_executive($report_id, $issues)
	{
		$this->Global_model->delete_data($report_id,'tbl_report_other_issues_executive','report_id');
		$json = stripslashes(htmlspecialchars($issues));
		$json = str_replace(array('{', '}', '&quot;'), array('','','"'), $json);
        $json = json_decode($json);
        $other_issues = $json;
		foreach ($other_issues as $key => $value) {
			$data = array(
				"report_id"                 => $report_id,
				"other_issues_executive"    => $value
			);
			$this->Api_model->save_data($data,"tbl_report_other_issues_executive");
		}
	}

	function update_wrapup_date($report_id,$dates)
	{
		$json = stripslashes(htmlspecialchars($dates));
		$json = str_replace(array("[","]","{","}"), '', $json);
		$json = str_replace(array('&quot;', '"'), '', $json); 
		$wrapup_date_array = explode(",",$json);
		foreach ($wrapup_date_array as $key => $value) {
			$data = array(
				"report_id"     => $report_id,
				"date"          => date_format(date_create($value),"Y-m-d")
			);
			$this->Api_model->save_data($data,"tbl_report_wrap_up_date");
		}
	}

	function DMFilter($date){
		$auditors = $this->Api_model->DMFilter("tbl_auditor_info", "fname as First_Name, mname as Middle_Name,lname as Last_Name,designation as Designation,company as Company,department as Department,email as Email,create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		$reviewer = $this->Api_model->DMFilter("tbl_reviewer_info", "fname as First_Name, mname as Middle_Name,lname as Last_Name,designation as Designation,company as Company,department as Department,email as Email,create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		$approver = $this->Api_model->DMFilter("tbl_approver_info", "fname as First_Name, mname as Middle_Name,lname as Last_Name,designation as Designation,company as Company,department as Department,email as Email,create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		$site = $this->Api_model->DMFilter("qv_audit_company", "name as Name, address1 as Street, city_name as City, Province_Name as Province, create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		$products = $this->Api_model->DMFilter("qv_products", "name as Company, product_name as Product, create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		$audit_type = $this->Api_model->DMFilter("tbl_audit_scope", "scope_name as Type, create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		$category = $this->Api_model->DMFilter("tbl_question_category", "category_name as Category, create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		$product_type = $this->Api_model->DMFilter("tbl_classification", "classification_name as Product_Type, create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		$standard_reference = $this->Api_model->DMFilter("tbl_standard_reference", "standard_name as Standard_Reference, create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		$disposition = $this->Api_model->DMFilter("tbl_disposition", "disposition_name as Disposition, create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		$distribution = $this->Api_model->DMFilter("tbl_distribution", "distribution_name as Distribution, create_date,update_date", "DATE(create_date) = '". $date ."' OR DATE(update_date) = '". $date ."' AND status = 1");
		//contact results
		$dm = array(
			"Auditors"           => $auditors,
			"Reviewer"           => $reviewer,
			"Approver"           => $approver,
			"Site"               => $site,
			"Products_Materials" => $products,
			"Audit_Type"         => $audit_type,
			"Category"           => $category,
			"Product_Type"       => $product_type,
			"Standard_Reference" => $standard_reference,
			"Disposition"        => $disposition,
			"Distribution"       => $distribution
		);
		$template = $this->Api_model->DMFilter("qv_template", "Standard_Name, Classification_Name,Template_Created as create_date,Template_Updated as update_date", "DATE(Template_Created) = '". $date ."' OR DATE(Template_Updated) = '". $date ."' AND status = 1");
		$result = array("Template"=>$template, "Data_Maintenance"=>$dm);
		echo json_encode(array("status"=>"success","date"=>$date,"result"=> $result));
	}

	function Email_to_Lead($reportid, $email, $name)
	{
        $lead_auditor = $this->Global_model->get_emails("email_lead_auditor",$reportid);
        $reportid =  $_POST['report_id'];
		$data = array("coauditor_submission"=>date("Y-m-d H:m:s"));
		$this->q_auditreport->update_date_submission($data,$reportid);
        foreach ($this->Global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        $this->email->from($email,$name);
        $this->email->to($lead_auditor[0]->email);
        $subject = "STATUS of ".$company."'s Report (" . $report_no . ")";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Dear " . $lead_auditor[0]->fname . ",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Done reviewing report for ".$company."'s' audit.";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Regards,";
        $message .= "<br>";
        $message .= "".$name."";
        $message .= "</body></html>";
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
        if($this->Global_model->get_report_status($reportid) < 3){
            $this->Global_model->change_report_status($reportid,2);
        }

        //add co -auditor submission date

        //check auditor id via email address
        $auditor_id = $this->Api_model->get_auditor_id_email($email);
        //check if exist in table
        if($this->Api_model->check_coauditor_submission($reportid,$auditor_id) > 0){
        	//update
        	$update_data = array(
        		"update_date"=>date("Y-m-d H:m:s")
        	);
        	$this->Api_model->update_coauditor_submission($update_data,$reportid,$auditor_id);
        } else {
        	//insert
        	$insert_data = array(
        		"report_id"       => $reportid,
        		"auditor_id"      => $auditor_id,
        		"create_date"     => date("Y-m-d H:m:s"),
        		"update_date"     => date("Y-m-d H:m:s"),
        	);
        	$this->Api_model->update_coauditor_submission($insert_data);
        }
	}

	public function generate_config_json(){
		$config = array(
				"approver"          => $this->Api_model->get_last_update("tbl_approver_info", "update_date"),
				"auditor"           => $this->Api_model->get_last_update("tbl_auditor_info", "update_date"),
				"category"          => $this->Api_model->get_last_update("tbl_question_category", "update_date"),
				"classification"    => $this->Api_model->get_last_update("tbl_classification", "update_date"),
				"disposition"       => $this->Api_model->get_last_update("tbl_disposition", "update_date"),
				"distribution"      => $this->Api_model->get_last_update("tbl_distribution", "update_date"),
				"product"           => $this->Api_model->get_last_update("tbl_product", "update_date"),
				"product_type"      => $this->Api_model->get_last_update("tbl_classification", "update_date"),
				"reviewer"          => $this->Api_model->get_last_update("tbl_reviewer_info", "update_date"),
				"site"              => $this->Api_model->get_last_update("tbl_company", "update_date"),
				"standard_reference"=> $this->Api_model->get_last_update("tbl_standard_reference", "update_date"),
				"type_audit"        => $this->Api_model->get_last_update("tbl_audit_scope", "update_date"),
			);
		$result['content'] = $config;
		$result['file'] = 'config';
		$this->create_json($result);
	}

	public function generate_audit_report_json($reportid)
	{
		// header('Content-type: Application/JSON');
		$report_id = $reportid;
		$report_summary = $this->Api_model->audit_report_json("tbl_report_summary", "report_id=" . $report_id);
		$approver = $this->Api_model->audit_report_json("tbl_approver", "report_id=" . $report_id);
		$reviewer = $this->Api_model->audit_report_json("tbl_reviewer", "report_id=" . $report_id);

		$scope = $this->Api_model->audit_report_json("tbl_report_audit_scope", "report_id=" . $report_id);
		$scope_array = array();
		foreach ($scope as $key => $value) {
			$scope_array[] = array(
				"scope_id"          => $value->audit_scope_id,
				"scope_detail"      => $value->scope_detail,
				"scope_product"     =>$this->Api_model->audit_report_json_filter("tbl_report_scope_product", "report_id =" . $report_id . " AND audit_scope_id=" . $value->audit_scope_id, "product_id, disposition_id")
			);
		}

		$activity = $this->Api_model->audit_report_json_filter("tbl_report_activities", "report_id=" . $report_id,"activity_id");
		$activity_array = array();
		foreach ($activity as $key => $value) {
			$activity_array[] = array(
				"activity_id"   => $value->activity_id,
				"scope_product" => $this->Api_model->audit_report_json_select("tbl_report_activities", "report_id =" . $report_id . " AND sub_item_id <> 0","sub_item_id")
			);
		}
		$report_array = array(
			"report_id"              => $report_summary[0]->report_id,
			"report_no"              => $report_summary[0]->report_no,
			"company_id"             => $report_summary[0]->company_id,
			"other_activities"       => $report_summary[0]->other_activities,
			"template_id"            => $report_summary[0]->template_id,
			"auditor_id"             => $report_summary[0]->auditor_id,
			"audited_areas"          => $report_summary[0]->audited_areas,
			"areas_to_consider"      => $report_summary[0]->areas_to_consider,
			"wrap_up_date"           => $report_summary[0]->wrap_up_date,
			"create_date"            => $report_summary[0]->create_date,
			"modified_date"          => $report_summary[0]->update_date,
			"status"                 => $report_summary[0]->status,
			"version"                => $report_summary[0]->version,
			"head_lead"              => $report_summary[0]->head_lead,
			"reviewer_id"            => $reviewer[0]->reviewer_id,
			"approver_id"            => $approver[0]->approver_id,
			"other_issues_audit"     => $this->Api_model->audit_report_json_filter("tbl_report_other_issues_audit", "report_id=" . $report_id,"other_issues_audit"),
			"other_issues_executive" => $this->Api_model->audit_report_json_filter("tbl_report_other_issues_executive", "report_id=" . $report_id,"other_issues_executive"),
			"translators"            => $this->Api_model->audit_report_json_filter("tbl_report_translator", "report_id=" . $report_id, "translator"),
			"audit_dates"            => $this->Api_model->audit_report_json_filter("tbl_report_audit_date", "report_id=" . $report_id, "audit_date"),
			"co_auditor_id"          => $this->Api_model->audit_report_json_filter("tbl_co_auditors", "report_id=" . $report_id, "auditor_id"),
			"scope"                  => $scope_array,
			"pre_audit_documents"    => $this->Api_model->audit_report_json_select("tbl_pre_audit_documents", "report_id =" . $report_id,"document_name"),
			"references"             => $this->Api_model->audit_report_json_select("tbl_references", "report_id =" . $report_id,"reference_name, issuer, reference_no,validity,issued"),
			"inspection"             => $this->Api_model->audit_report_json_select("tbl_inspection_changes", "report_id =" . $report_id,"changes"),
			"personel"               => $this->Api_model->audit_report_json_select("tbl_personnel", "report_id =" . $report_id,"name,designation"),
			"activities"             => $activity_array,
			"question"               => $this->Api_model->audit_report_json_select("tbl_report_answers", "report_id =" . $report_id,"question_id,answer_id,category_id,answer_details"),
			"recommendation"         => $this->Api_model->audit_report_json_select("tbl_report_element_recommendation", "report_id =" . $report_id,"element_id,recommendation"),
			"distribution"           => $this->Api_model->audit_report_json_select("tbl_report_distribution", "report_id =" . $report_id,"distribution_id"),
			"present_during_meeting" => $this->Api_model->audit_report_json_select("tbl_present_during_meeting", "report_id =" . $report_id,"name,position"),
			"other_distribution"     => $this->Api_model->audit_report_json_select("tbl_report_other_distribution", "report_id =" . $report_id,"other_distribution as others"),
		);

		$result['content'] = $report_array;
		$result['file'] = 'audit_report/' . $reportid;
		$this->create_json($result);
	}

	function add_to_history($supplier_id, $major_changes, $auditors, $co_auditors, $reviewer, $approver, $audit_date, $reportid, $report_no)
	{
		$data = array(
			"supplier_id"        => $supplier_id,
			"create_date"        => date("Y-m-d H:i:s"),
			"update_date"        => date("Y-m-d H:i:s"),
			"status"             => 1,
			"API"                => 1,
			"report_id"          => $reportid,
			"report_no"          => $report_no,
		);
		$history_id = $this->Global_model->save_data('tbl_audit_history', $data);

		//MAJOR CHANGES
		$h = json_decode($major_changes);
		$orders = 1;
		foreach ($h as $key => $value) {
			$data_changes = array(
				"history_id"    => $history_id,
				"changes"       => $value->changes,
				"orders"        => $orders
			);
			$this->Global_model->save_data('tbl_audit_history_changes', $data_changes);
			$orders++;
		} 

		//GET Names 
		$auditor_name = $this->Global_model->get_name("tbl_auditor_info","auditor_id = " . $auditors);
		$reviewer_name = $this->Global_model->get_name("tbl_reviewer_info","reviewer_id = " . $reviewer);
		$approver_name = $this->Global_model->get_name("tbl_approver_info","approver_id = " . $approver);

		//Lead AUDITOR
		$data_auditor = array(
				"history_id"    => $history_id,
				"inspector"     => $auditor_name
			);
		$this->Global_model->save_data('tbl_audit_history_inspectors', $data_auditor);

		//CO AUDITORS
		$a = json_decode($co_auditors);
		foreach ($a as $key => $value) {
			$data_coauditor = array(
				"history_id"    => $history_id,
				"inspector"     => $this->Global_model->get_name("tbl_auditor_info","auditor_id = " . $value->auditor_id)
			);
			$this->Global_model->save_data('tbl_audit_history_inspectors', $data_coauditor);
		} 	

		//AUDIT DATES
		$json = stripslashes(htmlspecialchars($audit_date));
		$json = str_replace(array("[","]","{","}"), '', $json);
		$json = str_replace(array('&quot;', '"'), '', $json); 
		$audit_date_array = explode(",",$json);
		foreach ($audit_date_array as $key => $value) {
			$data_audit_date = array(
				"history_id"    => $history_id,
				"date"          => date_format(date_create($value),"Y-m-d")
			);
			$this->Global_model->save_data('tbl_audit_history_dates', $data_audit_date);
		}

		//UPDATE JSON
		$this->site();
	}

	function logs()
	{	
		$href = base_url("api/logs");
		$limit = $this->input->get("limit");
		if($limit == ""){
			$limit = 30;
		} else {
			$limit = $_GET['limit'];
		}

		$report_id = $this->input->get("report_id");
		if($report_id == ""){
			$report_id = null;
			$href .= "?limit=" . ((int)$limit + 10);
		} else {
			$report_id = $_GET['report_id'];
			$href .= "?limit=" . ((int)$limit + 10) . "&report_id=" . $report_id;
		}
		
		$data['limit'] = $limit;
		$data['report_id'] = $report_id;
		$data['ahref'] = $href;

		$data['logs'] = $this->Global_model->get_api_logs($limit,$report_id);
		$this->load->view('layout/header');
		$this->load->view('api_logs',$data);

	} 

	function dart_log()
	{
		$id = $_GET['id'];
		$data['logs'] = $this->Global_model->dart_logs($id);
		$this->load->view('layout/header');
		$this->load->view('dart_report',$data);

	} 

	function dart_email_log()
	{
		$id = $_GET['id'];
		$data['logs'] = $this->Global_model->dart_logs($id);
		$this->load->view('layout/header');
		$this->load->view('dart_report_email',$data);

	} 

    function update_listing_all()
    {
        ini_set('max_execution_time', 3000);

        $report_summary = $this->Api_model->audit_report_json("tbl_report_listing", "status >= 0");
        foreach ($report_summary as $key => $value) {
            $this->report_listing($value->report_id, "Update");
            $this->generate_audit_report_json($value->report_id);
        }
        
    }

    function update_listing()
    {
		$reportid = $_GET['report_id'];
		$action = $this->input->get("action");
		$this->report_listing($reportid, "Update",null, $action);
		if($action != "final"){
			$this->generate_audit_report_json($reportid);
			$this->generate_analysis($reportid);
		}

        $this->audit_report_list();
        
    }

	function add_listing()
	{
		$reportid = $_GET['report_id'];
		$this->report_listing($reportid, "Save");
		$this->generate_audit_report_json($reportid);
	}
	function report_listing($reportid, $action, $post = null, $final = null)
	{
		$report_summary = $this->Api_model->audit_report_json("tbl_report_summary", "report_id=" . $reportid);
		// $gmp_score = $this->Api_model->audit_report_json("gmp_computation_rating", "report_id=" . $reportid);
		$gmp_coverage = $this->Api_model->audit_report_json("gmp_computation_coverage", "report_id=" . $reportid);
		$approval = $this->Api_model->audit_report_json("filter_approval_rate_leadtime", "report_id=" . $reportid);
		$company = $this->Api_model->audit_report_json("tbl_company", "company_id=" . $report_summary[0]->company_id);
		$template = $this->Api_model->audit_report_json("tbl_template", "template_id=" . $report_summary[0]->template_id);
		$audit_date_filter = $this->Api_model->audit_report_json("ar_audit_dates", "report_id=" . $reportid);
		$audit_date_sort = $this->Api_model->audit_report_json("qv_audit_date", "report_id=" . $reportid);
		$audit_date_listing = "";
		foreach ($audit_date_sort as $key => $value) {
			$audit_date_listing .= $value->audit_date .","; 
		}

		$coauditor = "";
		if( $post != null) {
			foreach (json_decode($post['co_auditor_id']) as $key => $value) {
				$coauditor .= $value->auditor_id . ","; 
			}
		}

		$product_id = "";
		$disposition_id = "";
		if( $post != null) {
			$scope = $post["scope"];
			$d = json_decode($scope);
			foreach ($d as $key => $value) {
				foreach ($value->scope_product as $key => $value) {
					$product_id .= $value->product_id . ","; 
					$disposition_id .= $value->disposition_id . ",";
				}
			}
		}

		$scope = $this->Api_model->audit_report_json("qv_report_scope", "report_id=" . $reportid);

		$type_audit = "";
		foreach ($scope as $key => $value) {
			$type_audit .= $value->audit_scope_id .",";
		}

		if( $post != null) {
			$data['co_auditor'] = substr($coauditor, 0, -1);
			$data['product'] = substr($product_id, 0, -1);
			$data['disposition'] = substr($disposition_id, 0, -1);
			$data['audit_date'] = substr($audit_date_listing, 0, -1);
		}

		$data['report_id'] = $report_summary[0]->report_id;
		$data['site_name'] = $company[0]->name;
		$data['product_type'] = $template[0]->classification_id;
		$data['country'] = $company[0]->country;
		$data['province_state'] = $company[0]->address2 . ", " . $company[0]->address3;
		$data['lead_auditor'] = $report_summary[0]->auditor_id;
		$data['first_audit_date'] = $audit_date_filter[0]->First_Audit_Date;
		$data['last_audit_date'] = $audit_date_filter[0]->Last_Audit_Date;
		$data['report_no'] = $report_summary[0]->report_no;
		$data['version'] = $report_summary[0]->version;
		$data['date_modified'] = $report_summary[0]->update_date;
		$data['type_of_audit'] = substr($type_audit, 0, -1);
			
		if(count($approval) > 0){
			$data['date_submitted'] =	$approval[0]->Submitted_to_DepartmentHead;
			$data['lead_time_submission'] = $approval[0]->LeadTime_Submission;
			$data['date_review'] = $approval[0]->Date_Review;
			$data['lead_time_review'] = $approval[0]->LeadTime_Review;
			$data['date_approved'] = $approval[0]->Date_Approved;
			$data['lead_time_approved'] = $approval[0]->LeadTime_Approve;
			$data['lead_time_total'] = $approval[0]->LeadTime_Total;
		} else {
			$data['lead_time_submission'] = 0;
			$data['lead_time_review'] = 0;
			$data['lead_time_approved'] = 0;
			$data['lead_time_total'] = 0;
		}

		
		$data['status'] = $report_summary[0]->status;

        $data['audit_date_formatted'] = $this->format_audit_date($this->Preview_report_model->get_list("tbl_report_audit_date",$reportid,"audit_date"));
		
		if($action == "Update"){
			$this->Api_model->update_data('report_id',$reportid,"tbl_report_listing",$data);
		} else {

			$this->Api_model->save_data2($data,"tbl_report_listing");
		}

		$file = getcwd().'/listing/'.$report_summary[0]->report_id.'.json';
		file_put_contents($file,'');
		chmod($file,0777);
		$fp = fopen($file, 'w');
		fwrite($fp, $this->lisitng_report($report_summary[0]->report_id));
		fclose($fp);


	}


	function generate_analysis($reportid){
		$details = $this->Template_model->get_data_by_id($reportid,'report_id','ar_general');
		$analysis = $this->Audit_report_analysis_model2->get_report_analysis_list_api("report_id = " . $reportid,9999,0);
		$data['question_count'] = $analysis[0]->question_count;
		$data['yes_answer_cnt'] = $analysis[0]->yes_answer_cnt;
		$data['no_answer_cnt'] = $analysis[0]->no_answer_cnt;

		$arrayobj = $this->Template_model->get_data_by_id($reportid,'report_id','gmp_answers_count');
		$template = $this->Preview_report_model->get_template($reportid);
		$template_details = $this->Template_model->get_data_by_id($template[0]->template_id,'template_id','tbl_template');
		$classification = $this->Template_model->get_data_by_id($template_details[0]->classification_id,'classification_id','tbl_classification');
		$auditdate_format =  $this->format_audit_date2($this->Audit_report_model->get_audit_dates($reportid));
		$answer_yes = 0;
		if(isset($arrayobj[0]->answer_yes)){
			$answer_yes = $arrayobj[0]->answer_yes; 
		}
		
		$answer_no = 0;
		if(isset($arrayobj[0]->answer_no)){
			$answer_no = $arrayobj[0]->answer_no;
		}

		$answer_nc = 0;
		if(isset($arrayobj[0]->answer_nc)){
			$answer_nc = $arrayobj[0]->answer_nc;
		}

		$answer_na = 0;
		if(isset($arrayobj[0]->answer_na)){
			$answer_na = $arrayobj[0]->answer_na;
		}

		$limit = 0;
		if(isset($classification[0]->limit)){
			$limit = $classification[0]->limit;
		}

		$no_critical = 0;
		if(isset($arrayobj[0]->category_critical)){
			$no_critical = $arrayobj[0]->category_critical;
		}
		
		$no_major = 0;
		if(isset($arrayobj[0]->category_major)){
			$no_major = $arrayobj[0]->category_major;
		}

		$no_minor = 0;
		if(isset($arrayobj[0]->category_minor)){
			$no_minor = $arrayobj[0]->category_minor;
		}
		

		$No_Of_Major_To_Have_Unacceptable_Disposition = $classification[0]->no_major;
		$No_Of_Critical_To_Have_Unacceptable_Disposition = $classification[0]->no_critical;

		$Total_No_of_Covered_Questions = (int)$answer_yes + (int)$answer_no;
		$Total_No_of_Applicable_Questions = (int)$Total_No_of_Covered_Questions + (int)$answer_nc;
		$Factor_for_Major = (((int)$answer_yes * 100) / $limit - (int)$Total_No_of_Applicable_Questions) / $No_Of_Major_To_Have_Unacceptable_Disposition;
		$Factor_for_Critical = (((int)$answer_yes * 100) / $limit - (int)$Total_No_of_Applicable_Questions) / $No_Of_Critical_To_Have_Unacceptable_Disposition;
		$Weight_for_Major = $no_major * $Factor_for_Major;
		$Weight_for_Critical = $no_critical * $Factor_for_Critical;
		$Weighted_Denominator = (int)$Total_No_of_Applicable_Questions + $Weight_for_Major + $Weight_for_Critical;
		if((int)$answer_yes > 0) {
			$Rating = ((int)$answer_yes / $Weighted_Denominator) * 100;
		} else {
			$Rating = 0;
		}

		if((int)$answer_yes > 0) {
			$coverage = ((int)$Total_No_of_Covered_Questions / $Total_No_of_Applicable_Questions ) * 100;
		} else {
			$coverage = 0;
		}

		
		

		$obj = array(
			"report_id"         					=> $details[0]->Report_ID,
			"report_no"         					=> $details[0]->Report_No,
			"name"              					=> $details[0]->Audited_Site,
			"yes"               					=> $answer_yes,
			"no"                					=> $answer_no,
			"NA"                					=> $answer_na,
			"NC"                					=> $answer_nc,
			"limit"                					=> $limit,
			"major_unacceptable"					=> $classification[0]->no_major,
			"critical_unacceptable"					=> $classification[0]->no_critical,
			"no_critical"       					=> $no_critical,
			"no_major"          					=> $no_major,
			"no_minor"          					=> $no_minor,
			"applicable_q"      					=> $Total_No_of_Applicable_Questions,
			"no_question"       					=> (int)$answer_yes + (int)$answer_no + (int)$answer_na + (int)$answer_nc,
			"audit_date"        					=> $auditdate_format,
			"Total_No_of_Covered_Questions"        	=> $Total_No_of_Covered_Questions,
			"Total_No_of_Applicable_Questions"      => $Total_No_of_Applicable_Questions,
			"Factor_for_Major"        				=> $Factor_for_Major,
			"Factor_for_Critical"        			=> $Factor_for_Critical,
			"Weight_for_Major"        				=> $Weight_for_Major,
			"Weight_for_Critical"        			=> $Weight_for_Critical,
			"Weighted_Denominator"        			=> $Weighted_Denominator,
			"rating"            					=> number_format($Rating,2),
			"coverage"          					=> number_format($coverage,2)
		);

		$file2 = getcwd().'/listing/'.$reportid.'_analysis.json';
        file_put_contents($file2,'');
        chmod($file2,0777);
        $fp2 = fopen($file2, 'w');
        fwrite($fp2, json_encode($obj));
		fclose($fp2);
		
		// echo "<pre>";
		// print_r($obj);

		//update report listing table
		$data = array(
			"gmp_score"=> number_format($Rating,2),
			"gmp_coverage"=> number_format($coverage,2)
		);
		$this->Api_model->update_data('report_id',$reportid,"tbl_report_listing",$data);
	}


	function format_audit_date2($data){
		$dates = array();
		foreach($data as $day){
			$timestamp = strtotime($day->audit_date);
			$dates[date('Y', $timestamp)][date('F', $timestamp)][] = date('d', $timestamp);
		}
		$result_final = "";
		foreach ($dates as $year => $months) {
			$result_year = $year;
			$result = "";
			$dates_first = "";
			$str = "";
			foreach ($months as $month => $date) {
				$str = array_pop($date);
				$dates_first = "";
				if(count($date) > 0 ){
					foreach ($date as $value) {
						$dates_first .= (int)$value . ", ";
					}
					$result .= rtrim(trim($dates_first), ',') . " & " . (int) $str . " " . $month  . ", ";
				} else {
					$result .= (int) $str . " " . $month . ", ";
				}
			}
			$result_final .= rtrim(trim($result), ',') . " " . $result_year . ", ";
		}

		return rtrim(trim($result_final), ',');
    }

    function format_audit_date($data){

		$dates = array();
		foreach($data as $day){
			$timestamp = strtotime($day['Date']);
			$dates[date('Y', $timestamp)][date('F', $timestamp)][] = date('d', $timestamp);
		}
		$result_final = "";

		$year_i = 0;
		$year_len = count($dates);
		$year_sep = "";

		foreach ($dates as $year => $months) {
			$result_year = $year;
			$result = "";
			$result_with_last_month = "";
			$dates_first = "";
			$str = "";
			$month_i = 0;
			$month_len = count($months);
			$month_sep = "";
			foreach ($months as $month => $date) {
				$str = array_pop($date);
				$dates_first = "";

				if ($month_i == $month_len - 1) {
					// last
					if($month_len > 1){
						$month_sep = " & ";
					} else {
						$month_sep = "";
					}
					
				} else {
					$month_sep = ", ";
				}

				if(count($date) > 0 ){
					foreach ($date as $value) {
						$dates_first .= (int)$value . ", ";
					}
					$result .=  $month_sep . rtrim(trim($dates_first), ',') . " & " . (int) $str . " " . $month   .", ";
				} else {
					$result .=  $month_sep . (int) $str . " " . $month ;
				}

				$month_i++;
			}

			if ($year_i == $year_len - 2) {

				if($year_len > 2){
					$result_final .= ltrim(rtrim(trim($result), ','), ',') . " " . $result_year . " & ";
				} else {
					$result_final .= ltrim(rtrim(trim($result), ','), ',') . " " . $result_year . ", ";
				}
				
			} else {
				$result_final .= ltrim(rtrim(trim($result), ','), ',') . " " . $result_year . ", ";
			}

			$year_i++;

			
		}

		return rtrim(trim($result_final), ',');
    }

    function format_audit_datex($data){

        $result = "";

        $months = array();
        foreach ($data as $date_str) {
          $timestamp = strtotime($date_str['Date']);
          $month = date('F', $timestamp);
          $date = date('d', $timestamp);
          $year = date('Y', $timestamp);

          if (empty($months[$month])) {
              $months[$month] = array();
          }

          $months[$month][] = $date;
        }

		// print_r($months);
		
        foreach ($months as $month => $dates) {
			$str = array_pop($dates);
            if(count($dates) > 0 ){
				$dates_first = "";
				foreach ($dates as $k => $v) {
					$dates_first .= (int)$v . ", ";
				}
                $result .= rtrim(trim($dates_first), ',') . " & " . (int) $str . " " . $month  . ", ";
            } else {
                $result .= (int) $str . " " . $month . ", ";
            }
            
        }
        $result = rtrim(trim($result), ',');
        return $result . " " . $year;
    }

	function lisitng_report($report_id)
	{
		set_time_limit(30); 
		$report = $this->Preview_report_model->get_report_summary($report_id);
		$report_summary = $this->Api_model->audit_report_json("tbl_report_summary", "report_id=" . $report_id);
		
		
		$history = $this->Global_model->get_where("tbl_audit_history", "supplier_id", $report[0]->company_id, null, "id");
      
		$data['Report_Summary'] = $this->Preview_report_model->get_report_summary($report_id);
		$data['Audit_Dates'] = $this->Preview_report_model->get_list("tbl_report_audit_date",$report_id);
        $data['Audit_Dates_Formatted'] = $this->format_audit_date($this->Preview_report_model->get_list("tbl_report_audit_date",$report_id));
		$data['Lead_Auditor'] = $this->Global_model->get_data_query("tbl_auditor_info", "auditor_id = " . $report_summary[0]->auditor_id);
		$data['Co_Auditors'] = $this->Preview_report_model->get_co_auditors($report_id);
		$data['Approver'] = $this->Preview_report_model->get_approvers($report_id);
		$data['Reviewer'] = $this->Preview_report_model->get_reviewer($report_id);
		$data['Translator'] = $this->Preview_report_model->get_translator($report_id);
		$data['Inspector'] = $this->Preview_report_model->get_inspectors($report_id);

		$company = $this->get_json(base_url() . "/json/data_maintenance/site.json");
        foreach ($company->company_list as $key => $value) {
            if($value->company_id == $report[0]->company_id){
				foreach ($value->audit_history as $a => $b) {
					$auditdate = array();
					foreach ($b->audit_dates as $c => $d) {
						$auditdate[] = array("Date"=>$d->inspection_date);
					}
					$data['Inspection_Audit_Dates'] = $auditdate;
					$data['Inspection_Audit_Dates_Formatted'] = $this->format_audit_date($auditdate);
					$data['Inspection_Inspector'] = $b->inspectors;
				}
            }
		}
		
        $data['Inspection_Changes'] = $this->Global_model->get_data_query("tbl_report_major_changes", "report_id = " . $report_id);
		$data['Activities'] = $this->get_activities($report_id);
		$data['Scope_Audit'] = $this->Preview_report_model->get_scope_audit($report_id);
		$data['Scope_Product'] = $this->get_report_scope_product($report_id);
		$data['Pre_Document'] = $this->Preview_report_model->get_pre_document($report_id);
		$data['Personel_Met'] = $this->Preview_report_model->get_personel_met($report_id);
		$data['Distribution'] = $this->Preview_report_model->get_distribution($report_id);
		$data['Disposition'] = $this->get_disposition($report_id);
		$data['Template'] = $this->Preview_report_model->get_template($report_id);
		$data['Template_Elements'] = $this->get_element_questions_answers($report_id, $report_summary[0]->template_id);
		$data['Element_Reco'] = $this->Preview_report_model->get_element_reco($report_id);
		$data['Present_During_Meeting'] = $this->Preview_report_model->get_present_during_meeting($report_id);
		$data['Remarks'] = $this->Preview_report_model->get_remarks($report_id);


		$license = $this->Preview_report_model->get_license($report_id);
		$license_array = array();
		foreach ($license as $key => $value) {
			$license_array[] = array(
				"reference_id"      => $value->reference_id,
				"report_id"         => $value->report_id,
				"reference_name"    => $value->reference_name,
				"issuer"            => $value->issuer,
				"reference_no"      => $value->reference_no,
				"validity"          => $value->validity,
				"create_date"       => $value->create_date,
				"update_date"       => $value->update_date,
				"issued"            => $value->issued
			);
		}
		$data['License'] = $license_array;

		$element = $this->Template_model->get_elements($report_summary[0]->template_id);
		foreach ($element as $key => $value) {
			$data['Observation_No'] = $this->Global_model->get_no_answer($report_id,$value->element_id);
		}

		$disposition = $this->Audit_report_model->get_disposition2($report_id);
		$disposition_array = array();
		foreach ($disposition as $key => $value) {
			$disp_product = array();
           	$scope_product = $this->Audit_report_model->get_disposistion_product($report_id,$value->disposition_id);
            foreach ($scope_product as $k => $v) {
            	$disp_product[] = array(
            		"product_name"    => $v->product_name
            	);
			}             
            $disposition_array[] = array(
            	"disposition_name"         => $value->disposition_name,
            	"disposition_label"        => $value->disposition_label,
            	"disposition_products"     => $disp_product,
            );
        }
		$data['Disposition_Product'] = $disposition_array;

		$data['Observation_Yes'] = $this->report_observation($report_id);
		$data['Recommendation'] = $recommendation = $this->Audit_report_model->get_element_recommendation($report_id);

		$data['other_issue_audit'] = $this->Global_model->get_data_query("qv_report_other_issues_audit", "report_id = " . $report_id);
		$data['other_issue_exec'] = $this->Global_model->get_data_query("qv_report_other_issues_executive", "report_id = " . $report_id);
		
		$data['Other_Distribution'] = $this->Global_model->get_data_query("tbl_report_other_distribution", "report_id = " . $report_id);
		
		$data['Audit_Observation'] = $this->Preview_report_model->get_audit_observation($report_id);
		$data['Signature_Stamp'] = $this->Preview_report_model->check_stamp($report_id);
		$data['Annexure_Observation'] = $this->annexure_observation($report_id);


		//activities
		$activities = $this->Audit_report_model->get_data_by_activity($report_summary[0]->template_id,'template_id','tbl_template_activities');
		$activity_array = array();
		foreach ($activities as $key => $value) {

			//sub activity
			$sub_activity = array();
			foreach ($this->Audit_report_model->get_sub_activities($value->activity_id) as $k => $v) {
				$sub_activity[] = array(
					"sub_name"     => $v->sub_item_name,
					"sub_box"      => 'asset/img/' . $this->Audit_report_model->check_used_subactivity($v->sub_item_id,$report_id)
				);
			};


			//activity
			$activity_array[] = array(
				"activity_name"     => $value->activity_name,
				"activity_box"      => 'asset/img/' . $this->Audit_report_model->check_used_activity($value->activity_id,$report_summary[0]->report_id),
				"sub_activity"      => $sub_activity
			);
		};

		$data['Report_activities'] = $activity_array;
		
		return json_encode($data,JSON_UNESCAPED_SLASHES);
	}

	function get_element_questions_answers($report_id,$template_id)
	{	 
		$element_order =1 ;
		$count_no = 0;
		$elements_result = $this->Preview_report_model->preview_report_get_element($template_id);
		$element_array = array();
        foreach ($elements_result as $key => $value) {  

            $element_questions = $this->Global_model->get_list_with_order("tbl_questions", "element_id = " . $value->element_id, "order_sort asc");
            $questions_array = array();

            foreach ($element_questions as $k => $v) {
                $question_result = $this->Preview_report_model->get_questions_answers($report_id, $v->element_id, $v->question_id);
                $answer_name = "";
                $answer_details = "";
                $category = "";
                $answer_no = "";
                if(count($question_result) > 0 ){
                    $answer_name = $question_result[0]->answer_name;
                    $answer_details = $question_result[0]->answer_details;
                    

                    if($question_result[0]->answer_id == 2){
                        $count_no ++;
                        $answer_no = $count_no;
                        $category = $question_result[0]->category_name;
                    } else { 
                        $answer_no = ""; 
                        $category = "";
                    }

                }

                $questions_array[] = array(
                    "question_id"       => $v->question_id,
                    "question"          => $v->question,
                    "answer_name"       => $answer_name,
                    "answer_details"    => $answer_details,
                    "category"          => $category,
                    "answer_no"         => $answer_no,
                );
            }

            $element_array[] = array(
                "element_id"        => $value->element_id,
                "element_name"      => $value->element_name,
                "order"             => $element_order,
                "questions"         => $questions_array
            );

            $element_order++;
        } 

		return $element_array;
	}

		public function report_observation($report_id)
	    {
	      $report= $this->Audit_report_model->get_report($report_id);
	      $element = $this->Template_model->get_elements($report[0]->template_id);

	      foreach ($element as $key => $value) {
	          # code...
	        $is_no = 0;
	        $is_yes = 0;
	        $is_na = 0;
	        $is_nc = 0;

	        $questions = $this->Global_model->get_element_answers($report_id,$value->element_id);

	        //counting all answers
	        foreach ($questions as $b => $a) {
	            if($a->answer_id == 1){
	                $is_yes ++;
	            }
	            if($a->answer_id == 2){
	                $is_no ++;
	            }
	            if($a->answer_id == 3){
	                $is_na ++;
	            }
	            if($a->answer_id == 4){
	                $is_nc ++;
	            }
	        }

	        //generating answers for annexure
	        $observation = "";

	        //ALL YES
	        if($is_na == 0 && $is_nc == 0 && $is_no == 0 && $is_yes != 0){
	            foreach ($questions as $c => $d) {
	                $observation .= $d->default_yes .". " . $d->answer_details . ". ";
	            }
	        }

	        //ALL NO
	        if($is_na == 0 && $is_nc == 0 && $is_no != 0 && $is_yes == 0){
	        	$observation .= "None.";
	        }

	        //ALL NA
	        if($is_na != 0 && $is_nc == 0 && $is_no == 0 && $is_yes == 0){
	        	$observation .= "Not Applicable.";
	        }

	        //ALL NC
	        if($is_na == 0 && $is_nc != 0 && $is_no == 0 && $is_yes == 0){
	        	$observation .= "Not Covered.";
	        }

	        //YES NO
	        if($is_na == 0 && $is_nc == 0 && $is_no != 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
                    if($d->answer_id == 1){
                        $observation .= $d->default_yes .". " . $d->answer_details . ". ";
                    }
	            }
	        }

	        //YES NC
	        if($is_na == 0 && $is_nc != 0 && $is_no == 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
	                if($d->answer_id == 1){
                        $observation .= $d->default_yes .". " . $d->answer_details . ". ";
                    }
	            }
	        }

	        //YES NA
	        if($is_na != 0 && $is_nc == 0 && $is_no == 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
	                if($d->answer_id == 1){
                        $observation .= $d->default_yes .". " . $d->answer_details . ". ";
                    }
	            }
	        }

	        //NO NA
	        if($is_na != 0 && $is_nc == 0 && $is_no != 0 && $is_yes == 0){
	        	$observation .= "None.";
	        }

	        //NO NC
	        if($is_na == 0 && $is_nc != 0 && $is_no != 0 && $is_yes == 0){
	        	$observation .= "None.";
	        }

	        //NA NC
	        if($is_na != 0 && $is_nc != 0 && $is_no == 0 && $is_yes == 0){
	        	$observation .= "Not Covered.";
	        }

	        //NA NO YES
	        if($is_na != 0 && $is_nc == 0 && $is_no != 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
	                if($d->answer_id == 1){
                        $observation .= $d->default_yes .". " . $d->answer_details . ". ";
                    }
	            }
	        }

	        //NC NO YES
	        if($is_na == 0 && $is_nc != 0 && $is_no != 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
	                if($d->answer_id == 1){
                        $observation .= $d->default_yes .". " . $d->answer_details . ". ";
                    }
	            }
	        }

	        //NC NA NO
	        if($is_na != 0 && $is_nc != 0 && $is_no != 0 && $is_yes == 0){
	        	$observation .= "None.";
	        }

	        //NC NA YES
	        if($is_na != 0 && $is_nc != 0 && $is_no == 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
	                if($d->answer_id == 1){
                        $observation .= $d->default_yes .". " . $d->answer_details . ". ";
                    }
	            }
	        }

	        //NC NA NO YES
	        if($is_na != 0 && $is_nc != 0 && $is_no != 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
	                if($d->answer_id == 1){
                        $observation .= $d->default_yes .". " . $d->answer_details . ". ";
                    }
	            }
	        }


	        $array_result[] = array(
	            'Element'      => $value->element_name, 
	            'Observation'  => $observation
	        );
	      }

	      return $array_result;

	    }

	    function annexure_observation($report_id)
	    { 
	      $report= $this->Audit_report_model->get_report($report_id);
	      $element = $this->Template_model->get_elements($report[0]->template_id);
	      $ctrx = 0;
	      foreach ($element as $key => $value) {
	          # code...
	        $is_no = 0;
	        $is_yes = 0;
	        $is_na = 0;
	        $is_nc = 0;

	        $questions = $this->Global_model->get_element_answers_not_yes($report_id,$value->element_id);

	        //counting all answers
	        foreach ($questions as $b => $a) {
	            if($a->answer_id == 1){
	                $is_yes ++;
	            }
	            if($a->answer_id == 2){
	                $is_no ++;
	            }
	            if($a->answer_id == 3){
	                $is_na ++;
	            }
	            if($a->answer_id == 4){
	                $is_nc ++;
	            }
	        }

	        //generating answers for annexure
	        $observation = array();

	        //ALL YES
	        if($is_na == 0 && $is_nc == 0 && $is_no == 0 && $is_yes != 0){
	        	$observation[] =  array(
                    'no'            => "-", 
                    'category'      => "-", 
                    'observation'   => "None Observed.", 
                );
	        }

	        //ALL NO
	        if($is_na == 0 && $is_nc == 0 && $is_no != 0 && $is_yes == 0){
	        	foreach ($questions as $c => $d) {
	                if($d->category_name != null){
                        if($d->answer_id == 2) { 
    	                    $ctrx ++;
    	                    $observation[] =  array(
                                'no'            => $ctrx,
                                'category'      => $d->category_name, 
                                'observation'   => $d->answer_details, 
                            );
                        }
	                }
	            }
	        }

	        //ALL NA
	        if($is_na != 0 && $is_nc == 0 && $is_no == 0 && $is_yes == 0){
	        	$observation[] =  array(
                    'no'            => "-", 
                    'category'      => "-", 
                    'observation'   => "Not Applicable.", 
                );
	        }

	        //ALL NC
	        if($is_na == 0 && $is_nc != 0 && $is_no == 0 && $is_yes == 0){
	        	$observation[] =  array(
                    'no'            => "-", 
                    'category'      => "-", 
                    'observation'   => "Not Covered.", 
                );
	        }

	        //YES NO
	        if($is_na == 0 && $is_nc == 0 && $is_no != 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
	                if($d->category_name != null){
                        if($d->answer_id == 2) { 
    	                    $ctrx ++;
    	                    $observation[] =  array(
                                'no'            => $ctrx,
                                'category'      => $d->category_name, 
                                'observation'   => $d->answer_details, 
                            );
                        }
	                }
	            }
	        }

	        //YES NC
	        if($is_na == 0 && $is_nc != 0 && $is_no == 0 && $is_yes != 0){
	        	$observation[] =  array(
                    'no'                => "-", 
                    'category'          => "-", 
                    'observation'       => "None Observed.", 
                );
	        }

	        //YES NA
	        if($is_na != 0 && $is_nc == 0 && $is_no == 0 && $is_yes != 0){
	        	$observation[] =  array(
                    'no'            => "-", 
                    'category'      => "-", 
                    'observation'   => "None Observed.", 
                );
	        }

	        //NO NA
	        if($is_na != 0 && $is_nc == 0 && $is_no != 0 && $is_yes == 0){
	        	foreach ($questions as $c => $d) {
	                if($d->category_name != null){
                        if($d->answer_id == 2) { 
    	                    $ctrx ++;
    	                    $observation[] =  array(
                                'no'            => $ctrx,
                                'category'      => $d->category_name, 
                                'observation'   => $d->answer_details, 
                            );
                        }
	                }
	            }
	        }

	        //NO NC
	        if($is_na == 0 && $is_nc != 0 && $is_no != 0 && $is_yes == 0){
	        	foreach ($questions as $c => $d) {
	                if($d->category_name != null){
                        if($d->answer_id == 2) { 
    	                    $ctrx ++;
    	                    $observation[] =  array(
                                'no'            => $ctrx,
                                'category'      => $d->category_name, 
                                'observation'   => $d->answer_details, 
                            );
                        }
	                }
	            }
	        }

	        //NA NC
	        if($is_na != 0 && $is_nc != 0 && $is_no == 0 && $is_yes == 0){
	        	$observation[] =  array(
                    'no'            => "-", 
                    'category'      => "-", 
                    'observation'   => "Not Covered.", 
                );
	        }

	        //NA NO YES
	        if($is_na != 0 && $is_nc == 0 && $is_no != 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
	                if($d->category_name != null){
                        if($d->answer_id == 2) { 
    	                    $ctrx ++;
    	                    $observation[] =  array(
                                'no'            => $ctrx,
                                'category'      => $d->category_name, 
                                'observation'   => $d->answer_details, 
                            );
                        }
	                }
	            }
	        }

	        //NC NO YES
	        if($is_na == 0 && $is_nc != 0 && $is_no != 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
	                if($d->category_name != null){
                        if($d->answer_id == 2) { 
    	                    $ctrx ++;
    	                    $observation[] =  array(
                                'no'            => $ctrx,
                                'category'      => $d->category_name, 
                                'observation'   => $d->answer_details, 
                            );
                        }
	                }
	            }
	        }

	        //NC NA NO
	        if($is_na != 0 && $is_nc != 0 && $is_no != 0 && $is_yes == 0){
	        	foreach ($questions as $c => $d) {
	                if($d->category_name != null){
                        if($d->answer_id == 2) { 
    	                    $ctrx ++;
    	                    $observation[] =  array(
                                'no'            => $ctrx,
                                'category'      => $d->category_name, 
                                'observation'   => $d->answer_details, 
                            );
                        }
	                }
	            }
	        }

	        //NC NA YES
	        if($is_na != 0 && $is_nc != 0 && $is_no == 0 && $is_yes != 0){
	        	$observation[] =  array(
                    'no'            => "-", 
                    'category'      => "-", 
                    'observation'   => "None Observed.", 
                );
	        }

	        //NC NA NO YES
	        if($is_na != 0 && $is_nc != 0 && $is_no != 0 && $is_yes != 0){
	        	foreach ($questions as $c => $d) {
	                if($d->category_name != null){
                        if($d->answer_id == 2) { 
    	                    $ctrx ++;
    	                    $observation[] =  array(
                                'no'            => $ctrx,
                                'category'      => $d->category_name, 
                                'observation'   => $d->answer_details, 
                            );
                        }
	                }
	            }
	        }

	        $array_result[] = array(
	            'Element'      => $value->element_name, 
	            'Observation'  => $observation
	        );
	      }

	      return $array_result;
	    }

		function get_disposition($report_id){
			$data = $this->Preview_report_model->get_disposition($report_id);

			$result = array();
			foreach ($data as $key => $value) {
				$products = $this->Preview_report_model->get_disposition_product($report_id, $value->disposition_id);
				$result_product = "";
				foreach ($products as $a => $b) {
					$result_product .= ", " . $b->product_name;
				}
				$result[] = array(
					"Disposition"  => $value->disposition_name,
					"Products"     => substr($result_product, 1)
				);
			}
			return $result;
		}

		function get_activities($report_id){
			$data = $this->Preview_report_model->get_activities($report_id);
			$array_return = array();
			foreach ($data as $key => $value) {
				$sub = $this->Preview_report_model->get_sub_activities($report_id,$value->activity_id);
				$array_return[] = array(
					"activity_name"    => $value->activity_name,
					"sub_activity"     => $sub
				); 
			}
			return $array_return;
		}

		function get_report_scope_product($report_id){
			$scopes = $this->Preview_report_model->get_scope_audit($report_id);
			$array = array();
			foreach ($scopes as $key => $value) {
				$product = $this->Preview_report_model->get_products($value->scope_id, $report_id);
				$array[] = array(
							"scope"          => $value->scope_name,
							"scope_details"  => $value->scope_detail,
							"products"       => $product
						);
			}
			return $array;
		} 

	public function get_report_analysis_view($report_id){
 
		$arrayobj = $this->Audit_report_analysis_model->get_report_analysis_view2($report_id);
        $template = $this->Preview_report_model->get_template($report_id);
        $template_details = $this->Template_model->get_data_by_id($template[0]->template_id,'template_id','tbl_template');
        $classification = $this->Template_model->get_data_by_id($template_details[0]->classification_id,'classification_id','tbl_classification');

        $gmp_coverage = $this->Template_model->get_data_by_id($report_id,'report_id','gmp_computation_coverage');

        $limit = $classification[0]->limit;

        $answer_yes = 0;
        if(isset($arrayobj[0]->answer_yes)){
            $answer_yes = $arrayobj[0]->answer_yes;
        }

        $answer_no = 0;
        if(isset($arrayobj[0]->answer_nc)){
            $answer_no = $arrayobj[0]->answer_nc;
        }

        $answer_nc = 0;
        if(isset($arrayobj[0]->answer_na)){
            $answer_nc = $arrayobj[0]->answer_na;
        }

        $no_critical = 0;
        if(isset($arrayobj[0]->category_critical)){
            $no_critical = $arrayobj[0]->category_critical;
        }

        $no_major = 0;
        if(isset($arrayobj[0]->category_major)){
            $no_major = $arrayobj[0]->category_major;
        }

        $no_minor = 0;
        if(isset($arrayobj[0]->category_minor)){
            $no_minor = $arrayobj[0]->category_minor;
        }


        $auditdate_format =  $this->format_audit_date2($this->Audit_report_model->get_audit_dates($report_id));

        $count_question = 0;
        $elements_result = $this->Preview_report_model->preview_report_get_element($template[0]->template_id);
        $element_array = array();
        foreach ($elements_result as $key => $value) {  
            $element_questions = $this->Global_model->get_list_with_order("tbl_questions", "element_id = " . $value->element_id, "order_sort asc");
            foreach ($element_questions as $k => $v) {
                $count_question++;
            } 
        } 

		//report listing
		$report_lisiting = $this->Global_model->report_listing_model(1,0,"tbl_report_listing.report_id = " . $report_id);
		

        if(count($arrayobj) > 0 ){
            $obj = array(
                "NA"                => $arrayobj[0]->answer_na,
                "NC"                => $arrayobj[0]->answer_nc,
                "name"              => $arrayobj[0]->name,
                "no"                => $arrayobj[0]->answer_no,
                "no_critical"       => $arrayobj[0]->category_critical,
                "no_major"          => $arrayobj[0]->category_major,
                "no_minor"          => $arrayobj[0]->category_minor,
                "question_count"    => count($arrayobj),
                "report_id"         => $arrayobj[0]->report_id,
                "report_no"         => $arrayobj[0]->report_no,
                "yes"               => $arrayobj[0]->answer_yes,
                "applicable_q"      => (int)$arrayobj[0]->answer_yes + (int)$arrayobj[0]->answer_no,
                "no_question"       => $count_question,
                "rating"            => number_format($report_lisiting[0]->gmp_score,2),
                "coverage"          => number_format($report_lisiting[0]->gmp_coverage,2),
                "audit_date"        => $auditdate_format
            );
        } else {
            $query = "report_id = " . $report_id;
            $report = $this->q_auditreport->get_list($query,1,0, "");

            $obj = array(
                "NA"                => 0,
                "NC"                => 0,
                "name"              => $report[0]->name,
                "no"                => 0,
                "no_critical"       => 0,
                "no_major"          => 0,
                "no_minor"          => 0,
                "question_count"    => 0,
                "report_id"         => $report_id,
                "report_no"         => $report[0]->Report_No,
                "yes"               => 0,
                "applicable_q"      => 0,
                "no_question"       => $count_question,
                "rating"            => 0,
                "coverage"          => 0,
                "audit_date"        => $auditdate_format
            );
        }

        $file2 = getcwd().'/listing/'.$report_id.'_analysis.json';
        file_put_contents($file2,'');
        chmod($file2,0777);
        $fp2 = fopen($file2, 'w');
        fwrite($fp2, json_encode($obj));
        fclose($fp2);
	}

	function login($post){ 
		$success = "";
		$message = "";
		$data = array();
		if(isset($post['email']) && $post['email'] != null){
			if(isset($post['password'])  && $post['password'] != null){
				$email = $post['email'];
				$password = $post['password'];
				$data = array();
				if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
					
					$ch = curl_init();

					curl_setopt($ch, CURLOPT_URL, base_url() . "azure/pwgrant.php");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS,
					            "email=".$email."&password=" . $password);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$server_output = curl_exec ($ch);
					curl_close ($ch);

					$response = json_decode($server_output,false);
					if($response->status == "success") {
						$login_details = $this->Global_model->check_exist('tbl_auditor_info',$email);
						if(count($login_details) > 0){

                            if($login_details[0]->status == 1){
                                $success = "success";
                                $message = "Login Success!";
                                $data[] = array(
                                    "email"     => $login_details[0]->email,
                                    "user_id"   => $login_details[0]->auditor_id,
                                    "fname"     => $login_details[0]->fname,
                                    "mname"     => $login_details[0]->mname,
                                    "lname"     => $login_details[0]->lname,
                                    "role"      => $login_details[0]->administrator,
                                );
                                echo json_encode(array('status' => $success,'message'=>$message, 'data'=>$data),JSON_PRETTY_PRINT);
                        
                            } else {
                                $success = "failed";
                                $message = "You are not allowed to log in. Please contact administrator.";
                                $data = array();
                                echo json_encode(array('status' => $success,'message'=>$message, 'data'=>null),JSON_PRETTY_PRINT);
                                die();
                            }
						} else {
							$success = "failed";
							$message = "You are not allowed to log in. Please contact administrator.";
							$data = array();
                            echo json_encode(array('status' => $success,'message'=>$message, 'data'=>null),JSON_PRETTY_PRINT);
                            die();
						}
					} else {
						$success = "failed";
						$message = "Email address/password does not match.";
						$data = array();
                        echo json_encode(array('status' => $success,'message'=>$message, 'data'=>null),JSON_PRETTY_PRINT);
                        die();
					}
				} else {
					$success = "failed";
					$message = "Invalid Email";
					$data = array();
                    echo json_encode(array('status' => $success,'message'=>$message, 'data'=>null),JSON_PRETTY_PRINT);
                    die();
				}
			} else {
				$success = "failed";
				$message = "Password is Required";
				$data = array();
                echo json_encode(array('status' => $success,'message'=>$message, 'data'=>null),JSON_PRETTY_PRINT);
                die();
			}
		} else {
			$success = "failed";
			$message = "Email is Required";
			$data = array();
            echo json_encode(array('status' => $success,'message'=>$message, 'data'=>null),JSON_PRETTY_PRINT);
            die();
		}

		
	}

    public function check_report_status($id)
    {
        $result = $this->Global_model->report_listing_model(1,0,"tbl_report_listing.report_id = " . $id);
        return $result[0]->status;
    }

























    ///for testing of API ony

    public function tester(){
        $this->load->helper('form');

        echo form_open(base_url("api"));

        //token
        echo "Token <br>" . form_input('token', '35ced0a2f0ad35bdc9ae075ee213ea4b8e6c2839');
        echo "<br>";
        echo "cmdEvent <br>" . form_input('cmdEvent', 'postInput');
        echo "<br>";
        echo "Report ID <br>" . form_input('report_id', '');
        echo "<br>";
        echo "Report No <br>" . form_input('report_no', '');

        //company
        $site = array();
        $site_array = $this->test_json("data_maintenance/site.json");
        foreach ($site_array->company_list as $key => $value) {
            $site[] = array(
                $value->company_id  =>  $value->company_name,
            );
        }
        echo "<br>";
        echo "Company <br>" . form_dropdown('company_id', $site);

    }

    function test_json($path)
    {
        $json_path = base_url() . "json/";
        $json = file_get_contents($json_path . $path);
        $obj = json_decode($json);
        return $obj;
    }


    function login_audit_trail($email, $action)
    {
        $auditor = $this->Global_model->get_data_query("tbl_auditor_info", "email = '" . $email . "'");

        $data['user'] = $auditor[0]->auditor_id;
        $data['page'] = "/api";
        $data['type'] = "Auditor";
        $data['role'] = 1;
        $data['email'] = $auditor[0]->email;
        $data['action'] = $action;
        $data['date'] = date('Y-m-d H:i:s'); 
        $table = 'tbl_audit_trail';
        $this->Audit_trail_model->save_data($table,$data);
    }

    function add_audit_trail($user_id, $action, $report_no)
    {
        $auditor = $this->Global_model->get_data_query("tbl_auditor_info", "auditor_id = " . $user_id);

        $data['user'] = $user_id;
        $data['page'] = "/api";
        $data['type'] = "Auditor";
        $data['role'] = 1;
        $data['email'] = $auditor[0]->email;
        $data['action'] = $action . " - " . $report_no;
        $data['date'] = date('Y-m-d H:i:s'); 
        $table = 'tbl_audit_trail';
        $this->Audit_trail_model->save_data($table,$data);
    }

    function update_audit_trail($co_auditor_email, $action, $report_id)
    {
        $auditor = $this->Global_model->get_data_query("tbl_auditor_info", "email = '" . $co_auditor_email . "'");
        $report = $this->Global_model->get_data_query("tbl_report_summary", "report_id = " . $report_id);

        $data['user'] = $auditor[0]->auditor_id;
        $data['page'] = "/api";
        $data['type'] = "co-Auditor";
        $data['role'] = 1;
        $data['email'] = $auditor[0]->email;
        $data['action'] = $action . " - " . $report[0]->report_no;
        $data['date'] = date('Y-m-d H:i:s'); 
        $table = 'tbl_audit_trail';
        $this->Audit_trail_model->save_data($table,$data);
    }

    public function update_audit_history()
    {
        $report_id = $this->input->get("report_id");
        //add to audit history
        //get report details
        $string = file_get_contents("./listing/".$report_id.".json");
        $details = json_decode($string, false);
        $report = $details->Report_Summary;
        $approver = $details->Approver;
        $reviewer = $details->Reviewer;
        $coauditor = $details->Co_Auditors;
        $audit_date = $details->Audit_Dates;
        

        //add history
        $data_history = array(
            "supplier_id"        => $report[0]->company_id,
            "create_date"        => date("Y-m-d H:i:s"),
            "update_date"        => date("Y-m-d H:i:s"),
            "status"             => 1,
            "API"                => 1,
            "report_id"          => $report[0]->report_id,
            "report_no"          => $report[0]->report_no,
        );
        $history_id = $this->Global_model->save_data('tbl_audit_history', $data_history);    

        //get changes
        $changes = $this->Template_model->get_data_by_id($report_id,'report_id','tbl_inspection_changes');
        $orders = 1;
        foreach ($changes as $key => $value) {
            $data_changes = array(
                "history_id"    => $history_id,
                "changes"       => $value->changes,
                "orders"        => $orders
            );
            $this->Global_model->save_data('tbl_audit_history_changes', $data_changes);
            $orders++;
        }

        //Lead AUDITOR
        $data_auditor = array(
            "history_id"    =>  $history_id,
            "inspector"     =>  $report[0]->fname . " " . $report[0]->lname . " (Lead Auditor)"
        );
        $this->Global_model->save_data('tbl_audit_history_inspectors', $data_auditor);

        //CO AUDITORS
        if(count($coauditor) > 0){
            foreach ($coauditor as $key => $value) {
                $data_coauditor = array(
                    "history_id"    =>  $history_id,
                    "inspector"     =>  $value->fname . " " . $value->lname . " (Co-Auditor)"
                );
                $this->Global_model->save_data('tbl_audit_history_inspectors', $data_coauditor);
            }   
        }


        if(count($audit_date) > 0){
            foreach ($audit_date as $key => $value) {
                $data_audit_date = array(
                    "history_id"    =>  $history_id,
                    "date"          =>  $value->Date
                );
                $this->Global_model->save_data('tbl_audit_history_dates', $data_audit_date);
            }
        }

        $this->site();
    }

    public function report_computation(){
        
        $report_id = 25;
        $arrayobj = $this->Audit_report_analysis_model->get_report_analysis_view2($report_id);

        $template = $this->Preview_report_model->get_template($report_id);
        $template_details = $this->Template_model->get_data_by_id($template[0]->template_id,'template_id','tbl_template');
        $classification = $this->Template_model->get_data_by_id($template_details[0]->classification_id,'classification_id','tbl_classification');

        $answer_yes = $arrayobj[0]->answer_yes;
        $answer_no = $arrayobj[0]->answer_no;
        $answer_nc = $arrayobj[0]->answer_nc;
        $answer_na = $arrayobj[0]->answer_na;

        $limit = $classification[0]->limit;
        $No_Of_Major_To_Have_Unacceptable_Disposition = $classification[0]->no_major;
        $No_Of_Critical_To_Have_Unacceptable_Disposition = $classification[0]->no_critical;

        $no_critical = $arrayobj[0]->category_critical;
        $no_major = $arrayobj[0]->category_major;
        $no_minor = $arrayobj[0]->category_minor;

        $Total_No_of_Covered_Questions = (int)$answer_yes + (int)$answer_no;
        $Total_No_of_Applicable_Questions = (int)$Total_No_of_Covered_Questions + (int)$answer_nc;
        $Factor_for_Major = (((int)$answer_yes * 100) / $limit - (int)$Total_No_of_Applicable_Questions) / $No_Of_Major_To_Have_Unacceptable_Disposition;
        $Factor_for_Critical = (((int)$answer_yes * 100) / $limit - (int)$Total_No_of_Applicable_Questions) / $No_Of_Critical_To_Have_Unacceptable_Disposition;
        $Weight_for_Major = $no_major * $Factor_for_Major;
        $Weight_for_Critical = $no_critical * $Factor_for_Critical;
        $Weighted_Denominator = (int)$Total_No_of_Applicable_Questions + $Weight_for_Major + $Weight_for_Critical;
        $Rating = ((int)$answer_yes / $Weighted_Denominator) * 100;
        $audit_dates = $this->Audit_report_model->get_audit_dates($report_id);

        $len = count($audit_dates);
        $auddate = "";
        $audyear = "";
        $audmonth = "";
        foreach ($audit_dates as $key => $value) {
            $audmonth = date_format(date_create($value->audit_date),"F");
            $audyear = date_format(date_create($value->audit_date),"Y");
            if ($key == $len - 1) {
               $auddate .= ' & ' . date_format(date_create($value->audit_date),"d");
            } else {
               $auddate .= ', ' . date_format(date_create($value->audit_date),"d");
            }
        }

        $auditdate_format =  substr($auddate,2) . " " . $audmonth .  " " . $audyear;

        if(count($arrayobj) > 0 ){
            $obj = array(
                "NA"                => $arrayobj[0]->answer_na,
                "NC"                => $arrayobj[0]->answer_nc,
                "name"              => $arrayobj[0]->name,
                "no"                => $arrayobj[0]->answer_no,
                "no_critical"       => $arrayobj[0]->category_critical,
                "no_major"          => $arrayobj[0]->category_major,
                "no_minor"          => $arrayobj[0]->category_minor,
                "question_count"    => count($arrayobj),
                "report_id"         => $arrayobj[0]->report_id,
                "report_no"         => $arrayobj[0]->report_no,
                "yes"               => $arrayobj[0]->answer_yes,
                "rating"            => number_format($Rating,2),
                "coverage"          => number_format($arrayobj[0]->Coverage,2),
                "audit_date"        => $auditdate_format
            );
        } else {
            $query = "report_id = " . $report_id;
            $report = $this->q_auditreport->get_list($query,1,0, "");

            $obj = array(
                "NA"                => 0,
                "NC"                => 0,
                "name"              => $report[0]->name,
                "no"                => 0,
                "no_critical"       => 0,
                "no_major"          => 0,
                "no_minor"          => 0,
                "question_count"    => 0,
                "report_id"         => $report_id,
                "report_no"         => $report[0]->Report_No,
                "yes"               => 0,
                "rating"            => 0,
                "coverage"          => 0,
                "audit_date"        => $auditdate_format
            );
        }
        echo "<pre>";
        print_r($obj);
    }


    public function devtest()
    {


        $this->load->helper('form');
        $this->load->library('javascript');

        echo '
            <style>
                input, select, textarea {
                    padding: 5px;
                    width: 50%;
                    border-radius: 3px;
                    border-style: solid;
                    border: solid 1px #ccc;
                }

            </style>
        ';
        echo form_open('api');

        //token
        echo "Token<br>";
        echo form_input('token', '35ced0a2f0ad35bdc9ae075ee213ea4b8e6c2839');
        echo "<br><br>";

        //event
        echo "Command Event<br>";
        echo form_input('cmdEvent', 'postInput');
        echo "<br><br>";

        $company = $this->get_json(base_url() . "/json/data_maintenance/site.json");
        $company_array[""] = "Select...";
        foreach ($company->company_list as $key => $value) {
            if($value->status == 1){
                $company_array[$value->company_id] = $value->company_name;
            }
        }


        echo "Company<br>";
        echo form_dropdown('company_id', $company_array);
        echo "<br><br>";


        //report id
        echo "Report ID (Blank if new report)<br>";
        echo form_input('report_id', '');
        echo "<br><br>";

        //report no
        echo "Report No (Blank if new report)<br>";
        echo form_input('report_no', '');
        echo "<br><br>";

        //template
        $template = $this->get_json(base_url() . "/json/template/template_list.json");
        $template_array[""] = "Select...";
        foreach ($template->template_list as $key => $value) {
            if($value->status == 1){
                $template_array[$value->template_id] = $value->template_id;
            }
        }


        echo "Template<br>";
        $template_config = array( 'name' => 'template_id', 'id'=>'template');
        echo form_dropdown($template_config, $template_array);
        echo "<br><br>";

        echo "Template Details<br>";
        echo "<div class='template_details'></div>";
        echo "<br><br>";


        //{"question_id":1094,"answer_id":1,"answer_details":"Question Answer Details 5","category_id":3}

        echo '
            <script type="text/javascript" src="' . base_url() .'asset/js/jquery-1.12.4.js"></script>
            <script type="text/javascript" src="' . base_url() .'asset/js/jquery-ui.js"></script>
            <script>
                $(document).on("change", "#template", function(el) {
                    var id = $(this).val();
                    $.get( "' . base_url() .'/json/template/"+id+".json", function( result ) {
                        var html = "";
                        $.each(result.elements, function(a,b){
                            console.log(b.questions);
                            $.each(b.questions, function(c,d){
                                
                                html += d.question + "<br>";
                                html += "<input type="text" name="question" value="+d.default_yes+">";
                            })
                        });
                        $(".template_details").html(html);
                    });
                });
            </script>
        ';

    }

    public function get_json($url)
    {
        return json_decode($this->samscurl->load($url));
        // $ch1 = curl_init();
        // curl_setopt($ch1, CURLOPT_URL,$url);
        // curl_setopt($ch1, CURLOPT_POST, 1);
        // curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
        // return json_decode(curl_exec ($ch1));
	}
	

	public function backup()
	{
		ini_set('max_execution_time', 0); 	
		ini_set('memory_limit','2048M');

		$this->load->dbutil();
		$prefs = array(
			'filename' => 'backup.sql',
			'format'        => 'zip', 
            'add_drop' => FALSE,
            'add_insert' => TRUE,
            'newline' => "\n" 
        );
		$backup = $this->dbutil->backup($prefs);

		// Load the file helper and write the file to your server
		$this->load->helper('file');
		write_file('./backup/'.date('Y-m-d_H-i-s', time()).'.zip', $backup);

		// Load the download helper and send the file to your desktop
		$this->load->helper('download');
		force_download(date('Y-m-d_H-i-s', time()).'.zip', $backup);
	}
}