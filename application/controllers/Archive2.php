<?php 

defined('BASEPATH') OR exit('No direct script access allowed');



class Archive2 extends CI_Controller{



	public function __construct() {

        parent::__construct();

        $this->load->library('session');

        $this->load->model('Archive2_model');

        $this->load->helper('url');

        $this->load->library('Pdf');

    }



    public function archive_template2()

	{	

		$data['content'] = 'archive_template/list2';

		$this->load->view('layout/layout',$data);

	}



	public function archive_report()

	{	

		$data['content'] = 'archive_report/list';

		$this->load->view('layout/layout',$data);

	}



	public function getlist_archive_template(){

		$table = $_POST['table'];

		$table1 = $_POST['table1'];

		$txt_search = $_POST['txt_search'];

		$limit = $_POST['limit'];

		$offset = $_POST['offset'];

		$offset = ($_POST['offset']-1)* $limit;

		$sort = $_POST['order_by'];

		$data = $this->Archive_model->getlist_archive_template($table, $table1, $limit,$offset,$sort,$txt_search);

		echo json_encode($data);

	}



	function getlist_archive_template_count(){

		$table = $_POST['table'];

		$table1 = $_POST['table1'];

		$txt_search = $_POST['txt_search'];

		$per_page = $_POST['limit'];

		$limit = '9999999';

		$offset = '0';

		$sort = $_POST['order_by'];

		$data = count($this->Archive_model->getlist_archive_template($table, $table1, $limit,$offset,$sort,$txt_search));

		$page_count = ceil($data/$per_page);

		echo $page_count;

	}



	function get_report_archive(){

		$limit = $_POST['limit'];

		$offset = ($_POST['offset']-1)* $limit;

		$data = $this->Archive_model->get_report_list_archive($limit,$offset);

		echo json_encode($data);

	}
	public function getlist_archive_template2(){

		// header('Content-type: Application/JSON');

		$limit = $_POST['limit'];

		$offset = $_POST['offset'];

		$query = $_POST['query'];


		echo json_encode($this->Archive2_model->getlist_archive_template2($query,$limit,$offset), JSON_PRETTY_PRINT);

	}
	 	public function get_pagination_archive_template(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Archive2_model->get_pagination_archive_template($query);

	}


}



?>