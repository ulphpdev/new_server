<?php 

defined('BASEPATH') OR exit('No direct script access allowed');



class Auditreport extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('global_model');
		$this->load->model('Audit_trail_model');
		$this->load->model('q_auditreport');
		$this->load->helper('url');

		if($this->session->userdata('sess_email')=='' ) { 
			redirect(base_url("login"));
		} else {
			if($this->session->userdata('type')== 'approver'){
				redirect(base_url("login/unsetSession"));
			}
			if($this->session->userdata('type')== 'reviewer'){
				redirect(base_url("login/unsetSession"));
			}
		}

	}

	public function index3()
	{	
		$data['content' ]= 'audit_report/list2';
		$data['type_of_products'] = $this->q_auditreport->group_list("tbl_classification", "classification_name");
		$data['auditors'] = $this->q_auditreport->group_list("tbl_auditor_info", "fname");
		$data['products_list'] = $this->q_auditreport->group_list("tbl_product", "product_name");
		$data['disposition_list'] = $this->q_auditreport->group_list("tbl_disposition");
		$data['country_list'] = $this->q_auditreport->get_country();
		$this->load->view('layout/layout',$data);
	}

	public function index()
	{	
		$data['content' ]= 'audit_report/list2';
		$data['type_of_products'] = $this->q_auditreport->group_list("tbl_classification", "classification_name");
		$data['auditors'] = $this->q_auditreport->group_list("tbl_auditor_info", "fname");
		$data['products_list'] = $this->q_auditreport->group_list("tbl_product", "product_name");
		$data['disposition_list'] = $this->q_auditreport->group_list("tbl_disposition");
		$data['country_list'] = $this->q_auditreport->get_country();
		$this->load->view('layout/layout',$data);
	}

}
