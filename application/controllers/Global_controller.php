<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Manila');
class Global_controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('global_model');
        $this->load->model('Audit_trail_model');
        $this->load->model('Template_model');
        $this->load->helper('url');
        $this->load->library('Pdf');
        $this->load->library('user_agent');
    }
public function getlist_standard(){
  $table = $_POST['table'];
  $table2 = "tbl_classification";
  $limit = $_POST['limit'];
  $offset = $_POST['offset'];
  $offset = ($_POST['offset']-1)* $limit;
  $sort = $_POST['order_by'];
  $data = $this->global_model->getlist_standard_modal($table, $table2, $limit,$offset,$sort);
  echo json_encode($data);
}
function getcount_global() {
  $table = $_POST['table'];
  $order_by = $_POST['order_by'];
  $per_page = $_POST['limit'];
  $limit = '9999999';
  $offset = '0';
  $data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));
  $page_count = ceil($data/$per_page);
  echo $page_count;
}
function getcount_global2() {
  $table = $_POST['table'];
  $order_by = $_POST['order_by'];
  $per_page = $_POST['limit'];
  $limit = '9999999';
  $offset = '0';
  $data = count($this->global_model->getlist_global_model2($table,$limit,$offset,$order_by));
  $page_count = ceil($data/$per_page);
  echo $page_count;
}
function delete_global(){
  $table = $_POST['table'];
  $field = $_POST['order_by'];
  $id = $_POST['id'];
  $this->global_model->delete_data($id, $table, $field);
}
function inactive_global_update(){
  $table = $_POST['table'];
  // $table1 = $_POST['table1'];
  $field = $_POST['order_by'];
  $id = $_POST['id'];
  $type = $_POST['type'];
  $data = array('status' => $type, 'update_date'=>date("Y-m-d H:i:s"));
  $this->global_model->inactive_global($id, $data, $field, $table);
}
public function action_global(){
  $module = $_POST['module'];
  $type = $_POST['type'];
  $data['id'] = $_POST['id'];
  $this->load->view($module.'/'.$type,$data);
}
function edit_global() {
  $table = $_POST['table'];
  // $table1 = $_POST['table1'];
  $field = $_POST['field'];
  $id = $_POST['id'];
  $data = $this->global_model->getedit_global($id,$table,$field);
  echo json_encode($data);

}
/* image manager */
function getimages(){
  $folder = $_POST['folder'];
  $i= 4;
  $files = glob("images/".$folder."*.*");
  echo "<div class = 'col-md-12 size' style='overflow:scroll; height: 350px;'> ";
  for ($i=0; $i<count($files); $i++)
  {
    $path = $files[$i];
     $filename = explode('/', $path);
     $filename =  $filename[count($filename)-1];
     $file_folder = explode('.',$filename);
     $ext = strtolower($file_folder[1]);
     if($ext=='jpeg' || $ext=='jpg' || $ext=='png' || $ext=='gif' || $ext=='doc' || $ext=='docx' || $ext=='xls' || $ext=='xlsx' || $ext=='pdf' ||$ext=='mp4'){
    echo "<div class = 'col-md-2 col-xs-2 col-sm-2 pad-5 image-con' style = 'margin:10px 9px; border-radius:3px;'> <div class = 'col-md-12 pad-0 size-img' style='overflow: hidden; height: 80px;'>
      <img style = 'position:absolute; display:none' class = 'check check_".$i."' width='15px' src = '".base_url()."asset/img/check.png'>
     <p style = 'display:none;'>" . $path;
      $file_type = 'file';
      if($ext=='doc' || $ext=='docx')
      {
        $path2 = base_url().'asset/img/doc.png';
      }
      elseif($ext=='xls' || $ext=='xlsx')
      {
        $path2 = base_url().'asset/img/excel.png';
      }
      elseif($ext=='pdf')
      {
        $path2 = base_url().'asset/img/pdf.png';
      }
      elseif($ext=='mp4')
      {
        $path2 = base_url().'asset/img/mp4.jpg';
      }
      else
      {
        $path2 = base_url().$path;
        $file_type = 'image';
      }
      echo '</p><img title="'.$filename.'" data-type="'.$file_type.'" file-name = "'.$path.'" class = "image-file-copy img-file_'.$i.'" data-id = "'.$i.'" style = "height:100%; cursor:pointer; text-align:center;" src="'.$path2.'" alt="random image" />'."</div><d style = 'font-size:10px;' title='".$filename."'>".substr($filename, 0, 17)."</d></div>";
     }
  }
  echo "</div>";
  echo "</div>";
}
function create_folder() {
  $directory = 'images/'.$_POST['new_folder'].'/';
  if (!is_dir($directory)) {
    mkdir($directory,0777,true);
    echo "yes";
  }
}
/* image manager */
/* upload module */
function uploadfile(){
  $filename = pathinfo($_FILES["file"]["name"]);
  $image_path = $filename['filename'].'_'.time().'.'.$filename['extension'];
  $path = 'images/'.$_POST['upload_path'];
  if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
        move_uploaded_file($_FILES['file']['tmp_name'], $path.$image_path);
    }
}
/* upload module */
function checkAction($action, $data, $table, $field, $id) {
  if($action=="Save"){
    $data = $this->global_model->save_data($table, $data); 
  } else {
    $this->global_model->update_data($field, $id, $table, $data);
    $data = $id;
  }
  return $data;
}
/* Start of User */
function user_array() {
  $table = $_POST['table'];
  $field = $_POST['field'];
  $action = $_POST['action'];
  
  if($action=="update") {
    $id = $_POST['id']; 
    $action = 'Update';
    $data['update_date'] = date("Y-m-d H:i:s");
  }else{
    $id = 0; 
    $action = 'Save';
    $data['update_date'] = date("Y-m-d H:i:s");
    $data['create_date'] = date("Y-m-d H:i:s");
    $data['status'] = $_POST['status'];
  }
  $data['fname'] = strip_tags($_POST['fname']);
  $data['mname'] = strip_tags($_POST['mname']);
  $data['lname'] = strip_tags($_POST['lname']);
  $data['designation'] = strip_tags($_POST['designation']);
  $data['department'] = strip_tags($_POST['division']);
  $data['company'] = strip_tags($_POST['company']);
  $data['administrator'] = strip_tags($_POST['administrator']);

  if($_POST['type'] == "auditor"){
    $data['reviewer'] = strip_tags($_POST['reviewer']);
    $data['approver'] = strip_tags($_POST['approver']);
  }

  if($_POST['type'] == "approver"){
    $data['acting_id'] = 0;
  }

  if($_POST['type'] == "reviewer"){
    $data['acting_id'] = 0;
  }
  
  $data['email'] = $_POST['email'];
  $reg_id = $this->checkAction($action, $data, $table, $field, $id);

  if($_POST['type'] == "auditor"){


    $data2['fname'] = strip_tags($_POST['fname']);
    $data2['mname'] = strip_tags($_POST['mname']);
    $data2['lname'] = strip_tags($_POST['lname']);
    $data2['designation'] = strip_tags($_POST['designation']);
    $data2['department'] = strip_tags($_POST['division']);
    $data2['company'] = strip_tags($_POST['company']);
    $data2['administrator'] = 0;
    $data2['email'] = strip_tags($_POST['email']);

    //acting Reviewer
    if($_POST['reviewer'] == 1){
      //check if exisitng

      $reviewer_count = count($this->global_model->get_list_data_query("tbl_reviewer_info", "acting_id = " . $reg_id . " AND status = 1"));
      if($reviewer_count > 0){
        //update
        $data2['acting_id'] = $reg_id;
        $data2['update_date'] = date("Y-m-d H:i:s");
        $this->checkAction("Update", $data2, "tbl_reviewer_info", "acting_id", $reg_id);
      } else {
        //add
        $data2['acting_id'] = $reg_id;
        $data2['update_date'] = date("Y-m-d H:i:s");
        $data2['create_date'] = date("Y-m-d H:i:s");
        $data2['status'] = 1;
        $this->checkAction("Save", $data2, "tbl_reviewer_info", "acting_id", $reg_id);
      }
    } else {
        $data2['update_date'] = date("Y-m-d H:i:s");
        $data2['status'] = -2;
        $this->checkAction("Update", $data2, "tbl_reviewer_info", "acting_id", $reg_id);  
    }


    //acting Approver
    if($_POST['approver'] == 1){
      //check if exisitng
      $approver_count = count($this->global_model->get_list_data_query("tbl_approver_info", "acting_id = " . $reg_id . " AND status = 1"));
      if($approver_count > 0){
        //update
        $data2['acting_id'] = $reg_id;
        $data2['update_date'] = date("Y-m-d H:i:s");
        $this->checkAction("Update", $data2, "tbl_approver_info", "acting_id", $reg_id);
      } else {
        //add
        $data2['acting_id'] = $reg_id;
        $data2['update_date'] = date("Y-m-d H:i:s");
        $data2['create_date'] = date("Y-m-d H:i:s");
        $data2['status'] = 1;
        $this->checkAction("Save", $data2, "tbl_approver_info", "acting_id", $reg_id);
      }
    } else {
        //remove
        $data2['update_date'] = date("Y-m-d H:i:s");
        $data2['status'] = -2;
        $this->checkAction("Update", $data2, "tbl_approver_info", "acting_id", $reg_id);
    }
  } 
}

function auditor_status_update(){
  $id = $_POST['id']; 
  $type = $_POST['type'];
  $data = array('status' => $type, 'update_date'=>date("Y-m-d H:i:s"));

  //auditor table
  $this->global_model->inactive_global($id, $data, "auditor_id", "tbl_auditor_info");

  //reviewer table
  $reviewer_count = count($this->Template_model->get_data_by_id($id,'acting_id','tbl_reviewer_info'));
    if($reviewer_count > 0){
        $this->checkAction("Update", $data, "tbl_reviewer_info", "acting_id", $id);  
    }

  //approver table
  $approver_count = count($this->Template_model->get_data_by_id($id,'acting_id','tbl_approver_info'));
  if($approver_count > 0){
        $this->checkAction("Update", $data, "tbl_approver_info", "acting_id", $id);  
  }
}

function save_category() {
  $table = $_POST['table'];
  $field = $_POST['field']; 
  $action = $_POST['action'];
  $id = "";
  if($action=="update") {
    $id = $_POST['id'];
    $action = 'Update';
    $data = array(
      'category_name' => ucwords(strip_tags($_POST['catname'])),
      'update_date' => date("Y-m-d H:i:s"),
    );
  }else{
    $action = 'Save';
    $data = array(
      'category_name' => ucwords(strip_tags($_POST['catname'])),
      'create_date' => date("Y-m-d H:i:s"),
      'update_date' => date("Y-m-d H:i:s"),
      'status' => 1,
    );
  }
  $data = json_decode(json_encode($data), FALSE);
  $this->checkAction($action, $data, $table, $field, $id);
  ;
}
function save_classification_category(){
  $data['classification_id'] = strip_tags($_POST['classification_id']);
  $data['category_id'] = strip_tags($_POST['category_id']);
  $data['description'] = strip_tags($_POST['description']);
  $table = 'tbl_classification_category';
  $this->global_model->save_data($table, $data); 
}
function update_classification_category(){
  $cat_id = strip_tags($_POST['cat_id']);
  $cat_class = strip_tags($_POST['catclass']);
  $description = strip_tags($_POST['description']);
  $this->global_model->update_classification_category($cat_id, $cat_class, $description); 
}
function save_classif() {
  $table = $_POST['table'];
  $field = $_POST['field'];
  $action = $_POST['action'];
  $id = "";
  // echo $_POST['category'];
  if($action=="update") {
    $id = $_POST['id'];
    $action = 'Update';
    $data = array(
      'classification_name' => ucwords(strip_tags($_POST['catname'])),
      'limit'=> $_POST['limit'],
      'no_major'=> $_POST['major'],
      'no_critical'=> $_POST['critical'],
      'update_date' => date("Y-m-d H:i:s"),
    );
  }else{
    $action = 'Save';
    $data = array(
      'classification_name' => ucwords(strip_tags($_POST['catname'])),
      'limit'=> $_POST['limit'],
      'no_major'=> $_POST['major'],
      'no_critical'=> $_POST['critical'],
      'create_date' => date("Y-m-d H:i:s"),
      'update_date' => date("Y-m-d H:i:s"),
      'status' => 1,
    );
  }
  $data = json_decode(json_encode($data), FALSE);
 $is_exist = $this->global_model->is_checking($table, 'classification_name', ucwords($_POST['catname']));
 echo $this->checkAction($action, $data, $table, $field, $id);
}
function save_temp() {
  $table = $_POST['table'];
  $field = $_POST['field'];
  $action = $_POST['action'];
  $id = "";
  if($action=="update") {
    $action = 'Update';
    $id = $_POST['id'];
    $data = array(
      'standard_name' => strip_tags($_POST['catname']),
      // 'classification_id' => ucwords($_POST['classif_id']),
      'update_date' => date("Y-m-d H:i:s"),
    );
  }else{
    $action = 'Save';
    $data = array(
      'standard_name' => strip_tags($_POST['catname']),
      // 'classification_id' => ucwords($_POST['classif_id']),
      'create_date' => date("Y-m-d H:i:s"),
      'update_date' => date("Y-m-d H:i:s"),
      'status' => 1,
    );
  }

  $data = json_decode(json_encode($data), FALSE);
  $this->checkAction($action, $data, $table, $field, $id);
}

function user_login() {
  $table = $_POST['table'];
  $field = $_POST['field'];
  $action = $_POST['action'];
  $id = "";
  if($action=="update") {
    $id = $_POST['id'];
  }
  $data = array(
    'name' => $_POST['name'],
    'status' => $_POST['status'],
    'role' => $_POST['role'],
    'email' => $_POST['email']
  );
  $data = json_decode(json_encode($data), FALSE);
  $this->checkAction($action, $data, $table, $field, $id);
}
public function get_where_user(){
        $field = $_POST['field'];
        $where = $_POST['where'];
        $table = $_POST['table'];
        $data = $this->global_model->get_where($table, $field, $where);
        echo json_encode(array('count' => count($data), 'result' => $data));
}
public function check_email_exist(){
  $email = $_POST['email'];
  $data = $this->global_model->check_email_table_exist($email,'tbl_auditor_info');
  echo $data;
}
/* End of user Login */
public function check_email(){ 
      $where = $_POST['user'];
      $data = $this->global_model->check_exist('tbl_auditor_info', $where);
      if(count($data) > 0){
        $role = $data[0]->administrator;
        $reg_id = $data[0]->auditor_id;
        $status = $data[0]->status;
        echo json_encode(array('status'=>$status, 'role' => $role, 'userid' => $reg_id,'name' => $data[0]->fname." ".$data[0]->lname ));
      } else {
        echo json_encode(array('status'=>0));
      }
}

public function get_data(){
  $data = $this->global_model->getdata();
  if($_POST['table']=='country'){   
    $array2 = array();
    foreach ($data as $key => $value) {
      $array = array();
      $array['country_id'] = $value->country_id;
      $array['country'] = $this->country_code_to_country($value->iso2_code);
      array_push($array2, $array);
    }
    $data = array();
    $data = $array2;
  }
  echo json_encode($data);
}
public function get_data_city_province(){
  $id = $_POST['id'];
  $field = $_POST['field'];
  $table = $_POST['table'];
  $data = get_data_city_province_model($table,$field,$id);
  echo json_encode($data);
}
public function convert_iso_name(){
  $names = json_decode(file_get_contents("http://country.io/names.json"), true);
  echo $names[$_POST['country']];
}
public function country_code_to_country( $code ){
    $country = '';
    if( $code == 'AF' ) $country = 'Afghanistan';
    if( $code == 'AX' ) $country = 'Aland Islands';
    if( $code == 'AL' ) $country = 'Albania';
    if( $code == 'DZ' ) $country = 'Algeria';
    if( $code == 'AS' ) $country = 'American Samoa';
    if( $code == 'AD' ) $country = 'Andorra';
    if( $code == 'AO' ) $country = 'Angola';
    if( $code == 'AI' ) $country = 'Anguilla';
    if( $code == 'AQ' ) $country = 'Antarctica';
    if( $code == 'AG' ) $country = 'Antigua and Barbuda';
    if( $code == 'AR' ) $country = 'Argentina';
    if( $code == 'AM' ) $country = 'Armenia';
    if( $code == 'AW' ) $country = 'Aruba';
    if( $code == 'AU' ) $country = 'Australia';
    if( $code == 'AT' ) $country = 'Austria';
    if( $code == 'AZ' ) $country = 'Azerbaijan';
    if( $code == 'BS' ) $country = 'Bahamas the';
    if( $code == 'BH' ) $country = 'Bahrain';
    if( $code == 'BD' ) $country = 'Bangladesh';
    if( $code == 'BB' ) $country = 'Barbados';
    if( $code == 'BY' ) $country = 'Belarus';
    if( $code == 'BE' ) $country = 'Belgium';
    if( $code == 'BZ' ) $country = 'Belize';
    if( $code == 'BJ' ) $country = 'Benin';
    if( $code == 'BM' ) $country = 'Bermuda';
    if( $code == 'BT' ) $country = 'Bhutan';
    if( $code == 'BO' ) $country = 'Bolivia';
    if( $code == 'BA' ) $country = 'Bosnia and Herzegovina';
    if( $code == 'BW' ) $country = 'Botswana';
    if( $code == 'BV' ) $country = 'Bouvet Island (Bouvetoya)';
    if( $code == 'BR' ) $country = 'Brazil';
    if( $code == 'IO' ) $country = 'British Indian Ocean Territory (Chagos Archipelago)';
    if( $code == 'VG' ) $country = 'British Virgin Islands';
    if( $code == 'BN' ) $country = 'Brunei Darussalam';
    if( $code == 'BG' ) $country = 'Bulgaria';
    if( $code == 'BF' ) $country = 'Burkina Faso';
    if( $code == 'BI' ) $country = 'Burundi';
    if( $code == 'KH' ) $country = 'Cambodia';
    if( $code == 'CM' ) $country = 'Cameroon';
    if( $code == 'CA' ) $country = 'Canada';
    if( $code == 'CV' ) $country = 'Cape Verde';
    if( $code == 'KY' ) $country = 'Cayman Islands';
    if( $code == 'CF' ) $country = 'Central African Republic';
    if( $code == 'TD' ) $country = 'Chad';
    if( $code == 'CL' ) $country = 'Chile';
    if( $code == 'CN' ) $country = 'China';
    if( $code == 'CX' ) $country = 'Christmas Island';
    if( $code == 'CC' ) $country = 'Cocos (Keeling) Islands';
    if( $code == 'CO' ) $country = 'Colombia';
    if( $code == 'KM' ) $country = 'Comoros the';
    if( $code == 'CD' ) $country = 'Congo';
    if( $code == 'CG' ) $country = 'Congo the';
    if( $code == 'CK' ) $country = 'Cook Islands';
    if( $code == 'CR' ) $country = 'Costa Rica';
    if( $code == 'CI' ) $country = 'Cote d\'Ivoire';
    if( $code == 'HR' ) $country = 'Croatia';
    if( $code == 'CU' ) $country = 'Cuba';
    if( $code == 'CY' ) $country = 'Cyprus';
    if( $code == 'CZ' ) $country = 'Czech Republic';
    if( $code == 'DK' ) $country = 'Denmark';
    if( $code == 'DJ' ) $country = 'Djibouti';
    if( $code == 'DM' ) $country = 'Dominica';
    if( $code == 'DO' ) $country = 'Dominican Republic';
    if( $code == 'EC' ) $country = 'Ecuador';
    if( $code == 'EG' ) $country = 'Egypt';
    if( $code == 'SV' ) $country = 'El Salvador';
    if( $code == 'GQ' ) $country = 'Equatorial Guinea';
    if( $code == 'ER' ) $country = 'Eritrea';
    if( $code == 'EE' ) $country = 'Estonia';
    if( $code == 'ET' ) $country = 'Ethiopia';
    if( $code == 'FO' ) $country = 'Faroe Islands';
    if( $code == 'FK' ) $country = 'Falkland Islands (Malvinas)';
    if( $code == 'FJ' ) $country = 'Fiji the Fiji Islands';
    if( $code == 'FI' ) $country = 'Finland';
    if( $code == 'FR' ) $country = 'France, French Republic';
    if( $code == 'GF' ) $country = 'French Guiana';
    if( $code == 'PF' ) $country = 'French Polynesia';
    if( $code == 'TF' ) $country = 'French Southern Territories';
    if( $code == 'GA' ) $country = 'Gabon';
    if( $code == 'GM' ) $country = 'Gambia the';
    if( $code == 'GE' ) $country = 'Georgia';
    if( $code == 'DE' ) $country = 'Germany';
    if( $code == 'GH' ) $country = 'Ghana';
    if( $code == 'GI' ) $country = 'Gibraltar';
    if( $code == 'GR' ) $country = 'Greece';
    if( $code == 'GL' ) $country = 'Greenland';
    if( $code == 'GD' ) $country = 'Grenada';
    if( $code == 'GP' ) $country = 'Guadeloupe';
    if( $code == 'GU' ) $country = 'Guam';
    if( $code == 'GT' ) $country = 'Guatemala';
    if( $code == 'GG' ) $country = 'Guernsey';
    if( $code == 'GN' ) $country = 'Guinea';
    if( $code == 'GW' ) $country = 'Guinea-Bissau';
    if( $code == 'GY' ) $country = 'Guyana';
    if( $code == 'HT' ) $country = 'Haiti';
    if( $code == 'HM' ) $country = 'Heard Island and McDonald Islands';
    if( $code == 'VA' ) $country = 'Holy See (Vatican City State)';
    if( $code == 'HN' ) $country = 'Honduras';
    if( $code == 'HK' ) $country = 'Hong Kong';
    if( $code == 'HU' ) $country = 'Hungary';
    if( $code == 'IS' ) $country = 'Iceland';
    if( $code == 'IN' ) $country = 'India';
    if( $code == 'ID' ) $country = 'Indonesia';
    if( $code == 'IR' ) $country = 'Iran';
    if( $code == 'IQ' ) $country = 'Iraq';
    if( $code == 'IE' ) $country = 'Ireland';
    if( $code == 'IM' ) $country = 'Isle of Man';
    if( $code == 'IL' ) $country = 'Israel';
    if( $code == 'IT' ) $country = 'Italy';
    if( $code == 'JM' ) $country = 'Jamaica';
    if( $code == 'JP' ) $country = 'Japan';
    if( $code == 'JE' ) $country = 'Jersey';
    if( $code == 'JO' ) $country = 'Jordan';
    if( $code == 'KZ' ) $country = 'Kazakhstan';
    if( $code == 'KE' ) $country = 'Kenya';
    if( $code == 'KI' ) $country = 'Kiribati';
    if( $code == 'KP' ) $country = 'Korea';
    if( $code == 'KR' ) $country = 'Korea';
    if( $code == 'KW' ) $country = 'Kuwait';
    if( $code == 'KG' ) $country = 'Kyrgyz Republic';
    if( $code == 'LA' ) $country = 'Lao';
    if( $code == 'LV' ) $country = 'Latvia';
    if( $code == 'LB' ) $country = 'Lebanon';
    if( $code == 'LS' ) $country = 'Lesotho';
    if( $code == 'LR' ) $country = 'Liberia';
    if( $code == 'LY' ) $country = 'Libyan Arab Jamahiriya';
    if( $code == 'LI' ) $country = 'Liechtenstein';
    if( $code == 'LT' ) $country = 'Lithuania';
    if( $code == 'LU' ) $country = 'Luxembourg';
    if( $code == 'MO' ) $country = 'Macao';
    if( $code == 'MK' ) $country = 'Macedonia';
    if( $code == 'MG' ) $country = 'Madagascar';
    if( $code == 'MW' ) $country = 'Malawi';
    if( $code == 'MY' ) $country = 'Malaysia';
    if( $code == 'MV' ) $country = 'Maldives';
    if( $code == 'ML' ) $country = 'Mali';
    if( $code == 'MT' ) $country = 'Malta';
    if( $code == 'MH' ) $country = 'Marshall Islands';
    if( $code == 'MQ' ) $country = 'Martinique';
    if( $code == 'MR' ) $country = 'Mauritania';
    if( $code == 'MU' ) $country = 'Mauritius';
    if( $code == 'YT' ) $country = 'Mayotte';
    if( $code == 'MX' ) $country = 'Mexico';
    if( $code == 'FM' ) $country = 'Micronesia';
    if( $code == 'MD' ) $country = 'Moldova';
    if( $code == 'MC' ) $country = 'Monaco';
    if( $code == 'MN' ) $country = 'Mongolia';
    if( $code == 'ME' ) $country = 'Montenegro';
    if( $code == 'MS' ) $country = 'Montserrat';
    if( $code == 'MA' ) $country = 'Morocco';
    if( $code == 'MZ' ) $country = 'Mozambique';
    if( $code == 'MM' ) $country = 'Myanmar';
    if( $code == 'NA' ) $country = 'Namibia';
    if( $code == 'NR' ) $country = 'Nauru';
    if( $code == 'NP' ) $country = 'Nepal';
    if( $code == 'AN' ) $country = 'Netherlands Antilles';
    if( $code == 'NL' ) $country = 'Netherlands the';
    if( $code == 'NC' ) $country = 'New Caledonia';
    if( $code == 'NZ' ) $country = 'New Zealand';
    if( $code == 'NI' ) $country = 'Nicaragua';
    if( $code == 'NE' ) $country = 'Niger';
    if( $code == 'NG' ) $country = 'Nigeria';
    if( $code == 'NU' ) $country = 'Niue';
    if( $code == 'NF' ) $country = 'Norfolk Island';
    if( $code == 'MP' ) $country = 'Northern Mariana Islands';
    if( $code == 'NO' ) $country = 'Norway';
    if( $code == 'OM' ) $country = 'Oman';
    if( $code == 'PK' ) $country = 'Pakistan';
    if( $code == 'PW' ) $country = 'Palau';
    if( $code == 'PS' ) $country = 'Palestinian Territory';
    if( $code == 'PA' ) $country = 'Panama';
    if( $code == 'PG' ) $country = 'Papua New Guinea';
    if( $code == 'PY' ) $country = 'Paraguay';
    if( $code == 'PE' ) $country = 'Peru';
    if( $code == 'PH' ) $country = 'Philippines';
    if( $code == 'PN' ) $country = 'Pitcairn Islands';
    if( $code == 'PL' ) $country = 'Poland';
    if( $code == 'PT' ) $country = 'Portugal, Portuguese Republic';
    if( $code == 'PR' ) $country = 'Puerto Rico';
    if( $code == 'QA' ) $country = 'Qatar';
    if( $code == 'RE' ) $country = 'Reunion';
    if( $code == 'RO' ) $country = 'Romania';
    if( $code == 'RU' ) $country = 'Russian Federation';
    if( $code == 'RW' ) $country = 'Rwanda';
    if( $code == 'BL' ) $country = 'Saint Barthelemy';
    if( $code == 'SH' ) $country = 'Saint Helena';
    if( $code == 'KN' ) $country = 'Saint Kitts and Nevis';
    if( $code == 'LC' ) $country = 'Saint Lucia';
    if( $code == 'MF' ) $country = 'Saint Martin';
    if( $code == 'PM' ) $country = 'Saint Pierre and Miquelon';
    if( $code == 'VC' ) $country = 'Saint Vincent and the Grenadines';
    if( $code == 'WS' ) $country = 'Samoa';
    if( $code == 'SM' ) $country = 'San Marino';
    if( $code == 'ST' ) $country = 'Sao Tome and Principe';
    if( $code == 'SA' ) $country = 'Saudi Arabia';
    if( $code == 'SN' ) $country = 'Senegal';
    if( $code == 'RS' ) $country = 'Serbia';
    if( $code == 'SC' ) $country = 'Seychelles';
    if( $code == 'SL' ) $country = 'Sierra Leone';
    if( $code == 'SG' ) $country = 'Singapore';
    if( $code == 'SK' ) $country = 'Slovakia (Slovak Republic)';
    if( $code == 'SI' ) $country = 'Slovenia';
    if( $code == 'SB' ) $country = 'Solomon Islands';
    if( $code == 'SO' ) $country = 'Somalia, Somali Republic';
    if( $code == 'ZA' ) $country = 'South Africa';
    if( $code == 'GS' ) $country = 'South Georgia and the South Sandwich Islands';
    if( $code == 'ES' ) $country = 'Spain';
    if( $code == 'LK' ) $country = 'Sri Lanka';
    if( $code == 'SD' ) $country = 'Sudan';
    if( $code == 'SR' ) $country = 'Suriname';
    if( $code == 'SJ' ) $country = 'Svalbard & Jan Mayen Islands';
    if( $code == 'SZ' ) $country = 'Swaziland';
    if( $code == 'SE' ) $country = 'Sweden';
    if( $code == 'CH' ) $country = 'Switzerland, Swiss Confederation';
    if( $code == 'SY' ) $country = 'Syrian Arab Republic';
    if( $code == 'TW' ) $country = 'Taiwan';
    if( $code == 'TJ' ) $country = 'Tajikistan';
    if( $code == 'TZ' ) $country = 'Tanzania';
    if( $code == 'TH' ) $country = 'Thailand';
    if( $code == 'TL' ) $country = 'Timor-Leste';
    if( $code == 'TG' ) $country = 'Togo';
    if( $code == 'TK' ) $country = 'Tokelau';
    if( $code == 'TO' ) $country = 'Tonga';
    if( $code == 'TT' ) $country = 'Trinidad and Tobago';
    if( $code == 'TN' ) $country = 'Tunisia';
    if( $code == 'TR' ) $country = 'Turkey';
    if( $code == 'TM' ) $country = 'Turkmenistan';
    if( $code == 'TC' ) $country = 'Turks and Caicos Islands';
    if( $code == 'TV' ) $country = 'Tuvalu';
    if( $code == 'UG' ) $country = 'Uganda';
    if( $code == 'UA' ) $country = 'Ukraine';
    if( $code == 'AE' ) $country = 'United Arab Emirates';
    if( $code == 'GB' ) $country = 'United Kingdom';
    if( $code == 'US' ) $country = 'United States of America';
    if( $code == 'UM' ) $country = 'United States Minor Outlying Islands';
    if( $code == 'VI' ) $country = 'United States Virgin Islands';
    if( $code == 'UY' ) $country = 'Uruguay, Eastern Republic of';
    if( $code == 'UZ' ) $country = 'Uzbekistan';
    if( $code == 'VU' ) $country = 'Vanuatu';
    if( $code == 'VE' ) $country = 'Venezuela';
    if( $code == 'VN' ) $country = 'Vietnam';
    if( $code == 'WF' ) $country = 'Wallis and Futuna';
    if( $code == 'EH' ) $country = 'Western Sahara';
    if( $code == 'YE' ) $country = 'Yemen';
    if( $code == 'ZM' ) $country = 'Zambia';
    if( $code == 'ZW' ) $country = 'Zimbabwe';
    if( $country == '') $country = $code;
    return $country;
}
function check_global_exist(){
  $table = $_POST['table'];
  $field = $_POST['field'];
  $where = $_POST['where'];
  $data = $this->global_model->check_global_exist_model($table,  $field, $where);
  $count = $data[0]->count;
  echo json_encode(array('count' => $count));
}


function type_audit_array() {
  $table = $_POST['table'];
  $field = $_POST['field'];
  $action = $_POST['action'];
  $id = "";
  if($action=="update") {
    $action ='Update';
    $id = $_POST['id'];
    $data = array(
      'scope_name' => ucwords(strip_tags($_POST['type'])),
      'update_date' => date('Y-m-d H:i:s')
    );
  }else{
    $action ='Save';
    $data = array(
      'scope_name' => ucwords(strip_tags($_POST['type'])),
      'create_date' => date('Y-m-d H:i:s'),
      'update_date' => date('Y-m-d H:i:s')
    );
  }
  $data = json_decode(json_encode($data), FALSE);
  $this->checkAction($action, $data, $table, $field, $id);
}
function disposition_array() {
  $table = $_POST['table'];
  $field = $_POST['field'];
  $action = $_POST['action'];
  $id = "";
  if($action=="update") {
    $action ='Update';
    $id = $_POST['id'];
    $data = array(
      'disposition_name' => ucwords($_POST['disposition']),
      'update_date' => date('Y-m-d H:i:s')
    );
  }else{
    $action = 'Save';
    $data = array(
      'disposition_name' => ucwords($_POST['disposition']),
      'create_date' => date('Y-m-d H:i:s'),
      'update_date' => date('Y-m-d H:i:s')
    );
  }
  $data = json_decode(json_encode($data), FALSE);
 $is_exist = $this->global_model->is_checking($table, 'disposition_name', ucwords($_POST['disposition']));
 if($is_exist==0){
  $this->checkAction($action, $data, $table, $field, $id);
  echo "new";
 }else{
  echo "duplicate";
 }
  // $this->checkAction($action, $data, $table, $field, $id);

}
function distribution_array() {
  $table = $_POST['table'];
  $field = $_POST['field'];
  $action = $_POST['action'];
  $id = "";
  if($action=="update") {
    $action ='Update';
    $id = $_POST['id'];
    $data = array(
      'distribution_name' => ucwords(strip_tags($_POST['distribution'])),
      'update_date' => date('Y-m-d H:i:s')
    );
  }else{
    $action ='Save';
    $data = array(
      'distribution_name' => ucwords(strip_tags($_POST['distribution'])),
      'create_date' => date('Y-m-d H:i:s'),
      'update_date' => date('Y-m-d H:i:s')
    );
  }
  $data = json_decode(json_encode($data), FALSE);
  $this->checkAction($action, $data, $table, $field, $id);
}

function get_by_id(){
  $table = $_POST['table'];
  $field = $_POST['field'];
  $where = $_POST['id'];
  $data = $this->global_model->get_where($table, $field, $where);
  echo json_encode($data);
}
public function getlist_manage_template(){
  $table = $_POST['table'];
  $table1 = $_POST['table1'];
  $txt_search = $_POST['txt_search'];
  $limit = $_POST['limit'];
  $offset = $_POST['offset'];
  $offset = ($_POST['offset']-1)* $limit;
  $sort = $_POST['order_by'];
  $data = $this->global_model->getlist_manage_template($table, $table1, $limit,$offset,$sort,$txt_search);
  echo json_encode($data);
}
function getlist_manage_template_count(){
  $table = $_POST['table'];
  $table1 = $_POST['table1'];
  $txt_search = $_POST['txt_search'];
  $per_page = $_POST['limit'];
  $limit = '9999999';
  $offset = '0';
   $sort = $_POST['order_by'];
  $data = count($this->global_model->getlist_manage_template($table, $table1, $limit,$offset,$sort,$txt_search));
  $page_count = ceil($data/$per_page);
  echo $page_count;
}
public function save_audit_trail($action){
    $data['user'] = $this->session->userdata('userid');
    $data['page'] = $this->agent->referrer();
    $data['type'] = $this->session->userdata('type');
    $data['role'] = $this->session->userdata('sess_role');
    $data['email'] = $this->session->userdata('sess_email');
    $data['action'] = ucwords($action);
    $data['date'] = date('Y-m-d H:i:s');
    $table = 'tbl_audit_trail';
    $this->Audit_trail_model->save_data($table,$data);
}

    public function insert_audit_trail()
    {
        $data['user'] = $this->session->userdata('userid');
        $data['page'] = $_POST['uri'];
        $data['type'] = $this->session->userdata('type');
        $data['role'] = $this->session->userdata('sess_role');
        $data['email'] = $this->session->userdata('sess_email');
        $data['action'] = strip_tags($_POST['action']);
        $data['date'] = date('Y-m-d H:i:s'); 
        $table = 'tbl_audit_trail';
        $this->Audit_trail_model->save_data($table,$data);



        //update config json
        $ch9 = curl_init();
        curl_setopt($ch9, CURLOPT_URL, base_url('api/generate_config_json'));
        curl_setopt($ch9, CURLOPT_POST, 1);
        curl_setopt($ch9, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch9);
        curl_close ($ch9);

    }

  public function get_no_answer(){
    $reportid = $_POST['report_id'];
    $elementid = $_POST['element_id'];
    echo json_encode($this->global_model->get_no_answer($reportid,$elementid));
  }
  function get_list_data(){
    $table = $_POST['table'];
    echo json_encode($this->global_model->get_list_data($table));
  }
  function get_question_classification(){
    $id = $_POST['id'];
    echo json_encode($this->global_model->get_question_classification($id));
  }
  function remove_question_classification(){
    $cat_id = $_POST['cat_id'];
    $cat_class = $_POST['cat_class'];
    echo json_encode($this->global_model->remove_question_classification($cat_id, $cat_class));
  }
  function trash_data(){
    $query = $_POST['query'];
    $table = $_POST['table'];
    echo json_encode($this->global_model->trash_data($query, $table));
  }
  function check_email_table(){
    $type = $_POST['type'];
    $email = $_POST['email'];
    $table = $_POST['table'];
    if($type == '1'){
        //check if same email
          $query = $_POST['query'];
          return $this->global_model->check_email_table_same($email, $table, $query);
    } else {
        //check if email exist
        return $this->global_model->check_email_table_exist($email, $table);
    }
  }
  /////////////////////////////////////////////////////////////////
  public function getlist_global_dm(){
    $table = $_POST['table'];
    $data = $this->global_model->getlist_global_model_dm($table);
    echo json_encode($data);
  }
  public function getlist_global(){
    $table = $_POST['table'];
    $limit = $_POST['limit'];
    $offset = $_POST['offset'];
    $query = $_POST['query'];
    $order_by = $_POST['order_by'];
    $status = $_POST['status'];
    $data = $this->global_model->getlist_global_model($table,$limit,$offset,$query,$order_by,$status);
    echo json_encode($data);
  }
  public function getlist_pagination() {
    $table = $_POST['table'];
    $query = $_POST['query'];
    $status = $_POST['status'];
    $data = count($this->global_model->getlist_pagination($table,$query,$status));
    $page_count = $data;
    echo $page_count;
  }
  public function count_active_record(){
    $table = $_POST['table'];
    $max = $_POST['max'];
    $count = $this->global_model->count_active_record($table);
    if($count >= $max){
      echo false;
    } else {
      echo true;
    }
  }
  public function no_of_active_record(){
    $table = $_POST['table'];
    $count = $this->global_model->count_active_record($table);
    echo $count;
  }
  public function add_to_admin()
  {
    $email = $_POST['email'];
    $name = $_POST['name'];
    $auditor_id = $_POST['auditor_id'];
    $data['email'] = $email;
    $data['role'] = 0;
    $data['status'] = 1;
    $data['name'] = $name;
    $data['reg_id'] = $auditor_id;
    $data['type'] = "Auditor";
    $this->global_model->save_data("tbl_cms_login", $data);
  }
  public function get_active_list()
  {
      $table = $_POST['table'];
      echo json_encode($this->global_model->get_list_data_active($table));
  }
  public function check_if_exist()
  {
    $table = $_POST['table'];
    $query = $_POST['query'];
    $count = $this->global_model->check_if_exist($table, $query);
    if($count > 0){
      echo 'true';
    } else {
      echo 'false';
    }
  }

  public function getlist_in_use()
  {
     $query = $_POST['query'];
     echo $this->global_model->getlist_in_use($query);
  }

  public function check_email_approval(){ 
      $where = $_POST['user'];
      $table = $_POST['table'];
      $data = $this->global_model->check_exist_approve($table, $where);
      if(count($data) > 0){
        $role = $data[0]->administrator;
        $status = $data[0]->status;

        if($table == "tbl_approver_info"){
          $reg_id = $data[0]->approver_id;
          $audit_trail_type = "Approver";
        } else {
          $reg_id = $data[0]->reviewer_id;
          $audit_trail_type = "Reviewer";
        } 

        $newdata = array(
              'sess_email_approval'  => $data[0]->email,
              'sess_role_approval'  => 0,
              'type_approval' => "Auditor",
              'userid_approval' => $reg_id,
              'logged_in_approval' => TRUE,
              'name_approval' => $data[0]->fname . " " .  $data[0]->lname    
        );
        $this->session->set_userdata($newdata);

        //ADD AUDIT TRAIL
        // foreach ($this->global_model->get_report_details($report_id) as $value) {
        //     $report_no = $value->Report_No;
        // }

        $audit_trail_data['user'] = $reg_id;
        $audit_trail_data['page'] = "/";
        $audit_trail_data['type'] = $audit_trail_type;
        $audit_trail_data['role'] = 0;
        $audit_trail_data['email'] = $data[0]->email;
        $audit_trail_data['action'] = "Login";
        $audit_trail_data['date'] = date('Y-m-d H:i:s'); 
        $this->Audit_trail_model->save_data('tbl_audit_trail',$audit_trail_data);

        echo json_encode(array('status'=>$status, 'role' => $role, 'userid' => $reg_id,'name' => $data[0]->fname." ".$data[0]->lname ));
      } else {
        echo json_encode(array('status'=>0));
      }
      
  }

  public function getlist_global_archive(){
    $limit = $_POST['limit'];
    $offset = $_POST['offset'];
    $query = $_POST['query'];
    $data = $this->global_model->report_listing_model_archive($limit,$offset,$query);
    echo json_encode($data);
  }

  public function getlist_global_group(){
    $limit = $_POST['limit'];
    $offset = $_POST['offset'];
    $query = $_POST['query'];
    $data = $this->global_model->report_listing_model($limit,$offset,$query);
    echo json_encode($data);
  }



  public function display_email()
  {
    $logg_id = $_POST['userid'];
    $reportid = $_POST['report_id'];
    $data = $this->global_model->display_email($reportid, $logg_id);
    if($data > 0){
      echo true;
    } else { 
      echo false;
    }
  }


  public function get_navigation()
  {
    $role = $this->session->userdata('sess_role');
    $main_menu = $this->global_model->get_list_data_query("menu_item", "role <= " . $role);
    foreach ($main_menu as $key => $value) {

        $sub_menu = $this->global_model->get_sub_menu($value->id);
        $menu[] = array(
          "id"=>$value->id,
          "name"=>$value->menu,
          "icon"=>base_url() . $value->icon,
          "url"=> $value->url,
          "sub_menu"=>$sub_menu,
        );
    }

    echo json_encode($menu,true);
  }

  public function getlist_dm()
  {
    $table = $_POST['table'];
    $limit = $_POST['limit'];
    $offset = $_POST['offset'];
    $query = $_POST['query'];
    $data = $this->global_model->getlist_dm($table,$limit,$offset,$query);
    echo json_encode($data);
  }

    public function update_user_acting()
    {
        if($_POST['status'] == -2){
            $id = $_POST['id'];
            $data = array(
                "update_date"   => date("Y-m-d H:i:s"),
                $_POST['type']  => 0
            );
            $this->global_model->update_data("auditor_id", $id, "tbl_auditor_info", $data);


            if($_POST['type'] = "approver"){
                $this->global_model->delete_data($id, "tbl_approver_info", "acting_id"); 
            }

            if($_POST['type'] = "reviewer"){
                $this->global_model->delete_data($id, "tbl_reviewer_info", "acting_id"); 
            }
               
        }
    }


}