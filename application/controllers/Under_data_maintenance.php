<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Taipei');
class Under_data_maintenance extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('global_model');
		$this->load->model('Data_maintenance_model');
		$this->load->helper('url');
		
	}
	// public function index()
	// {	
	// 	if($this->session->userdata('sess_email')=='' ) { 
	// 		$this->load->view('login');
	// 	}else{
	// 		$data['content'] = 'auditor/list2';
	// 		$this->load->view('layout/layout',$data);
	// 	}
		

		public function get_list_data_maintenance(){

		// header('Content-type: Application/JSON');

		$limit = $_POST['limit'];

		$offset = $_POST['offset'];

		$query = $_POST['query'];

		$table = $_POST['table'];
		$order_by = $_POST['order_by'];
		echo json_encode($this->Data_maintenance_model->get_list_data_maintenance($query,$limit,$offset,$table,$order_by), JSON_PRETTY_PRINT);

	}
		public function get_list_data_maintenance_product(){

		// header('Content-type: Application/JSON');

		$limit = $_POST['limit'];

		$offset = $_POST['offset'];

		$query = $_POST['query'];

		// $table = $_POST['table'];
		// $order_by = $_POST['order_by'];
		echo json_encode($this->Data_maintenance_model->get_list_data_maintenance_product($query,$limit,$offset), JSON_PRETTY_PRINT);

	}
	// 	public function get_list_data_maintenance_standard(){

	// 	// header('Content-type: Application/JSON');

	// 	$limit = $_POST['limit'];

	// 	$offset = $_POST['offset'];

	// 	$query = $_POST['query'];

	// 	// $table = $_POST['table'];
	// 	// $order_by = $_POST['order_by'];
	// 	echo json_encode($this->Data_maintenance_model->get_list_data_maintenance_standard($query,$limit,$offset), JSON_PRETTY_PRINT);

	// }

	//
		public function get_pagination_auditor(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Data_maintenance_model->get_pagination_auditor($query);

	}
		public function get_pagination_reviewer(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Data_maintenance_model->get_pagination_reviewer($query);

	}
		public function get_pagination_approver(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Data_maintenance_model->get_pagination_approver($query);

	}
		public function get_pagination_site(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Data_maintenance_model->get_pagination_site($query);

	}
		public function get_pagination_product_materials(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Data_maintenance_model->get_pagination_product_materials($query);

	}
		public function get_pagination_audit_scope(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Data_maintenance_model->get_pagination_audit_scope($query);

	}
		public function get_pagination_category(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Data_maintenance_model->get_pagination_category($query);

	}
		public function get_pagination_product_type(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Data_maintenance_model->get_pagination_product_type($query);

	}
		public function get_pagination_standard(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Data_maintenance_model->get_pagination_standard($query);

	}
		public function get_pagination_distribution(){

		header('Content-type: Application/JSON');

		$query = $_POST['query'];

		echo $this->Data_maintenance_model->get_pagination_distribution($query);

	}
	// start of delete data maintenance
	public function data_maintenance_delete(){
		$id = $_POST['id'];
		$status = $_POST['status'];
		$table = $_POST['table'];
		$order_by = $_POST['order_by'];
		$this->Data_maintenance_model->data_maintenance_delete($id, $status, $table, $order_by);

	}
	// public function data_maintenance_delete_reviewer(){
	// 	$id = $_POST['id'];
	// 	$status = $_POST['status'];
	// 	$table = $_POST['table'];
	// 	$this->Data_maintenance_model->data_maintenance_delete_reviewer($id, $status, $table);

	// }
	// public function data_maintenance_delete_approver(){
	// 	$id = $_POST['id'];
	// 	$status = $_POST['status'];
	// 	$table = $_POST['table'];
	// 	$this->Data_maintenance_model->data_maintenance_delete_approver($id, $status, $table);

	// }
	// public function data_maintenance_delete_site(){
	// 	$id = $_POST['id'];
	// 	$status = $_POST['status'];
	// 	$table = $_POST['table'];
	// 	$this->Data_maintenance_model->data_maintenance_delete_site($id, $status, $table);

	// }
	// public function data_maintenance_delete_product(){
	// 	$id = $_POST['id'];
	// 	$status = $_POST['status'];
	// 	$table = $_POST['table'];
	// 	$this->Data_maintenance_model->data_maintenance_delete_product($id, $status, $table);

	// }
	// public function data_maintenance_delete_typeofaudit(){
	// 	$id = $_POST['id'];
	// 	$status = $_POST['status'];
	// 	$table = $_POST['table'];
	// 	$order_by = $_POST['order_by'];
	// 	$this->Data_maintenance_model->data_maintenance_delete_typeofaudit($id, $status, $table, $order_by);

	// }

}
