<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class C_archive extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('session');
		$this->load->model('q_archive');
		$this->load->model('q_auditreport');
		$this->load->helper('url');
	}

	public function get(){
		header('Content-type: Application/JSON');
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$query = $_POST['query'];

		$report = $this->q_archive->get_list($query,$limit,$offset);
		$result_array = array();
		foreach ($report as $key => $value) {
			$report_id = $value->Report_ID;
			$report_audit_date = $this->q_auditreport->get_report_audit_date($report_id);
			$result_array[] = array(
				 	"Report_ID" => $value->Report_ID,
			        "Report_No"=> $value->Report_No,
			        "Template_ID"=> $value->Template_ID,
			        "Audited_Site"=>$value->Audited_Site,
			        "Classification_ID"=> $value->Classification_ID,
			        "Auditor_Name"=> $value->Auditor_Name,
			        "Auditor_ID"=> $value->Auditor_ID,
			        "Report_Status"=> $value->Report_Status,
			        "TemplateID"=> $value->TemplateID,
			        "Template_Created"=> $value->Template_Created,
			        "Template_Updated"=> $value->Template_Updated,
			        "Template_Version"=> $value->Template_Version,
			        "Standard_Name"=> $value->Standard_Name,
			        "Classification_Code"=> $value->Classification_Code,
			        "Classification_Name"=> $value->Classification_Name,
			        "name"=> $value->name,
			        "address1"=> $value->address1,
			        "address2"=> $value->city_name,
			        "address3"=> $value->province_name,
			        "country"=> $value->country,
			        "zip_code"=> $value->zip_code,
			        "status"=> $value->status,
			        "type"=> $value->type,
			        "background"=> $value->background,
			        "create_date"=> $value->create_date,
			        "update_date"=> $value->update_date,
			        "Country_Code"=> $value->country_code,
			        "Country_Name"=> $value->country_name,
			        "Province_Name"=> $value->province_name,
			        "city_name"=> $value->city_name,
			        "audited_areas"=> $value->audited_areas,
			        "areas_to_consider"=> $value->areas_to_consider,
			        "Auditor_Company"=> $value->Auditor_Company,
			        "other_issues_audit"=> $value->other_issues_audit,
			        "report_submission"=>$value->report_submission,
                    "report_issuance"=>$value->report_issuance,
                    "version"=>$value->version,
			        "audit_date_formatted"=>$value->audit_date_formatted,
			        "Audit_Dates"=>$report_audit_date
				);
		}

		echo json_encode($result_array, JSON_PRETTY_PRINT);
	}

	public function get_pagination(){
		header('Content-type: Application/JSON');
		$query = $_POST['query'];
		echo $this->q_archive->get_pagination();
	}

}