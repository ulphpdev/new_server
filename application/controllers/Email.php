<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Email extends CI_Controller {
    public function __construct()
    {
       parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('email');
        $this->load->model('Audit_trail_model');
        $this->load->model('global_model');
        $this->load->model('q_auditreport');
        $this->load->model('Audit_report_model');

        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
    }
    
    function check2()
    {
        $w = stream_get_wrappers();
        echo 'openssl: ', extension_loaded ('openssl') ? 'yes':'no', "\n";
        echo "<br>";
        echo 'http wrapper: ', in_array('http', $w) ? 'yes':'no', "\n";
        echo "<br>";
        echo 'https wrapper: ', in_array('https', $w) ? 'yes':'no', "\n";
        echo "<br>";
        echo 'wrappers: <pre>', var_dump($w) . '</pre>';
    }
    function check_url()
    {
        $pdf = 'http://sams.webqa.unilab.com.ph/generate/preview_template?template_id=264';
        $data = file_get_contents($pdf);
        if ($data !== FALSE) {
           echo $data;
        } else {
            $error = error_get_last();
            echo $error['message'];
        }
        
    }

    function send_template(){

        $to = $_POST['to'];
        $subject = $_POST['subject'];
        $message1 = $_POST['message'];
        $template_id = $_POST['template_id'];
        $email = explode(",",$to);  
        $arr_count = count($email);
        $ctr = 1;

        // $pdf = base_url().'generate/preview_template?template_id='.$template_id;
        // $pdf_attach = './email_attach/Template-' . $template_id . '.pdf';
        // file_put_contents($pdf_attach, file_get_contents($pdf));
        // $this->email->attach($pdf_attach);
        
        foreach ($email as $key => $value) {
            $this->email->from($this->session->userdata('sess_email'),$this->session->userdata('name'));
            $this->email->to($value);
            $this->email->subject($subject);
            $message = '<html><head><title></title></head><body style="font-family:Arial">';
            $message .= '<p>' . $message1 . '</p>';
            $message .= '<b>Attachment: </b>';
            $message .= '   <a style="text-decoration:none;" href="'.base_url("download").'?id='.$template_id.'&action=preview_template">';
            $message .= '      <p style="font-size: 12px; margin-top: 3px;">Template '.$template_id.'</p>';
            $message .= '   </a>';
            $message .= '</body></html>';
            
            $this->email->message($message);

            if($this->email->send()){
                echo $this->send_alert($ctr,$arr_count);
            }else{
                echo 'error';
            }
            $ctr++;
        }

        
    }

    function sendmail_other_approved() { 

        ini_set("allow_url_fopen", 1);
        $report_id = $_POST['report_id'];
        $to = $_POST['sender_other'];
        $subject = $_POST['subject_other'];
        $message1 = $_POST['message_other'];
        $email = explode(",",$to);  
        $arr_count = count($email);
        $ctr = 1;

        $files_to_send = explode(",",$_POST['files']);  

        foreach ($email as $key => $value) {
            $this->email->from($this->session->userdata('sess_email'),$this->session->userdata('name'));
            $this->email->to($value);
            $this->email->subject($subject);

            $message = '<html><head><title></title></head><body style="font-family:Arial">';
            $message .= '<p>' . $message1 . '</p>';
            $message .= '<b>Attachments: </b>';
            $message .= '<table style="width: 100%;">';
            $message .= '<tr>';
            foreach ($files_to_send as $v_files) {
                $message .= '<td style="width: 150px;">';
                $message .= '   <a style="text-decoration:none;" href="'.base_url("generate").'/'.$v_files.'?report_id='.$report_id.'&action=D">';
                $message .= '       <div style="background-color: #e7e7e5; margin: 5px; border: 1px solid #636059; width: 90%; height: 20px;box-shadow: 2px 3px #888888; text-align: center; padding: 5px;">';
                $message .= '           <p style="font-size: 12px; margin-top: 3px;">'.$v_files.'</p>';
                $message .= '       </div>';
                $message .= '   </a>';
                $message .= '</td>';
            }   

            $message .= '</tr>';
            $message .= '</table>';
            $message .= '</body></html>';


            // $message = '';
            // $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
            // $message .= "Hi,";
            // $message .= "<br>";
            // $message .= $message1;
            // $message .= '<br>';
            // $message .= $attach;
            // $message .= '<br>';
            // $message .= '<br>';
            // $message .= 'Thank you.';
            // $message .= "</body></html>";
            
            $this->email->message($message);

            if($this->email->send()){
                echo $this->send_alert($ctr,$arr_count);
            }else{
                echo 'error';
            }
            $ctr++;
        }

    }

    public function send_alert($ctr,$arr_count){
        if($arr_count == $ctr){
            echo 'success';
        }
    }


    public function email_to_coauditor()
    {
        $reportid = $_POST['report_id'];

        foreach($this->global_model->get_emails("email_lead_auditor",$reportid) as $value){
            $lead_auditor_email = $value->email;
            $lead_auditor_name = $value->fname . " " . $value->lname;
        }

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        
        $emails_coaudit = "";
        $coauditor_names = "Sent to : ";
        foreach($this->global_model->get_emails("email_coauditors",$reportid) as $value){

            $coauditor_names .= $value->fname . " " . $value->lname . " (" .$value->email. "), ";

            $this->email->from($this->session->userdata('sess_email'),$this->session->userdata('name'));
            $this->email->to($value->email);

            $subject = "Co-Auditor's Review - " . $company . " (" . $report_no . ")";
            $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
            $message .= "Dear ".$value->fname. ",";
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Please log in to DART to view the audit of ".$company.".";
            $message .= "<br>";
            $message .= "This is for your review and comments.";  
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Thank you.";
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Regards,<br>";
            $message .= $this->session->userdata('name');
            $message .= "</body></html>";

            $this->email->subject($subject);
            $this->email->message($message);

            $this->email->send();
        }

        //ADD REMARKS
        $this->Audit_report_model->add_report_submission_status_auditor($reportid,substr(trim($coauditor_names),0,-1),"Submitted to Co-Auditor(s)","Auditor");



        if($this->global_model->report_submission_check($reportid) > 0){
            //update
            $data = array("coauditor"=>date('Y-m-d'));
            $this->global_model->report_submission_update($reportid,$data);
        } else {
            //insert
            $data = array("report_id"=>$reportid,"coauditor"=>date('Y-m-d'));
            $this->global_model->report_submission_insert($data);
        }


        //check if status is greater than 7
        if($this->global_model->get_report_status($reportid) <= 7){
            $this->global_model->change_report_status($reportid,1);
        }

        //UPDATE REPORT LIST JSON
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, base_url("api/generate_audit_report_json") . "/" . $reportid);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        //generate_audit_report_json
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, base_url("api/audit_report_list"));
        curl_setopt($ch1, CURLOPT_POST, 1);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch1);
        curl_close ($ch1);

    }

    public function email_to_depthead()
    {
        $reportid = $_POST['report_id'];

        foreach($this->global_model->get_emails("email_lead_auditor",$reportid) as $value){
            $lead_auditor_email = $value->email;
            $lead_auditor_name = $value->fname . " " . $value->lname;
        }

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        $reviewer = $this->global_model->get_emails("email_reviewer",$reportid);

        //echo $value->email;
        $this->email->from($this->session->userdata('sess_email'),$this->session->userdata('name'));
        $this->email->to($reviewer[0]->email);

        $subject = "Department Head's Review - " . $company . " (" . $report_no . ")";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Dear ".$reviewer[0]->fname. ",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Please refer to the link below with regards to the audit of ".$company.".";
        $message .= "<br>";
        $message .= "This is for your review and approval.";
        $message .= "<br>";
        $message .= "<br>";
        $message .= '<a href="'.base_url("approval").'?report_id='.$reportid.'&action=department_head">Link to Audit Report Summary</a>';
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Thank you.";
        $message .= "</body></html>";

        $this->email->subject($subject);
        $this->email->message($message);
 
        $this->email->send();

        if($this->global_model->report_submission_check($reportid) > 0){
            //update
            $data = array("reviewer"=>date('Y-m-d H:i:s'));
            $this->global_model->report_submission_update($reportid,$data);
        } else {
            //insert
            $data = array("report_id"=>$reportid,"reviewer"=>date('Y-m-d H:i:s'));
            $this->global_model->report_submission_insert($data);
        }

        if($this->global_model->get_report_status($reportid) <= 7){
            $this->global_model->change_report_status($reportid,3);
        }

        ///SUBMISSION DATE
        $data = array("report_submission"=>date("Y-m-d  H:i:s"));
        $this->q_auditreport->update_date_submission($data,$reportid);

        //UPDATE REPORT LIST JSON
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, base_url("api/generate_audit_report_json") . "/" . $reportid);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        //generate_audit_report_json
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, base_url("api/audit_report_list"));
        curl_setopt($ch1, CURLOPT_POST, 1);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch1);
        curl_close ($ch1);

        //ADD REMARKS
        $this->Audit_report_model->add_report_submission_status_auditor($reportid,"","Submitted to Department Head","Auditor");

    }

    public function email_to_divhead()
    {
        $reportid = $_POST['report_id'];

        $lead_auditor = $this->global_model->get_emails("email_lead_auditor",$reportid);
        $reviewer = $this->global_model->get_emails("email_reviewer",$reportid);
        $approver = $this->global_model->get_emails("email_approver",$reportid);

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        $this->email->from($this->session->userdata('sess_email'),$this->session->userdata('name'));
        $this->email->to($approver[0]->email);

        $subject = "Division Head's Approval - " . $company . " (" . $report_no . ")";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Dear " . $approver[0]->fname . ",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Please refer to the link below with regards to the audit of ".$company.".";
        $message .= "<br>";
        $message .= "This is for your review and final approval.";
        $message .= "<br>";
        $message .= "<br>";
        $message .= '<a href="'.base_url("approval").'?report_id='.$reportid.'&action=division_head">Link to Audit Report Summary</a>';
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Thank you.";
        $message .= "</body></html>";

        $this->email->subject($subject);
        $this->email->message($message);

        echo $this->email->send();

        if($this->global_model->report_submission_check($reportid) > 0){
            //update
            $data = array("approver"=>date('Y-m-d'));
            $this->global_model->report_submission_update($reportid,$data);
        } else {
            //insert
            $data = array("approver"=>$reportid,"coauditor"=>date('Y-m-d'));
            $this->global_model->report_submission_insert($data);
        }

        if($this->global_model->get_report_status($reportid) <= 7){
            $this->global_model->change_report_status($reportid,4);
        }

        ///SUBMISSION DATE
        // $data = array("report_submission"=>date("Y-m-d  H:i:s"));
        // $this->q_auditreport->update_date_submission($data,$reportid);

        //UPDATE REPORT LIST JSON
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, base_url("api/generate_audit_report_json") . "/" . $reportid);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        //generate_audit_report_json
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, base_url("api/audit_report_list"));
        curl_setopt($ch1, CURLOPT_POST, 1);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch1);
        curl_close ($ch1);

        //REMARKS
        $this->Audit_report_model->add_report_submission_status_auditor($reportid,"","Submitted to Division Head","Auditor");

    }

}