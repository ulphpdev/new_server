<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Taipei');
class Audit_report_link extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->library('session');
    //   $this->load->library('Pdf');
    $this->load->model('Audit_report_model');
    $this->load->model('Template_model');
    $this->load->model('global_model');
    $this->load->helper('url');
    if($this->session->userdata('sess_email')=='' ) { 
      $data_sesssion = array(
          "session_link"=> "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
        );
      $this->session->set_userdata($data_sesssion);
      redirect(base_url("login"));
    }
  }
  public function index()
  {
    echo $this->session->userdata("session_link");
  }
  function report_link() {
    $this->load->view('layout/header');
    $report_id = $_GET['report_id'];
    $this->load->view('audit_report_link');
  }
  function report_link_remarks_divisionhead() {
    $this->load->view('layout/header');
    $report_id = $_GET['report_id'];
    $this->load->view('report_link_remarks_divisionhead');
  }
  function report_link_remarks_departmenthead() {
    $this->load->view('layout/header');
    $report_id = $_GET['report_id'];
    $this->load->view('report_link_remarks_departmenthead');
  }
  function add_status()
    {
             $report_id = $_POST['id'];
         $status = $_POST['status'];
         $report_submission = $_POST['report_submission'];
        $this->Audit_report_model->add_status($report_id, $status, $report_submission);
        // echo 'success';
    }
    function add_status1()
    {
             $report_id = $_POST['id'];
         $status = $_POST['status'];
        $this->Audit_report_model->add_status1($report_id, $status);
        // echo 'success';
    }
    function add_status2()
    {
             $report_id = $_POST['id'];
         $status = $_POST['status'];
        $this->Audit_report_model->add_status2($report_id, $status);
        // echo 'success';
    }
    function add_remarks_departmenthead()
    {
          $report_id = $_POST['id'];
         $status = $_POST['status'];
        $this->Audit_report_model->add_remarks_departmenthead($report_id, $status);
        // echo 'success';
    }
  function add_remarks()
    {
             $report_id = $_POST['id'];
         $status = $_POST['status'];
         $report_issuance = $_POST['report_issuance'];
        $this->Audit_report_model->add_remarks($report_id, $status, $report_issuance);
        // echo 'success';
    }
    function add_new_remarks()
    {
        $report_id = $_POST['id'];
        $remarks = $_POST['remarks'];
        $create_date = $_POST['create_date'];
        $approver_id = $this->session->userdata('userid_approval');
        $type = $_POST['type'];
        $this->Audit_report_model->add_new_remarks($report_id, $remarks, $create_date, $approver_id, $type);
        // echo 'success';
    }
    function add_new_remarks_departmenthead()
    {
             $report_id = $_POST['id'];
         $remarks = $_POST['remarks'];
         $create_date = $_POST['create_date'];
         $reviewer_id = $_POST['reviewer_id'];
         $type = $_POST['type'];
        $this->Audit_report_model->add_new_remarks_departmenthead($report_id, $remarks, $create_date, $reviewer_id, $type);
        // echo 'success';
    }
    function reject_remarks()
    {
             $report_id = $_POST['id'];
         $remarks = $_POST['remarks'];
         $create_date = $_POST['create_date'];
         $approver_id = $this->session->userdata('userid_approval');
         $type = $_POST['type'];
        $this->Audit_report_model->reject_remarks($report_id, $remarks, $create_date, $approver_id, $type);
        // echo 'success';
    }
    function reject_remarks_departmenthead()
    {
      $report_id = $_POST['id'];
      $remarks = $_POST['remarks'];
      $create_date = $_POST['create_date'];
      $reviewer_id = $_POST['reviewer_id'];
      $type = $_POST['type'];
      $this->Audit_report_model->reject_remarks_departmenthead($report_id, $remarks, $create_date, $reviewer_id, $type);
    }
    function add_report_submission_status(){
      $report_id = $_POST['report_id'];
      $remarks = $_POST['remarks'];
      $status = $_POST['status'];
      $position = $_POST['position'];
      // if($status != "Approved") {
      //     $version = $this->Audit_report_model->get_version($report_id);
      //     $new_version = (int)$version + 1;
      //     $this->Audit_report_model->update_version($report_id,$new_version);
      // }
      $this->Audit_report_model->add_report_submission_status($report_id,$remarks,$status,$position);
    }
    function signature_stamp()
    {
      $report_id = $_POST['report_id'];
      $field = $_POST['field'];
      $data = array(
          "report_id"=>$report_id,
          $field => date("Y-m-d H:i:s")
        );
      $this->Audit_report_model->signature_stamp($data, "save");
      
    }

    function signature_stamp_approved()
    {
      $report_id = $_POST['report_id'];
      $field = $_POST['field'];
      $data = array(
          "report_id"=>$report_id,
          $field => date("Y-m-d H:i:s")
        );

      $exist = $this->global_model->check_global_exist_model('tbl_report_signature_stamp',  'report_id', 'report_id = ' . $report_id);
      $count = $exist[0]->count;
      echo $count;
      if($count > 0){
        $this->Audit_report_model->update_signature_stamp($report_id,$data);
      } else {
        $this->Audit_report_model->signature_stamp($data, "save");
      }
     
    }

}
