<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Taipei');

class Approval extends CI_Controller {

  public function __construct() {
    parent::__construct();

    $this->load->library('session');
    $this->load->model('Preview_report_model');
    $this->load->model('Audit_report_model');
    $this->load->model('Audit_trail_model');
    $this->load->model('Template_model');
    $this->load->model('Global_model');
    $this->load->helper('url');
    $this->load->library('email');
    $this->email->set_newline("\r\n");
  }

  public function index()
  {
    //Login
    $data['report_id'] = $_GET['report_id'];
    $data['action'] = $_GET['action'];
    $this->load->view('approval/login', $data);

  }

  function report_link() {
    $this->load->view('layout/header');
    $report_id = $_GET['report_id'];
    $this->load->view('audit_report_link');
  }

  function division_head() {

    if($this->session->userdata('sess_email_approval')=='' ) {
      redirect(base_url("approval") . "?report_id=" . $_GET['report_id'] . "&action=division_head");
    }

    $this->load->view('layout/header');

     //check if the logged user is the reviewer of the report
    $logged_id = $this->session->userdata('userid_approval');
    $report_id = $_GET['report_id'];
    $count = $this->Global_model->check_if_approval("tbl_approver", "approver_id = " . $logged_id , $report_id);
    if( $count > 0){

        //check if report status
        $string = file_get_contents("./listing/".$report_id.".json");
        $details = json_decode($string, false);
        $report = $details->Report_Summary;
        if($report[0]->status >= 0){

            if($report[0]->status > 3){

                //reviewer of the report
                $this->load->view('approval/report_link_remarks_divisionhead');

            } else {

                //not available for viewing
                $data['error_message'] = "You are not allowed to review this report yet.";
                $this->load->view('approval/error_page', $data);

            }

        } else {

            //deleted OR Archived
            $data['error_message'] = "This report is now unavailable for viewing.";
            $this->load->view('approval/error_page', $data);

            
        }

    } else { 
      //not the reviewer of the report
      $data['error_message'] = "You are not authorized to access this report. ";
      $this->load->view('approval/error_page', $data);
    }

  }

  function department_head() {

    if($this->session->userdata('sess_email_approval')=='' ) {
      redirect(base_url("approval") . "?report_id=" . $_GET['report_id'] . "&action=department_head");
    }

    $this->load->view('layout/header');


    //check if the logged user is the reviewer of the report
    $logged_id = $this->session->userdata('userid_approval');
    $report_id = $_GET['report_id'];

    $count = $this->Global_model->check_if_approval("tbl_reviewer", "reviewer_id = " . $logged_id , $report_id);
    if( $count > 0){

        $string = file_get_contents("./listing/".$report_id.".json");
        $details = json_decode($string, false);
        $report = $details->Report_Summary;
        if($report[0]->status >= 0){

            if($report[0]->status > 2){

                //reviewer of the report
                $this->load->view('approval/report_link_remarks_departmenthead');

            } else {

                //not available for viewing
                $data['error_message'] = "You are not allowed to review this report yet.";
                $this->load->view('approval/error_page', $data);

            }
            

        } else {

            //deleted OR Archived
            $data['error_message'] = "This report is now unavailable for viewing.";
            $this->load->view('approval/error_page', $data);


        }

    } else {
      //not the reviewer of the report
      $data['error_message'] = "You are not authorized to access this report. ";
      $this->load->view('approval/error_page', $data);
    }


  }

  function add_status()
  {
    $report_id = $_POST['id'];
    $status = $_POST['status'];
    $report_submission = $_POST['report_submission'];
    $this->Audit_report_model->add_status($report_id, $status, $report_submission);
  }

  function add_status1()
  {
    $report_id = $_POST['id'];
    $status = $_POST['status'];
    $this->Audit_report_model->add_status1($report_id, $status);
  }

  function add_status2()
  {
    $report_id = $_POST['id'];
    $status = $_POST['status'];
    $this->Audit_report_model->add_status2($report_id, $status);
  }

  function add_remarks_departmenthead()
  {
    $report_id = $_POST['id'];
    $status = $_POST['status'];
    $this->Audit_report_model->add_remarks_departmenthead($report_id, $status);
  }

  function add_remarks()
  {
    $report_id = $_POST['id'];
    $status = $_POST['status'];
    $report_issuance = $_POST['report_issuance'];
    $this->Audit_report_model->add_remarks($report_id, $status, $report_issuance);
  }

  function add_new_remarks()
  {
      $report_id = $_POST['id'];
      $remarks = $_POST['remarks'];
      $create_date = $_POST['create_date'];
      $approver_id = $this->session->userdata('userid_approval');
      $type = $_POST['type'];
      $this->Audit_report_model->add_new_remarks($report_id, $remarks, $create_date, $approver_id, $type);
  }

  function add_new_remarks_departmenthead()
  {
    $report_id = $_POST['id'];
    $remarks = $_POST['remarks'];
    $create_date = $_POST['create_date'];
    $reviewer_id = $_POST['reviewer_id'];
    $type = $_POST['type'];
    $this->Audit_report_model->add_new_remarks_departmenthead($report_id, $remarks, $create_date, $reviewer_id, $type);
  }

  function reject_remarks()
  {
    $report_id = $_POST['id'];
    $remarks = $_POST['remarks'];
    $create_date = $_POST['create_date'];
    $approver_id = $this->session->userdata('userid_approval');
    $type = $_POST['type'];
    $this->Audit_report_model->reject_remarks($report_id, $remarks, $create_date, $approver_id, $type);
  }

  function reject_remarks_departmenthead()
  {
    $report_id = $_POST['id'];
    $remarks = $_POST['remarks'];
    $create_date = $_POST['create_date'];
    $reviewer_id = $_POST['reviewer_id'];
    $type = $_POST['type'];
    $this->Audit_report_model->reject_remarks_departmenthead($report_id, $remarks, $create_date, $reviewer_id, $type);
  }

  function add_report_submission_status()
  {
    $report_id = $_POST['report_id'];
    $remarks = $_POST['remarks'];
    $status = $_POST['status'];
    $position = $_POST['position'];
    $this->Audit_report_model->add_report_submission_status($report_id,$remarks,$status,$position);
  }

  function signature_stamp($report_id,$field)
  {
    $data = array(
        "report_id"=>$report_id,
        $field => date("Y-m-d H:i:s")
      );
    $this->Audit_report_model->signature_stamp($data, "save");

  }

  public function department_head_reject()
  {

    $report_id = $_POST['id'];
    $remarks = $_POST['remarks'];
    $create_date = $_POST['create_date'];
    $reviewer_id = $_POST['reviewer_id'];
    $type = $_POST['type'];

    //ADD REMARKS
    $this->Audit_report_model->reject_remarks_departmenthead($report_id, $remarks, $create_date, $reviewer_id, $type);

    //ADD STATUS
    $this->Audit_report_model->add_report_submission_status($report_id,$remarks,"Submitted to Lead Auditor & Co-Auditor(s)","Department Head");

    //EMAIL NOTIFICATION
    $this->email_to_co_auditor_rejected($report_id, $remarks);

    //REPORT SATATUS
    $this->Global_model->change_report_status($report_id,6);

    //UPDATE REPORT LIST JSON
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, base_url("api/generate_audit_report_json") . "/" . $report_id);
    // curl_setopt($ch, CURLOPT_POST, 1);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch);
    // curl_close ($ch);
    $this->samscurl->load(base_url("api/generate_audit_report_json") . "/" . $report_id);

    //generate_audit_report_json
    // $ch1 = curl_init();
    // curl_setopt($ch1, CURLOPT_URL, base_url("api/audit_report_list"));
    // curl_setopt($ch1, CURLOPT_POST, 1);
    // curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch1);
    // curl_close ($ch1);
    $this->samscurl->load(base_url("api/audit_report_list");

    //UPDATE AUDIT REPORT JSON LISTING
    // $ch2 = curl_init();
    // curl_setopt($ch2, CURLOPT_URL, base_url('api/update_listing') . "?report_id=" . $report_id. "&action=final");
    // curl_setopt($ch2, CURLOPT_POST, 1);
    // curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch2);
    // curl_close ($ch2);
    $this->samscurl->load(base_url('api/update_listing') . "?report_id=" . $report_id. "&action=final");

    //UPDATE AUDIT REPORT LIST JSON
    // $ch2 = curl_init();
    // curl_setopt($ch2, CURLOPT_URL, base_url('api/audit_report_list'));
    // curl_setopt($ch2, CURLOPT_POST, 1);
    // curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch2);
    // curl_close ($ch2);
    $this->samscurl->load(base_url('api/audit_report_list'));


    //ADD AUDIT TRAIL
    foreach ($this->Global_model->get_report_details($report_id) as $value) {
          $report_no = $value->Report_No;
      }
    $audit_trail_data['user'] = $this->session->userdata('userid_approval');
    $audit_trail_data['page'] = "/approval";
    $audit_trail_data['type'] = "Reviewer";
    $audit_trail_data['role'] = $this->session->userdata('sess_role_approval');
    $audit_trail_data['email'] = $this->session->userdata('sess_email_approval');
    $audit_trail_data['action'] = "Return " . $report_no . " back to Lead & Co-Auditor(s)";
    $audit_trail_data['date'] = date('Y-m-d H:i:s');
    $this->Audit_trail_model->save_data('tbl_audit_trail',$audit_trail_data);



  }

  public function department_head_approved()
  {
    $report_id = $_POST['id'];
    $remarks = $_POST['remarks'];
    $create_date = $_POST['create_date'];
    $reviewer_id = $_POST['reviewer_id'];
    $type = $_POST['type'];

    //REMARKS
    $this->Audit_report_model->add_new_remarks_departmenthead($report_id, $remarks, $create_date, $reviewer_id, $type);

    //ADD STATUS
    $this->Audit_report_model->add_report_submission_status($report_id,$remarks,"Submitted to Division Head","Department Head");

    //EMAIL NOTIFICATION TO LEAD AND CO AUDITOR
    $this->email_to_co_auditor_approve_from_reviewer($report_id,$remarks);

    //EMAIL TO DIVISION HEAD
    $this->email_to_division_head($report_id);


    //UPDATE REPORT LIST JSON
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, base_url("api/generate_audit_report_json") . "/" . $report_id);
    // curl_setopt($ch, CURLOPT_POST, 1);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch);
    // curl_close ($ch);
    $this->samscurl->load(base_url("api/generate_audit_report_json") . "/" . $report_id);

    //generate_audit_report_json
    // $ch1 = curl_init();
    // curl_setopt($ch1, CURLOPT_URL, base_url("api/audit_report_list"));
    // curl_setopt($ch1, CURLOPT_POST, 1);
    // curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch1);
    // curl_close ($ch1);
    $this->samscurl->load(base_url("api/audit_report_list"));

    //UPDATE AUDIT REPORT JSON LISTING
    // $ch2 = curl_init();
    // curl_setopt($ch2, CURLOPT_URL, base_url('api/update_listing') . "?report_id=" . $report_id . "&action=final");
    // curl_setopt($ch2, CURLOPT_POST, 1);
    // curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch2);
    // curl_close ($ch2);
    $this->samscurl->load(base_url('api/update_listing') . "?report_id=" . $report_id . "&action=final");
    
    //UPDATE AUDIT REPORT LIST JSON
    // $ch2 = curl_init();
    // curl_setopt($ch2, CURLOPT_URL, base_url('api/audit_report_list'));
    // curl_setopt($ch2, CURLOPT_POST, 1);
    // curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch2);
    // curl_close ($ch2);
    $this->samscurl->load(base_url('api/audit_report_list'));

    //ADD SIGNATURE STAMP
    $this->signature_stamp($report_id,"review_date");

    //ADD AUDIT TRAIL
    foreach ($this->Global_model->get_report_details($report_id) as $value) {
          $report_no = $value->Report_No;
      }
    $audit_trail_data['user'] = $this->session->userdata('userid_approval');
    $audit_trail_data['page'] = "/approval";
    $audit_trail_data['type'] = "Reviewer";
    $audit_trail_data['role'] = $this->session->userdata('sess_role_approval');
    $audit_trail_data['email'] = $this->session->userdata('sess_email_approval');
    $audit_trail_data['action'] = "Submit " . $report_no . " to Division Head";
    $audit_trail_data['date'] = date('Y-m-d H:i:s');
    $this->Audit_trail_model->save_data('tbl_audit_trail',$audit_trail_data);
  }


  public function division_head_reject()
  {
    $report_id = $_POST['id'];
    $remarks = $_POST['remarks'];
    $create_date = $_POST['create_date'];
    $approver_id = $_POST['approver_id'];
    $type = $_POST['type'];

    //ADD REMARKS
    $this->Audit_report_model->reject_remarks_departmenthead($report_id, $remarks, $create_date, $approver_id, $type);

    //ADD STATUS
    $this->Audit_report_model->add_report_submission_status($report_id,$remarks,"Submitted to Lead Auditor & Co-Auditor(s)","Division Head");

    //NOTIFICATION
    $this->email_to_co_auditor_rejected_from_approver($report_id,$remarks);

    //REPORT SATATUS
    $this->Global_model->change_report_status($report_id,7);

    //remove reviewer stamp
    $this->Preview_report_model->reset_stamp($report_id);

    //UPDATE REPORT LIST JSON
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, base_url("api/generate_audit_report_json") . "/" . $report_id);
    // curl_setopt($ch, CURLOPT_POST, 1);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch);
    // curl_close ($ch);
    $this->samscurl->load(base_url("api/generate_audit_report_json") . "/" . $report_id);

    //generate_audit_report_json
    // $ch1 = curl_init();
    // curl_setopt($ch1, CURLOPT_URL, base_url("api/audit_report_list"));
    // curl_setopt($ch1, CURLOPT_POST, 1);
    // curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch1);
    // curl_close ($ch1);
    $this->samscurl->load(base_url("api/audit_report_list"));

    //UPDATE AUDIT REPORT JSON LISTING
    // $ch2 = curl_init();
    // curl_setopt($ch2, CURLOPT_URL, base_url('api/update_listing') . "?report_id=" . $report_id. "&action=final");
    // curl_setopt($ch2, CURLOPT_POST, 1);
    // curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch2);
    // curl_close ($ch2);
    $this->samscurl->load(base_url('api/update_listing') . "?report_id=" . $report_id. "&action=final");

    //UPDATE AUDIT REPORT LIST JSON
    // $ch2 = curl_init();
    // curl_setopt($ch2, CURLOPT_URL, base_url('api/audit_report_list'));
    // curl_setopt($ch2, CURLOPT_POST, 1);
    // curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch2);
    // curl_close ($ch2);
    $this->samscurl->load(base_url('api/audit_report_list'));

    //ADD AUDIT TRAIL
    foreach ($this->Global_model->get_report_details($report_id) as $value) {
          $report_no = $value->Report_No;
      }
    $audit_trail_data['user'] = $this->session->userdata('userid_approval');
    $audit_trail_data['page'] = "/approval";
    $audit_trail_data['type'] = "Approver";
    $audit_trail_data['role'] = $this->session->userdata('sess_role_approval');
    $audit_trail_data['email'] = $this->session->userdata('sess_email_approval');
    $audit_trail_data['action'] = "Return " . $report_no . " back to Lead & Co-Auditor(s)";
    $audit_trail_data['date'] = date('Y-m-d H:i:s');
    $this->Audit_trail_model->save_data('tbl_audit_trail',$audit_trail_data);

  }

  public function division_head_approved()
  {
    $report_id = $_POST['id'];
    $remarks = $_POST['remarks'];
    $create_date = $_POST['create_date'];
    $approver_id = $_POST['approver_id'];
    $type = $_POST['type'];

    //ADD REMARKS
    $this->Audit_report_model->reject_remarks_departmenthead($report_id, $remarks, $create_date, $approver_id, $type);

    //ADD STATUS
    $this->Audit_report_model->add_report_submission_status($report_id,$remarks,"Approved by Division Head","Division Head");

    //SIGNATURE
    $this->signature_stamp_approved($report_id,"approved_date");

    //EMAIL NOTIFICATION
    $this->email_to_co_auditor_approve_from_approver($report_id,$remarks);

    //UPDATE REPORT LIST JSON
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, base_url("api/generate_audit_report_json") . "/" . $report_id);
    // curl_setopt($ch, CURLOPT_POST, 1);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch);
    // curl_close ($ch);
    $this->samscurl->load(base_url("api/generate_audit_report_json") . "/" . $report_id);

    //generate_audit_report_json
    // $ch1 = curl_init();
    // curl_setopt($ch1, CURLOPT_URL, base_url("api/audit_report_list"));
    // curl_setopt($ch1, CURLOPT_POST, 1);
    // curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch1);
    // curl_close ($ch1);
    $this->samscurl->load(base_url("api/audit_report_list"));

    //UPDATE AUDIT REPORT JSON LISTING
    // $ch2 = curl_init();
    // curl_setopt($ch2, CURLOPT_URL, base_url('api/update_listing') . "?report_id=" . $report_id. "&action=final");
    // curl_setopt($ch2, CURLOPT_POST, 1);
    // curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch2);
    // curl_close ($ch2);
    $this->samscurl->load(base_url('api/update_listing') . "?report_id=" . $report_id. "&action=final");

    //EXPORT PDF REPORT
    // $ch3 = curl_init();
    // curl_setopt($ch3, CURLOPT_URL, base_url('generate/audit_report') . "?report_id=" . $report_id. "&action=F");
    // curl_setopt($ch3, CURLOPT_POST, 1);
    // curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch3);
    // curl_close ($ch3);
    $this->samscurl->load(base_url('generate/audit_report') . "?report_id=" . $report_id. "&action=F");

    // $ch4 = curl_init();
    // curl_setopt($ch4, CURLOPT_URL, base_url('generate/executive_summary') . "?report_id=" . $report_id. "&action=F");
    // curl_setopt($ch4, CURLOPT_POST, 1);
    // curl_setopt($ch4, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch4);
    // curl_close ($ch4);
    $this->samscurl->load(base_url('generate/executive_summary') . "?report_id=" . $report_id. "&action=F");

    // $ch5 = curl_init();
    // curl_setopt($ch5, CURLOPT_URL, base_url('generate/annexure_pdf') . "?report_id=" . $report_id. "&action=F");
    // curl_setopt($ch5, CURLOPT_POST, 1);
    // curl_setopt($ch5, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch5);
    // curl_close ($ch5);
    $this->samscurl->load(base_url('generate/annexure_pdf') . "?report_id=" . $report_id. "&action=F");

    // $ch6 = curl_init();
    // curl_setopt($ch6, CURLOPT_URL, base_url('generate/annexure_doc') . "?report_id=" . $report_id. "&action=F");
    // curl_setopt($ch6, CURLOPT_POST, 1);
    // curl_setopt($ch6, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch6);
    // curl_close ($ch6);
    $this->samscurl->load(base_url('generate/annexure_doc') . "?report_id=" . $report_id. "&action=F");

    // $ch7 = curl_init();
    // curl_setopt($ch7, CURLOPT_URL, base_url('generate/raw_data') . "?report_id=" . $report_id. "&action=F");
    // curl_setopt($ch7, CURLOPT_POST, 1);
    // curl_setopt($ch7, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch7);
    // curl_close ($ch7);
    $this->samscurl->load(base_url('generate/raw_data') . "?report_id=" . $report_id. "&action=F");

    // $ch8 = curl_init();
    // curl_setopt($ch8, CURLOPT_URL, base_url('api/update_audit_history') . "?report_id=" . $report_id);
    // curl_setopt($ch8, CURLOPT_POST, 1);
    // curl_setopt($ch8, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch8);
    // curl_close ($ch8);
    $this->samscurl->load(base_url('api/update_audit_history') . "?report_id=" . $report_id);

    //UPDATE AUDIT HISTORY JSON
    // $ch9 = curl_init();
    // curl_setopt($ch9, CURLOPT_URL, base_url('api/audit_history'));
    // curl_setopt($ch9, CURLOPT_POST, 1);
    // curl_setopt($ch9, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch9);
    // curl_close ($ch9);
    $this->samscurl->load(base_url('api/audit_history'));

    //UPDATE AUDIT REPORT LIST JSON
    // $ch2 = curl_init();
    // curl_setopt($ch2, CURLOPT_URL, base_url('api/audit_report_list'));
    // curl_setopt($ch2, CURLOPT_POST, 1);
    // curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    // $server_output = curl_exec ($ch2);
    // curl_close ($ch2);
    $this->samscurl->load(base_url('api/audit_report_list'));

    //ADD AUDIT TRAIL
    foreach ($this->Global_model->get_report_details($report_id) as $value) {
          $report_no = $value->Report_No;
      }
    $audit_trail_data['user'] = $this->session->userdata('userid_approval');
    $audit_trail_data['page'] = "/approval";
    $audit_trail_data['type'] = "Approver";
    $audit_trail_data['role'] = $this->session->userdata('sess_role_approval');
    $audit_trail_data['email'] = $this->session->userdata('sess_email_approval');
    $audit_trail_data['action'] = "Approve  " . $report_no . ".";
    $audit_trail_data['date'] = date('Y-m-d H:i:s');
    $this->Audit_trail_model->save_data('tbl_audit_trail',$audit_trail_data);

  }

  function email_to_co_auditor_approve_from_approver($reportid,$remarks)
  {

      $lead_auditor = $this->Global_model->get_emails("email_lead_auditor",$reportid);
      $approver = $this->Global_model->get_emails("email_approver",$reportid);

      foreach ($this->Global_model->get_report_details($reportid) as $value) {
          $report_no = $value->Report_No;
          $company = $value->name;
      }

      $this->email->from($approver[0]->email,$approver[0]->fname ." " . $approver[0]->lname);
      $this->email->to($lead_auditor[0]->email);

      $subject = "STATUS of ".$company."'s  Report (" . $report_no . ")";
      $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
      $message .= "Dear " . $lead_auditor[0]->fname . ",";
      $message .= "<br>";
      $message .= "<br>";
      $message .= "The report for ".$company."'s audit was approved by the Division Head.  The audit report is now final and complete.";
      $message .= "<br>";
      if($remarks != ""){
        $message .= "<br>";
        $message .= "Remarks : " . $remarks;
      }
      $message .= "<br><br>";
      $message .= "Thank you.";
      $message .= "</body></html>";

      $this->email->subject($subject);
      $this->email->message($message);

      $this->email->send();

      $this->Global_model->change_report_status($reportid,5);

      //email co-auditors
      foreach($this->Global_model->get_emails("email_coauditors",$reportid) as $value){

          $this->email->from($approver[0]->email,$approver[0]->fname ." " . $approver[0]->lname);
          $this->email->to($value->email);

          $subject = "STATUS of ".$company."'s  Report (" . $report_no . ")";
          $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
          $message .= "Dear " . $value->fname . ",";
          $message .= "<br>";
          $message .= "<br>";
          $message .= "The report for ".$company."'s audit was approved by the Division Head.  The audit report is now final and complete.";
          $message .= "<br>";
          $message .= "<br>";
          $message .= "Thank you.";
          $message .= "</body></html>";

          $this->email->subject($subject);
          $this->email->message($message);

          $this->email->send();
      }

  }


  function signature_stamp_approved($report_id,$field)
  {
    $data = array(
        "report_id"=>$report_id,
        $field => date("Y-m-d H:i:s")
      );

    $exist = $this->Global_model->check_global_exist_model('tbl_report_signature_stamp',  'report_id', 'report_id = ' . $report_id);
    $count = $exist[0]->count;
    echo $count;
    if($count > 0){
      $this->Audit_report_model->update_signature_stamp($report_id,$data);
    } else {
      $this->Audit_report_model->signature_stamp($data, "save");
    }

  }


  function email_to_co_auditor_rejected_from_approver($reportid,$remarks)
  {

      $lead_auditor = $this->Global_model->get_emails("email_lead_auditor",$reportid);
      $approver = $this->Global_model->get_emails("email_approver",$reportid);

      foreach ($this->Global_model->get_report_details($reportid) as $value) {
          $report_no = $value->Report_No;
          $company = $value->name;
      }

      $this->email->from($approver[0]->email,$approver[0]->fname ." " . $approver[0]->lname);
      $this->email->to($lead_auditor[0]->email);

      $subject = "COMMENTS on ".$company."'s  Report (" . $report_no . ")";
      $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
      $message .= "Dear " . $lead_auditor[0]->fname . ",";
      $message .= "<br>";
      $message .= "<br>";
      $message .= "I have reviewed the Audit Report for ".$company.".";
      $message .= "<br>";
      $message .= "<br>";
      $message .= "Remarks: " . $remarks;
      $message .= "<br>";
      $message .= "<br>";
      $message .= "Thank you.";
      $message .= "</body></html>";

      $this->email->subject($subject);
      $this->email->message($message);

      $this->email->send();


      //email co-auditors
      foreach($this->Global_model->get_emails("email_coauditors",$reportid) as $value){

          $this->email->from($approver[0]->email,$approver[0]->fname ." " . $approver[0]->lname);
          $this->email->to($value->email);

          $subject = "COMMENTS on ".$company."'s  Report (" . $report_no . ")";
          $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
          $message .= "Dear " . $value->fname . ",";
          $message .= "<br>";
          $message .= "<br>";
          $message .= "I have reviewed the Audit Report for ".$company.".";
          $message .= "<br>";
          $message .= "<br>";
          $message .= "Remarks: " . $remarks;
          $message .= "<br>";
          $message .= "<br>";
          $message .= "Thank you.";
          $message .= "</body></html>";

          $this->email->subject($subject);
          $this->email->message($message);

          $this->email->send();
      }

  }
  function email_to_division_head($reportid)
  {

      $lead_auditor = $this->Global_model->get_emails("email_lead_auditor",$reportid);
      $reviewer = $this->Global_model->get_emails("email_reviewer",$reportid);
      $approver = $this->Global_model->get_emails("email_approver",$reportid);

      foreach ($this->Global_model->get_report_details($reportid) as $value) {
          $report_no = $value->Report_No;
          $company = $value->name;
      }

      $this->email->from($reviewer[0]->email,$reviewer[0]->fname ." " . $reviewer[0]->lname);
      $this->email->to($approver[0]->email);

      $subject = "Division Head's Approval - " . $company . " (" . $report_no . ")";
      $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
      $message .= "Dear " . trim($approver[0]->fname) . ",";
      $message .= "<br>";
      $message .= "<br>";
      $message .= "Please refer to the link below with regards to the audit of ".$company.".";
      $message .= "<br>";
      $message .= "This is for your review and final approval.";
      $message .= "<br>";
      $message .= "<br>";
      $message .= '<a href="'.base_url("approval").'?report_id='.$reportid.'&action=division_head">Link to Audit Report Summary</a>';
      $message .= "<br>";
      $message .= "<br>";
      $message .= "Thank you.";
      $message .= "</body></html>";

      $this->email->subject($subject);
      $this->email->message($message);

      $this->email->send();



      $this->Global_model->change_report_status($reportid,4);

   // $reviewer[0]->fname;

  }

  function email_to_co_auditor_approve_from_reviewer($reportid,$remarks)
  {

      $lead_auditor = $this->Global_model->get_emails("email_lead_auditor",$reportid);
      $reviewer = $this->Global_model->get_emails("email_reviewer",$reportid);

      foreach ($this->Global_model->get_report_details($reportid) as $value) {
          $report_no = $value->Report_No;
          $company = $value->name;
      }

      $this->email->from($reviewer[0]->email,$reviewer[0]->fname ." " . $reviewer[0]->lname);
      $this->email->to($lead_auditor[0]->email);

      $subject = "STATUS of ".$company."'s  Report (" . $report_no . ")";
      $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
      $message .= "Dear " . $lead_auditor[0]->fname . ",";
      $message .= "<br>";
      $message .= "<br>";
      $message .= "The report for ".$company."'s audit was approved by the Department Head. It was submitted for the Division Head's approval.";
      $message .= "<br>";
      if($remarks != ""){
        $message .= "<br>";
        $message .= "Remarks: " . $remarks;
      }
      $message .= "<br>";
      $message .= "<br>";
      $message .= "Thank you.";
      $message .= "</body></html>";

      $this->email->subject($subject);
      $this->email->message($message);

      $this->email->send();

      //email co-auditors
      foreach($this->Global_model->get_emails("email_coauditors",$reportid) as $value){

          $this->email->from($reviewer[0]->email,$reviewer[0]->fname ." " . $reviewer[0]->lname);
          $this->email->to($value->email);

          $subject = "STATUS of ".$company."'s  Report (" . $report_no . ")";
          $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
          $message .= "Dear " . $value->fname . ",";
          $message .= "<br>";
          $message .= "<br>";
          $message .= "The report for ".$company."'s audit was approved by the Department Head. It was submitted for the Division Head's approval.";
          $message .= "<br>";
          $message .= "<br>";
          $message .= "Thank you.";
          $message .= "</body></html>";

          $this->email->subject($subject);
          $this->email->message($message);

          $this->email->send();
      }
  }

  function email_to_co_auditor_rejected($reportid, $remarks)
  {

    $lead_auditor = $this->Global_model->get_emails("email_lead_auditor",$reportid);
    $reviewer = $this->Global_model->get_emails("email_reviewer",$reportid);

    foreach ($this->Global_model->get_report_details($reportid) as $value) {
        $report_no = $value->Report_No;
        $company = $value->name;
    }

    $this->email->from($reviewer[0]->email,$reviewer[0]->fname ." " . $reviewer[0]->lname);
    $this->email->to($lead_auditor[0]->email);

    $subject = "COMMENTS on ".$company."'s  Report (" . $report_no . ")";
    $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
    $message .= "Dear " . $lead_auditor[0]->fname . ",";
    $message .= "<br>";
    $message .= "<br>";
    $message .= "I have reviewed the Audit Report for ".$company.".";
    $message .= "<br>";
    $message .= "<br>";
    $message .= "Remarks: " . $remarks;
    $message .= "<br>";
    $message .= "<br>";
    $message .= "Thank you.";
    $message .= "</body></html>";

    $this->email->subject($subject);
    $this->email->message($message);

    $this->email->send();

    //email co-auditors
    foreach($this->Global_model->get_emails("email_coauditors",$reportid) as $value){

        $this->email->from($reviewer[0]->email,$reviewer[0]->fname ." " . $reviewer[0]->lname);
        $this->email->to($value->email);

        $subject = "COMMENTS on ".$company."'s  Report (" . $report_no . ")";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Dear " . $value->fname . ",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "I have reviewed the Audit Report for ".$company.".";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Remarks: " . $remarks;
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Thank you.";
        $message .= "</body></html>";

        $this->email->subject($subject);
        $this->email->message($message);

        $this->email->send();
    }

  }
}
