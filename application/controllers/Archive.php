<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Archive extends CI_Controller{

	public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Archive_model');
        $this->load->helper('url');
        $this->load->library('Pdf');

       	if($this->session->userdata('sess_email')=='' ) { 
			redirect(base_url("login"));
		} else {
			if($this->session->userdata('type')== 'approver'){
				redirect(base_url("login/unsetSession"));
			}
			if($this->session->userdata('type')== 'reviewer'){
				redirect(base_url("login/unsetSession"));
			}
		}

    }

    public function template()
	{	
		$data['content'] = 'archive_template/list2';
		$this->load->view('layout/layout',$data);
	}

	public function report()
	{	
		$data['content'] = 'archive_report/list2';
		$this->load->view('layout/layout',$data);
	}

	public function getlist_archive_template(){
		$table = $_POST['table'];
		$table1 = $_POST['table1'];
		$txt_search = $_POST['txt_search'];
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$offset = ($_POST['offset']-1)* $limit;
		$sort = $_POST['order_by'];
		$data = $this->Archive_model->getlist_archive_template($table, $table1, $limit,$offset,$sort,$txt_search);
		echo json_encode($data);
	}

	function getlist_archive_template_count(){
		$table = $_POST['table'];
		$table1 = $_POST['table1'];
		$txt_search = $_POST['txt_search'];
		$per_page = $_POST['limit'];
		$limit = '9999999';
		$offset = '0';
		$sort = $_POST['order_by'];
		$data = count($this->Archive_model->getlist_archive_template($table, $table1, $limit,$offset,$sort,$txt_search));
		$page_count = ceil($data/$per_page);
		echo $page_count;
	}

	function get_report_archive(){
		$limit = $_POST['limit'];
		$offset = ($_POST['offset']-1)* $limit;
		$data = $this->Archive_model->get_report_list_archive($limit,$offset);
		echo json_encode($data);
	}
}

?>