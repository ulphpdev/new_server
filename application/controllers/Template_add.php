<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Template_add extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Home_model');
        $this->load->model('template_model');
        $this->load->helper('url');
        
    }
    public function index()
    {
        $data['content'] = 'template_add';
        $this->load->view('layout/layout', $data);
    }
    public function insert_template_solo()
    {
       
        $data = array(
                'classification_id' => $this->input->post('classification_name'),
                'standard_id' => $this->input->post('template_name'),
                'create_date' => date('Y-m-d')
            );
        $data  = $this->template_model->add_template_solo($data);
         echo json_encode($data);
        // return $data;
        // $data1  = array(
        //     'template_id' => $id2
        // );
        // echo json_encode($data1);
    }
    function insert_template()
    {
        $post   = $_POST;
        $data1  = array(
            'element_name' => $post['element_name'],
            'create_date' => date('Y-m-d')
        );
        $table1 = 'tbl_elements';
        $id1    = $this->template_model->add_template($table1, $data1);
        // return $id1;
    echo json_encode($id1);

    }
     
    
    function insert_template2()
    {
        $data = array(
            'mandatory' => $this->input->post('mandatory'),
            'element_id' => $this->input->post('element_id'),
            'template_id' => $this->input->post('template_id'),
            'question' => $this->input->post('question'),
            'default_yes' => $this->input->post('default_yes_val'),
            'create_date' => $this->input->post('date_created')
        );
        $data = $this->template_model->add_template2($data);
        // echo 'success';
    }
    function insert_activity()
    {
        $post   = $_POST;
        $data1  = array(
            'activity_name' => $post['activity_name'],
            'create_date' => date('Y-m-d')
        );
        $table1 = 'tbl_activities';
        $id1    = $this->template_model->add_template($table1, $data1);
        // return $id;
        if ($id1) {
            $data2 = array(
                'activity_id' => $id1,
                'template_id' => $post['template_id'],
                'create_date' => date('Y-m-d')
                
            );
        }
        $table2 = 'tbl_template_activities';
        $id2    = $this->template_model->add_template($table2, $data2);
        $data1  = array(
            'activity_id' => $id1
        );
        echo json_encode($data1);
        
    }
    function insert_sub_activity()
    {
        $data = array(
            'activity_id' => $this->input->post('activity_id'),
            'sub_item_name' => $this->input->post('sub_item_name'),
            'create_date' => $this->input->post('date_created')
        );
        $data = $this->template_model->add_sub_act($data);
    }
    //draft test
    function insert_template_draft()
    {
        $post   = $_POST;
        $data1  = array(
            'element_name' => $post['element_name'],
            'create_date' => date('Y-m-d')
        );
        $table1 = 'tbl_elements';
        $id1    = $this->template_model->add_template($table1, $data1);
        // return $id;
        if ($id1) {
            $data2 = array(
                'classification_id' => $post['classification_name'],
                'standard_id' => $post['template_name'],
                'status' => ('status'),
                'create_date' => date('Y-m-d')
            );
        }
        $table2 = 'tbl_template';
        $id2    = $this->template_model->add_template($table2, $data2);
        $data1  = array(
            'element_id' => $id1,
            'template_id' => $id2
        );
        echo json_encode($data1);
    }
    
    function insert_template2_draft()
    {
        $data = array(
            'element_id' => $this->input->post('element_id'),
            'template_id' => $this->input->post('template_id'),
            'status' => $this->input->post('status'),
            'question' => $this->input->post('question'),
            'default_yes' => $this->input->post('default_yes_val'),
            'create_date' => $this->input->post('date_created')
        );
        $data = $this->template_model->add_template2($data);
    }
    function search()
    {
        $data  = $this->input->post('txt_search');
        $data1 = $this->template_model->search($data);
        echo json_encode($data);
        print_r($data1);
    }
    function edit_template()
    {
        $id   = $_POST['id'];
        $data = $this->template_model->edit_template($id);
        echo json_encode($data);

    }
    function get_data_template()
    {
        $table = $_POST['table'];
        $data  = $this->template_model->getdata_model($table);
        echo json_encode($data);
    }
    function get_element_question()
    {   
        $table = $_POST['table'];
        $id = $_POST['id'];
        // $field = $_POST['field'];
        $data  = $this->template_model->get_elementquestion_model($id,$table);
        echo json_encode($data);
    }
    function get_questions()
    {   
        // $table = $_POST['table'];
        $id = $_POST['id'];
        // $field = $_POST['field'];
        $data  = $this->template_model->get_question_model($id);
        echo json_encode($data);
    }
    function get_mandatory()
    {   
        // $table = $_POST['table'];
        $id = $_POST['id'];
        $question_id = $_POST['question_id'];
        $data  = $this->template_model->get_mandatory_model($id, $question_id);
        echo json_encode($data);
    }
    public function update_element()
    {
       
        $id = $_POST['element_id'];
        $element_name = $_POST['element_name'];
        $update_date = $_POST['update_date'];
   
        
        $this->template_model->update_element_model($id, $element_name, $update_date);
         // echo json_encode($data);
        // return $data;
        // $data1  = array(
        //     'template_id' => $id2
        // );
        // echo json_encode($data1);
    }
     public function edit_template_reference()
    {
        $classification_id = $_POST['classification_id'];
        $template_id = $_POST['template_id'];
        $standard_id = $_POST['standard_id'];
  
        $update_date = $_POST['update_date'];

       $this->template_model->edit_template_reference($classification_id, $update_date, $template_id, $standard_id);
    }
    public function update_question()
    {
        // $update_delete = $_POST['update_delete'];
        $id = $_POST['element_id'];
        $question_id = $_POST['question_id'];
        $mandatory = $_POST['mandatory'];
        $question = $_POST['question'];
        $update_date = $_POST['update_date'];
   
        
        $this->template_model->update_question_model($id, $question, $mandatory, $question_id, $update_date);
  
    }
    public function update_default()
    {
       
        // $id = $_POST['element_id'];
        $question_id = $_POST['question_id'];
        $default_yes = $_POST['default_yes'];
        $update_date = $_POST['update_date'];
   
        
        $this->template_model->update_default_model($default_yes, $question_id, $update_date);
  
    }
    function get_activity()
    {   
        $table = $_POST['table'];
        $id = $_POST['id'];
        // $field = $_POST['field'];
        $data  = $this->template_model->get_activity_model($id, $table);
        echo json_encode($data);
    }
    public function update_activity()
    {
       
        // $id = $_POST['element_id'];
        $activity_id = $_POST['activity_id'];
        $activity_name = $_POST['activity_name'];
        $update_date = $_POST['update_date'];
   
        
        $this->template_model->update_activity_model($activity_name, $activity_id, $update_date);
  
    }
     function get_sub_activity()
    {   
        $template_id = $_POST['template_id'];
        $id = $_POST['id'];
        // $field = $_POST['field'];
        $data  = $this->template_model->get_sub_activity_model($id, $template_id);
        echo json_encode($data);
    }
    public function update_sub_activity()
    {
       
        // $id = $_POST['element_id'];
        // $sub_item_id = $_POST['sub_item_id'];
        $sub_item_id = $_POST['sub_item_id'];
        $sub_item_name = $_POST['sub_item_name'];
        $update_date = $_POST['update_date'];
   
        
        $this->template_model->update_sub_activity_model($sub_item_name, $sub_item_id, $update_date);
  
    }

}